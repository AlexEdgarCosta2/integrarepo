import xml.etree.ElementTree as ET
import glob
import sys
import os

typesMap = {}
version = ""
outFile = "package.out"
mask = "package_*.xml"

for file in glob.glob(mask):
   tree = ET.parse(file)
   root = tree.getroot()

   for child in root:
      membersList = set()

      for subChild in child:
         if ("members" in subChild.tag):
            membersList.add(subChild.text)

         if ("name" in subChild.tag):
            if subChild.text in typesMap:
            	typesMap[subChild.text] = typesMap.get(subChild.text).union(membersList)
            else:
            	typesMap[subChild.text] = membersList

            membersList = set()

      if ("version" in child.tag and version == ""):
         version = child.text

root = ET.Element("Package", xmlns="http://soap.sforce.com/2006/04/metadata")

for type in typesMap:
    types = ET.SubElement(root, "types")
    ET.SubElement(types, "name").text = type

    for member in typesMap.get(type):
        ET.SubElement(types, "members").text = member

ET.SubElement(root, "version").text = version

tree = ET.ElementTree(root)

tree.write(outFile, encoding="UTF-8")
/**
 * Trigger for Lead object.
 *
 * @author ach
 */
trigger LeadTrigger on Lead (after delete, after insert, after undelete, 
after update, before delete, before insert, before update)
{
    new TriggerTemplateV2.TriggerManager(new LeadTriggerHandler()).runHandlers();
}
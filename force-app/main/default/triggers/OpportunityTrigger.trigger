trigger OpportunityTrigger on Opportunity (after delete, after insert, after undelete, 
after update, before delete, before insert, before update)  {
	TriggerTemplate.TriggerManager aTriggerManager = new TriggerTemplate.TriggerManager();
	
	aTriggerManager.addHandler(new OpportunityTriggerHandlerBasic(), new List<TriggerTemplate.TriggerAction> 
		{	TriggerTemplate.TriggerAction.beforeInsert, 
		TriggerTemplate.TriggerAction.beforeUpdate,
		TriggerTemplate.TriggerAction.afterupdate});
	aTriggerManager.runHandlers();
}
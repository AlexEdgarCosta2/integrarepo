/**
 * @description       : Case Trigger
 * @author            : alexandre.costa@integra-biosciences.com
 * @group             :
 * @last modified on  : 04-28-2021
 * @last modified by  : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
trigger CaseTrigger on Case (after delete, after insert, after undelete,
after update, before delete, before insert, before update) {
    new TriggerTemplateV2.TriggerManager(new CaseTriggerHandler()).runHandlers();
}
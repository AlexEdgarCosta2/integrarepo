/**
 * @description       :
 * @author            : ??????
 * @group             :
 * @last modified on  : 05-06-2021
 * @last modified by  : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   05-06-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
trigger QuoteLineItemTrigger on QuoteLineItem (before insert, after insert, after update, before delete) {
	if (Trigger.isInsert && Trigger.isBefore) {
		// can't place it to "QuoteLineItemTriggerHandler" as when we auto create a new QuoteLineItem by manual creation of OpportunityLineItem "QuoteLineItemTriggerHandler.onBeforeInsert()" is not launched
		QuoteLineItemService.getDataFromSyncedOpportunityLineItems(Trigger.new);
	}
	if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate))
	{
		if (Trigger.isInsert)
		{
			QuoteLineItemService.createQuoteLIHistoty(Trigger.newMap, null);
			QuoteLineItemService.setDiscountApprovalProcesses(Trigger.new, null);
		}
		else
		{
			QuoteLineItemService.createQuoteLIHistoty(Trigger.newMap,Trigger.oldMap);
			QuoteLineItemService.setDiscountApprovalProcesses(Trigger.new, Trigger.oldMap);
		}
	}
	if (Trigger.isDelete && Trigger.isBefore)
	{
		QuoteLineItemService.createQuoteLIHistoty(null, Trigger.oldMap);
	}

	new TriggerTemplateV2.TriggerManager(new QuoteLineItemTriggerHandler()).runHandlers();
}
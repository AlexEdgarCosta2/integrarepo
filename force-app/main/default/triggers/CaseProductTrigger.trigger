/**
*	@description Lead Trigger
*
*	@author ach
*	@copyright PARX
*/
trigger CaseProductTrigger on CaseProduct__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update)
{
	new TriggerTemplateV2.TriggerManager(new CaseProductTriggerHandler()).runHandlers();
}
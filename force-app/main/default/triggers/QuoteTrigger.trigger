trigger QuoteTrigger on Quote (before insert, before update, after update, before delete)  {
    new TriggerTemplateV2.TriggerManager(new QuoteTriggerHandlerBasic()).runHandlers();
}
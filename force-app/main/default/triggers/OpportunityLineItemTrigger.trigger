/**
 * Trigger on OpportunitylineItemTrigger
 *
 * @author PARX, ach
 */
trigger OpportunityLineItemTrigger on OpportunityLineItem (after delete, after insert, after undelete, 
after update, before delete, before insert, before update)
{
    if (Trigger.isInsert && Trigger.isAfter) {
        // can't place it to "OpportunitylineItemTriggerHandler" as when we click "Start Sync" the processed operation is "Delete" and "TriggerTemplateV2" does not run "Insert" operations
        OpportunityLineItemService.getDataFromQuoteLineItems(Trigger.new);
    }
    if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate))
    {
        if (Trigger.isInsert)
        {
            OpportunityLineItemService.createObjectLIHistoty(Trigger.newMap, null);
        }
        else 
        {
            OpportunityLineItemService.createObjectLIHistoty(Trigger.newMap,Trigger.oldMap);
        }
    }
    if (Trigger.isDelete && Trigger.isBefore)
    {
        OpportunityLineItemService.createObjectLIHistoty(null, Trigger.oldMap);
    }
    new TriggerTemplateV2.TriggerManager(new OpportunitylineItemTriggerHandler()).runHandlers();
}
import { LightningElement, wire, track, api } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';

import NAME_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Name_Table_Header_Label';
import SALES_PRICE_TABLE_HEADER_lABEL from '@salesforce/label/c.Sales_Price_Table_Header_Label';
import LIST_PRICE_TABLE_HEADER_lABEL from '@salesforce/label/c.List_Price_Table_Header_Label';
import QUANTITY_TABLE_HEADER_lABEL from '@salesforce/label/c.Quantity_Table_Header_Label';
import CODE_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Code_Table_Header_Label';
import FAMILY_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Family_Table_Header_Label';
import TYPE_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Type_Field_Label';
import LIST_PRICE_TIER_TABLE_HEADER_lABEL from '@salesforce/label/c.List_Price_Tier_Table_Header_Label';
import TIER_PRICING_TABLE_HEADER_lABEL from '@salesforce/label/c.Tier_Pricing_Table_Header_Label';


import { groupBy } from 'c/libCollectionUtils';
import { fireEvent } from 'c/pubsub';

export default class ProductPickerSelectedProductItem extends LightningElement {
    labels = {
        NAME_TABLE_HEADER_lABEL,
        SALES_PRICE_TABLE_HEADER_lABEL,
        LIST_PRICE_TABLE_HEADER_lABEL,
        QUANTITY_TABLE_HEADER_lABEL,
        CODE_TABLE_HEADER_lABEL,
        FAMILY_TABLE_HEADER_lABEL,
        TYPE_TABLE_HEADER_lABEL,
        LIST_PRICE_TIER_TABLE_HEADER_lABEL,
        TIER_PRICING_TABLE_HEADER_lABEL
    };

    @api productLineItemWrapper;
    @api quote;
    @api opportunity;
    @track items;

    @track productLineItemWrapperLocal;

    @wire(CurrentPageReference) pageRef;
    

    get pLitem(){
		console.log('item', JSON.parse(JSON.stringify(this.productLineItemWrapper)));
      return JSON.parse(JSON.stringify(this.productLineItemWrapper));
    } 

    @api
    get scalingItems() {
        var scalingResult = this.pLitem.productScaling;
        scalingResult = scalingResult.trim();
        console.log('selected product item scalingResult :: '+scalingResult);
        var resultArr = scalingResult.split("\n");
        let mp = [];
        
        if(resultArr.length > 0){
            for(var ele in resultArr){ 	
                var temp = resultArr[ele].split(":");
                var tempkey = temp[0].replace("≤","");
                var tempval = temp[1];
               mp.push({key: tempkey, value: tempval }); 
            }
        }
        console.log(mp);
        return mp;
    }

    // connectedCallback() {
    //     console.log('connectedCallback called ');
        
    // }
    
    handleChange(event) {
        // console.log('handleChange event :: '+event);
        // const field = event.target.name;
        // var value;
        // if (event.target.type === "number")
        // {
        //     value = parseFloat(event.target.value);    
        // }
        // else
        // {
        //     value = event.target.value;    
        // }
        
        // if (field.split('.').length === 1)
        // {
        //     this.productLineItemWrapperLocal[field] = value;
        // } else if (field.split('.').length === 2)
        // {
        //     this.productLineItemWrapperLocal[field.split('.')[0]][field.split('.')[1]] = value;
        // }
        // this.dispatchEvent(new CustomEvent('itemchange',{
        //     detail: {
        //       productLineItemWrapper : this.productLineItemWrapperLocal
        //   }
        // }));
        
    }

    handleDeleteClick (event) {
        console.log('handleDeleteClick called ProductPickerSelectedProductItem '+event.target.value);

        const deleteevent = new CustomEvent('selecteddeleteclick', {
            // detail contains only primitives
            detail: event.target.value
        });
        // Fire the event from c-tile
        this.dispatchEvent(deleteevent);
    }

    handleResetSalesPriceClick (event) {
        this.template.querySelectorAll('lightning-input').forEach(inputElement => {
            if(inputElement.name === 'salespricename') {
                inputElement.value = this.pLitem.listPriceTierPricing;
                this.handleSalesPriceBlur();
            }
        });
    }

    handleDemoEqChange (event) {
        
        var recID = event.target.value;
        if(event.target.checked === true){
            const demoeqevent = new CustomEvent('demoeqclick', {
                detail:  recID
            });
            this.dispatchEvent(demoeqevent);
        }
    }

    handleSalesPriceBlur (event) {
        console.log('handleSalesPriceBlur called '+this.pLitem.product.ProductCode);
        var changedSalesVal;

        this.template.querySelectorAll('lightning-input').forEach(inputElement => {
            if(inputElement.name === 'salespricename'){
                console.log(inputElement.value);
                changedSalesVal = inputElement.value;
            }
        });
        var productkey = this.pLitem.product.ProductCode;
        const changesalespriceevent = new CustomEvent('salespricechange', {
            detail:  {productkey : productkey,
                        changedSalesVal : changedSalesVal  }
        });
        this.dispatchEvent(changesalespriceevent);
    }
}
import { LightningElement, wire, track, api } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, fireEvent, unregisterAllListeners } from 'c/pubsub';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


import QUANTITY_TABLE_HEADER_lABEL from '@salesforce/label/c.Quantity_Table_Header_Label';
import CODE_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Code_Table_Header_Label';
import NAME_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Name_Table_Header_Label';
import FAMILY_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Family_Table_Header_Label';
import TYPE_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Type_Table_Header_Label';
import TIER_PRICING_TABLE_HEADER_lABEL from '@salesforce/label/c.Tier_Pricing_Table_Header_Label';
import NET_WEIGHT_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Net_Weight_Table_Header_Label';
import LABEL_SHORT_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Label_Short_Table_Header_Label';
import PRODUCT_PICKER_MESSAGE_NO_PRODUCTS from '@salesforce/label/c.Product_picker_Message_No_Products';


import { getRecord } from 'lightning/uiRecordApi';

export default class ProductPickerProductAlert extends LightningElement {

    labels = {
        QUANTITY_TABLE_HEADER_lABEL,
        CODE_TABLE_HEADER_lABEL,
        NAME_TABLE_HEADER_lABEL,
        FAMILY_TABLE_HEADER_lABEL,
        TYPE_TABLE_HEADER_lABEL,
        TIER_PRICING_TABLE_HEADER_lABEL,
        NET_WEIGHT_TABLE_HEADER_lABEL,
        LABEL_SHORT_TABLE_HEADER_lABEL,
        PRODUCT_PICKER_MESSAGE_NO_PRODUCTS
    };

    @api recordId;
    @api objectApiName;


    @track oppId ;
    @track quoteId;
    
    @wire(CurrentPageReference) pageRef;
    
    @wire(getRecord, { recordId: '$oppId', fields: ['Opportunity.Id', 'Opportunity.CurrencyIsoCode', 'Opportunity.Pricebook2Id', 'Opportunity.Account.Name'] })
    opportunity;
    @wire(getRecord, { recordId: '$quoteId', fields: ['Quote.Id', 'Quote.CurrencyIsoCode', 'Quote.Pricebook2Id'] })
    quote;

    @wire(CurrentPageReference) pageRef;

   
    async connectedCallback() {
        registerListener('filterProducts', this.handleFilterProducts, this);
    }

    disconnectedCallback (){
        unregisterAllListeners();
    }

    showToast(title, msg, responseStatus) {
        const event = new ShowToastEvent({
            title: title,
            message: msg,
            variant: responseStatus
        });
        this.dispatchEvent(event);
    }
}
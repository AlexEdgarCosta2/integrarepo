/* eslint-disable vars-on-top */
/* eslint-disable no-console */
/* eslint-disable guard-for-in */
import { LightningElement, wire, api, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, fireEvent, unregisterAllListeners } from 'c/pubsub';

import PRODUCTPICKER_PRODUCTALERT_ALERT_TABLE_HEADER_LABEL from '@salesforce/label/c.ProductPicker_ProductAlert_Alert_Table_Header_Label';

export default class ProductPickerProductItem extends LightningElement {
    @api productLineItemWrapper;
    @api productContext;

    @wire(CurrentPageReference) pageRef;

    labels = {
        PRODUCTPICKER_PRODUCTALERT_ALERT_TABLE_HEADER_LABEL
    };

    @api
    get scalingItems() {
        var scalingResult = this.productLineItemWrapper.productScaling;
        scalingResult = scalingResult.trim();
        var resultArr = scalingResult.split("\n");
        let mp = [];
        
        if(resultArr.length > 0){
            for(var ele in resultArr){ 	
                var temp = resultArr[ele].split(":");
                var tempkey = temp[0].replace("≤","");
                var tempval = temp[1];
               mp.push({key: tempkey.trim(), value: tempval.trim() }); 
            }
        }
        return mp;
    }

    @api
    get context() {
        if(this.productContext === 'Product')
        {
            return true;
        }else if(this.productContext === 'Alerts')
        {
            return true;
        }
        return false;
    }

    handleQuantityChange(event) {
        console.log('handleQuantityChange :: '+this.productContext);
        const itemchangeevent = new CustomEvent('itemchange',{detail: {
                me: this.productLineItemWrapper,
                quantity: event.target.value}});
        this.dispatchEvent(itemchangeevent);
    }

    quantityChange(component) {
        if (component.which == 13){
            // console.log('handleQuantityChange enter called');
            const quantitychangeevent = new CustomEvent('quantitychangeevent');
            this.dispatchEvent(quantitychangeevent);
        }       
    }

}
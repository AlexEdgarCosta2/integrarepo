import { LightningElement,track, api } from 'lwc';
import createQuotesApex from '@salesforce/apex/NewQuoteController.createQuote';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ModalPopupLWC extends LightningElement {

    numberOfQuotes = "1";

    @track isError = false;
    @track isSaving = false;
    @api recordId;

    handleChange(event) {

        const field = event.target.name;
        var value = event.target.value;

        this.numberOfQuotes = value;

        this.isError = false;
    }

    keycheck(component, event, helper) {
        if (component.which == 13) {
            if(this.numberOfQuotes > 5 || this.numberOfQuotes < 1) {
                this.isError = true;
            } else {
                this.saveQuotes(event);
            }
        }
    }

    handleClick(event) {
        this.clickedButtonLabel = event.target.label;

        if(this.numberOfQuotes > 5 || this.numberOfQuotes < 1) {
            this.isError = true;
        } else {
            this.saveQuotes(event);
        }
    }

    saveQuotes(event) {
        this.isSaving = true;
        createQuotesApex({
            opportunityId : this.recordId,
            quoteNumber : this.numberOfQuotes
        })
        .then(result => {
            this.isSaving = false;

            const event = new ShowToastEvent({
                title: 'Quotes created',
                message: 'Created ' + this.numberOfQuotes + ' Quote(s) with success!',
                variant: 'success'
            });
            this.dispatchEvent(event);

            let retVal = '';
            if(result.length === 1) {
                retVal = result[0];
            }

            const closeQA = new CustomEvent('close', {
                detail: { retVal }
            });

            this.dispatchEvent(closeQA);

        })
        .catch(error => {
            this.isSaving = false;

            const event = new ShowToastEvent({
                title : 'Error',
                message : 'Error creating Quotes. Please Contact System Admin',
                variant : 'error'
            });
            this.dispatchEvent(event);

            let retVal = '';

            const closeQA = new CustomEvent('close', {
                detail: { retVal }
            });

            this.dispatchEvent(closeQA);
        });
    }
}
/* eslint-disable no-dupe-class-members */
/* eslint-disable no-console */
import { LightningElement, wire, track, api } from 'lwc';
import { getObjectInfo, getPicklistValues } from 'lightning/uiObjectInfoApi';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


import PRODUCT_OBJECT from '@salesforce/schema/Product2';
import FAMILY_FIELD from '@salesforce/schema/Product2.Family';
import TYPE_FIELD from '@salesforce/schema/Product2.Family';
import PACKAGE_FIELD from '@salesforce/schema/Product2.ProductType__c';

import { getRecord } from 'lightning/uiRecordApi';

//Labels
import CARD_LABEL from '@salesforce/label/c.Filter_Component_Label';
import CLEAR_BUTTON_lABEL from '@salesforce/label/c.Clear_Button_Label';
import FILTER_BUTTON_lABEL from '@salesforce/label/c.Filter_Button_Label';
import PRODUCT_CODE_lABEL from '@salesforce/label/c.Product_Code_Field_Label';
import NAME_lABEL from '@salesforce/label/c.Name_Field_Label';
//import PRODUCT_TYPE_LABEL from '@salesforce/label/c.Product_Type_Label';
// import FAMILY_lABEL from '@salesforce/label/c.Family_Field_Label';
import PRODUCT_TYPE_lABEL from '@salesforce/label/c.Product_Type_Field_Label';
import SEARCH_TERM_lABEL from '@salesforce/label/c.Product_Search_Term_Field_Label';

const OPPFIELDS = ['Opportunity.Id', 'Opportunity.CurrencyIsoCode', 'Opportunity.Pricebook2Id', 'Opportunity.Account.Name'];
const QTEFIELDS = ['Quote.Id', 'Quote.CurrencyIsoCode', 'Quote.Pricebook2Id'];

export default class ProductPickerProductFilter extends LightningElement {

    labels = {
        CARD_LABEL,
        CLEAR_BUTTON_lABEL,
        FILTER_BUTTON_lABEL,
        PRODUCT_CODE_lABEL,
        NAME_lABEL,
        PRODUCT_TYPE_lABEL,
        SEARCH_TERM_lABEL
    };

    @api recordId;
    @api objectApiName;


    @track oppId ;
    @track quoteId;

    @wire(CurrentPageReference) pageRef;

    @track opportunity;
    @track quote;
    @track currency;
    
    @wire(getRecord, { recordId: '$oppId', fields: OPPFIELDS })
    oppwiredRecord({ error, data }) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading contact',
                    message,
                    variant: 'error',
                }),
            );
        } else if (data) {
            this.opportunity = data;
            this.currency = this.opportunity.fields.CurrencyIsoCode.value;
            console.log('currency :: '+this.currency);
        }
    }

    @api
    get filterCurrency(){
        return CARD_LABEL + ' '+this.currency+ ')';
    }

    @wire(getRecord, { recordId: '$quoteId', fields: QTEFIELDS })
    qtewiredRecord({ error, data }) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading contact',
                    message,
                    variant: 'error',
                }),
            );
        } else if (data) {
            this.quote = data;
            this.currency = this.quote.fields.CurrencyIsoCode.value;
            console.log('currency :: '+this.currency);
        }
    }

    @api
    refresh() {
        console.log('productfilter :: refresh called');
        this.handleClear();
        if (this.objectApiName=== "Opportunity")
        {
            console.log('productfilter :: in opportunity');
            this.oppId = this.recordId;

        } else if (this.objectApiName=== "Quote")
        {
            console.log('productfilter :: in quote');
            this.quoteId = this.recordId;
        }
    }

    handleFilter() {
        // fire contactSelected event
        console.log('productfilter :: handleFilter called ');
        const filter = {};
        this.template.querySelectorAll('lightning-input, lightning-combobox').forEach(inputElement => {
            filter[inputElement.getAttribute("data-fieldapiname")] = {value: inputElement.value, operator: inputElement.getAttribute("data-compoperator")}
        });

        // if (this.objectApiName=== "Opportunity")
        // {
        //     filter.CurrencyIsoCode = {value: this.opportunity.data.fields.CurrencyIsoCode.value, operator: 'EQUALS'}
        //     filter.Pricebook2Id = {value: this.opportunity.data.fields.Pricebook2Id.value, operator: 'EQUALS'}
        // } 
        // else if (this.objectApiName=== "Quote")
        // {
        //     filter.CurrencyIsoCode = {value: this.quote.data.fields.CurrencyIsoCode.value, operator: 'EQUALS'}
        //     filter.Pricebook2Id = {value: this.quote.data.fields.Pricebook2Id.value, operator: 'EQUALS'}
        // }
        // filter['Product2.IsActive'] = {value: true, operator: 'EQUALS'}

        

        // if (!filter.Pricebook2Id.value)
        // {
        //     this.dispatchEvent(
        //         new ShowToastEvent({
        //             title: 'No pricebook selected, products can not be queried',
        //             message: 'No pricebook selected, products can not be queried',
        //             variant: 'error',
        //         }),
        //     );
        //     return;
        // }

        fireEvent(this.pageRef, 'filterProducts', {
            filter: filter
        });
    }   
    handleClear() {
        console.log('productfilter :: handleClear called');
        this.template.querySelectorAll('lightning-input, lightning-combobox').forEach(inputElement => {
            inputElement.value = '';
        });
    }   
    connectedCallback() {
        // subscribe to searchKeyChange event
        console.log('productfilter :: connectedCallback called');
        this.handleClear();
        this.refresh();
    }

    @wire(getObjectInfo, { objectApiName: PRODUCT_OBJECT })
    objectInfo;

    @track error;

    @track TypePicklistValues;
    @wire(getPicklistValues,{ recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: TYPE_FIELD})
    wiredTypePicklistValues({ error, data }) {
        if (data) {
            const x = JSON.parse(JSON.stringify(data));
            x.values.unshift({attributes: null, label: "-Select-", validFor: Array(0), value: ''})
            this.TypePicklistValues = x;
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.TypePicklistValues = undefined;
        }
    }    

    @track FamilyPicklistValues;
    @wire(getPicklistValues,{ recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: FAMILY_FIELD})
    wiredFamilyPicklistValues({ error, data }) {
        if (data) {
            const x = JSON.parse(JSON.stringify(data));
            x.values.unshift({attributes: null, label: "-Select-", validFor: Array(0), value: ''})
            this.FamilyPicklistValues = x;
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.FamilyPicklistValues = undefined;
        }
    }    

    @track PackagePicklistValues;
    @wire(getPicklistValues,{ recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: PACKAGE_FIELD})
    wiredPackagePicklistValues({ error, data }) {
        if (data) {
            const x = JSON.parse(JSON.stringify(data));
            x.values.unshift({attributes: null, label: "-Select-", validFor: Array(0), value: ''})
            this.PackagePicklistValues = x;
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.PackagePicklistValues = undefined;
        }
    }    

    keycheck(component, event, helper){
        if (component.which == 13){
            this.handleFilter();
        }
    }
}
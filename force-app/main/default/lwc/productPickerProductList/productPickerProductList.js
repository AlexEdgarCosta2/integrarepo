/* eslint-disable no-dupe-class-members */
/* eslint-disable no-debugger */
/* eslint-disable vars-on-top */
/* eslint-disable guard-for-in */
/* eslint-disable no-console */
import { LightningElement, wire, track, api } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import getFilteredProducts from '@salesforce/apex/ProductPickerControllerLWC.getFilteredProducts';
import { registerListener, fireEvent, unregisterAllListeners } from 'c/pubsub';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

// import { loadstyle } from 'lightning/platformResourceLoader';
// import cssResource from '@salesforce/resourceUrl/'

import CARD_LABEL from '@salesforce/label/c.Product_Selection_Component_Label';
import RELATED_PRODUCT_CARD_LABEL from '@salesforce/label/c.Related_Product_Component_Label';
import ADD_BUTTON_lABEL from '@salesforce/label/c.Add_to_Selected_Products_Button_Label';
import SELECTALL_BUTTON_lABEL from '@salesforce/label/c.Select_All_Products_Button_Label';
import QUANTITY_TABLE_HEADER_lABEL from '@salesforce/label/c.Quantity_Table_Header_Label';
import CODE_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Code_Table_Header_Label';
import NAME_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Name_Table_Header_Label';
import FAMILY_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Family_Table_Header_Label';
import TYPE_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Type_Table_Header_Label';
import TIER_PRICING_TABLE_HEADER_lABEL from '@salesforce/label/c.Tier_Pricing_Table_Header_Label';
//import PACKAGE_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Package_Table_Header_Label';
import NET_WEIGHT_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Net_Weight_Table_Header_Label';
import LABEL_SHORT_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Label_Short_Table_Header_Label';
import PRODUCT_PICKER_MESSAGE_NO_PRODUCTS from '@salesforce/label/c.Product_picker_Message_No_Products';
import PRODUCT_PICKER_MESSAGE_NO_SERVICEPRODUCTS from '@salesforce/label/c.PRODUCT_PICKER_MESSAGE_NO_SERVICEPRODUCTS';
import PRODUCTPICKER_PRODUCTALERT_ALERT_TABLE_HEADER_LABEL from '@salesforce/label/c.ProductPicker_ProductAlert_Alert_Table_Header_Label';
import PRODUCT_PICKER_MESSAGE_SELECT_PRODUCTS from '@salesforce/label/c.Product_picker_Message_Select_Products';


export default class ProductPickerProductList extends LightningElement {    
    labels = {
        CARD_LABEL,
        RELATED_PRODUCT_CARD_LABEL,
        ADD_BUTTON_lABEL,
        SELECTALL_BUTTON_lABEL,
        QUANTITY_TABLE_HEADER_lABEL,
        CODE_TABLE_HEADER_lABEL,
        NAME_TABLE_HEADER_lABEL,
        FAMILY_TABLE_HEADER_lABEL,
        TYPE_TABLE_HEADER_lABEL,
        TIER_PRICING_TABLE_HEADER_lABEL,
        //PACKAGE_TABLE_HEADER_lABEL,
        NET_WEIGHT_TABLE_HEADER_lABEL,
        LABEL_SHORT_TABLE_HEADER_lABEL,
        PRODUCT_PICKER_MESSAGE_NO_PRODUCTS,
        PRODUCT_PICKER_MESSAGE_NO_SERVICEPRODUCTS,
        PRODUCTPICKER_PRODUCTALERT_ALERT_TABLE_HEADER_LABEL,
        PRODUCT_PICKER_MESSAGE_SELECT_PRODUCTS
    };

    @api recordId;
    @api objectApiName;
    @api state;
    @track items;
    @track productFilter = {};
    @track loading = false;
    @api productContext;
    @api prodIds;
    @api alertactive = false;

    @api
    refresh() {
        console.log('getAlertsData called '+this.productContext);
        this.error = undefined;
        this.items = [];
    }
    
    @wire(CurrentPageReference) pageRef;

    @api
    get context() {
        if(this.productContext === 'Product')
        {
            return true;
        }
        return false;
    }

    get alertContext() {
        if(this.productContext === 'Alerts')
        {
            return true;
        }
        return false;
    }

    get styleClass() { 
        return this.alertactive ? 'padding-top: 1%; padding-bottom: 1%; border-color: red;' : 'padding-top: 1%; padding-bottom: 1%;';
      }
   
    async connectedCallback() {
        registerListener('filterProducts', this.handleFilterProducts, this);

        if(this.productContext === 'Alerts'){
           this.alertactive = true;
        }
    }

    disconnectedCallback (){
        unregisterAllListeners();
    }

    handleFilterProducts(productFilter) {
        this.productFilter = productFilter;
        console.log('productList :: handleFilterProducts called '+JSON.stringify(this.productFilter));
        this.filter();
    }

    handleAdd() {
	    console.log('handleAdd called');

        console.log('this items :: ', JSON.parse(JSON.stringify(this.items)));
        var newItems = this.items.filter(item => item.quantity > 0);
        console.log('items s:: ' + newItems.length);

        if (this.items != null && newItems != null && newItems.length > 0) {
            fireEvent(this.pageRef, 'addProductsToSelection', this.items); 
            console.log('handleAdd :: '+this.productContext);
            this.filter();
        }else{
            this.showToast('',this.labels.PRODUCT_PICKER_MESSAGE_SELECT_PRODUCTS,'Info');
        }
    }

    handleSelectAll() {
        console.log('handleSelectAll called');
       // eslint-disable-next-line guard-for-in
       
       // change the quantity for all items to 1
       for(var ele in this.items){
            console.log('index ::  '+ this.items[ele].quantity);
            this.items[ele].quantity = 1;
       }
    }

    filter() {
       console.log('productList :: filter called ');
        this.items = [];
        this.loading = true;
        getFilteredProducts({filter: this.productFilter.filter, recordId: this.recordId,productContext:this.productContext, prodIds: this.prodIds })//(this.productFilter, recordId)
            .then(result => {
               console.log('>>>>>>>>>>>>>>>>>>>', JSON.parse(JSON.stringify(result)));
               this.items = result; 
                
               console.log('this.productScaling :: '+this.items[0].productScaling);

               if (this.productContext === 'Related Products'){
                    console.log('filter -> getFilteredProducts -> In related products '+this.items.length);
                    const relProdEvent = new CustomEvent('relatedproductfound', {detail: {'relatedProdSize' : this.items.length}});
                    this.dispatchEvent(relProdEvent);
               }
               
               this.error = undefined;
               this.loading = false;
            })
            .catch(error => {
                this.error = error;
                this.items = undefined;
                this.loading = false;
            });
    }

    onitemchange(event)
    {
        const changeditem = event.detail.me;    
        console.log('changed product ID '+changeditem.product.Id);
        this.selectedItem = this.items.find(
            item => item.product.Id === changeditem.product.Id
        );

        this.selectedItem.quantity = event.detail.quantity;
    }

    showToast(title, msg, responseStatus) {
        const event = new ShowToastEvent({
            title: title,
            message: msg,
            variant: responseStatus
        });
        this.dispatchEvent(event);
    }

    @api
    getAlertsData() {
        console.log('getAlertsData params  '+this.prodIds+ ' productContext '+this.productContext);
        console.log('getAlertsData params recordId '+this.recordId);
        console.log('getAlertsData '+JSON.stringify(this.prodIds));

        var prodId = '('+this.prodIds+')';
        console.log('getAlertsData prodId '+prodId);
        
        getFilteredProducts({filter: null, recordId: this.recordId,productContext:this.productContext, prodIds: prodId })
            .then(result => {
               console.log('getAlertsData result :: '+result +'PC '+this.productContext+ ' alertIds '+this.prodIds);
              
                this.items = result; 
                console.log('>>>>>>>>>>>>>>>>>>> '+this.items.length);

                // if(this.items.length > 0){
                    const serviceEvent = new CustomEvent('servicesfound', {detail: {'size' : this.items.length}});
                    this.dispatchEvent(serviceEvent);
                // }
                
                this.error = undefined;
                this.loading = false;
            })
            .catch(error => {
                this.error = error;
                this.items = undefined;
                this.loading = false;
            });
    }
}
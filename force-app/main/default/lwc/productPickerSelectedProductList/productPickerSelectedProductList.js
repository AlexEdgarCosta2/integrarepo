/* eslint-disable guard-for-in */
/* eslint-disable vars-on-top */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
import { LightningElement, wire, track, api } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import addLineItems from '@salesforce/apex/ProductPickerControllerLWC.addLineItems';
import getOpportunityLineItems from '@salesforce/apex/ProductPickerControllerLWC.getOpportunityLineItems';
import getQuoteLineItems from '@salesforce/apex/ProductPickerControllerLWC.getQuoteLineItems';
import getSelectedProducts from '@salesforce/apex/ProductPickerControllerLWC.getSelectedProducts';
import addSelectedProductToOpp from '@salesforce/apex/ProductPickerControllerLWC.addSelectedProductToOpp';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { registerListener, fireEvent } from 'c/pubsub';
import { NavigationMixin } from 'lightning/navigation';
import { reduceErrors } from 'c/ldsUtils';
import { getRecord } from 'lightning/uiRecordApi';


import CARD_LABEL from '@salesforce/label/c.Selected_Products_Component_Label';
import ADD_BUTTON_lABEL from '@salesforce/label/c.Add_Button_Label';
import CLEAR_BUTTON_lABEL from '@salesforce/label/c.Clear_Button_Label';
import BACK_TO_BUTTON_lABEL from '@salesforce/label/c.Back_to_Button_Label';
import NAME_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Name_Table_Header_Label';
import SALES_PRICE_TABLE_HEADER_lABEL from '@salesforce/label/c.Sales_Price_Table_Header_Label';
import LIST_PRICE_TABLE_HEADER_lABEL from '@salesforce/label/c.List_Price_Table_Header_Label';
import LIST_PRICE_TIER_TABLE_HEADER_lABEL from '@salesforce/label/c.List_Price_Tier_Table_Header_Label';
import QUANTITY_TABLE_HEADER_lABEL from '@salesforce/label/c.Quantity_Table_Header_Label';
import DEMO_EQ_TABLE_HEADER_lABEL from '@salesforce/label/c.Demo_Eq_Table_Header_Label';
import PRODUCT_FAMILY_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Family_Table_Header_Label';
import PRODUCT_TYPE_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Type_Table_Header_Label';
import TIER_PRICING_TABLE_HEADER_lABEL from '@salesforce/label/c.Tier_Pricing_Table_Header_Label';
import PRODUCT_PICKER_MESSAGE_SUCCESS_TITLE from '@salesforce/label/c.Product_picker_Message_Success_Title';
import PRODUCT_PICKER_MESSAGE_SUCCESS from '@salesforce/label/c.Product_picker_Message_Success';
import PRODUCT_PICKER_MESSAGE_ERROR_TITLE from '@salesforce/label/c.Product_picker_Message_Error_Title';
import PRODUCT_PICKER_MESSAGE_ERROR from '@salesforce/label/c.Product_picker_Message_Error';
import PRODUCT_PICKER_MESSAGE_SELECT_PRODUCTS from '@salesforce/label/c.Product_picker_Message_Select_Products';
import CODE_TABLE_HEADER_lABEL from '@salesforce/label/c.Product_Code_Table_Header_Label';

export default class ProductPickerSelectedProductList extends NavigationMixin(LightningElement) {
    labels = {
        CARD_LABEL,
        ADD_BUTTON_lABEL,
        CLEAR_BUTTON_lABEL,
        BACK_TO_BUTTON_lABEL,
        NAME_TABLE_HEADER_lABEL,
        SALES_PRICE_TABLE_HEADER_lABEL,
        LIST_PRICE_TABLE_HEADER_lABEL,
        QUANTITY_TABLE_HEADER_lABEL,
        DEMO_EQ_TABLE_HEADER_lABEL,
        PRODUCT_FAMILY_TABLE_HEADER_lABEL,
        PRODUCT_TYPE_TABLE_HEADER_lABEL,
        LIST_PRICE_TIER_TABLE_HEADER_lABEL,
        TIER_PRICING_TABLE_HEADER_lABEL,
        CODE_TABLE_HEADER_lABEL,
        PRODUCT_PICKER_MESSAGE_SUCCESS_TITLE,
        PRODUCT_PICKER_MESSAGE_SELECT_PRODUCTS,
        PRODUCT_PICKER_MESSAGE_SUCCESS,
        PRODUCT_PICKER_MESSAGE_ERROR_TITLE,
        PRODUCT_PICKER_MESSAGE_ERROR
    };
    @api recordId;
    @api objectApiName;
    @track error = null;
    @track items = [];
    @track maxPos = 10;
    @track productFilter;

    @wire(getRecord, {
        recordId: '$oppId', fields: ['Opportunity.Id', 'Opportunity.CurrencyIsoCode',
            'Opportunity.Pricebook2Id', 'Opportunity.Account.Id',
            'Opportunity.Account.Name', 'Opportunity.Account.DebtorNumber_CCUST__c',
            'Opportunity.Account.Parent.DebtorNumber_CCUST__c', 'Opportunity.Account.Parent.Name',
            'Opportunity.Account.SalesDivision_CKZ03__c', 'Opportunity.Account.Parent.SalesDivision_CKZ03__c',
            'Opportunity.Account.PriceBracketCustomer_CDISC__c', 'Opportunity.Account.Parent.PriceBracketCustomer_CDISC__c']
    })
    opportunity;
    @wire(getRecord, {
        recordId: '$quoteId', fields: ['Quote.Id', 'Quote.CurrencyIsoCode', 'Quote.Pricebook2Id', 'Quote.Account.Id',
            'Quote.Account.Name', 'Quote.Account.DebtorNumber_CCUST__c', 'Quote.Account.Parent.DebtorNumber_CCUST__c',
            'Quote.Account.SalesDivision_CKZ03__c', 'Quote.Account.Parent.SalesDivision_CKZ03__c',
            'Quote.Account.PriceBracketCustomer_CDISC__c', 'Quote.Account.Parent.PriceBracketCustomer_CDISC__c']
    })
    quote;




    @api
    refresh() {
        this.error = undefined;
        this.items = [];
        console.log('refresh called....');
        if (this.objectApiName === "Opportunity") {
            this.oppId = this.recordId;
            console.log('refresh opportunity');
            getOpportunityLineItems({ opportunityId: this.oppId })
                .then(result => {
                    this.handleAddProductsToSelection(result);
                    //this.items = result;
                    this.error = undefined;
                })
                .catch(error => {
                    //this.items = [];
                    this.error = error;
                });
        }
        else if (this.objectApiName === "Quote") {
            this.quoteId = this.recordId;
            console.log('refresh quote');
            getQuoteLineItems({ quoteId: this.quoteId })
                .then(result => {
                    this.handleAddProductsToSelection(result);
                    //this.items = result;
                    this.error = undefined;
                })
                .catch(error => {
                    //this.items = [];
                    this.error = error;
                });
        }
    }


    @wire(CurrentPageReference) pageRef;

    async connectedCallback() {
        registerListener('addProductsToSelection', this.handleAddProductsToSelection, this);
        //this.refresh();
    }


    get backToObjectButtonLabel() {
        return this.labels.BACK_TO_BUTTON_lABEL + ' ' + this.objectApiName;
    }


    handleAddProductsToSelection(items) {
        console.log('this items :: ' + this.items);
        var newItems = items.filter(
            item => item.quantity > 0
        );

        console.log('items s:: ' + newItems.length);
        if (this.items != null && newItems != null && newItems.length > 0) {
            this.callgetSelectedProducts(this.recordId, JSON.stringify(newItems), JSON.stringify(this.items));
        }else{
            this.showToast('',this.labels.PRODUCT_PICKER_MESSAGE_SELECT_PRODUCTS,'Info');
        }
    }

    @api
    callgetSelectedProducts(oppId, newSelectedproducts, alreadyselectedProducts) {
        console.log(' callgetSelectedProducts called...');
        var selectedProdIds = [];
        var selction = newSelectedproducts;

        var prdArr = JSON.parse(selction);
        prdArr.forEach(element => {
            var product = JSON.parse(JSON.stringify(element.product));
            console.log('prdArr element.product :: ' + product);
            var prdObj = JSON.stringify(product.Id);
            selectedProdIds.push(prdObj);
            console.log('prdArr element.product :: ' + prdObj);
        });
        
        console.log('selction :: '+selectedProdIds);

        //event fired for fetching the related 'Alert/Services'; this event is fired to productpickercontainer.cmp
        const selectedProdforAlertsEvent = new CustomEvent('selectedprodforalerts', {detail: {selectedProdIds}});
        this.dispatchEvent(selectedProdforAlertsEvent);


        if (newSelectedproducts != null && alreadyselectedProducts != null) {
            getSelectedProducts({ recordId: oppId, newSelectedproducts: newSelectedproducts, alreadyselectedProducts: alreadyselectedProducts })
                .then(result => {
                    // console.log('callgetSelectedProducts results :: ' + result);
                    this.items = JSON.parse(result);
                    console.log('getSelectedProducts', JSON.parse(JSON.stringify(this.items)));
                    console.log(this.items[0].productScaling);
                    // console.log('callgetSelectedProducts items ::' + JSON.stringify(this.items));
                })
                .catch(error => {
                    this.error = error;
                });
        }
    }

    handleClear() {
        this.items = [];
    }

    // async handleToOpp() {
    //     window.location.assign('/' + this.recordId);
    // }

    onitemchange(event) {
        console.log('onitemchange called of selected product list');
    }

    get errorMessages() {
        return reduceErrors(this.error);
    }

    handleAdd() {
        console.log('handleAdd called :: ');
        if(this.items.length > 0 )
        {
            this.calladdSelectedProductToOpp();
        }else{
            this.showToast('',this.labels.PRODUCT_PICKER_MESSAGE_SELECT_PRODUCTS,'Info');
        }
    }

    @api
    calladdSelectedProductToOpp() {
        // console.log('i am in calladdSelectedProductToOpp '+JSON.stringify(this.items));
        console.log('calladdSelectedProductToOpp', JSON.parse(JSON.stringify(this.items)));
        addSelectedProductToOpp({ recordId: this.recordId, selectedProducts: JSON.stringify(this.items)})
            .then(result => {
            //console.log('callgetSelectedProducts results :: '+result);
            // this.handleAddProductsToSelection(result); 
            // this.items = JSON.parse(result); 
            console.log('result ' + result);
            if (result === 'Success'){
                console.log('result ' + result);

                this.showToast(this.labels.PRODUCT_PICKER_MESSAGE_SUCCESS_TITLE, this.labels.PRODUCT_PICKER_MESSAGE_SUCCESS, 'success');
    
                this[NavigationMixin.Navigate]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: this.recordId,
                        actionName: 'view'
                    }
                });            
            }

        }).catch(error => {
            this.error = error;
            console.log('error:');
            console.log(error);
            this.showToast(this.labels.PRODUCT_PICKER_MESSAGE_ERROR_TITLE, this.labels.PRODUCT_PICKER_MESSAGE_ERROR, 'error');
        });
    }

    showToast(title, msg, responseStatus) {
        const event = new ShowToastEvent({
            title: title,
            message: msg,
            variant: responseStatus
        });
        this.dispatchEvent(event);
    }

    handledelete(event) {
        for(var item in this.items){
            // console.log('item :: '+this.items[item].product.Id);
            if(this.items[item].product.Id === event.detail)
            {
                // removes the item from the this.items array
                this.items.splice(item,1);
            }
       }
    }

    handledemoeqclick(event) {
        var response = JSON.stringify(event.detail);
        console.log('handledemoeqclick :: '+event.detail);
        for(var item in this.items){
            console.log('item :: '+this.items[item].product.Id);

            if(this.items[item].product.Id === event.detail)
            {
                console.log('product id matched '+this.items[item].product.Id);
                this.items[item].isDemoEquipment = true;
            }
       }
    }

    handlesalespricechange(event) {
        console.log('handlesalespricechange :: '+JSON.stringify(event.detail));
        var eventresp = JSON.parse(JSON.stringify(event.detail));
        console.log(eventresp.productkey);
        console.log(eventresp.changedSalesVal);     
        
        for(var item in this.items){
            console.log('item :: '+this.items[item].product.ProductCode);

            if(this.items[item].product.ProductCode === eventresp.productkey)
            {
                // this.items[item].product.UnitPrice = eventresp.changedSalesVal;
                this.items[item].unitPrice = eventresp.changedSalesVal;
            }
        }
    }
}
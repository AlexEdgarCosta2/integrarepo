/*
*   @description Test class for OpportunityTriggerHandlerBasic functionality.
*
*   @author ach
*   @copyright PARX
*/
@isTest
public with sharing class Test_OpportunityTriggerHandlerBasic
{
    /*
    *   @description 
    */
    static testMethod void testUniqueNames()
    {
        TriggerTemplateV2.setupUnitTest();
        Account anAccount = new Account(Name = 'asd account', BillingCountryCode = 'US', BillingState = 'California');
        insert anAccount;
        
        Opportunity testOpportunity = new Opportunity(Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert', 
                CloseDate = date.today());
        TriggerTemplateV2.startUnitTest();
        Test.startTest();
        insert testOpportunity;
        Test.stopTest();
        testOpportunity = [SELECT Id, Name FROM Opportunity WHERE Id = :testOpportunity.Id];
        
        System.assertEquals(String.valueOf(Date.today().year()).substring(2) + '00000001', testOpportunity.Name, 'wrong generated name.');
    }
    
    /*
    *   @description 
    */
    static testMethod void testUniqueNames2()
    {
        TriggerTemplateV2.setupUnitTest();
        Account anAccount = new Account(Name = 'asd account', BillingCountryCode = 'US', BillingState = 'California');
        insert anAccount;
        
        insert new Opportunity(Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert', 
                CloseDate = date.today());
        
        List<Opportunity> testOpportunitys = new List<Opportunity>{new Opportunity(Name = 'asd', AccountId = anAccount.Id, 
                StageName = 'Interessiert', CloseDate = date.today()), new Opportunity(Name = 'asd', AccountId = anAccount.Id, 
                StageName = 'Interessiert', CloseDate = date.today()), new Opportunity(Name = 'asd', AccountId = anAccount.Id, 
                StageName = 'Interessiert', CloseDate = date.today())};
        insert testOpportunitys;
        
        testOpportunitys.get(1).Name = String.valueOf(Date.today().year()).substring(2) + '00000001';
        TriggerTemplateV2.startUnitTest();
        Test.startTest();
        try
        {
            update testOpportunitys.get(1);
            System.assert(false, 'the exception shoud be thrown.');
        } catch (DmlException theException)
        { 
        }
        Test.stopTest();
    }
    
    /*
    *   @description 
    */
    static testMethod void testUniqueNames3()
    {
        TriggerTemplateV2.setupUnitTest();
        Account anAccount = TestUtil.getAccount();
        insert anAccount;
        
        insert new Opportunity(Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert', 
                CloseDate = date.today(), CurrencyIsoCode = 'USD');
        
        List<Opportunity> testOpportunitys = new List<Opportunity>{
                    new Opportunity(Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert', CloseDate = date.today()),
                    new Opportunity(Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert', CloseDate = date.today()),
                    new Opportunity(Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert', CloseDate = date.today())};
        insert testOpportunitys;
        
        testOpportunitys.get(1).Name = 'asd';
        testOpportunitys.get(2).Name = 'asd';

        TriggerTemplateV2.startUnitTest();
        Test.startTest();
        try
        {
            update testOpportunitys;
            System.assert(false, 'the exception shoud be thrown.');
        } catch (DmlException theException)
        { 
        }
        Test.stopTest();
    }
    
    @IsTest static void shouldRecalculateDiscounts()
    {
        TriggerTemplateV2.setupUnitTest();
        Account anAccount = TestUtil.getAccount();
        insert anAccount;
        
        Id pricebookId = Test.getStandardPricebookId();
    
        Opportunity testOpportunity = new Opportunity(Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert',
            CloseDate = date.today(), CurrencyIsoCode = 'USD', Pricebook2Id = pricebookId);
        
        insert testOpportunity;
        
        Product2 productA = new Product2(Name = 'Product A', ProductType__c = 'Products and Accessories');
        insert productA;
        
        PricebookEntry pricebookEntryA = new PricebookEntry(Product2Id = productA.Id, Pricebook2Id = pricebookId, UnitPrice = 100, IsActive = true, CurrencyIsoCode = 'USD');
        insert pricebookEntryA;
        
        System.debug([SELECT Id FROM Product2 LIMIT 10]);
        
        OpportunityLineItem opportunityProduct = new OpportunityLineItem(OpportunityId = testOpportunity.Id,
            PricebookEntryId = pricebookEntryA.Id, Quantity = 1, TotalPrice = pricebookEntryA.UnitPrice,
            ListPriceOneItem__c = pricebookEntryA.UnitPrice);
        
        insert opportunityProduct;
        
        OpportunityLineItem updatedOpportunityProduct = [SELECT UnitPrice FROM OpportunityLineItem WHERE Id = :opportunityProduct.Id LIMIT 1];
        System.assertEquals(pricebookEntryA.UnitPrice, updatedOpportunityProduct.UnitPrice, 'Price should be equal!');
    
        TriggerTemplateV2.startUnitTest();
        testOpportunity.DiscountPercentage__c = testOpportunity.DiscountServices__c = testOpportunity.DiscountDisposables__c = 10.0;
        update testOpportunity;
    
        updatedOpportunityProduct = [SELECT UnitPrice FROM OpportunityLineItem WHERE Id = :opportunityProduct.Id LIMIT 1];
        System.assertNotEquals(pricebookEntryA.UnitPrice, updatedOpportunityProduct.UnitPrice, 'Price should be discounted!');
    }
}
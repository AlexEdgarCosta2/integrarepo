/**
 * @description       :
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-29-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
@IsTest
private class Test_ProductPickerOpportunityCtrl
{

    public static Opportunity testOpportunity;
    public static Quote testQuote;

    private static void initData()
    {
        AccountGroup__c anAccountGroup = new AccountGroup__c(Name = 'asd');
        insert anAccountGroup;

        Account anAccount = TestUtil.getAccount();
        anAccount.AccountGroup__c = anAccountGroup.Id;
        insert anAccount;

        List<Product2> aProducts = new List<Product2>{
                new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductNameDE__c = 'test 1 de', ProductNameFR__c = 'test 1 FR', ProductFamily__c = 'f1', PricingGroup__c = '1',
                        ProductLanguage__c = 'English',ProductType__c = 'Services'),
                new Product2(Name = 'inactive product', IsActive = false, ProductCode = 'test_asd2', ProductNameDE__c = 'test de ', ProductNameFR__c = 'test FR', ProductFamily__c = 'f1'),
                new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductNameDE__c = 'test 2 de', ProductNameFR__c = 'test 2 FR', ProductFamily__c = 'f1', PricingGroup__c = '1'),
                new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductNameDE__c = 'test 3 de', ProductNameFR__c = 'test 3 FR', ProductFamily__c = 'f2', PricingGroup__c = '2')
        };
        insert aProducts;

        testOpportunity = new Opportunity(Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert', CloseDate = date.today(), ProductLanguage__c = 'English',
                                         CurrencyIsoCode = 'USD');
        insert testOpportunity;
        testOpportunity = [
                SELECT Id, CurrencyIsoCode
                FROM Opportunity
                WHERE Id = :testOpportunity.Id
        ];

        List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
                new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
        insert markets;

        List<PriceEntry__c> aPriceEntries = new List<PriceEntry__c>{
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 10, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),
            //valid date is older than for previous price:
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 9, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today().addDays(-2), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),
            //wrong market:
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 100, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today(), Market__c = markets[1].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),
            //wrong valid date:
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 99999, SellingPrice__c = 100, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today().addDays(2), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),

            new PriceEntry__c(Product__c = aProducts.get(2).Id, ScalingTo__c = 6, SellingPrice__c = 200,
                    ScalingTo2__c = 999999, DiscountTier2__c = 25,
                    ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),
            new PriceEntry__c(Product__c = aProducts.get(3).Id, ScalingTo__c = 3, SellingPrice__c = 600,
                    ScalingTo2__c = 999999, DiscountTier2__c = 10, //540, not 500
                    ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 1, SellingPrice__c = 20, ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode)
        };
        insert aPriceEntries;

        Id pricebookId = Test.getStandardPricebookId();

        List<PricebookEntry> pbes = new List<PricebookEntry>{
            new PricebookEntry(Pricebook2id = pricebookId, Product2id = aProducts.get(0).ID, isActive = true, CurrencyIsoCode = 'USD', unitPrice = 100),
            new PricebookEntry(Pricebook2id = pricebookId, Product2id = aProducts.get(1).ID, isActive = true, CurrencyIsoCode = 'USD', unitPrice = 100),
            new PricebookEntry(Pricebook2id = pricebookId, Product2id = aProducts.get(2).ID, isActive = true, CurrencyIsoCode = 'USD', unitPrice = 100),
            new PricebookEntry(Pricebook2id = pricebookId, Product2id = aProducts.get(3).ID, isActive = true, CurrencyIsoCode = 'USD', unitPrice = 100)
        };
        insert pbes;
    }

    @IsTest
    public static void test()
    {
        initData();

        PageReference p = Page.ProductPickerOpportunity;
        p.getParameters().put('id', testOpportunity.Id);
        Test.setCurrentPage(p);

        String filteredProductsStr = ProductPickerOpportunityCtrl.getFilteredProducts(testOpportunity.Id, 'test_asd', 'active', 'f', '');

        List<LineItemWrapper> filteredProducts = (List<LineItemWrapper>)JSON.deserialize(filteredProductsStr, List<LineItemWrapper>.class);

        LineItemWrapper filterP = filteredProducts[0];
        system.debug('filterP: '+filterP);
        LineItemWrapper pLIW = new LineItemWrapper(filterP.product,filterP.productName,
                                                                                                   filterP.pps,filterP.standardPrices,
                                                                                                  filterP.overallDiscount,filterP.disposablesDiscount,
                                                                                                  filterP.servicesDiscount,filterP.productType,
                                                                                                  true,true);


        system.assertEquals(3, filteredProducts.size(), 'wrong number of filtered products');

        for (LineItemWrapper w : filteredProducts) {
            w.quantity = '1';
        }

        String productsStr = JSON.serializePretty(filteredProducts);
        String selectedProductsStr = JSON.serializePretty(new List<LineItemWrapper>());
        Map<String, String> selectedProductsMap = ProductPickerOpportunityCtrl.getSelectedProducts(testOpportunity.Id, productsStr, selectedProductsStr);
        List<LineItemWrapper> products = (List<LineItemWrapper>)JSON.deserialize(selectedProductsMap.get('products'), List<LineItemWrapper>.class);
        List<LineItemWrapper> selectedProducts = (List<LineItemWrapper>)JSON.deserialize(selectedProductsMap.get('selectedProducts'), List<LineItemWrapper>.class);

        system.assertEquals(3, products.size(), 'wrong number of filtered products');
        system.assertEquals(3, selectedProducts.size(), 'wrong number of selected products');

        selectedProductsStr = ProductPickerOpportunityCtrl.removeSelectedProduct(testOpportunity.Id, selectedProducts[0].product.Id, selectedProductsMap.get('selectedProducts'));
        selectedProducts = (List<LineItemWrapper>)JSON.deserialize(selectedProductsStr, List<LineItemWrapper>.class);

        system.assertEquals(2, selectedProducts.size(), 'wrong number of selected products');

        ProductPickerOpportunityCtrl.addSelectedProductToOpp(testOpportunity.Id, selectedProductsStr);

        List<OpportunityLineItem> oppLineItems = [
            SELECT Id
            FROM OpportunityLineItem
            WHERE OpportunityId =: testOpportunity.Id
        ];

        system.assertEquals(2, oppLineItems.size(), 'wrong number of OpportunityLineItems');

        Map<String, List<Map<String, String>>> picklistOptions = ProductPickerOpportunityCtrl.getPicklistOptions();



        ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity);
        ProductPickerOpportunityController testopp = new ProductPickerOpportunityController(sc);
        testopp.getAvailableLanguages();

        system.assertEquals(true, picklistOptions.size() > 0);

    }

    public static testMethod void testOverallDiscount()
    {
        initData();
        testOpportunity.DiscountPercentage__c = 10.10;
        testOpportunity.ProductLanguage__c ='French';
        testOpportunity.DiscountDisposables__c = 5.00;
        testOpportunity.DiscountServices__c = 4.00;
        update testOpportunity;

        Product2 prod = new Product2();
        prod.Name = 'active 1';
        prod.IsActive = true;
        prod.ProductCode = 'test_asd1';
        prod.ProductType__c = 'Services';
        insert prod;


        PageReference aPage = Page.ProductPickerOpportunity;
        aPage.getParameters().put('id', testOpportunity.Id);
        Test.setCurrentPage(aPage);
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(testOpportunity);
        ProductPickerOpportunityController aController = new ProductPickerOpportunityController(sc);
        aController.getServicesDiscount();
        aController.filterProductFamily = 'f1';
        aController.filterProductCode = 'test_asd1';
        aController.filterProductName = 'active';
        aController.filterLanguage = 'English';
        aController.getServicesDiscount();
        aController.getDisposablesDiscount();
        aController.getOverallDiscount();
        aController.doFiterProducts();

        aController.filterLanguage = '';

        aController.doAddFilteredProducts();

        PricingService.applyTiers(aController.selectedLineItemWrappers,
            aController.getOverallDiscount(),
            aController.getDisposablesDiscount(),
            aController.getServicesDiscount());

        aController.doClearFilter();
        aController.doClearProducts();
        aController.getProductNameForLanguage(prod,'French');
        aController.doOverrideTiers();
        Test.stopTest();
    }


    public static testMethod void testOverallDiscount1()
    {
        initData();
//        testOpportunity.DiscountPercentage__c = null;
        testOpportunity.DiscountPercentage__c = 0;
        testOpportunity.ProductLanguage__c ='German';
        testOpportunity.DiscountDisposables__c = 5.00;
        testOpportunity.DiscountServices__c = 4.00;
        update testOpportunity;

        Product2 prod = new Product2();
        prod.Name = 'active 1';
        prod.IsActive = true;
        prod.ProductCode = 'test_asd1';
        prod.ProductType__c = 'Services';
        insert prod;


        PageReference aPage = Page.ProductPickerOpportunity;
        aPage.getParameters().put('id', testOpportunity.Id);
        Test.setCurrentPage(aPage);
        Test.startTest();
        ProductPickerController.priceBookId = null;
        ApexPages.StandardController sc = new ApexPages.standardController(testOpportunity);
        ProductPickerOpportunityController aController = new ProductPickerOpportunityController(sc);
        aController.getServicesDiscount();
        aController.getMainAccountGroup();
        aController.filterProductFamily = 'f1';
        aController.filterProductCode = 'test_asd1';
        aController.filterProductName = 'active';
        aController.filterLanguage = 'English';
        aController.filterProductType = 'Ser';
        aController.getServicesDiscount();
        aController.getDisposablesDiscount();
        aController.getOverallDiscount();
        aController.doFiterProducts();

        aController.filterLanguage = '';

        aController.doAddFilteredProducts();
        PricingService.applyTiers(aController.selectedLineItemWrappers,
            aController.getOverallDiscount(),
            aController.getDisposablesDiscount(),
            aController.getServicesDiscount());

        aController.doClearFilter();
        aController.doClearProducts();
        aController.getProductNameForLanguage(prod,'German');
        aController.doOverrideTiers();
        Test.stopTest();
    }


}
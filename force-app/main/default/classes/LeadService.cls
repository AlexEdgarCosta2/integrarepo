/**
 * Lead Service class. Contains methods for updating leads currency and opportunity contact on lead conversion.
 *
 * @author ach, eru
 * @copyright PARX
 */
public with sharing class LeadService
{

    //Basic part of the manager name
    public static final String JOB_NAME = 'Change Leads Currency Job';

    /**
     * @description Changes leads currency based on owner's currency and executes the update.
     */
    public static void updateCurrencyAndExecuteUpdate(List<Lead> newLeads)
    {
        Set<Id> ownerIds = getOwnerIds(newLeads);
        Map<Id, User> owners = new Map<Id, User>([SELECT Id, Name, DefaultCurrencyIsoCode FROM User WHERE Id IN :ownerIds]);
        List<Lead> leadsForUpdate = new List<Lead>();
        for (Lead aLead : newLeads)
        {
            if(owners.containsKey(aLead.OwnerId) && owners.get(aLead.OwnerId).DefaultCurrencyIsoCode != aLead.CurrencyIsoCode)
            {
                aLead.CurrencyIsoCode = owners.get(aLead.OwnerId).DefaultCurrencyIsoCode;
                leadsForUpdate.add(aLead);
            }
        }
        if(!leadsForUpdate.isEmpty())
        {
            update leadsForUpdate;
        }
    }
    
    /**
     * @description find contacts if email is present
     */
    public static List<Lead> CheckDuplicates(List<Lead> newValues, Map<id, Lead> oldMap)
    {
        List<Lead> FilteredLeads = new List<Lead>();
        Map<string, list<lead>> LeadEmailMap = new Map<string, list<lead>>();
        for(Lead ld : newValues)
        {
            if(ld.Email != null){
                if(oldMap != null){
                    if(ld.Email != oldMap.get(ld.id).Email){
                        ld.New_Contact__c = true;
                        if(!LeadEmailMap.containsKey(ld.email.tolowercase()))
                            LeadEmailMap.put(ld.email.tolowercase(), new list<lead>{ld});
                        else
                            LeadEmailMap.get(ld.email.tolowercase()).add(ld);
                    }
                }
                else{
                    if(!LeadEmailMap.containsKey(ld.email.tolowercase()))
                        LeadEmailMap.put(ld.email.tolowercase(), new list<lead>{ld});
                    else
                        LeadEmailMap.get(ld.email.tolowercase()).add(ld);
                }
            }
        }
        if(!LeadEmailMap.isEmpty()){
            for(Contact C : [select Email from Contact where Email in: LeadEmailMap.keySet()]){
                if(LeadEmailMap.containsKey(C.Email.tolowercase())){
                    for(Lead L : LeadEmailMap.get(C.Email.tolowercase())){
                        L.New_Contact__c = false;
                        FilteredLeads.add(L);
                    }
                }
            }
        }
        return FilteredLeads;
    }
    /**
     * @description Updates currency of lead from Owner. We do this on every update because its a requirement.
     */
    public static List<Lead> updateCurrency(List<Lead> newValues)
    {
        Set<Id> ownerIds = getOwnerIds(newValues);
        Map<Id, User> owners = new Map<Id, User>([SELECT Id, DefaultCurrencyIsoCode FROM User WHERE Id IN :ownerIds]);
        for(Lead lead : newValues)
        {
            if(owners.containsKey(lead.OwnerId))
            {
                lead.CurrencyIsoCode = owners.get(lead.OwnerId).DefaultCurrencyIsoCode;
            }
        }
        return newValues;
    }

    /**
     *  @description If lead is converted - attach the contact to the opportunity.
     */
    public static void updateRelatedOpportunityOnConvert(List<Lead> newValues, Map<Id, sObject> oldValues)
    {
        Set<sObject> aModifiedObjects = TriggerUtils.getModifiedObjectsObj(new List<String>{'IsConverted'}, newValues, oldValues);
        List<Opportunity> anOpportunities = new List<Opportunity>();
        for (sObject anObject : aModifiedObjects)
        {
            Lead anItem = (Lead)anObject;
            if (anItem.ConvertedContactId != null && anItem.ConvertedOpportunityId != null)
            {
                anOpportunities.add(new Opportunity(Id = anItem.ConvertedOpportunityId, Contact__c = anItem.ConvertedContactId));
            }
        }
        update anOpportunities;
    }

    /**
     *  Method, that is used to schedule a batch.
     *  Only one batch will be scheduled at one moment.
     */
    public static void scheduleBatch()
    {
        DateTime aDTNow = DateTime.now();
        String aJobName = ((aDTNow.minute() / 5) + 1) * 5 + ' ' + aDTNow.hour() + ' ' + aDTNow.day() + ' ' + aDTNow.month() + ' ? ' + aDTNow.year(); // jobs must be uniquely named
        List<CronJobDetail> aJobs = [SELECT Id, Name FROM CronJobDetail WHERE Name like : (JOB_NAME + '%')];
        if (aJobs.size() == 0) // shedule job only if we have no same runing jobs
        {
            try
            {
                System.scheduleBatch(new ChangeLeadCurrencyBatch(), JOB_NAME + aJobName, 1);
            } catch (Exception ex)
            {
                System.debug('*** LeadService: scheduleBatch(), exception when trying to schedule batch: ' + ex.getMessage());
            }
        }
    }

    /**
     * @description Collects owner ids
     */
    private static Set<Id> getOwnerIds(List<Lead> newLeads)
    {
        Set<Id> ownerIds = new Set<Id>();
        for(Lead lead : newLeads)
        {
            if(lead.OwnerId != null && String.valueOf(lead.OwnerId).startsWith('005'))
            {
                ownerIds.add(lead.OwnerId);
            }
        }
        return ownerIds;
    }
}
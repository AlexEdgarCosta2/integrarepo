/**
 * @description       : Case service.
 * @author            : ???
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 2.0   04-28-2021   alexandre.costa@integra-biosciences.com   Added the approveRejectsDiscounts
**/
public with sharing class CaseService {

    /**
    * @description When a case is created check if it's an approval case. If do approve/reject the Opportunity or Quote
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param newCases
    **/
    public static void approveRejectsDiscounts(List<Case> newCases) {

        System.debug(LoggingLevel.DEBUG, '[approveRejectsDiscounts] BEGIN');

        Map<String, String> oppQuoteToApproveReject = new Map<String, String>();
        Map<String, String> quoteApproverEmail = new Map<String, String>();
        Boolean flagOpportunity = false;
        Boolean flagQuote = false;

        Set<Id> oppQuoteIds = new Set<Id>();
        for(Case aCase : newCases) {
            //
            // Check is case is for an approval
            //
            if(aCase.RecordTypeId == Constants.CASE_DISCOUNTS_APPROVAL_RT_ID && aCase.Subject.contains(Constants.DISCOUNTS_APPROVAL_MAIL_SUBJECT_TAG)) {
                String oppQuoteId = aCase.Subject.right(18);

                String approvalStatus = ApprovalUtil.getDiscountsApprovalStatus(aCase.Description);

                if(approvalStatus != '') {
                    Try {
                        id aux = (Id)oppQuoteId;

                        flagQuote = (Utils.getObjectType(aux) == Constants.OBJECT_TYPE_QUOTE);

                        if(flagQuote) {
                            oppQuoteIds.add(aux);
                            oppQuoteToApproveReject.put(oppQuoteId, approvalStatus);
                            quoteApproverEmail.put(oppQuoteId, aCase.SuppliedEmail);
                        }
                    } catch(Exception e) {
                        System.debug(LoggingLevel.ERROR, 'Error : ' + e.getmessage());
                    }
                }
            }
        }

        System.debug(LoggingLevel.DEBUG, '[approveRejectsDiscounts] oppQuoteIds : ' + oppQuoteIds);

        Set<Id> validIds = new Set<Id>();
        if(flagQuote && !oppQuoteIds.isEmpty()) {
            if (flagQuote) {
                List<Quote> quoteList = QuoteSelector.getQuotesByIds(oppQuoteIds);
                for(Quote quote : quoteList) {
                    if(quote.Approval_Status__c == Constants.APPROVAL_STATUS_PENDING) {
                        quote.Approvers__c = quoteApproverEmail.get(quote.Id);
                        validIds.add(quote.Id);
                    }
                }
            }
        }

        for(String key : oppQuoteToApproveReject.keySet()) {
            if(validIds.contains((id)key)) {
                System.debug(LoggingLevel.DEBUG, '[approveRejectsDiscounts] ' + (id)key + ' - ' + oppQuoteToApproveReject.get(key));
                ApprovalUtil.approveRejectProcess((id)key, oppQuoteToApproveReject.get(key));
            }
        }

        //
        // Need to get the quotes again because the approval process has updated them.
        //
        if(!validIds.isEmpty()) {
            List<Quote> quotesToUpdate = new List<Quote>();
            List<Quote> quoteList = QuoteSelector.getQuotesByIds(validIds);
            for(Quote quote : quoteList) {
                quote.Approvers__c = quoteApproverEmail.get(quote.Id);
                quotesToUpdate.add(quote);

            }
            update quotesToUpdate;
        }
    }

	/**
	* Roll up summary on lineitems for Case object.
	*/
	public static void rollupSumaryToCases(List<CaseProduct__c> lineItems)
	{
		// The first step is to create a context for RollupSummaryEngine, by specifying parent and child objects and
		// the lookup relationship field name
		RollupSummaryEngine.Context ctx = new RollupSummaryEngine.Context(Case.SobjectType, // parent object
				CaseProduct__c.SobjectType,  // child object
				Schema.SObjectType.CaseProduct__c.fields.Case__c); // relationship field name
		ctx.add(
			new RollupSummaryEngine.RollupSummaryField(
				Schema.SObjectType.Case.fields.TotalAppliedCost__c,
				Schema.SObjectType.CaseProduct__c.fields.AppliedCost__c,
				RollupSummaryEngine.RollupOperation.Sum
		    ));

		ctx.add(
			new RollupSummaryEngine.RollupSummaryField(
				Schema.SObjectType.Case.fields.TotalEffectiveCost__c,
				Schema.SObjectType.CaseProduct__c.fields.EffectiveCost__c,
				RollupSummaryEngine.RollupOperation.Sum
		    ));
		Sobject[] masters = RollupSummaryEngine.rollUp(ctx, lineItems);
		// Persiste the changes in master
		update masters;
	}
}

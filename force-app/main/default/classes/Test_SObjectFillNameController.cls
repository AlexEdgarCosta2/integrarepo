/*
*	@description Test class for SObjectFillNameController functionality.
*
*	@author ach
*	@copyright PARX
*/
@isTest
public with sharing class Test_SObjectFillNameController
{
	/*
	*	@description check redirect.
	*/
	static testMethod void testRedirect()
	{
		
		ApexPages.StandardController sc = new ApexPages.StandardController(new Opportunity());
		SObjectFillNameController controller = new SObjectFillNameController(sc);
		controller.redirect();
	}
}
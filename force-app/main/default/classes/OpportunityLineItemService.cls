/**
 * @description       : Opp Line Item Service Class
 * @author            : ?????
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class OpportunityLineItemService {

	public static void syncToQuoteLineItems(List<OpportunityLineItem> opportunityLineItems) {
		Set<Id> opportunityIds = new Set<Id>();
		Set<Id> quoteIds = new Set<Id>();

		System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems] BEGIN');

		for (OpportunityLineItem opportunityLineItem : opportunityLineItems) {
			opportunityIds.add(opportunityLineItem.OpportunityId);
		}

		for (Opportunity opportunity : [
			SELECT Id, SyncedQuoteId
			FROM Opportunity
			WHERE Id IN:opportunityIds
		]) {
			quoteIds.add(opportunity.SyncedQuoteId);
		}

		if (quoteIds.isEmpty()) {
			return;
		}

		List<QuoteLineItem> quoteLineItems = [
			SELECT Id, OpportunityLineItemId, PricebookEntryId, Quote.IsSyncing, Description, DemoEquipment__c, SortOrder__c, UnitPrice, Quantity, ListPriceTierPricing__c, ListPriceOneItem__c, List_Price__c, ListPrice
			FROM QuoteLineItem
			WHERE QuoteId IN :quoteIds
		];

		System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems] quoteIds : ' + quoteIds);
		System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems] quoteLineItems : ' + quoteLineItems);

		Map<Id, OpportunityLineItem> opportunityLineItemMap = new Map<Id, OpportunityLineItem>(opportunityLineItems);
		Set<QuoteLineItem> quoteLineItemsToUpdate = new Set<QuoteLineItem>();

		for (QuoteLineItem quoteLineItem : quoteLineItems) {
			if (!opportunityLineItemMap.containsKey(quoteLineItem.OpportunityLineItemId)) {
				continue;
			}
			OpportunityLineItem opportunityLineItem = opportunityLineItemMap.get(quoteLineItem.OpportunityLineItemId);
			if (opportunityLineItem == null || !quoteLineItem.Quote.IsSyncing) {
				continue;
			}

			Boolean needToUpdate = ( (quoteLineItem.UnitPrice != opportunityLineItem.UnitPrice) ||
									 (quoteLineItem.Quantity != opportunityLineItem.Quantity) ||
									 (quoteLineItem.PricebookEntryId != opportunityLineItem.PricebookEntryId) ||
									 (quoteLineItem.ListPriceTierPricing__c != opportunityLineItem.ListPriceTierPricing__c) ||
									 (quoteLineItem.ListPriceOneItem__c != opportunityLineItem.ListPriceOneItem__c) ||
									 (quoteLineItem.SortOrder__c != opportunityLineItem.SortOrder__c) ||
									 (quoteLineItem.Description != opportunityLineItem.Description) ||
									 (quoteLineItem.DemoEquipment__c != opportunityLineItem.DemoEquipment__c) );

			System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems] needToUpdate : ' + needToUpdate);

			/*
			for (String customFieldToSync : OpportunityQuoteHelper.customLineItemFieldsToSync) {

				System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems]');

				System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems] customFieldToSync : ' + customFieldToSync);
				System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems] opportunityLineItem.get(customFieldToSync) : ' + opportunityLineItem.get(customFieldToSync));
				System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems] quoteLineItem.get(customFieldToSync) : ' + quoteLineItem.get(customFieldToSync));

				if (quoteLineItem.get(customFieldToSync) != opportunityLineItem.get(customFieldToSync)) {
					quoteLineItem.put(customFieldToSync, opportunityLineItem.get(customFieldToSync));
					System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems] opportunityLineItem.get(customFieldToSync)');
					System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems] quoteLineItem : ' + quoteLineItem);
					needToUpdate = true;
				}
			}
			*/

			if (needToUpdate) {
				quoteLineItem.UnitPrice = opportunityLineItem.UnitPrice;
				quoteLineItem.Quantity = opportunityLineItem.Quantity;
				if(quoteLineItem.PricebookEntryId == null && opportunityLineItem.PricebookEntryId != null) {
					quoteLineItem.PricebookEntryId = opportunityLineItem.PricebookEntryId;
				}
				quoteLineItem.ListPriceTierPricing__c = opportunityLineItem.ListPriceTierPricing__c;
				quoteLineItem.ListPriceOneItem__c = opportunityLineItem.ListPriceOneItem__c;
				quoteLineItem.SortOrder__c = opportunityLineItem.SortOrder__c;
				quoteLineItem.Description = opportunityLineItem.Description;
				quoteLineItem.DemoEquipment__c = opportunityLineItem.DemoEquipment__c;

				quoteLineItemsToUpdate.add(quoteLineItem);
			}
		}

		System.debug(LoggingLevel.DEBUG, '[syncToQuoteLineItems] quoteLineItemsToUpdate : ' + quoteLineItemsToUpdate);

		if (!quoteLineItemsToUpdate.isEmpty()) {
			update new List<QuoteLineItem>(quoteLineItemsToUpdate);
		}
	}

	public static void getDataFromQuoteLineItems(List<OpportunityLineItem> opportunityLineItemsFromTrigger) {
		Set<Id> opportunityIds = new Set<Id>();
		Set<Id> quoteIds = new Set<Id>();

		System.debug(LoggingLevel.DEBUG, '[getDataFromQuoteLineItems] BEGIN');

		List<OpportunityLineItem> opportunityLineItems = [
			SELECT Id, OpportunityId, PricebookEntryId, Description, DemoEquipment__c, List_Price__c, SortOrder__c, UnitPrice, Quantity, ListPriceTierPricing__c, ListPriceOneItem__c, ListPrice
			FROM OpportunityLineItem
			WHERE Id IN :opportunityLineItemsFromTrigger
		];

		for (OpportunityLineItem opportunityLineItem : opportunityLineItems) {
			opportunityIds.add(opportunityLineItem.OpportunityId);
		}

		for (Opportunity opportunity : [
			SELECT Id, SyncedQuoteId
			FROM Opportunity
			WHERE Id IN:opportunityIds
		]) {
			quoteIds.add(opportunity.SyncedQuoteId);
		}

		if (quoteIds.isEmpty()) {
			return;
		}

		List<QuoteLineItem> quoteLineItems = [
			SELECT Id, OpportunityLineItemId, PricebookEntryId, Quote.IsSyncing, Description, DemoEquipment__c, SortOrder__c, UnitPrice, Quantity, List_Price__c, ListPriceTierPricing__c, ListPriceOneItem__c, ListPrice
			FROM QuoteLineItem
			WHERE QuoteId IN :quoteIds
			AND Quote.IsSyncing = true
		];

		if (quoteLineItems.isEmpty()) {
			return;
		}

		Map<Id, OpportunityLineItem> opportunityLineItemMap = new Map<Id, OpportunityLineItem>(opportunityLineItems);
		List<OpportunityLineItem> opportunityLineItemsToUpdate = new List<OpportunityLineItem>();

		for (QuoteLineItem quoteLineItem : quoteLineItems) {
			Boolean needToUpdate = false;

			if (!opportunityLineItemMap.containsKey(quoteLineItem.OpportunityLineItemId)) {
				continue;
			}
			OpportunityLineItem opportunityLineItem = opportunityLineItemMap.get(quoteLineItem.OpportunityLineItemId);

			for (String customFieldToSync : OpportunityQuoteHelper.customLineItemFieldsToSync) {
				if (opportunityLineItem.get(customFieldToSync) != quoteLineItem.get(customFieldToSync)) {
					opportunityLineItem.put(customFieldToSync, quoteLineItem.get(customFieldToSync));

					needToUpdate = true;
				}
			}

			if (needToUpdate) {
				opportunityLineItemsToUpdate.add(opportunityLineItem);
			}
		}

		if (!opportunityLineItemsToUpdate.isEmpty()) {
			update opportunityLineItemsToUpdate;
		}
	}

	public static void createObjectLIHistoty(Map<Id, sObject> newMap, Map<Id, sObject> oldMap)
	{

		List<sObject> ids;
		if (newMap != null)
		{
			ids = newMap.values();
		}
		else
		{
			ids = oldMap.values();
		}

		SObjectType objectType = ids[0].Id.getSObjectType();

		SObjectType typeOfLIHistory;
		if (String.valueOf(objectType) == 'OpportunityLineItem')
		{
			typeOfLIHistory = Schema.getGlobalDescribe().get('OpportunityLineItemHistory__c');
		}
		else
		{
			typeOfLIHistory = Schema.getGlobalDescribe().get('QuoteLineItemHistory__c');
		}

		Map<String, Schema.SObjectField> mapFields = objectType.getDescribe().fields.getMap();

		List<SObject> objectLIHistories = new List<SObject>();
		Map<Id, sObject> tempMainMap = new Map<Id, sObject>();
		if (newMap != null && !newMap.isEmpty())
		{
			tempMainMap = newMap;
		}
		else if (newMap == null && oldMap != null && !oldMap.isEmpty())
		{
			tempMainMap = oldMap;
		}

		for (SObject itemNew : tempMainMap.values())
		{
			SObject newObjectLIHistory = createNewObjectLineItemHistory(itemNew, typeOfLIHistory);
			//update
			if (newMap != null && !newMap.isEmpty() && oldMap != null && !oldMap.isEmpty())
			{
				SObject oldQuoteLIHistory = oldMap.get(itemNew.Id);

				String log = createLogValue(mapFields, itemNew, oldQuoteLIHistory);

				if (String.isNotBlank(log))
				{
					newObjectLIHistory.put('Log__c', log);
					newObjectLIHistory.put('Type__c',QuoteLineItemService.TYPE_PRODUCT_CHANGED);
					objectLIHistories.add(newObjectLIHistory);
				}
			}
			//create
			else if (newMap != null && (oldMap == null || oldMap.isEmpty()))
			{
				String log = '-';

				if (String.isNotBlank(log))
				{
					newObjectLIHistory.put('Log__c', log);
					newObjectLIHistory.put('Type__c',QuoteLineItemService.TYPE_PRODUCT_ADDED);
					objectLIHistories.add(newObjectLIHistory);
				}
			}
			//delete
			else if (newMap == null && (oldMap != null && !oldMap.isEmpty()))
			{
				String log = '-';

				if (String.isNotBlank(log))
				{
					newObjectLIHistory.put('Log__c', log);
					newObjectLIHistory.put('Type__c',QuoteLineItemService.TYPE_PRODUCT_REMOVED);
					objectLIHistories.add(newObjectLIHistory);
				}
			}
		}
		insert objectLIHistories;
	}

	public static String createLogValue(Map<String, Schema.SObjectField> mapFields, SObject itemNew, SObject oldQuoteLIHistory)
	{
		String logValue;
		Set<String> nameOfFieldsTrack = new Set<String>{'unitprice','quantity', 'demoequipment__c', 'discount__c'};
		for (String str : mapFields.keyset())
		{

			if (oldQuoteLIHistory != null && itemNew.get(str) != oldQuoteLIHistory.get(str) && nameOfFieldsTrack.contains(str))
			{
				if (String.isBlank(logValue))
				{
					logValue = '';
				}
				logValue += 'Old: ' + str + ' ' + String.valueOf(oldQuoteLIHistory.get(str)) +  '\n';
				logValue += 'New: ' + str + ' ' + String.valueOf(itemNew.get(str)) +  '\n';
			}
			else if (oldQuoteLIHistory == null)
			{
				if (String.isBlank(logValue))
				{
					logValue = '';
				}
				logValue += 'Old: ' + str + ' ' + String.valueOf(itemNew.get(str)) +  '\n';

			}
		}
		return logvalue;
	}

	public static SObject createNewObjectLineItemHistory(SObject objectLI, SObjectType objectType)
	{
		sObject newObjectLIHistory = objectType.newSObject();
		if (String.valueOf(objectType) == 'OpportunityLineItemHistory__c')
		{
			newObjectLIHistory.put('Opportunity__c', (Id)objectLI.get('OpportunityId'));
		}
		else
		{
			newObjectLIHistory.put('Quote__c', (Id)objectLI.get('QuoteId'));
			newObjectLIHistory.put('QuoteLineItem__c', (Id)objectLI.get('Id'));
		}

		newObjectLIHistory.put('Product__c', (Id)objectLI.get('Product2Id'));
		return newObjectLIHistory;
	}

}
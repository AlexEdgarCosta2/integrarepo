/**
 * @description       :
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
@IsTest
public with sharing class Test_PricingService {

    @TestSetup
    static void makeData() {
        AccountGroup__c anAccountGroup = TestUtil.getAccountGroup('asd');
        insert anAccountGroup;

        Account anAccount = TestUtil.getAccount('asd account', anAccountGroup.Id, 'US', 'CA');
        insert anAccount;

        List<Opportunity> oppList = new List<Opportunity> {
            TestUtil.getOpportunity('opportunity1', anAccount.Id, 'Interessiert', date.today(), 'USD', 10.00, 0.00, 0.00)
        };
        insert oppList;

        List<Product2> aProducts = new List<Product2> {
            TestUtil.getProduct('active 1', 'test_asd1', Constants.PRODUCT_TYPE_ACCESSORIES, '1')
        };
        insert aProducts;

        List<Market__c> markets = new List<Market__c> {
            TestUtil.getMarket('America', 'US; CA')
        };
        insert markets;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry> {
            TestUtil.getPricebookEntry(pricebookID, aProducts.get(0).Id, 0, 'USD')
        };
        insert standardPrices;

        List<PriceEntry__c> standardPrices2 = new List<PriceEntry__c> {
            TestUtil.getPriceEntry(aProducts.get(0).Id, 10000, 'USD', 1, 10, 9999, 0.05, 0.1, markets[0].Id)
        };
        insert standardPrices2;
    }

    @IsTest
    static void testProductScaling() {

        Test.startTest();

        List<PriceEntry__c> standardPrices2 = TestUtil.selectPriceEntry();

        String productScaling = PricingService.getProductScaling(0,
            Constants.PRODUCT_TYPE_ACCESSORIES,
            0,
            0,
            standardPrices2[0]);

        System.assertEquals(productScaling, '≤ 1: 10,000.00 \n≤ 10: 9,995.00 \n≤ 9999: 9,990.00 \n');

        productScaling = PricingService.getProductScaling(10,
            Constants.PRODUCT_TYPE_ACCESSORIES,
            0,
            0,
            standardPrices2[0]);

        System.assertEquals(productScaling, '≤ 1: 10,000.00 \n≤ 10: 9,995.00 \n≤ 9999: 9,990.00 \n');

        productScaling = PricingService.getProductScaling(0,
            Constants.PRODUCT_TYPE_ACCESSORIES,
            10,
            0,
            standardPrices2[0]);

        System.assertEquals(productScaling, '≤ 1: 10,000.00 \n≤ 10: 9,995.00 \n≤ 9999: 9,990.00 \n');

        productScaling = PricingService.getProductScaling(0,
            Constants.PRODUCT_TYPE_ACCESSORIES,
            0,
            10,
            standardPrices2[0]);

        System.assertEquals(productScaling, '≤ 1: 10,000.00 \n≤ 10: 9,995.00 \n≤ 9999: 9,990.00 \n');

        Test.stopTest();
    }

    @IsTest
    static void testApplyTiers() {
        Test.startTest();

        List<Product2> productList = TestUtil.selectProduct();

        Set<Id> productIds = new Set<Id>();
        productIds.add(productList[0].Id);

        String countryCode = '%' + 'US' + '%';

        Map<String, PriceEntry__c> priceEntryMap = PriceEntrySelector.getpriceEntryMapByProducts2(productIds);

        List<Account> accountList = TestUtil.selectAccount();

        List<PriceEntry__c> priceEntryList = PricingService.getCurrentPriceEntryList(priceEntryMap,
            productList[0].Id,
            null,
            'USD',
            'US',
            0.00,
            0.00,
            0.00);

        LineItemWrapper lw =
        new LineItemWrapper(productList[0],
            productList[0].Name,
            priceEntryList,
            priceEntryList,
            0,
            0,
            0,
            productList[0].ProductType__c);

        List<LineItemWrapper> lwList = new List<LineItemWrapper>();

        lwList.add(lw);

        PricingService.applyTiers(lwList,
            0,
            0,
            0);

        System.assertEquals(1, lwList[0].applicableTier);

        lwList[0].qty = 5;

        PricingService.applyTiers(lwList,
            0,
            0,
            0);

        System.assertEquals(2, lwList[0].applicableTier);

        lwList[0].qty = 15;

        PricingService.applyTiers(lwList,
            0,
            0,
            0);

        System.assertEquals(3, lwList[0].applicableTier);

        Test.stopTest();
    }
}

/**
 * =============================================================
 * AUTHOR         : Alex Costa
 * CREATE DATE    : 08.04.2021
 * PURPOSE        : Generic Utilitarian class
 * SPECIAL NOTES  :
 * =============================================================
 * Change History :
 * Date        Author               Description
 * ==========  ==================   ============================
 * 08.04.2021  Alex Costa           Created
 * =============================================================
 */
public with sharing class Utils {

    /**
    * @description getObjectType
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param objId
    * @return String
    **/
    public static String getObjectType(Id objId) {
        String objectType = '';

        if (String.valueOf(objId).startsWith('006')) {
            objectType = Constants.OBJECT_TYPE_OPPORTUNITY;
        } else if (String.valueOf(objId).startsWith('0Q0')) {
            objectType = Constants.OBJECT_TYPE_QUOTE;
        } else if (String.valueOf(objId).startsWith('00k')) {
            objectType = Constants.OBJECT_TYPE_OPPORTUNITY_LINE_ITEM;
        } else if (String.valueOf(objId).startsWith('0QL')) {
            objectType = Constants.OBJECT_TYPE_QUOTE_LINE_ITEM;
        }

        return objectType;
    }

	/**
	* @description isDecimalNullOrZero
	* @author alexandre.costa@integra-biosciences.com | 04-28-2021
	* @param decimalValue
	* @return Boolean
	**/
	public static Boolean isDecimalNullOrZero(Decimal decimalValue) {
		return (decimalValue == null || decimalValue == 0.00);
	}

    /**
    * @description getLastFromList
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param inList
    * @return SObject
    **/
    public static SObject getLastFromList(List<SObject> inList) {
        return inList.get(inList.size() - 1);
    }

    /**
    * @description decimalToCurrencyString
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param decNumber
    * @return String
    **/
    public static String decimalToCurrencyString(Decimal decNumber) {
        //return (decNumber.format().contains('.')?decNumber.format():(decNumber.format()+'.00'));

        return stringFormatCurrency(decNumber.format());
    }

    /**
    * @description stringFormatCurrency
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param s
    * @return String
    **/
    public static String stringFormatCurrency(String s) {
        if (!s.contains('.')) {
            s = s + '.00';
        } else {
          Integer dPos = s.indexOf('.');
          if (s.length() - dPos < 3) { s = s + '0'; }
        }
        return s;
    }
}
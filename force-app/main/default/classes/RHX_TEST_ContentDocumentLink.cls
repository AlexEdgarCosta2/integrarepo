@isTest(SeeAllData=true)
public class RHX_TEST_ContentDocumentLink {
  static testMethod void RHX_Testmethod() {
      
        id acctId = [SELECT Id FROM account LIMIT 1].id;
  
        List<sObject> sourceList = [SELECT Id FROM contentdocumentlink where linkedentityid =:acctID LIMIT 1];
        
        if(sourceList.size() == 0) {
           ContentDocumentLink cdl = new ContentDocumentLink();
           sourceList.add(cdl);
            
           ContentVersion cv = new ContentVersion();

           cv.ContentLocation = 'S';
           cv.ContentDocumentId = cdl.id;
           cv.title = 'test';
           cv.PathOnClient = 'test';
           cv.VersionData = EncodingUtil.base64Decode('test');

           insert cv;

           cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId;
           cdl.LinkedEntityId = acctid;
           cdl.ShareType = 'V';

        }
        
        

      rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}
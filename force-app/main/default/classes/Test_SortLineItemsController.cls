/*
*   @description Test class for SortLineItemsController functionality.
*
*   @author ach
*   @copyright PARX
*/
@isTest
public with sharing class Test_SortLineItemsController
{
    public static Opportunity testOpportunity;
    
    public static List<OpportunityLineItem> items;
    
    /*
    *   @description test sorting functionality for opportunity.
    */
    static testMethod void testOpportunity()
    {
        TriggerTemplateV2.setupUnitTest();
        initData();
        
        PageReference aPage = Page.SortOppLineItems;
        aPage.getParameters().put('id', testOpportunity.Id);
        Test.setCurrentPage(aPage);
        Test.startTest();
        TriggerTemplateV2.startUnitTest();
        SortLineItemsController controller = new SortLineItemsController();
        System.assertEquals(0, controller.fromSectionServices.size(), 'Wrong number of products with Type Services');
        System.assertEquals(1, controller.fromSectionAccessories.size(), 'Wrong number of products with Type Accessories');
        System.assertEquals(2, controller.fromSectionDisposables.size(), 'Wrong number of products with Type Disposables');
        
        //save
        controller.doSave();
        OpportunityLineItem item1 = [SELECT Id, SortOrder__c FROM OpportunityLineItem WHERE Id = :items[0].Id];
        System.assertEquals(1, item1.SortOrder__c, 'Wrong SortOrder__c');
        OpportunityLineItem item2 = [SELECT Id, SortOrder__c FROM OpportunityLineItem WHERE Id = :items[1].Id];
        System.assertEquals(1, item2.SortOrder__c, 'Wrong SortOrder__c');
        OpportunityLineItem item3 = [SELECT Id, SortOrder__c FROM OpportunityLineItem WHERE Id = :items[2].Id];
        System.assertEquals(2, item3.SortOrder__c, 'Wrong SortOrder__c');
        
        //change order
        controller.rankNumber = '1';
        controller.productType = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES;
        controller.doRankDown();
        System.assertEquals(item2.Id, controller.fromSectionDisposables[1].itemId, 'Wrong line item');
        
        controller.rankNumber = '2';
        controller.doRankUp();
        System.assertEquals(item2.Id, controller.fromSectionDisposables[0].itemId, 'Wrong line item');
        
        aPage.getParameters().put('oldRank', '1');
        aPage.getParameters().put('newRank', '2');
        aPage.getParameters().put('productType', SortLineItemsController.PRODUCT_TYPE_DISPOSABLES);
        Test.setCurrentPage(aPage);
        controller.doChangeRank();
        Test.stopTest();
        
        System.assertEquals(item2.Id, controller.fromSectionDisposables[1].itemId, 'Wrong line item');
    }
    
        /*
    *   @description test sorting functionality.
    */
    static testMethod void testCase()
    {
        TriggerTemplateV2.setupUnitTest();
        initData();
        Case caseObj = new Case();
        insert caseObj;
        List<Product2> aProducts = [SELECT Id FROM Product2];
        List<CaseProduct__c> lineItems = new List<CaseProduct__c>{
                new CaseProduct__c(Case__c = caseObj.Id, EffectiveCost__c = 2, AppliedCost__c = 3, Quantity__c = 2, Product__c = aProducts[0].Id),
                new CaseProduct__c(Case__c = caseObj.Id, EffectiveCost__c = 10, AppliedCost__c = 20, Quantity__c = 1, Product__c = aProducts[1].Id),
                new CaseProduct__c(Case__c = caseObj.Id, EffectiveCost__c = 10, AppliedCost__c = 20, Quantity__c = 1, Product__c = aProducts[2].Id)};
        insert lineItems;
        PageReference aPage = Page.SortOppLineItems;
        aPage.getParameters().put('id', caseObj.Id);
        Test.setCurrentPage(aPage);
        Test.startTest();
        TriggerTemplateV2.startUnitTest();
        SortLineItemsController controller = new SortLineItemsController();
        System.assertEquals(0, controller.fromSectionServices.size(), 'Wrong number of products with Type Services');
        System.assertEquals(1, controller.fromSectionAccessories.size(), 'Wrong number of products with Type Accessories');
        System.assertEquals(2, controller.fromSectionDisposables.size(), 'Wrong number of products with Type Disposables');
        
        //save
        controller.doSave();
        CaseProduct__c item1 = [SELECT Id, SortOrder__c FROM CaseProduct__c WHERE Id = :lineItems[0].Id];
        System.assertnotEquals(null, item1.SortOrder__c, 'Wrong SortOrder__c');
        CaseProduct__c item2 = [SELECT Id, SortOrder__c FROM CaseProduct__c WHERE Id = :lineItems[1].Id];
        System.assertnotEquals(null, item2.SortOrder__c, 'Wrong SortOrder__c');
        CaseProduct__c item3 = [SELECT Id, SortOrder__c FROM CaseProduct__c WHERE Id = :lineItems[2].Id];
        System.assertnotEquals(null, item3.SortOrder__c, 'Wrong SortOrder__c');
        Test.stopTest();
    }
    
    //changes made on 2nd July
    static testmethod void testConstructor(){
        initData();
       Opportunity opp = [select id from Opportunity ];
        test.startTest();
        SortLineItemsController controller = new SortLineItemsController(opp.Id);
        test.stopTest();
    }
    //Changes complete
    private static void initData()
    {
        AccountGroup__c anAccountGroup = new AccountGroup__c(Name = 'asd');
        insert anAccountGroup;
        
        Account anAccount = new Account(Name = 'asd account', AccountGroup__c = anAccountGroup.Id, BillingCountryCode = 'US', BillingState='California',CurrencyIsoCode='USD');
        insert anAccount;
        
        List<Product2> aProducts = new List<Product2>
        {
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '2')};
        insert aProducts;
        
        List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
                new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
        insert markets;
        
        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true,CurrencyIsoCode='USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true,CurrencyIsoCode='USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true,CurrencyIsoCode='USD')
        };
        insert standardPrices; 
        
        testOpportunity = new Opportunity(Name = 'opportunity1', AccountId = anAccount.Id, StageName = 'Interessiert', CloseDate = date.today(),CurrencyIsoCode='USD');
        insert testOpportunity;
        
        items = new List<OpportunityLineItem>
        {
            new OpportunityLineItem(PricebookEntryId = standardPrices[0].Id, OpportunityId = testOpportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = standardPrices[1].Id, OpportunityId = testOpportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = standardPrices[2].Id, OpportunityId = testOpportunity.Id, Quantity = 1, TotalPrice = 1)
        };
        insert items;
    }
}
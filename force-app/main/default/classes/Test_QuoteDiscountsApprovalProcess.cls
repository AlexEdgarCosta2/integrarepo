/**
 * @description       : Test Discounts Approval (Quotes)
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
@isTest
public with sharing class Test_QuoteDiscountsApprovalProcess {
    @TestSetup
    static void makeData() {
        TestUtil.createQuoteBase();
    }

    @IsTest
    static void testInsertNoAppoval() {
        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, null);
        System.assertEquals(quoteList[0].Approval_Type__c, null);
    }






    /*
    @IsTest
    static void testInsertApprovalTier1() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        //
        // * Product list price 10700
        // *
        // * Quantities 1, 4, 99999
        // *
        // * 4% 10272
        // *
        // * 8% 9844
        // *
        //
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 1, 10260.00);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Pending');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }
    */
/*
    @IsTest
    static void testInsertQuantityTier2NoApproval() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');


        // * Product list price 10700
        // *
        //  * Quantities 1, 4, 99999
        //  *
        //  * 4% 10272
        //  *
        //  * 8% 9844
        //  *

        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 3, 10260.00);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, null);
        System.assertEquals(quoteList[0].Approval_Type__c, null);
    }
*/
/*
    @IsTest
    static void testInsertQuantityTier2Approval() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        //
        //  * Product list price 10700
        //  *
        //  * Quantities 1, 4, 99999
        //  *
        //  * 4% 10272
        //  * 8% 9844
        //  *
        //
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 3, 9843.00);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Pending');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }
*/
/*
    @IsTest
    static void testInsertQuantityTier3Approval() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        //  *
        //  * Product list price 10700
        //  *
        //  * Quantities 1, 4, 99999
        //  *
        //  * 4% 10272
        //  * 8% 9844
        //  *
        //  *
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 9843.00);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Pending');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }
*/
/*
    @IsTest
    static void testInsertQuantityTier3NoApproval() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        //
        //  * Product list price 10700
        //  *
        //  * Quantities 1, 4, 99999
        //  *
        //  * 4% 10272
        //  * 8% 9844
        //  *
        //
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 9844.00);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, null);
        System.assertEquals(quoteList[0].Approval_Type__c, null);
    }
*/

    @IsTest
    static void testInsertDiscountBig20() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        /**
         * Product list price 10700
         *
         * Quantities 1, 4, 99999
         *
         * 4% 10272
         * 8% 9844
         *
         * 20% 8560
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 1, 1560.00);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Pending');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }

    @IsTest
    static void testInsertDiscountBig20T2() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        /**
         * Product list price 10700
         *
         * Quantities 1, 4, 99999
         *
         * 4% 10272
         * 8% 9844
         *
         * 20% 8560
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 3, 1560.00);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Pending');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }

    @IsTest
    static void testInsertDiscountBig20T3() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        /**
         * Product list price 10700
         *
         * Quantities 1, 4, 99999
         *
         * 4% 10272
         * 8% 9844
         *
         * 20% 8560
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 1560.00);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Pending');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }

    @IsTest
    static void testInsertDiscountPromotionByProduct() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        List<Market__c> marketList = TestUtil.selectMarkets();

        Promotion_Product__c promo = TestUtil.getActivePromotionProduct1(products[0].Id, marketList[3].Id);

        insert promo;

        /**
         * Even setting the unit price at 10 almost 100% discount there should be no approva because the product is in promotion
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 60);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, null);
        System.assertEquals(quoteList[0].Approval_Type__c, null);
    }

    @IsTest
    static void testInsertDiscountPromotionByProduct2() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        List<Market__c> marketList = TestUtil.selectMarkets();

        Promotion_Product__c promo = TestUtil.getActivePromotionProduct2(products[0].Id, marketList[3].Id);

        insert promo;

        /**
         * Even setting the unit price at 10 almost 100% discount there should be no approva because the product is in promotion
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 60);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, null);
        System.assertEquals(quoteList[0].Approval_Type__c, null);
    }



    @IsTest
    static void testInsertDiscountPromotionByFamily() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        List<Market__c> marketList = TestUtil.selectMarkets();

        Promotion_Product__c promo = TestUtil.getActivePromotionFamily1('VIAFLO 96', marketList[3].Id);

        insert promo;

        /**
         * Even setting the unit price at 10 almost 100% discount there should be no approva because the product is in promotion
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 60);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, null);
        System.assertEquals(quoteList[0].Approval_Type__c, null);
    }

    @IsTest
    static void testInsertDiscountPromotionByFamily2() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        List<Market__c> marketList = TestUtil.selectMarkets();

        Promotion_Product__c promo = TestUtil.getActivePromotionFamily2('VIAFLO 96', marketList[3].Id);

        insert promo;

        /**
         * Even setting the unit price at 10 almost 100% discount there should be no approva because the product is in promotion
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 60);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, null);
        System.assertEquals(quoteList[0].Approval_Type__c, null);
    }

    @IsTest
    static void testInsertDiscountPromotionByProduct_With_Threshold() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        List<Market__c> marketList = TestUtil.selectMarkets();

        Promotion_Product__c promo = TestUtil.getActivePromotionProduct1(products[0].Id, marketList[3].Id);

        promo.Discount_Threshold__c = 50;

        insert promo;

        /**
         * Setting a discount threshold of 50%. In this case an approval should be set because the discount is bigger than 50%
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 60);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Pending');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }

    @IsTest
    static void testInsertDiscountPromotionByProduct2_With_Threshold() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        List<Market__c> marketList = TestUtil.selectMarkets();

        Promotion_Product__c promo = TestUtil.getActivePromotionProduct2(products[0].Id, marketList[3].Id);

        promo.Discount_Threshold__c = 50;

        insert promo;

        /**
         * Setting a discount threshold of 50%. In this case an approval should be set because the discount is bigger than 50%
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 60);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Pending');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }



    @IsTest
    static void testInsertDiscountPromotionByFamily_With_Threshold() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        List<Market__c> marketList = TestUtil.selectMarkets();

        Promotion_Product__c promo = TestUtil.getActivePromotionFamily1('VIAFLO 96', marketList[3].Id);

        promo.Discount_Threshold__c = 50;

        insert promo;

        /**
         * Setting a discount threshold of 50%. In this case an approval should be set because the discount is bigger than 50%
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 60);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Pending');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }

    @IsTest
    static void testInsertDiscountPromotionByFamily2_With_Threshold() {
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        List<Market__c> marketList = TestUtil.selectMarkets();

        Promotion_Product__c promo = TestUtil.getActivePromotionFamily2('VIAFLO 96', marketList[3].Id);

        promo.Discount_Threshold__c = 50;

        insert promo;

        /**
         * Setting a discount threshold of 50%. In this case an approval should be set because the discount is bigger than 50%
         */
        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 60);

        insert quoteLi;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Pending');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }

    @IsTest
    static void testApprove() {
        TriggerTemplateV2.setupUnitTest();
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 1560.00);

        insert quoteLi;

        Case aCase = TestUtil.getDiscountsApproval_Approved_Case(quoteLineItems[0].quoteId);

        insert aCase;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Approved');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }

    @IsTest
    static void testReject() {
        TriggerTemplateV2.setupUnitTest();
        Test.startTest();

        List<QuoteLineItem> quoteLineItems = TestUtil.selectAllQuoteLineItems();

        List<Product2> products = TestUtil.selectProduct();

        List<PricebookEntry> standardPrices = TestUtil.selectStandardPriceBookByProductAndCurrency(products[0].Id, 'EUR');

        QuoteLineItem quoteLi = TestUtil.getQuoteLineItem(standardPrices[0].Id, quoteLineItems[0].quoteId, products[0].Id, 6, 1560.00);

        insert quoteLi;

        Case aCase = TestUtil.getDiscountsApproval_Rejected_Case(quoteLineItems[0].quoteId);

        insert aCase;

        Test.stopTest();

        List<Quote> quoteList = TestUtil.selectAllQuote();

        System.assertEquals(quoteList[0].Approval_Status__c, 'Rejected');
        System.assertEquals(quoteList[0].Approval_Type__c, 1);
    }
}

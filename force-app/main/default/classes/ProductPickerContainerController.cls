public with sharing class ProductPickerContainerController {

    @AuraEnabled(cacheable=false)
    public static String getRelatedProductIds(String productIds){
        system.debug('getRelatedProductIds :: '+productIds);

        productIds = productIds.remove('(').remove(')').remove('"');
        List<String> prodString = productIds.split(',');
        system.debug('prodString :: '+prodString);

        ResponseJSONWrapper wrapper = new ResponseJSONWrapper();
        String response = '';
        List<SObject> sObjList = new List<SObject>();
        List<String> relatedProductIds = new List<String>();
        if(productIds != null) {
            String query = 'SELECT RelatedProduct__c from RelatedProduct__c where Product__r.Id IN: prodString and Type__c = \'Services\'';

            try {
                sObjList = Database.query(query);

                for(SObject obj : sObjList){
                    System.debug('ProductPickerContainerController :: '+obj);
                    RelatedProduct__c relatedProdObj = (RelatedProduct__c)obj;
                    relatedProductIds.add(relatedProdObj.RelatedProduct__c);
                }

                if(!relatedProductIds.isEmpty())
                {
                    wrapper.status = 'Success';
                    wrapper.prodIdList = relatedProductIds;
                    system.debug('relatedProductIds :: '+relatedProductIds);
                }

            } catch (Exception e) {
                wrapper.status = 'Failure';
                wrapper.prodIdList = null;
            }
        }
        return response = JSON.serialize(wrapper);
    }

    @AuraEnabled
    public static List<LineItemWrapper> getServiceProducts(String productIds, String recordId){

        System.debug(' getServiceProducts productIds :: ' +productIds);
        System.debug(' getServiceProducts recordId :: ' +recordId);

        /*
            6001 service product found
            getFilteredProducts alertIds :: (01t57000006UZdIAAW) productContext Alerts
            no service product found
            getFilteredProducts alertIds :: (01t1X0000037ctiQAA) productContext Alerts
        */

        List<LineItemWrapper> liw = ProductPickerControllerLWC.getFilteredProducts(null, recordId, 'Alerts', productIds);
        System.debug('getServiceProducts liw :: '+liw);

        return liw;
    }

    public class ResponseJSONWrapper {
        public String status;
        public List<String> prodIdList;
    }
}
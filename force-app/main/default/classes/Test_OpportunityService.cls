/**
 * @description       :
 * @author            : PARX
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 2.0   06-07-2021   alexandre.costa@integra-biosciences.com   Changed to use the test data factory
**/
@isTest
public with sharing class Test_OpportunityService {

	@testSetup
	public static void setupData()
	{
		TriggerTemplateV2.setupUnitTest();
		System.runAs(TestUtil.createAdmin(true))
		{

			/*
			Account account = TestUtil.getAccount();
			insert account;

			List<Product2> aProducts = new List<Product2>
			{
				new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
				new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
				new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')};
			insert aProducts;

			List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
				new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
			insert markets;

			Id pricebookId = Test.getStandardPricebookId();
			List<PricebookEntry> standardPrices = new List<PricebookEntry>
			{
				new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
				new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
				new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
			};
			insert standardPrices;

			Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3), CurrencyIsoCode = 'USD');
			insert opportunity;

			List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>{
				new OpportunityLineItem(PricebookEntryId = standardPrices[0].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1, ListPriceOneItem__c = 100),
				new OpportunityLineItem(PricebookEntryId = standardPrices[1].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1,  ListPriceOneItem__c = 100),
				new OpportunityLineItem(PricebookEntryId = standardPrices[2].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1,  ListPriceOneItem__c = 100)
			};
			insert opportunityLineItems;
			*/

			TestUtil.createOpportunityBase();

            List<Opportunity> opportunityList = TestUtil.selectAllOpportunity();

			System.assertEquals(
				0, [
					SELECT Id
					FROM Quote
				].size(),
				'Wrong amount of Quotes');

			// Create 1st Quote
			NewQuoteController.createQuote(opportunityList[0].Id, 1);

			List<Quote> quotes = [
				SELECT Id, QuoteNumber, Name, DiscountDisposables__c, DiscountProducts__c, DiscountServices__c
				FROM Quote
				ORDER BY CreatedDate ASC
			];
			System.assertEquals(
				1,
				quotes.size(),
				'Quote record has not been created'
			);
			System.assertEquals(0, quotes[0].DiscountDisposables__c, 'Quote has wrong initial DiscountDisposables__c');
			System.assertEquals(0, quotes[0].DiscountProducts__c, 'Quote has wrong initial DiscountProducts__c');
			System.assertEquals(0, quotes[0].DiscountServices__c, 'Quote has wrong initial DiscountServices__c');

			opportunityList[0].SyncedQuoteId = quotes[0].Id;
			update opportunityList[0];
		}
	}

	@isTest
	public static void test_syncToQuotes() {
		Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
		Quote quote = [SELECT Id FROM Quote LIMIT 1];

		TriggerTemplateV2.startUnitTest();
		Test.startTest();

		OpportunityQuoteHelper.isSyncedFromQuote = false;

		opportunity.DiscountDisposables__c = 10;
		opportunity.DiscountPercentage__c = 20;
		opportunity.DiscountServices__c = 30;
		update opportunity;

		Test.stopTest();

		quote = [
			SELECT Id, DiscountDisposables__c, DiscountProducts__c, DiscountServices__c
			FROM Quote
			WHERE Id = :quote.Id
		];

		System.assertEquals(0, quote.DiscountDisposables__c, 'DiscountDisposables__c has not been synced');
		System.assertEquals(0, quote.DiscountProducts__c, 'DiscountProducts__c has not been synced');
		System.assertEquals(0, quote.DiscountServices__c, 'DiscountServices__c has not been synced');
	}
}
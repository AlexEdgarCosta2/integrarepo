/**
 * @description       : Selector for price entries
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public without sharing class PriceEntrySelector {
/*
    public static Map<Id, List<PriceEntry__c>> getpriceEntryMapByProducts(Set<Id> queriedProducts,
                                                                          String mainCurrency,
                                                                          String countryCode) {

        Map<Id, List<PriceEntry__c>> priceEntryMap = new Map<Id, List<PriceEntry__c>>();

        for (PriceEntry__c aPriceEntry : [SELECT Id, Product__c,
                                                 ScalingTo__c, SellingPrice__c,
                                                 ScalingTo2__c, DiscountTier2__c,
                                                 ScalingTo3__c, DiscountTier3__c,
                                                 AccountGroup__c, ValidFrom__c
                                            FROM PriceEntry__c WHERE Product__c IN : queriedProducts AND CurrencyIsoCode = :mainCurrency AND
                                                 Market__r.CountryList__c LIKE :countryCode AND ValidFrom__c <= :System.Today() ORDER BY ValidFrom__c ASC]) {

            if(priceEntryMap.get(aPriceEntry.Product__c) == null) {
                List<PriceEntry__c> priceEntryList = new List<PriceEntry__c>();
                priceEntryList.add(aPriceEntry);
                priceEntryMap.put(aPriceEntry.Product__c, priceEntryList);
            } else {
                priceEntryMap.get(aPriceEntry.Product__c).add(aPriceEntry);
            }
        }

        return priceEntryMap;
    }

    public static Map<String, List<PriceEntry__c>> getpriceEntryMapByProducts(Set<Id> queriedProducts) {

        Map<String, List<PriceEntry__c>> priceEntryMap = new Map<String, List<PriceEntry__c>>();

        for (PriceEntry__c aPriceEntry : [SELECT Id, Product__c,
                                                 ScalingTo__c, SellingPrice__c,
                                                 ScalingTo2__c, DiscountTier2__c,
                                                 ScalingTo3__c, DiscountTier3__c,
                                                 AccountGroup__c, ValidFrom__c,
                                                 CurrencyIsoCode,
                                                 Market__r.CountryList__c
                                            FROM PriceEntry__c WHERE Product__c IN : queriedProducts AND ValidFrom__c <= :System.Today() ORDER BY ValidFrom__c ASC]) {

            List<String> contries = aPriceEntry.Market__r.CountryList__c.split(';');

            for(String country : contries) {
                String key = aPriceEntry.Product__c + '|' + aPriceEntry.CurrencyIsoCode + '|' + country.trim();

                if(priceEntryMap.get(key) == null) {
                    List<PriceEntry__c> priceEntryList = new List<PriceEntry__c>();
                    priceEntryList.add(aPriceEntry);
                    priceEntryMap.put(key, priceEntryList);
                } else {
                    priceEntryMap.get(key).add(aPriceEntry);
                }
            }
        }

        return priceEntryMap;
    }
    */

    /**
    * @description Get for a Set of ids
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param queriedProducts
    * @return Map<String, PriceEntry__c>
    **/
    public static Map<String, PriceEntry__c> getpriceEntryMapByProducts2(Set<Id> queriedProducts) {

        Map<String, PriceEntry__c> priceEntryMap = new Map<String, PriceEntry__c>();

        for (PriceEntry__c aPriceEntry : [SELECT Id, Product__c,
                                                 ScalingTo__c, SellingPrice__c,
                                                 ScalingTo2__c, DiscountTier2__c,
                                                 ScalingTo3__c, DiscountTier3__c,
                                                 AccountGroup__c, ValidFrom__c,
                                                 CurrencyIsoCode,
                                                 Market__r.CountryList__c
                                            FROM PriceEntry__c WHERE Product__c IN : queriedProducts AND ValidFrom__c <= :System.Today() ORDER BY ValidFrom__c ASC]) {

            List<String> contries = aPriceEntry.Market__r.CountryList__c.split(';');

            for(String country : contries) {
                String key = '|' + aPriceEntry.Product__c + '|' + aPriceEntry.CurrencyIsoCode + '|' + country.trim();
                if(aPriceEntry.AccountGroup__c != null) {
                    key = ((String )(aPriceEntry.AccountGroup__c)).left(15) + '|' + aPriceEntry.Product__c + '|' + aPriceEntry.CurrencyIsoCode + '|' + country.trim();
                }

                priceEntryMap.put(key, aPriceEntry);
/*
                if(priceEntryMap.get(key) == null) {
                    List<PriceEntry__c> priceEntryList = new List<PriceEntry__c>();
                    priceEntryList.add(aPriceEntry);
                    priceEntryMap.put(key, priceEntryList);
                } else {
                    priceEntryMap.get(key) = aPriceEntry;
                }
                */
            }
        }

        return priceEntryMap;
    }

    /**
    * @description Get for a list of products
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param queriedProducts
    * @return Map<String, PriceEntry__c>
    **/
    public static Map<String, PriceEntry__c> getpriceEntryMapByProducts2(List<Product2> queriedProducts) {

        Map<String, PriceEntry__c> priceEntryMap = new Map<String, PriceEntry__c>();

        for (PriceEntry__c aPriceEntry : [SELECT Id, Product__c,
                                                 ScalingTo__c, SellingPrice__c,
                                                 ScalingTo2__c, DiscountTier2__c,
                                                 ScalingTo3__c, DiscountTier3__c,
                                                 AccountGroup__c, ValidFrom__c,
                                                 CurrencyIsoCode,
                                                 Market__r.CountryList__c
                                            FROM PriceEntry__c WHERE Product__c IN : queriedProducts AND ValidFrom__c <= :System.Today() ORDER BY ValidFrom__c ASC]) {

            List<String> contries = aPriceEntry.Market__r.CountryList__c.split(';');

            for(String country : contries) {
                String key = '|' + aPriceEntry.Product__c + '|' + aPriceEntry.CurrencyIsoCode + '|' + country.trim();
                if(aPriceEntry.AccountGroup__c != null) {
                    key = ((String )(aPriceEntry.AccountGroup__c)).left(15) + '|' + aPriceEntry.Product__c + '|' + aPriceEntry.CurrencyIsoCode + '|' + country.trim();
                }

                priceEntryMap.put(key, aPriceEntry);
            }
        }

        return priceEntryMap;
    }
}
/**
 * @description  : Trigger Handler for the Quote Object
 * @author       : PARX
**/
public with sharing class QuoteTriggerHandlerBasic extends TriggerTemplateV2.TriggerHandlerAdapter
{
	public override void onBeforeInsert (List<sObject> newList)
	{
		QuoteService.setQuoteName((List<Quote>)newList);
		QuoteService.setPriceBookId((List<Quote>)newList);
	}

	public override void onBeforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap)
	{
		QuoteService.setQuoteName((List<Quote>)newList);
	}

	public override void onAfterInsert(List<sObject> newList, Map<Id, sObject> newMap)
	{}

	public override void onAfterUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap)
	{
		QuoteService.syncToOpportunities((List<Quote>)newList);
		OpportunityQuoteHelper.updateLineItemsWithOverallDiscount(newList, oldMap);
		QuoteService.syncLineItemsToOpportunities((List<Quote>)newList);
	}


	public override void onBeforeDelete (List<sObject> oldList, Map<Id, sObject> oldMap)
	{
		QuoteService.checkQuoteOnSync((List<Quote>)oldMap.values());
	}
}
/*
*   @description Produkt picker functionality.
*
*   @author ach, yle,Kamal Patil (Persistent System ltd.), ikl
*   @copyright PARX
*/
public abstract class ProductPickerController
{
    public static final List<String> QUERY_FIELDS = new List<String>{'ProductCode', 'Name', 'Family', 'Description',
        'ProductFamily__c','ProductType__c','ProductFamilyNumber__c', 'PricingGroup__c',
        'ProductLanguage__c', 'ProductNameDE__c', 'ProductNameFR__c'};

    public Opportunity opportunity{get;set;}

    /* Get the Standard Price Book */
    public static ID priceBookId = Constants.standardPriceBookId;
/*    {
        get {
            if (priceBookId == null ) {
                priceBookId = [SELECT Id FROM PriceBook2 WHERE IsStandard = true AND IsActive = true].Id;
            }
            return priceBookId;
        }
        set;
    }*/
    //############## FILTERS #############
    public String filterProductCode {get; set;}

    public String filterProductFamily {get; set;}

    public String filterProductName {get; set;}

    public String filterProductType {get; set;}

    public String filterLanguage {get; set;}

    // param that contains the product id that should be deleted from the selected list/ or used in ovrriding tier price functionality.
    public String productIdParam {get;set;}

    // queried items from DB
    public List<LineItemWrapper> lineItemWrappers {get;set;}

    // selected products by user
    public List<LineItemWrapper> selectedLineItemWrappers {get;set;}

    //collects list price
    public Map<String, Double> productToListPrice;

    /**
    *   @description default constructor.
    */
    public ProductPickerController()
    {
        initMainObject();
        lineItemWrappers = new List<LineItemWrapper>();
        selectedLineItemWrappers = new List<LineItemWrapper>();
        productToListPrice = new Map<String, Double>();
    }

//******************************** Methods For Main Object (Opportunity or Case) ******************************//
    public abstract void initMainObject();
    public abstract Id getMainId();
    public abstract String getMainCurrency();
    public abstract String getMainBillingCountryCode();
    public abstract Id getMainAccountGroup();
    public abstract PageReference doAddProducts();
    public abstract PageReference cloneOpportunityWithLineItem();
    public abstract PageReference reCalculateProductPrice();
    public abstract String getLanguageForProducts();
    protected abstract void applyAdditionalConditions(AndCondition andCondition);
    public abstract Decimal getOverallDiscount();
    public abstract Decimal getDisposablesDiscount();
    public abstract Decimal getServicesDiscount();
 //******************************** End of Methods For Main Object (Opportunity or Case) ******************************//

    /**
    *   Generate list of available product language options.
    */
    public List<SelectOption> getAvailableLanguages()
    {
        List<SelectOption> options = new List<SelectOption>{new SelectOption('', '--None--')};
        Schema.DescribeFieldResult fieldResult = Product2.ProductLanguage__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }

    /**
    *   @description redirect to the previous opportunity page.
    */
    public PageReference redirectToPreviousPage()
    {
        PageReference pr = new PageReference('/' + getMainId());
        pr.setRedirect(true);
        System.debug(pr);
        return pr;
    }
    /**
    *   @description filter the products.
    */
    public void doFiterProducts()
    {
        lineItemWrappers.clear();
        SoqlBuilder query = new SoqlBuilder();
        List<String> fieldsToQuery = New List<String>(QUERY_FIELDS);
        fieldsToQuery.add('( SELECT Id FROM PricebookEntries WHERE CurrencyIsoCode = \'' + getMainCurrency() +'\')');
        query.selectx(fieldsToQuery);
        query.fromx('Product2');

        AndCondition andCond = new AndCondition();
        andCond.add(new FieldCondition('IsActive', true));
        applyAdditionalConditions(andCond);
        if (String.isNotBlank(filterProductName))
        {
            String fieldForProductName = 'Name';
            if (getLanguageForProducts() == 'French')
            {
                fieldForProductName = 'ProductNameFR__c';
            } else if (getLanguageForProducts() == 'German')
            {
                fieldForProductName = 'ProductNameDE__c';
            }
            andCond.add(new FieldCondition(fieldForProductName).likex('%' + filterProductName+ '%'));
        }
        if (String.isNotBlank(filterProductCode))
        {
            andCond.add(new FieldCondition('ProductCode').likex('%' + filterProductCode + '%'));
        }

        if (String.isNotBlank(filterProductFamily))
        {
            andCond.add(new FieldCondition('ProductFamily__c').likex('%' + filterProductFamily+ '%'));
        }
        if (String.isNotBlank(filterProductType))
        {
            andCond.add(new FieldCondition('ProductType__c').likex('%' + filterProductType+ '%'));
        }
        if (String.isNotBlank(filterLanguage))
        {
            andCond.add(new FieldCondition('ProductLanguage__c').likex(filterLanguage));
        }
        query.wherex(andCond).orderByx(new OrderBy('name'))/*.limitx(500)*/;
        system.debug('Query: '+query.toSoql());
        List<Product2> queriedProducts = Database.query(query.toSoql());


        //Map<Id, List<PriceEntry__c>> productWithStandardPrices = new Map<Id, List<PriceEntry__c>>();
        //Map<Id, List<PriceEntry__c>> productWithSpecialPrices = new Map<Id, List<PriceEntry__c>>();
        String countryCode = '%' + getMainBillingCountryCode() + '%';
        System.debug('+++++++++++++++++++++++++++++++++++++++++++++++++++++');
        System.debug('countryCode = ' + countryCode);
        System.debug('getMainCurrency = ' + getMainCurrency());
        System.debug('products = ' + queriedProducts);



        Map<String, PriceEntry__c> priceEntryMap = PriceEntrySelector.getpriceEntryMapByProducts2(queriedProducts);

/*

        for (PriceEntry__c aPriceEntry : [SELECT Id, Product__c, ScalingTo__c, SellingPrice__c, ScalingTo2__c, DiscountTier2__c, ScalingTo3__c, DiscountTier3__c, AccountGroup__c, ValidFrom__c
                                          FROM PriceEntry__c WHERE Product__c IN : queriedProducts AND CurrencyIsoCode = :getMainCurrency() AND
                                          Market__r.CountryList__c LIKE :countryCode AND ValidFrom__c <= :System.Today() ORDER BY ValidFrom__c ASC])
        {
            System.debug('aPriceEntry = ' + aPriceEntry);
            //system.debug(Logginglevel.DEBUG,'.... query aPriceEntry:' + aPriceEntry);
            if (aPriceEntry.AccountGroup__c == null) // standard price
            {
                addNewPrice(productWithStandardPrices, aPriceEntry);

                // get list price and put it into the product and list price map
                if (!this.productToListPrice.containsKey(aPriceEntry.Product__c))
                {
                    this.productToListPrice.put(aPriceEntry.Product__c, aPriceEntry.SellingPrice__c);
                }
            }
            else if (aPriceEntry.AccountGroup__c == getMainAccountGroup() &&
                     (getOverallDiscount() == null || getDisposablesDiscount() == null|| getServicesDiscount()== null)) // special price
            {
                addNewPrice(productWithSpecialPrices, aPriceEntry);
            }
        }
*/


        // all lineitems should have the PricebookEntry. We did't use it (we use PriceEntry__c instead), so we need to create the fake
        // PricebookEntry.
        List<PricebookEntry> pListToInsert = new List<PricebookEntry>();
        system.debug('Size: '+queriedProducts.size());
        for (Product2 p : queriedProducts)
        {
            LineItemWrapper tempWrapper = null;

            List<PriceEntry__c> currentPriceEntry = PricingService.getCurrentPriceEntryList(priceEntryMap,
                p.Id,
                getMainAccountGroup(),
                getMainCurrency(),
                getMainBillingCountryCode(),
                getOverallDiscount(),
                getDisposablesDiscount(),
                getServicesDiscount());

            List<PriceEntry__c> standardPriceEntry;
            if(getMainAccountGroup() != null) {
                standardPriceEntry = PricingService.getCurrentPriceEntryList(priceEntryMap,
                    p.Id,
                    null,
                    getMainCurrency(),
                    getMainBillingCountryCode(),
                    getOverallDiscount(),
                    getDisposablesDiscount(),
                    getServicesDiscount());
            } else {
                standardPriceEntry = currentPriceEntry;
            }

            if(currentPriceEntry != null) {
                tempWrapper = new LineItemWrapper(p, getProductNameForLanguage(p, getLanguageForProducts()),
                    currentPriceEntry,
                    standardPriceEntry,
                    getOverallDiscount(),
                    getDisposablesDiscount(),
                    getServicesDiscount(),
                    p.ProductType__c);
            }

            /*

            if (productWithSpecialPrices.containsKey(p.Id) && productWithSpecialPrices.get(p.Id).size() > 0)
            {
                // use special prices
                tempWrapper = new LineItemWrapper(p, getProductNameForLanguage(p, getLanguageForProducts()), productWithSpecialPrices.get(p.Id), productWithStandardPrices.get(p.Id), getOverallDiscount(),getDisposablesDiscount(),getServicesDiscount(),p.ProductType__c);
            }
            else if (productWithStandardPrices.containsKey(p.Id) && productWithStandardPrices.get(p.Id).size() > 0)
            {
                // use standard prices
                tempWrapper = new LineItemWrapper(p, getProductNameForLanguage(p, getLanguageForProducts()), productWithStandardPrices.get(p.Id), productWithStandardPrices.get(p.Id), getOverallDiscount(),getDisposablesDiscount(),getServicesDiscount(),p.ProductType__c);
            }

            */

            if (tempWrapper != null) //we have at least one price.
            {
                lineItemWrappers.add(tempWrapper);
                tempWrapper.productToListPrice = this.ProductToListPrice;

                // if a product does not have a price book entry create one
                if (p.PricebookEntries.size() == 0)
                {
                    pListToInsert.add(new PricebookEntry(UnitPrice= 0, Product2Id = p.id, IsActive = true,
                            Pricebook2Id = priceBookId, UseStandardPrice = false, CurrencyIsoCode = getMainCurrency()));
                }
            } else
            {
                // no prices (all are in wrong currency)
            }
        }
        limitTheListOfWrappers(lineItemWrappers);
//        insert pListToInsert;
    }

    public String getProductNameForLanguage(Product2 product, String language)
    {
        String result = product.Name;
        if (language == 'French')
        {
            result = product.ProductNameFR__c;
        } else if (language == 'German')
        {
            result = product.ProductNameDE__c;
        }
        return result;
    }

    public void limitTheListOfWrappers(List<LineItemWrapper> items)
    {
        if (items.size() > 500)
        {
            for (Integer i = items.size()-1; i > 500-1; i --)
            {
                items.remove(i);
            }
        }
    }

    /**
    *   @description add new price to the produkt map.
        public void addNewPrice(Map<Id, List<PriceEntry__c>> theProductWithPrices, PriceEntry__c thePriceEntry)
    */
    public List<PriceEntry__c> addNewPrice(Map<Id, List<PriceEntry__c>> theProductWithPrices, PriceEntry__c thePriceEntry)
    {
        if (!theProductWithPrices.containsKey(thePriceEntry.Product__c))
        {
            theProductWithPrices.put(thePriceEntry.Product__c, new List<PriceEntry__c>());
        }
        List<PriceEntry__c> prices = theProductWithPrices.get(thePriceEntry.Product__c);
        if (prices.size() > 0 && prices[prices.size()-1].ScalingTo__c == thePriceEntry.ScalingTo__c)
        {
            // same scale, we should check valid from. The latest should win.
            if (prices[prices.size()-1].ValidFrom__c < thePriceEntry.ValidFrom__c)
            {
                //our price win, replace the existing price with our new one.
                prices.remove(prices.size()-1);
                prices.add(thePriceEntry);
            } else
            {
                // our price lost at the same price, do nothing
            }
        } else
        {
            // add price anyway
            prices.add(thePriceEntry);
        }
        return prices;
    }

    public Boolean isBottomSectionValid()
    {
        system.debug('in isBottomSectionValid()');
        Boolean result = true;
        for(LineItemWrapper wrapper : lineItemWrappers)
        {
            wrapper.qty = 0;
            if (wrapper.quantity != '0')
            {
                if (wrapper.quantity.length() > 0)
                {
                    String toParse = wrapper.quantity.replace(',', '.');
                    //parse Decimal
                    try
                    {
                        wrapper.qty = Decimal.valueOf(toParse);
                    } catch(Exception e)
                    {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Value \'' + wrapper.quantity + '\' cannot be converted from Text to Number'));
                        result = false;
                    }
                } else
                {
                    wrapper.quantity = '0';
                }
            }
        }
        system.debug('in isBottomSectionValid() '+result);
        return result;
    }

    /**
    *   @description add filtered products with quantity > 0 to the selected products section.
    */
    public void doAddFilteredProducts()
    {
        //
        system.debug(Logginglevel.DEBUG,'.... doAddFilteredProducts');
        //selectedLineItemWrappers.clear();
        if (isBottomSectionValid())
        {
            List<LineItemWrapper> aNewItems = new List<LineItemWrapper>();
            for (LineItemWrapper wrapper : lineItemWrappers)
            {
                boolean isProductAlreadyExist = false;
                if (wrapper.qty != null && wrapper.qty > 0)
                {
                    // in case if a product is already selected, add quantity and reset quantiy.
                    for (LineItemWrapper selectedWrapper : selectedLineItemWrappers)
                    {
                        if (selectedWrapper.product.Id == wrapper.product.Id)
                        {
                            // products are the same
                            selectedWrapper.qty += wrapper.qty;
                            addExistingProduct(selectedWrapper);
                            isProductAlreadyExist = true;
                        }
                    }
                    if (!isProductAlreadyExist)
                    {
                        aNewItems.add(wrapper.getCopy());
                    }
                    wrapper.qty = 0;
                    wrapper.quantity = '0';
                }
            }
            selectedLineItemWrappers.addAll(aNewItems);
            //applyTiers();

            PricingService.applyTiers(selectedLineItemWrappers,
                getOverallDiscount(),
                getDisposablesDiscount(),
                getServicesDiscount());


        }
    }

    protected virtual void addExistingProduct(LineItemWrapper wrapper)
    {}

    /**
    *   @description Apply tiers to the selected products.
    */
    /*
    public void applyTiers()
    {
        system.debug('Inside applyTiers');
        // sum quantity for PricingGroup__c
        Map<String, Decimal> aPricingGroupQuantityMap= new Map<String, Decimal>(); // Pricing group code => Quantity relation
        for (LineItemWrapper wrapper : selectedLineItemWrappers)
        {
	        System.debug('wrapper = ' + wrapper);
	        System.debug('wrapper.product.PricingGroup__c = ' + wrapper.product.PricingGroup__c);
            if (wrapper.product.PricingGroup__c != null)
            {
                // create a map that accumulates quantity to a specific pricing group.
                aPricingGroupQuantityMap.put(wrapper.product.PricingGroup__c, aPricingGroupQuantityMap.containsKey(wrapper.product.PricingGroup__c) ?
                        aPricingGroupQuantityMap.get(wrapper.product.PricingGroup__c) + wrapper.qty :
                        wrapper.qty);
            }
        }

        for (LineItemWrapper aSelectedwrapper : selectedLineItemWrappers)
        {
	        System.debug('--- process line item ---');
            system.debug(Logginglevel.DEBUG,'applyTiers:.... aSelectedwrapper:' + aSelectedwrapper);
            Decimal previousAmount = 0;
            if (aSelectedwrapper.pps != null)
            {
                Decimal aQuantity = aSelectedwrapper.qty;
                String productType = aSelectedwrapper.productType;

                // init the quantity to the related pricing group
                if (NumberUtils.isDecimalNull(getOverallDiscount()) && aSelectedwrapper.product.PricingGroup__c != null && aPricingGroupQuantityMap.containsKey((aSelectedwrapper.product.PricingGroup__c)))
                {
                    aQuantity = aPricingGroupQuantityMap.get(aSelectedwrapper.product.PricingGroup__c);
                }
                for (PriceEntry__c aPriceEntry : aSelectedwrapper.pps)
                {
                    // this part is to get the appropriate price according to the given quantity
                    // e.g. if quantity is 2 and there are two prices one which is for quantity 1 and one for quantity smaller then 4 then take the price for 4
                    Decimal sellingPrice = aPriceEntry.SellingPrice__c;
	                if(getOverallDiscount() != null  && productType == 'Products and Accessories' && getOverallDiscount() != 0.00)
                    {
                        //apply overall discount
                        sellingPrice = (aPriceEntry.SellingPrice__c * (1 - 0.01 * getOverallDiscount())).setScale(2);
                        System.debug(loggingLevel.WARN, 'getOverallDiscount....sellingPrice: ' + sellingPrice);
                    }
                    else if(getDisposablesDiscount() != null  && productType == 'Disposables' && getDisposablesDiscount() != 0.00)
                    {
	                    //apply Disposables discount
                        sellingPrice = (aPriceEntry.SellingPrice__c * (1 - 0.01 * getDisposablesDiscount())).setScale(2);
                        System.debug(loggingLevel.WARN, 'getDisposablesDiscount....sellingPrice: ' + sellingPrice);
                    }
                    else if(getServicesDiscount() != null  && productType == 'Services' && getServicesDiscount() != 0.00)
                    {
	                    //apply Services discount
                        sellingPrice = (aPriceEntry.SellingPrice__c * (1 - 0.01 * getServicesDiscount())).setScale(2);
                        System.debug(loggingLevel.WARN, 'sellingPrice....sellingPrice: ' + sellingPrice);
                    }
                    else
                    {
	                    if (aQuantity > aPriceEntry.ScalingTo__c)
                        {
                            //check ScallingTo2
                            if (aPriceEntry.ScalingTo2__c != null && aPriceEntry.DiscountTier2__c != null)
                            {
                                sellingPrice = (aPriceEntry.SellingPrice__c * (1 - 0.01 * aPriceEntry.DiscountTier2__c)).setScale(2);
                                if (aQuantity > aPriceEntry.ScalingTo2__c)
                                {
                                    //check ScallingTo3
                                    if (aPriceEntry.ScalingTo3__c != null && aQuantity > aPriceEntry.ScalingTo2__c && aPriceEntry.DiscountTier3__c != null)
                                    {
                                        sellingPrice = (aPriceEntry.SellingPrice__c * (1 - 0.01 * aPriceEntry.DiscountTier3__c)).setScale(2);
                                    }
                                }
                            }
                        }
                    }
	                System.debug('sellingPrice = ' + sellingPrice);
                    applyTiersForWrapper(aSelectedwrapper, sellingPrice);
                    aSelectedwrapper.listPriceTierPricing = sellingPrice;
                    if (aPriceEntry.AccountGroup__c != null && this.productToListPrice.containsKey(aSelectedwrapper.product.id) && aPriceEntry.AccountGroup__c == getMainAccountGroup())
                    {
                        aSelectedwrapper.item.ListPriceOneItem__c = this.productToListPrice.get(aSelectedwrapper.product.id);
                    }
                }
            }
        }
    }
*/
/*
    protected virtual void applyTiersForWrapper(LineItemWrapper wrapper, Decimal sellingPrice)
    {
        // in case of deleting a product from the list a unit price and list price can differ. In this case don't assign the selling price to the unit price
        if (wrapper.UnitPrice != wrapper.listPriceTierPricing)
        {
        }
        else
        {
            //system.debug(Logginglevel.DEBUG, '.... aPriceEntry: good');
            wrapper.UnitPrice = sellingPrice;
        }
    }

    protected virtual void applyTiersForWrapper(LineItemWrapper wrapper, PriceEntry__c aPriceEntry)
    {
        // in case of deleting a product from the list a unit price and list price can differ. In this case don't assign the selling price to the unit price
        if (wrapper.UnitPrice != wrapper.listPriceTierPricing)
        {
        }
        else
        {
            //system.debug(Logginglevel.DEBUG, '.... aPriceEntry: good');
            wrapper.UnitPrice = aPriceEntry.SellingPrice__c;
        }
    }
*/
    /**
    *   @description Overide tiers price when user clicks on icon.
    */
    public void doOverrideTiers()
    {
        for (Integer i = 0; i < selectedLineItemWrappers.size(); i++)
        {
            if (productIdParam == selectedLineItemWrappers.get(i).product.Id)
            {
                selectedLineItemWrappers.get(i).UnitPrice = selectedLineItemWrappers.get(i).item.ListPriceOneItem__c;
                break;
            }
        }
    }

    /**
    *   @description delete products from the selected items section.
    */
    public void doDeleteItem()
    {
        for (Integer i = 0; i < selectedLineItemWrappers.size(); i++)
        {
            if (productIdParam == selectedLineItemWrappers.get(i).product.Id)
            {
                selectedLineItemWrappers.remove(i);
                break;
            }
        }
        //applyTiers();
        PricingService.applyTiers(selectedLineItemWrappers,
            getOverallDiscount(),
            getDisposablesDiscount(),
            getServicesDiscount());
    }

    /**
    *   @description clear the product filter.
    */
    public void doClearFilter()
    {
        filterProductCode = '';
        filterProductName = '';
        filterProductFamily = '';
    }

    /**
    *   @description clear selected products.
    */
    public void doClearProducts()
    {
        selectedLineItemWrappers.clear();
    }
}
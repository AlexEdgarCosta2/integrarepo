/**
 * =============================================================
 * AUTHOR         : Alex Costa
 * CREATE DATE    : 08.04.2021
 * PURPOSE        : Class to Query Opportunity Line Item Records
 * SPECIAL NOTES  :
 * =============================================================
 * Change History :
 * Date        Author               Description
 * ==========  ==================   ============================
 * 08.04.2021  Alex Costa           Created
 * ============================================================= 
 */
public with sharing class OpportunityLineItemSelector {
    public static List<OpportunityLineItem> getOpportunityLineItemsByIds(Set<Id> oppLineItemIds) {
        return [SELECT Id, OpportunityId, Product2Id, Product2.Name, Product2.ProductType__c, Quantity, UnitPrice FROM OpportunityLineItem 
                 WHERE Id in : oppLineItemIds];
    }
}
/**
 * @description       :
 * @author            : ????
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-29-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
@isTest
public with sharing class Test_QuoteLineItemService {

    @TestSetup
    static void makeData(){
        TestUtil.createOpportunityBase();
    }

    @isTest
    public static void test_syncToOpportunityLineItems() {
        TriggerTemplateV2.setupUnitTest();
/*
        Account account = new Account(Name = 'asd account', BillingCountryCode = 'US',BillingState='California');
        insert account;

        List<Product2> aProducts = new List<Product2>{
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')
        };
        insert aProducts;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>{
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert standardPrices;

        PriceBook2 customPriceBook = new PriceBook2();
        customPriceBook.Name = 'Custom Pricebook';
        customPriceBook.IsActive = true;
        insert customPriceBook;

        List<PricebookEntry> customPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert customPrices;

        Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3), CurrencyIsoCode = 'USD', Pricebook2Id = customPriceBook.Id);
        insert opportunity;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(PricebookEntryId = customPrices[0].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[1].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[2].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1)
        };
        insert opportunityLineItems;
*/
        Opportunity opportunity = TestUtil.selectAllOpportunity()[0];

        System.assertEquals(
            0, [
                SELECT Id
                FROM Quote
            ].size(),
            'Wrong amount of Quotes');

        // Create 1st Quote
        NewQuoteController.createQuote(opportunity.Id, 1);

        List<Quote> quotes = [
            SELECT Id, QuoteNumber, Name
            FROM Quote
            ORDER BY CreatedDate ASC
        ];
        System.assertEquals(
            1,
            quotes.size(),
            'Quote record has not been created'
        );
        System.assertEquals(
            3,
            [
                SELECT Id, UnitPrice, Quantity, SortOrder__c
                FROM QuoteLineItem
                ORDER BY CreatedDate ASC
            ].size(),
            'Quote Line Items have not been created'
        );

        opportunity.SyncedQuoteId = quotes[0].Id;
        update opportunity;

        List<QuoteLineItem> quoteLineItems = [
            SELECT Id, UnitPrice, Quantity, SortOrder__c
            FROM QuoteLineItem
            ORDER BY CreatedDate ASC
        ];
        quoteLineItems[0].UnitPrice = 10700;
        quoteLineItems[0].Quantity = 1;
        quoteLineItems[0].SortOrder__c = 1;
        quoteLineItems[1].UnitPrice = 80;
        quoteLineItems[1].Quantity = 2;
        quoteLineItems[1].SortOrder__c = 2;
        quoteLineItems[2].UnitPrice = 90;
        quoteLineItems[2].Quantity = 3;
        quoteLineItems[2].SortOrder__c = 3;

        update quoteLineItems;

        List<OpportunityLineItem> opportunityLineItems = [
            SELECT Id, UnitPrice, Quantity, SortOrder__c
            FROM OpportunityLineItem
        ];
        System.assertEquals(1, opportunityLineItems[0].Quantity, 'Opportunity Line Item has not been synced');
        System.assertEquals(10700, opportunityLineItems[0].UnitPrice, 'Opportunity Line Item has not been synced');
        System.assertEquals(1, opportunityLineItems[0].SortOrder__c, 'Opportunity Line Item has not been synced');
        System.assertEquals(2, opportunityLineItems[1].Quantity, 'Opportunity Line Item has not been synced');
        System.assertEquals(80, opportunityLineItems[1].UnitPrice, 'Opportunity Line Item has not been synced');
        System.assertEquals(2, opportunityLineItems[1].SortOrder__c, 'Opportunity Line Item has not been synced');
        System.assertEquals(3, opportunityLineItems[2].Quantity, 'Opportunity Line Item has not been synced');
        System.assertEquals(90, opportunityLineItems[2].UnitPrice, 'Opportunity Line Item has not been synced');
        System.assertEquals(3, opportunityLineItems[2].SortOrder__c, 'Opportunity Line Item has not been synced');
    }

    @isTest
    public static void test_getDataFromSyncedOpportunityLineItems() {
        TriggerTemplateV2.setupUnitTest();
/*
        Account account = TestUtil.getAccount();
        insert account;

        List<Product2> aProducts = new List<Product2>{
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')
        };
        insert aProducts;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>{
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert standardPrices;

        PriceBook2 customPriceBook = new PriceBook2();
        customPriceBook.Name = 'Custom Pricebook';
        customPriceBook.IsActive = true;
        insert customPriceBook;

        List<PricebookEntry> customPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert customPrices;

        Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3), CurrencyIsoCode = 'USD', Pricebook2Id = customPriceBook.Id);
        insert opportunity;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(PricebookEntryId = customPrices[0].Id, OpportunityId = opportunity.Id, Quantity = 1, UnitPrice = 10, SortOrder__c = 1, ListPriceOneItem__c = 11, ListPriceTierPricing__c = 12, DemoEquipment__c = true),
            new OpportunityLineItem(PricebookEntryId = customPrices[1].Id, OpportunityId = opportunity.Id, Quantity = 2, UnitPrice = 20, SortOrder__c = 2, ListPriceOneItem__c = 13, ListPriceTierPricing__c = 14, DemoEquipment__c = false),
            new OpportunityLineItem(PricebookEntryId = customPrices[2].Id, OpportunityId = opportunity.Id, Quantity = 3, UnitPrice = 30, SortOrder__c = 3, ListPriceOneItem__c = 15, ListPriceTierPricing__c = 16, DemoEquipment__c = true)
        };
        insert opportunityLineItems;
*/

        List<Product2> prodList = TestUtil.selectProduct();

        List<PricebookEntry> priceBookEntryList = TestUtil.selectStandardPriceBookByProductAndCurrency(prodList[0].Id, 'EUR');

        Opportunity opportunity = TestUtil.selectAllOpportunity()[0];

        System.assertEquals(
            0, [
                SELECT Id
                FROM Quote
            ].size(),
            'Wrong amount of Quotes');

        // Create 1st Quote
        NewQuoteController.createQuote(opportunity.Id, 1);

        List<Quote> quotes = [
            SELECT Id, QuoteNumber, Name
            FROM Quote
            ORDER BY CreatedDate ASC
        ];
        System.assertEquals(
            1,
            quotes.size(),
            'Quote record has not been created'
        );
        List<QuoteLineItem> quoteLineItems = [
            SELECT Id, OpportunityLineItemId, UnitPrice, Quantity, SortOrder__c, ListPriceOneItem__c, ListPriceTierPricing__c, DemoEquipment__c
            FROM QuoteLineItem
            ORDER BY CreatedDate ASC
        ];
        System.debug('quoteLineItems = ' + quoteLineItems);
        System.assertEquals(
            3,
            quotelineItems.size(),
            'Quote Line Items have not been created'
        );

        opportunity.SyncedQuoteId = quotes[0].Id;
        update opportunity;

        System.debug([
            SELECT Id, OpportunityLineItemId, UnitPrice, Quantity, SortOrder__c, ListPriceOneItem__c, ListPriceTierPricing__c, DemoEquipment__c
            FROM QuoteLineItem
            ORDER BY CreatedDate ASC
        ]);
        OpportunityLineItem newOpportunityLineItem = new OpportunityLineItem();
        newOpportunityLineItem.PricebookEntryId = priceBookEntryList[0].Id;
        newOpportunityLineItem.OpportunityId = opportunity.Id;
        newOpportunityLineItem.Quantity = 5;
        newOpportunityLineItem.UnitPrice = 70;
        newOpportunityLineItem.SortOrder__c = 4;
        newOpportunityLineItem.ListPriceOneItem__c = 21;
        newOpportunityLineItem.ListPriceTierPricing__c = 31;
        newOpportunityLineItem.DemoEquipment__c = true;
        insert newOpportunityLineItem;

        QuoteLineItem newQuoteLineItem = [
            SELECT Id, UnitPrice, Quantity, SortOrder__c, ListPriceOneItem__c, ListPriceTierPricing__c, DemoEquipment__c
            FROM QuoteLineItem
            WHERE OpportunityLineItemId = :newOpportunityLineItem.Id
        ];

        System.assertEquals(5, newQuoteLineItem.Quantity, 'Quantity has not been synced');
        System.assertEquals(70, newQuoteLineItem.UnitPrice, 'UnitPrice has not been synced');
        System.assertEquals(4, newQuoteLineItem.SortOrder__c, 'SortOrder__c has not been synced');
        System.assertEquals(21, newQuoteLineItem.ListPriceOneItem__c, 'ListPriceOneItem__c has not been synced');
        System.assertEquals(31, newQuoteLineItem.ListPriceTierPricing__c, 'ListPriceTierPricing__c has not been synced');
        System.assertEquals(true, newQuoteLineItem.DemoEquipment__c, 'DemoEquipment__c has not been synced');
    }

    @isTest
    static void createQuoteLIHistories()
    {
        TriggerTemplateV2.setupUnitTest();
/*
        Account account = new Account(Name = 'asd account', BillingCountryCode = 'US',BillingState='California');
        insert account;

        List<Product2> aProducts = new List<Product2>{
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')
        };
        insert aProducts;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>{
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert standardPrices;

        PriceBook2 customPriceBook = new PriceBook2();
        customPriceBook.Name = 'Custom Pricebook';
        customPriceBook.IsActive = true;
        insert customPriceBook;

        List<PricebookEntry> customPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert customPrices;

        Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3), CurrencyIsoCode = 'USD', Pricebook2Id = customPriceBook.Id);
        insert opportunity;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(PricebookEntryId = customPrices[0].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[1].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[2].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1)
        };
        insert opportunityLineItems;
*/

        Opportunity opportunity = TestUtil.selectAllOpportunity()[0];

        System.assertEquals(
            0, [
                SELECT Id
                FROM Quote
            ].size(),
            'Wrong amount of Quotes');

        // Create 1st Quote
        NewQuoteController.createQuote(opportunity.Id, 1);

        List<QuoteLineItem> quoteLineItems = [
            SELECT Id, UnitPrice, Quantity, SortOrder__c
            FROM QuoteLineItem
            ORDER BY CreatedDate ASC
        ];
        quoteLineItems[0].UnitPrice = 70;
        quoteLineItems[0].Quantity = 1;
        quoteLineItems[0].SortOrder__c = 1;
        quoteLineItems[1].UnitPrice = 80;
        quoteLineItems[1].Quantity = 2;
        quoteLineItems[1].SortOrder__c = 2;
        quoteLineItems[2].UnitPrice = 90;
        quoteLineItems[2].Quantity = 3;
        quoteLineItems[2].SortOrder__c = 3;

        update quoteLineItems;

        delete quoteLineItems[0];

        List<QuoteLineItemHistory__c> histories = [SELECT Id FROM QuoteLineItemHistory__c];
        System.assert(histories.size() > 3);
    }
}
/**
 * @description       :
 * @author            : alexandre.costa@integra-biosciences.com
 * @group             :
 * @last modified on  : 05-05-2021
 * @last modified by  : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-30-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
@isTest
public with sharing class Test_OpportunityLineItemService {

    @TestSetup
    static void makeData() {
        TestUtil.createOpportunityBase();
    }

    @isTest
    public static void test_syncToQuoteLineItems() {
        TriggerTemplateV2.setupUnitTest();
/*
        Account account = TestUtil.getAccount();
        insert account;

        List<Product2> aProducts = new List<Product2>{
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')
        };
        insert aProducts;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>{
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert standardPrices;

        PriceBook2 customPriceBook = new PriceBook2();
        customPriceBook.Name = 'Custom Pricebook';
        customPriceBook.IsActive = true;
        insert customPriceBook;

        List<PricebookEntry> customPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert customPrices;

        Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3), CurrencyIsoCode = 'USD', Pricebook2Id = customPriceBook.Id);
        insert opportunity;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(PricebookEntryId = customPrices[0].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[1].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[2].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1)
        };
        insert opportunityLineItems;
*/

        Opportunity opportunity = TestUtil.selectAllOpportunity()[0];

        System.assertEquals(
            0, [
                SELECT Id
                FROM Quote
            ].size(),
            'Wrong amount of Quotes');

        // Create 1st Quote
        NewQuoteController.createQuote(opportunity.Id, 1);

        List<Quote> quotes = [
            SELECT Id, QuoteNumber, Name
            FROM Quote
            ORDER BY CreatedDate ASC
        ];
        System.assertEquals(
            1,
            quotes.size(),
            'Quote record has not been created'
        );
        System.assertEquals(
            3,
            [
                SELECT Id, UnitPrice, Quantity, SortOrder__c
                FROM QuoteLineItem
                ORDER BY CreatedDate ASC
            ].size(),
            'Quote Line Items have not been created'
        );

        opportunity.SyncedQuoteId = quotes[0].Id;
        update opportunity;

        Test.startTest();

        List<OpportunityLineItem> opportunityLineItems = [
            SELECT Id, UnitPrice, SortOrder__c, Quantity
            FROM OpportunityLineItem
            WHERE OpportunityId  = :opportunity.Id
            ORDER BY CreatedDate ASC
        ];

        opportunityLineItems[0].UnitPrice = 10;
        opportunityLineItems[0].SortOrder__c = 1;
        opportunityLineItems[0].Quantity = 1;
        opportunityLineItems[1].UnitPrice = 20;
        opportunityLineItems[1].SortOrder__c = 2;
        opportunityLineItems[1].Quantity = 2;
        opportunityLineItems[2].UnitPrice = 30;
        opportunityLineItems[2].SortOrder__c = 3;
        opportunityLineItems[2].Quantity = 3;

        update opportunityLineItems;

        Test.stopTest();

        List<QuoteLineItem> quoteLineItems = [
            SELECT Id, UnitPrice, Quantity, SortOrder__c
            FROM QuoteLineItem
            ORDER BY CreatedDate ASC
        ];

        System.assertEquals(10, quoteLineItems[0].UnitPrice, 'UnitPrice has not been synced');
        System.assertEquals(1, quoteLineItems[0].SortOrder__c, 'SortOrder__c has not been synced');
        System.assertEquals(1, quoteLineItems[0].Quantity, 'Quantity has not been synced');
        System.assertEquals(20, quoteLineItems[1].UnitPrice, 'UnitPrice has not been synced');
        System.assertEquals(2, quoteLineItems[1].SortOrder__c, 'SortOrder__c has not been synced');
        System.assertEquals(2, quoteLineItems[1].Quantity, 'Quantity has not been synced');
        System.assertEquals(30, quoteLineItems[2].UnitPrice, 'UnitPrice has not been synced');
        System.assertEquals(3, quoteLineItems[2].SortOrder__c, 'SortOrder__c has not been synced');
        System.assertEquals(3, quoteLineItems[2].Quantity, 'Quantity has not been synced');
    }

    @isTest
    public static void test_getDataFromQuoteLineItems() {
        TriggerTemplateV2.setupUnitTest();
/*
        Account account = TestUtil.getAccount();
        insert account;

        List<Product2> aProducts = new List<Product2>{
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')
        };
        insert aProducts;

        System.debug('#### ' + [select id, CurrencyIsoCode from Pricebook2 where isStandard=true]);

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>{
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert standardPrices;

        PriceBook2 customPriceBook = new PriceBook2();
        customPriceBook.Name = 'Custom Pricebook';
        customPriceBook.IsActive = true;

        insert customPriceBook;

        List<PricebookEntry> customPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert customPrices;

        Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3), CurrencyIsoCode = 'USD', Pricebook2Id = customPriceBook.Id);
        insert opportunity;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(PricebookEntryId = customPrices[0].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[1].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[2].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1)
        };
        insert opportunityLineItems;
*/

        Test.startTest();

        Opportunity opportunity = TestUtil.selectAllOpportunity()[0];

        System.assertEquals(
            0, [
                SELECT Id
                FROM Quote
            ].size(),
            'Wrong amount of Quotes');

        // Create 1st Quote
        NewQuoteController.createQuote(opportunity.Id, 1);

        List<Quote> quotes = [
            SELECT Id, QuoteNumber, Name
            FROM Quote
            ORDER BY CreatedDate ASC
        ];
        System.assertEquals(
            1,
            quotes.size(),
            'Quote record has not been created'
        );

        List<QuoteLineItem> quoteLineItems = [
            SELECT Id, UnitPrice, Quantity, SortOrder__c
            FROM QuoteLineItem
            ORDER BY CreatedDate ASC
        ];
        System.assertEquals(
            3,
            quoteLineItems.size(),
            'Quote Line Items have not been created'
        );

        quoteLineItems[0].UnitPrice = 10700;
        quoteLineItems[0].SortOrder__c = 1;
        quoteLineItems[0].Quantity = 1;
        quoteLineItems[1].UnitPrice = 20;
        quoteLineItems[1].SortOrder__c = 2;
        quoteLineItems[1].Quantity = 2;
        quoteLineItems[2].UnitPrice = 30;
        quoteLineItems[2].SortOrder__c = 3;
        quoteLineItems[2].Quantity = 3;

        update quoteLineItems;

        opportunity.SyncedQuoteId = quotes[0].Id;
        update opportunity;

        List<OpportunityLineItem> opportunityLineItems = [
            SELECT Id, UnitPrice, SortOrder__c, Quantity
            FROM OpportunityLineItem
            ORDER BY CreatedDate ASC
        ];

        System.assertEquals(3, opportunityLineItems.size(), 'Existing Opportunity Line Items have not been deleted after Sync started');

        opportunityLineItems = [
            SELECT Id, UnitPrice, SortOrder__c, Quantity
            FROM OpportunityLineItem
            WHERE OpportunityId = :opportunity.Id
            ORDER BY CreatedDate ASC
        ];
        System.assertEquals(3, opportunityLineItems.size(), 'Opportunity Line Items have not been created after Sync started');

        Test.stopTest();

        System.assertEquals(10700, opportunityLineItems[0].UnitPrice, 'UnitPrice has not been synced');
        System.assertEquals(null, opportunityLineItems[0].SortOrder__c, 'SortOrder__c has not been synced');
        System.assertEquals(1, opportunityLineItems[0].Quantity, 'Quantity has not been synced');
        System.assertEquals(20, opportunityLineItems[1].UnitPrice, 'UnitPrice has not been synced');
        System.assertEquals(null, opportunityLineItems[1].SortOrder__c, 'SortOrder__c has not been synced');
        System.assertEquals(2, opportunityLineItems[1].Quantity, 'Quantity has not been synced');
        System.assertEquals(30, opportunityLineItems[2].UnitPrice, 'UnitPrice has not been synced');
        System.assertEquals(null, opportunityLineItems[2].SortOrder__c, 'SortOrder__c has not been synced');
        System.assertEquals(3, opportunityLineItems[2].Quantity, 'Quantity has not been synced');
    }

    @isTest
    static void createOpportunityLIHistories()
    {
        TriggerTemplateV2.setupUnitTest();
/*
        Account account = new Account(Name = 'asd account', BillingCountryCode = 'US',BillingState='California');
        insert account;

        List<Product2> aProducts = new List<Product2>{
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')
        };
        insert aProducts;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>{
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert standardPrices;

        PriceBook2 customPriceBook = new PriceBook2();
        customPriceBook.Name = 'Custom Pricebook';
        customPriceBook.IsActive = true;
        insert customPriceBook;

        List<PricebookEntry> customPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert customPrices;

        Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3), CurrencyIsoCode = 'USD', Pricebook2Id = customPriceBook.Id);
        insert opportunity;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(PricebookEntryId = customPrices[0].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[1].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[2].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1)
        };
        insert opportunityLineItems;
*/

        List<OpportunityLineItem> oppLineItems = [
            SELECT Id, UnitPrice, Quantity
            FROM OpportunityLineItem
            ORDER BY CreatedDate ASC
        ];
        oppLineItems[0].UnitPrice = 70;
        oppLineItems[0].Quantity = 1;
        update oppLineItems;

        delete oppLineItems[1];

        List<OpportunityLineItemHistory__c> histories = [SELECT Id FROM OpportunityLineItemHistory__c];
        System.assert(histories.size() > 3);
    }
}
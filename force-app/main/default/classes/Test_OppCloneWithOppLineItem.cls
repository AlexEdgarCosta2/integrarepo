/*
*   @description Test class for OppCloneWithOppLineItem functionality.
*
*   @author Kamal Patil
*   @copyright PARX
*/
@isTest
public class Test_OppCloneWithOppLineItem 
{
    public static Opportunity testOpportunity;

    private static void initData()
    {
        TriggerTemplateV2.setupUnitTest();
        AssignmentRule__c assignmentRule = TestUtil.createAssignment('US', 'CA', UserInfo.getUserId(), true);

        AccountGroup__c anAccountGroup = new AccountGroup__c(Name = 'asd');
        insert anAccountGroup;
        
        Account anAccount = TestUtil.getAccount();
        anAccount.CurrencyIsoCode = 'USD';
        insert anAccount;
        anAccount = [SELECT Id, Name, AccountGroup__c, BillingCountry, BillingState, CurrencyIsoCode FROM Account WHERE Id =: anAccount.Id LIMIT 1];
        
        List<Product2> aProducts = new List<Product2>{
                new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductNameDE__c = 'test 1 de', ProductNameFR__c = 'test 1 FR', ProductFamily__c = 'f1', PricingGroup__c = '1', 
                        ProductLanguage__c = 'English'), 
                new Product2(Name = 'inactive product', IsActive = false, ProductCode = 'test_asd2', ProductNameDE__c = 'test de ', ProductNameFR__c = 'test FR', ProductFamily__c = 'f1'),
                new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductNameDE__c = 'test 2 de', ProductNameFR__c = 'test 2 FR', ProductFamily__c = 'f1', PricingGroup__c = '1'),
                new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductNameDE__c = 'test 3 de', ProductNameFR__c = 'test 3 FR', ProductFamily__c = 'f2', PricingGroup__c = '2')};
        insert aProducts;
        
        List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
                new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
        insert markets;
        
        List<PriceEntry__c> aPriceEntries = new List<PriceEntry__c>{//CurrencyIsoCode = anAccount.CurrencyIsoCode,
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 10, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = anAccount.CurrencyIsoCode), 
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 9, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today().addDays(-2), Market__c = markets[0].Id, CurrencyIsoCode = anAccount.CurrencyIsoCode),
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 100, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today(), Market__c = markets[1].Id, CurrencyIsoCode = anAccount.CurrencyIsoCode), 
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 99999, SellingPrice__c = 100, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today().addDays(2), Market__c = markets[0].Id, CurrencyIsoCode = anAccount.CurrencyIsoCode), 
            new PriceEntry__c(Product__c = aProducts.get(2).Id, ScalingTo__c = 6, SellingPrice__c = 200, 
                    ScalingTo2__c = 999999, DiscountTier2__c = 25,
                    ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = anAccount.CurrencyIsoCode), 
            new PriceEntry__c(Product__c = aProducts.get(3).Id, ScalingTo__c = 3, SellingPrice__c = 600, 
                    ScalingTo2__c = 999999, DiscountTier2__c = 10, //540, not 500
                    ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = anAccount.CurrencyIsoCode), 
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 1, SellingPrice__c = 20, ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = anAccount.CurrencyIsoCode)};
        insert aPriceEntries;
        
        PricebookEntry pbe2 = new PricebookEntry(isActive=true,product2id = aProducts.get(0).Id, pricebook2id = Test.getStandardPricebookId(), unitprice = 4.99, CurrencyIsoCode = anAccount.CurrencyIsoCode);
        insert pbe2;
        
        testOpportunity = new Opportunity(DiscountPercentage__c=10,DiscountDisposables__c=10,DiscountServices__c=10,
                                          Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert', 
                                          CloseDate = date.today(),CurrencyIsoCode = anAccount.CurrencyIsoCode);
        insert testOpportunity;
        List<OpportunityLineItem> anItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(Product2Id = aProducts.get(0).Id, OpportunityId = testOpportunity.Id,Quantity = 1000000 ,PricebookEntryId=pbe2.Id,
                                        UnitPrice = 30.00, ListPriceTierPricing__c = 30.00, ListPriceOneItem__c=40.00),
            new OpportunityLineItem(Product2Id = aProducts.get(0).Id, OpportunityId = testOpportunity.Id,Quantity = 1000000 ,PricebookEntryId=pbe2.Id,
                                        UnitPrice = 30.00, ListPriceTierPricing__c = 30.00, ListPriceOneItem__c=40.00),
            new OpportunityLineItem(Product2Id = aProducts.get(0).Id, OpportunityId = testOpportunity.Id,Quantity = 1000000 ,PricebookEntryId=pbe2.Id,
                                        UnitPrice = 30.00, ListPriceTierPricing__c = 30.00, ListPriceOneItem__c=40.00)
        };
        insert anItems;
    }
    
    /*
    *   @description test main logic.
    */
    static testMethod void testMainLogic()
    {
        initData();
        
        TriggerTemplateV2.startUnitTest();
        PageReference aPage = Page.cloneOpportunityPage;
        update new Opportunity(Id = testOpportunity.Id, DiscountDisposables__c = 5);
        aPage.getParameters().put('id', testOpportunity.Id);
        Test.setCurrentPage(aPage);
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(testOpportunity);
        OppCloneWithOppLineItem  aController = new OppCloneWithOppLineItem (sc);
        
        List<OpportunitylineItem> anItems = [Select Id,Product2Id,OpportunityId,Quantity,UnitPrice,PricebookEntryId,
                                           ListPriceTierPricing__c,ListPriceOneItem__c From OpportunityLineItem 
                                           where OpportunityId = :testOpportunity.Id];

        aController.cloneOpportunityWithLineItem();
        aController.calculateNewProductPrice(testOpportunity.Id, testOpportunity.Id);
        System.assertEquals(3, anItems.size(), 'wrong number of line items');
        aController.reCalculateProductPrice();
        System.assertEquals(10, testOpportunity.DiscountPercentage__c , 'Percentage Discount not updated');
        System.assertEquals(10, testOpportunity.DiscountDisposables__c , 'Disposables Discount not updated');
        System.assertEquals(10, testOpportunity.DiscountServices__c , 'Services Discount not updated');
        Test.stopTest();
    }
}
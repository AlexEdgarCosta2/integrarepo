public with sharing class RecalculateProductPriceController extends ProductPickerController {

	private ApexPages.StandardController controller {get; set;}

	public SObject record {get; set;}
	public Id recordId {get; set;}
	public Boolean isQuote = false;
	public Boolean isOpportunity = false;

	public RecalculateProductPriceController(ApexPages.StandardController controller)
	{
		//initialize the standard controller
		this.controller = controller;
		// load the current record
		record = controller.getRecord();
	}

	public override void initMainObject()
	{
		this.recordId = (Id) ApexPages.currentPage().getParameters().get('id');
		SObjectType currentSObjectType = this.recordId.getSobjectType();
		SObjectType opportunitySObjectType = Schema.getGlobalDescribe().get('Opportunity');
		SObjectType quoteSObjectType = Schema.getGlobalDescribe().get('Quote');

		if (currentSObjectType == opportunitySObjectType) {
			this.isOpportunity = true;

			this.record = [
				SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, Contact__c, DiscountPercentage__c,
					DiscountDisposables__c, DiscountServices__c
				FROM Opportunity
				WHERE Id = :this.recordId
			];
		} else if (currentSObjectType == quoteSObjectType) {
			this.isQuote = true;

			this.record = [
				SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, ProductLanguage__c, DiscountProducts__c,
					DiscountDisposables__c, DiscountServices__c
				FROM Quote
				WHERE Id = :this.recordId
			];
		}
	}

	public override Id getMainId()
	{
		return this.record.Id;
	}

	public override String getMainCurrency()
	{
		return (String) this.record.get('CurrencyIsoCode');
	}

	public override String getLanguageForProducts()
	{
		String languageToReturn = '';
		if (isOpportunity)
		{
			Opportunity opportunityRecord = (Opportunity) this.record;
			if (opportunityRecord.ProductLanguage__c != null)
			{
				languageToReturn = opportunityRecord.ProductLanguage__c;
			}
		}
		else
		{
			Quote quoteRecord = (Quote) this.record;
			languageToReturn = quoteRecord.ProductLanguage__c;
		}
		return languageToReturn;
	}

	public override String getMainBillingCountryCode()
	{
		return (String) this.record.getSObject('Account').get('BillingCountryCode');
	}

	public override Id getMainAccountGroup()
	{
		return (Id) this.record.getSObject('Account').get('AccountGroup__c');
	}

	protected override void applyAdditionalConditions(AndCondition andCondition)
	{
		andCondition.add(new FieldCondition('DoNotShowOnOpportunity__c', Operator.NOT_EQUALS, true));
	}

	public override Decimal getOverallDiscount()
	{
		return this.isOpportunity ?
			(Decimal) this.record.get('DiscountPercentage__c') :
			(Decimal) this.record.get('DiscountProducts__c');
	}
	public override Decimal getDisposablesDiscount()
	{
		return (Decimal) this.record.get('DiscountDisposables__c');
	}
	public override Decimal getServicesDiscount()
	{
		return (Decimal) this.record.get('DiscountServices__c');
	}

	public override PageReference reCalculateProductPrice()
	{
		reCalculateProductPrice(this.recordId);
		if (UserInfo.getUiThemeDisplayed() == 'Theme3' || UserInfo.getUiThemeDisplayed() == 'Theme2')
		{
			return new PageReference('/' + this.recordId);
		}
		return null;
	}

	public void reCalculateProductPrice(Id recordId)
	{
		List<SObject> anItems = new List<SObject>();
		Map<Id, List<SObject>> productIdMap = new Map<Id, List<SObject>>();
		List<SObject> lineItemList = new List<SObject>();

		if (recordId != null)
		{
			if (this.isOpportunity) {
				this.record = [
					SELECT Id,Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, DiscountPercentage__c, DiscountDisposables__c, DiscountServices__c
					FROM Opportunity
					WHERE Id = :recordId
				];
			} else {
				this.record = [
					SELECT Id,Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, DiscountProducts__c, DiscountDisposables__c, DiscountServices__c
					FROM Quote
					WHERE Id = :recordId
				];
			}
			
			if (!this.isOpportunity) {
				record.put('DiscountProducts__c', 0);
				//record.put('DiscountPercentage__c', null);
				record.put('DiscountDisposables__c', 0);
				record.put('DiscountServices__c', 0);
			}

			update this.record;

			if (this.isOpportunity) {
				anItems = new List<OpportunityLineItem>([
					SELECT Id, Product2Id, product2.PricingGroup__c, OpportunityId, Quantity, UnitPrice, PricebookEntryId,
						ListPriceTierPricing__c, ListPriceOneItem__c
					FROM OpportunityLineItem
					WHERE OpportunityId = :recordId
					FOR UPDATE
				]);
			} else {
				anItems = new List<QuoteLineItem>([
					SELECT Id, Product2Id, product2.PricingGroup__c, QuoteId, Quantity, UnitPrice, PricebookEntryId,
						ListPriceTierPricing__c, ListPriceOneItem__c
					FROM QuoteLineItem
					WHERE QuoteId = :recordId
					FOR UPDATE
				]);
			}

			for (SObject lineItem : anItems)
			{
				Id productId = (Id) lineItem.get('product2Id');
				List<SObject> lineItemsForProduct = productIdMap.get(productId);
				if (lineItemsForProduct == null) {
					lineItemsForProduct = new List<SObject>();
				}
				lineItemsForProduct.add(lineItem);
				productIdMap.put(productId, lineItemsForProduct);
			}
			lineItemList = applyNewPrice(productIdMap);
			if (lineItemList != null || lineItemList.size() > 0)
			{
				update lineItemList;
			}
		}
	}

	public List<SObject> applyNewPrice(Map<Id, List<SObject>> productIdMap)
	{
		List<PriceEntry__c> newStdPrices = new List<PriceEntry__c>();
		List<PriceEntry__c> newSpecPrices = new List<PriceEntry__c>();

		Map<Id, List<PriceEntry__c>> productWithStandardPrices = new Map<Id, List<PriceEntry__c>>();
		Map<Id, List<PriceEntry__c>> productWithSpecialPrices = new Map<Id, List<PriceEntry__c>>();

		String countryCode = '%' + getMainBillingCountryCode() + '%';
		Decimal aQuantity ;
		Decimal sellingPrice ;
		Decimal sellingSpecPrice ;

		for (PriceEntry__c aPriceEntry : [
			SELECT id, Product__c, ScalingTo__c, SellingPrice__c, ScalingTo2__c, DiscountTier2__c, ScalingTo3__c, DiscountTier3__c, AccountGroup__c, ValidFrom__c
			FROM PriceEntry__c
			WHERE Product__c IN : productIdMap.keySet()
			AND CurrencyIsoCode = :getMainCurrency()
			AND	Market__r.CountryList__c LIKE :countryCode
			AND ValidFrom__c <= :System.Today()
			ORDER BY ValidFrom__c DESC
		])
		{
			if (aPriceEntry.AccountGroup__c == null) // standard price
			{
				newStdPrices.addAll(addNewPrice(productWithStandardPrices, aPriceEntry)) ;
			}
			else if (
				aPriceEntry.AccountGroup__c == getMainAccountGroup() &&
				(
					getOverallDiscount() == null ||
						getDisposablesDiscount() == null ||
						getServicesDiscount()== null
				)
			) // special price
			{
				newSpecPrices.addAll(addNewPrice(productWithSpecialPrices, aPriceEntry));
			}
		}

		Set<SObject> lineItemList = new Set<SObject>();
		List<SObject> lineItemList1 = new List<SObject>();
		Map<Id, PriceEntry__c> priceEntryStdMap = new Map<Id,PriceEntry__c>();
		Map<Id, PriceEntry__c> priceEntrySpecMap = new Map<Id,PriceEntry__c>();

		for (List<SObject> lineItems : productIdMap.values())
		{
			lineItemList.addAll(lineItems);
		}

		Map<String, Decimal> aPricingGroupQuantityMap= new Map<String, Decimal>(); // Pricing group code => Quantity relation
		for (SObject lineItem : lineItemList)
		{
			String pricingGroup = (String) lineItem.getSObject('product2').get('PricingGroup__c');
			if (pricingGroup != null)
			{
				// create a map that commulates quantity to a specific pricing group.
				aPricingGroupQuantityMap.put(pricingGroup,
					aPricingGroupQuantityMap.containsKey(pricingGroup) ?
						aPricingGroupQuantityMap.get(pricingGroup) + (Decimal) lineItem.get('Quantity') :(Decimal) lineItem.get('Quantity'));
			}
		}
		for (PriceEntry__c priceEntryRecord: newStdPrices)
		{
			priceEntryStdMap.put(priceEntryRecord.Product__c, priceEntryRecord);
		}
		for (PriceEntry__c priceEntryRecord: newSpecPrices)
		{
			priceEntrySpecMap.put(priceEntryRecord.Product__c, priceEntryRecord);
		}
		for (SObject lineItem : lineItemList)
		{
			String pricingGroup = (String) lineItem.getSObject('product2').get('PricingGroup__c');
			Id productId = (Id) lineItem.get('Product2Id');
			if (pricingGroup != null && aPricingGroupQuantityMap.containsKey((pricingGroup)))//getOverallDiscount() == null &&
			{
				aQuantity = aPricingGroupQuantityMap.get(pricingGroup);
			}
			else
			{
				aQuantity = (Decimal) lineitem.get('Quantity');
				//aQuantity = aPricingGroupQuantityMap.get(lineItem.Product2.PricingGroup__c);
			}

			if (priceEntryStdMap.containsKey(productId)) {
				sellingPrice = priceEntryStdMap.get(productId).SellingPrice__c;
				lineItem.put('ListPriceTierPricing__c', sellingPrice);
				lineItem.put('UnitPrice', sellingPrice);
				lineItem.put('ListPriceOneItem__c', sellingPrice);
			}

			if (
				(priceEntrySpecMap != null || priceEntrySpecMap.size() > 0) &&
					priceEntrySpecMap.get(productId) != null &&
					priceEntrySpecMap.get(productId).SellingPrice__c != null
				) {
				sellingSpecPrice = priceEntrySpecMap.get(productId).SellingPrice__c;
				lineItem.put('ListPriceTierPricing__c', sellingSpecPrice);
				lineItem.put('UnitPrice', sellingSpecPrice);
			} else {
				if (priceEntryStdMap.containsKey(productId) && aQuantity > priceEntryStdMap.get(productId).ScalingTo__c)
				{
					if (
						priceEntryStdMap.get(productId).ScalingTo2__c != null &&
						priceEntryStdMap.get(productId).DiscountTier2__c != null
					)
					{
						sellingPrice = (priceEntryStdMap.get(productId).SellingPrice__c * (1 - 0.01 * priceEntryStdMap.get(productId).DiscountTier2__c)).setScale(2);
						lineItem.put('ListPriceTierPricing__c', sellingPrice);
						lineItem.put('UnitPrice', sellingPrice);
						lineItem.put('ListPriceOneItem__c', priceEntryStdMap.get(productId).SellingPrice__c);
						if (aQuantity > priceEntryStdMap.get(productId).ScalingTo2__c)
						{
							if (
								priceEntryStdMap.get(productId).ScalingTo3__c != null &&
									aQuantity > priceEntryStdMap.get(productId).ScalingTo2__c &&
									priceEntryStdMap.get(productId).DiscountTier3__c != null
							)
							{
								sellingPrice = (priceEntryStdMap.get(productId).SellingPrice__c * (1 - 0.01 * priceEntryStdMap.get(productId).DiscountTier3__c)).setScale(2);
								lineItem.put('ListPriceTierPricing__c', sellingPrice);
								lineItem.put('UnitPrice', sellingPrice);
								lineItem.put('ListPriceOneItem__c', priceEntryStdMap.get(productId).SellingPrice__c);
							}
						}
					}
				}
			}
			lineItemList1.add(lineItem);
		}
		return lineItemList1;
	}

	/**
	*   @description add products to the Database.
	*/
	public override PageReference doAddProducts()
	{
		return redirectToPreviousPage();
	}

	public override PageReference cloneOpportunityWithLineItem()
	{
//		Opportunity newOpp;
//		this.record = [SELECT o.AccountId, o.Account.Name, o.Contact__c,
//			o.Amount, o.CloseDate, o.Id,o.Name, o.StageName
//		FROM Opportunity o
//		WHERE o.Id = :opp.id];
//		opp.CloseDate = opp.CloseDate;
//		opp.Name =  opp.Name;
//		opp.StageName = 'Opportunity Analysis';
//
//		newOpp = opp.clone(false);
//		insert newOpp;
//		if(newOpp.Id != null)
//		{
//			calculateNewProductPrice(opp.Id,newOpp.Id);
//		}

		return new PageReference('/' + this.recordId);
	}
}
/**
 * Opportunity Sort Opportunity LineItems.
 *
 * @author ach
 */
public with sharing class SortLineItemsController
{
    public static String PRODUCT_TYPE_SERVICES = 'Services';
    
    public static String PRODUCT_TYPE_DISPOSABLES = 'Disposables';
    
    public static String PRODUCT_TYPE_ACCESSORIES = 'Products and Accessories';
    
    public List<Wrapper> fromSectionServices{get;set;}
    
    public List<Wrapper> fromSectionDisposables{get;set;}
    
    public List<Wrapper> fromSectionAccessories{get;set;}
    
    private Map<String, List<Wrapper>> wrappersByProductType{get;set;}
    
    public Id mainObjectId{get;set;}
    
    public String name{get;set;}
    
    public String objectName{get;set;}
    
    // param that contains rank number
    public String rankNumber {get;set;}
    
    // param that contains productType
    public String productType {get;set;}
    
    /**
    * Constructor.
    */
    public SortLineItemsController()
    {
        this.fromSectionServices = new List<Wrapper>();
        this.fromSectionDisposables = new List<Wrapper>();
        this.fromSectionAccessories = new List<Wrapper>();
        this.wrappersByProductType = new Map<String, List<Wrapper>>{PRODUCT_TYPE_SERVICES => fromSectionServices,
                PRODUCT_TYPE_DISPOSABLES => fromSectionDisposables, PRODUCT_TYPE_ACCESSORIES => fromSectionAccessories};
        mainObjectId = (Id)Apexpages.currentPage().getParameters().get('id');
        objectName = TriggerUtils.getsObjectNameById(mainObjectId);
        if (objectName == 'opportunity')
        {
            this.name = [SELECT Id, Name FROM Opportunity WHERE Id=:mainObjectId].Name;
            List<OpportunityLineItem> items = [SELECT Id, SortOrder__c, PricebookEntry.Product2.ProductCode, PricebookEntry.Product2Id, 
                    PricebookEntry.Product2.ProductType__c, PricebookEntry.Product2.Name, PricebookEntry.Product2.Id FROM OpportunityLineItem 
                    WHERE OpportunityId = :mainObjectId ORDER BY SortOrder__c NULLS LAST, PricebookEntry.ProductCode];
            initLineItemSections(items);
        }else
        {
            this.name = [SELECT Id, CaseNumber FROM Case WHERE Id=:mainObjectId].CaseNumber;
            List<CaseProduct__c> items = [SELECT Id, SortOrder__c, Product__r.ProductCode, 
                    Product__r.ProductType__c, Product__r.Name, Product__r.Id FROM CaseProduct__c 
                    WHERE Case__c = :mainObjectId ORDER BY SortOrder__c NULLS LAST, Product__r.ProductCode];
            initLineItemSections(items);
        }
        updateRanks(this.fromSectionServices);
        updateRanks(this.fromSectionDisposables);
        updateRanks(this.fromSectionAccessories);
    }
    
    /**
    * Constructor for Lightning functionality.
    */
    public SortLineItemsController(Id oppId)
    {
        system.debug('SortLineItemsController oppId :: '+oppId);
        this.fromSectionServices = new List<Wrapper>();
        this.fromSectionDisposables = new List<Wrapper>();
        this.fromSectionAccessories = new List<Wrapper>();
        this.wrappersByProductType = new Map<String, List<Wrapper>>{PRODUCT_TYPE_SERVICES => fromSectionServices,
        PRODUCT_TYPE_DISPOSABLES => fromSectionDisposables, PRODUCT_TYPE_ACCESSORIES => fromSectionAccessories};
        mainObjectId = oppId;
        objectName = TriggerUtils.getsObjectNameById(mainObjectId);
        if (objectName == 'opportunity')
        {
            this.name = [SELECT Id, Name FROM Opportunity WHERE Id=:mainObjectId].Name;
            List<OpportunityLineItem> items = [SELECT Id, SortOrder__c, PricebookEntry.Product2.ProductCode, PricebookEntry.Product2Id,
                    PricebookEntry.Product2.ProductType__c, PricebookEntry.Product2.Name, PricebookEntry.Product2.Id FROM OpportunityLineItem
            WHERE OpportunityId = :mainObjectId ORDER BY SortOrder__c NULLS LAST, PricebookEntry.ProductCode];
            initLineItemSections(items);
        }
        else if (objectName == 'quote')
        {
            System.debug('objectName inside quote loop');
            this.name = [SELECT Id, Name FROM Quote WHERE Id=:mainObjectId].Name;
            List<QuoteLineItem> items = [SELECT Id, SortOrder__c, Product2.ProductCode, Product2Id,
                    Product2.ProductType__c, Product2.Name, Product2.Id FROM QuoteLineItem
                    WHERE QuoteId = :mainObjectId ORDER BY SortOrder__c NULLS LAST, Product2.ProductCode];
            initLineItemSections(items);
        }
        else
        {
            this.name = [SELECT Id, CaseNumber FROM Case WHERE Id=:mainObjectId].CaseNumber;
            List<CaseProduct__c> items = [SELECT Id, SortOrder__c, Product__r.ProductCode,
                    Product__r.ProductType__c, Product__r.Name, Product__r.Id FROM CaseProduct__c
            WHERE Case__c = :mainObjectId ORDER BY SortOrder__c NULLS LAST, Product__r.ProductCode];
            initLineItemSections(items);
        }
        updateRanks(this.fromSectionServices);
        updateRanks(this.fromSectionDisposables);
        updateRanks(this.fromSectionAccessories);
    }
    
    /**
    * Init sections.
    */
    private void initLineItemSections(List<SObject> items)
    {
        if(objectName == 'Opportunity')
        {
            for (OpportunityLineItem item: (List<OpportunityLineItem>)items)
            {
                this.wrappersByProductType.get(item.PricebookEntry.Product2.ProductType__c).add(new Wrapper(item.Id, item.PricebookEntry.Product2));
            }
        }
        else if(objectName == 'Quote')
        {
            for (QuoteLineItem item: (List<QuoteLineItem>)items)
            {
                this.wrappersByProductType.get(item.Product2.ProductType__c).add(new Wrapper(item.Id, item.Product2));
            }
        }
    }
    
    /**
    * Init sections.
    */
    private void initLineItemSections(List<CaseProduct__c> items)
    {
        for (CaseProduct__c item: items)
        {
            this.wrappersByProductType.get(item.Product__r.ProductType__c).add(new Wrapper(item.Id, item.Product__r));
        }
    }
    
    /**
    * Save line items to DB.
    */
    public void save()
    {
        if (objectName == 'opportunity')
        {
            List<OpportunityLineItem> items = new List<OpportunityLineItem>();
            items.addAll(getOppItemsFromWrappers(this.fromSectionServices));
            items.addAll(getOppItemsFromWrappers(this.fromSectionDisposables));
            items.addAll(getOppItemsFromWrappers(this.fromSectionAccessories));
            update items;
        } 
        else if (objectName == 'quote')
        {
            List<QuoteLineItem> items = new List<QuoteLineItem>();
            items.addAll(getQuoteItemsFromWrappers(this.fromSectionServices));
            items.addAll(getQuoteItemsFromWrappers(this.fromSectionDisposables));
            items.addAll(getQuoteItemsFromWrappers(this.fromSectionAccessories));
            update items;
        }  
        else
        {
            List<CaseProduct__c> items = new List<CaseProduct__c>();
            items.addAll(getCaseItemsFromWrappers(this.fromSectionServices));
            items.addAll(getCaseItemsFromWrappers(this.fromSectionDisposables));
            items.addAll(getCaseItemsFromWrappers(this.fromSectionAccessories));
            update items;
        }
    }
    
    /**
    * Do save action.
    */
    public PageReference doSave()
    {
        save();
        return redirectToPreviousPage();
    }
    
    /**
    * Generate Opp line items form wrappers.
    */
    List<OpportunityLineItem> getOppItemsFromWrappers(List<Wrapper> wrappers)
    {
        List<OpportunityLineItem> result = new List<OpportunityLineItem>();
        for(Wrapper wrapper : wrappers)
        {
            result.add(new OpportunityLineItem(Id = wrapper.itemId, SortOrder__c = Integer.valueOf(wrapper.rank)));
        }
        return result;
    }

     /**
    * Generate Opp line items form wrappers.
    */
    List<QuoteLineItem> getQuoteItemsFromWrappers(List<Wrapper> wrappers)
    {
        List<QuoteLineItem> result = new List<QuoteLineItem>();
        for(Wrapper wrapper : wrappers)
        {
            result.add(new QuoteLineItem(Id = wrapper.itemId, SortOrder__c = Integer.valueOf(wrapper.rank)));
        }
        return result;
    }
    
    /**
    * Generate Case line items form wrappers.
    */
    List<CaseProduct__c> getCaseItemsFromWrappers(List<Wrapper> wrappers)
    {
        List<CaseProduct__c> result = new List<CaseProduct__c>();
        for(Wrapper wrapper : wrappers)
        {
            result.add(new CaseProduct__c(Id = wrapper.itemId, SortOrder__c = Integer.valueOf(wrapper.rank)));
        }
        return result;
    }
    
    /**
    *   Rank up the item.
    */
    public void doRankUp()
    {
        Integer aRank = Integer.valueOf(this.rankNumber) -1;
        moveItem(aRank, aRank-1, this.productType);
    }
    
    /**
    *   Rank down the item.
    */
    public void doRankDown()
    {
        Integer aRank = Integer.valueOf(this.rankNumber)-1;
        moveItem(aRank, aRank+1, this.productType);
    }
    
    /**
    * Move line item from old position to new position.
    */
    private void moveItem(Integer oldPosition, Integer newPosition, String productType)
    {
        moveItem(oldPosition, newPosition, this.wrappersByProductType.get(productType));
    }
    
    /**
    * Move item from one position to another.
    */
    private void moveItem(Integer oldPosition, Integer newPosition, List<Wrapper> wrappers)
    {
        Wrapper acurrentWrapper = wrappers[oldPosition];
        wrappers.remove(oldPosition);
        if (newPosition == wrappers.size())
        {
            wrappers.add(acurrentWrapper);
        }else
        {
            wrappers.add(newPosition, acurrentWrapper);
        }
        updateRanks(wrappers);
    }
    
    /**
    *   Do change the rank.
    */
    public void doChangeRank()
    {
        String aNewRankParam = Apexpages.currentPage().getParameters().get('newRank');
        String anOldRankParam = Apexpages.currentPage().getParameters().get('oldRank');
        String productType = Apexpages.currentPage().getParameters().get('productType');
        system.debug('### productType:' + productType);
        List<Wrapper> wrappers = this.wrappersByProductType.get(productType);
        system.debug('### wrappers.size():' + wrappers.size());
        Integer anOldRank = Integer.valueOf(anOldRankParam)-1;
        Integer aNewRank;
        //validate the rank
        boolean isValid = true;
        if (aNewRankParam == null || !aNewRankParam.isNumeric())
        {
            isValid = false;
        }
        
        if (isValid)
        {
            aNewRank = Integer.valueOf(aNewRankParam)-1;
            //check rankge
            if (aNewRank < 0 || aNewRank > wrappers.size()-1)
            {
                isValid = false;
            }
        }
        system.debug('### isValid:' + isValid);
        if (isValid)
        {
            moveItem(anOldRank, aNewRank, wrappers);
        }else
        {
            //set back the old rank
            wrappers[anOldRank].rank = '' + (anOldRank + 1);
        }
    }
    
    /**
    *   Set correct rank for every item (Just store a position of items in the list)
    */
    private void updateRanks(List<Wrapper> wrappers)
    {
        Integer i = 1;
        
        for (Wrapper wrapper: wrappers)
        {
            wrapper.rank = '' + i++;
        }
    }
    
    /**
    *   @description redirect to the previous page.
    */
    public PageReference redirectToPreviousPage()
    {
        PageReference newPage = new PageReference('/' + this.mainObjectId);
        newPage.setRedirect(true);
        return newPage;
    }
    
    /**
    * Wrapper for line items.
    */
    public class Wrapper
    {
        public OpportunityLineItem item{get;set;}// just for rank field

        public String rank{get;set;}
        
        public Product2 product{get;set;}
        
        public Id itemId{get;set;}
        
        public Wrapper(Id itemId, Product2 product)
        {
            this.item = new OpportunityLineItem(Description = '');
            this.rank = '';
            this.itemId = itemId;
            this.product = product;
        }
    }
}
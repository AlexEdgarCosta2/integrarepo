/**
 * =============================================================
 * AUTHOR         : Alex Costa
 * CREATE DATE    : 08.04.2021
 * PURPOSE        : Service class to handle pricing.
 *
 * SPECIAL NOTES  : Depends on the LineItemWrapper Class.
 * =============================================================
 * Change History :
 * Date        Author               Description
 * ==========  ==================   ============================
 * 08.04.2021  Alex Costa           Created
 * 09.04.2021  Alex Costa           Added method to create the Product Scaling.
 * =============================================================
 */
public with sharing class PricingService {

    /**
     *  Apply Tiers
     *  This method applies discounts on the Opportunity line items (or Quore line itens) represented in the List<LineItemWrapper> selectedLineItemWrappers.
     *
     * The discount is allways the best one. Based on tier discound and overal discound (defined at the Opportunity or Quote)
     */
    public static void applyTiers(List<LineItemWrapper> selectedLineItemWrappers,
                                  Decimal overallDiscount,
                                  Decimal disposablesDiscount,
                                  Decimal servicesDiscount) {
                                  //Id mainAccountId,
                                  //Map<String, Double> productToListPrice) {

        System.debug(LoggingLevel.DEBUG, '[applyTiers]');
        //
        // create a map that accumulates quantity to a specific pricing group.
        //
        // sum quantity for PricingGroup__c
        Map<String, Decimal> aPricingGroupQuantityMap = new Map<String, Decimal>(); // Pricing group code => Quantity relation

        for (LineItemWrapper wrapper : selectedLineItemWrappers) {

	        System.debug(LoggingLevel.DEBUG, '[applyTiers] wrapper = ' + wrapper);
	        System.debug(LoggingLevel.DEBUG, '[applyTiers] wrapper.product.PricingGroup__c = ' + wrapper.product.PricingGroup__c);

            if (wrapper.product.PricingGroup__c != null) {
                aPricingGroupQuantityMap.put(wrapper.product.PricingGroup__c, aPricingGroupQuantityMap.containsKey(wrapper.product.PricingGroup__c) ?
                        aPricingGroupQuantityMap.get(wrapper.product.PricingGroup__c) + wrapper.qty :
                        wrapper.qty);
            }
        }

        for (LineItemWrapper aSelectedwrapper : selectedLineItemWrappers) {
            system.debug(Logginglevel.DEBUG,'[applyTiers] aSelectedwrapper :' + aSelectedwrapper);
            Decimal previousAmount = 0;

            if (aSelectedwrapper.pps != null) {

                Decimal aQuantity = aSelectedwrapper.qty;
                String productType = aSelectedwrapper.productType;

                // init the quantity to the related pricing group
                if (NumberUtils.isDecimalNull(overallDiscount) && aSelectedwrapper.product.PricingGroup__c != null &&
                    aPricingGroupQuantityMap.containsKey((aSelectedwrapper.product.PricingGroup__c))) {
                    aQuantity = aPricingGroupQuantityMap.get(aSelectedwrapper.product.PricingGroup__c);
                }

                for (PriceEntry__c aPriceEntry : aSelectedwrapper.pps) {

                    applyTier(aSelectedwrapper,
                        aPriceEntry,
                        overallDiscount,
                        disposablesDiscount,
                        servicesDiscount,
                        aQuantity,
                        productType,
                        false);
                }

                for (PriceEntry__c aPriceEntry : aSelectedwrapper.standardPrices) {

                    applyTier(aSelectedwrapper,
                        aPriceEntry,
                        overallDiscount,
                        disposablesDiscount,
                        servicesDiscount,
                        aQuantity,
                        productType,
                        true);
                }

            } else {
                aSelectedwrapper.listPriceTierPricing = 0;
            }
        }
    }

    private static void applyTier(LineItemWrapper aSelectedwrapper,
                                  PriceEntry__c aPriceEntry,
                                  Decimal overallDiscount,
                                  Decimal disposablesDiscount,
                                  Decimal servicesDiscount,
                                  Decimal aQuantity,
                                  String productType,
                                  Boolean flagStandard) {

        // this part is to get the appropriate price according to the given quantity
        // e.g. if quantity is 2 and there are two prices one which is for quantity 1 and one for quantity smaller then 4 then take the price for 4

        System.debug(LoggingLevel.DEBUG, '[applyTier] aPriceEntry : ' + aPriceEntry);
        System.debug(LoggingLevel.DEBUG, '[applyTier] flagStandard : ' + flagStandard);
        System.debug(LoggingLevel.DEBUG, '[applyTier] aPriceEntry.SellingPrice__c : ' + aPriceEntry.SellingPrice__c);

        if(!flagStandard) {
            Decimal discountToApply;
            Decimal mainDiscount = 0;
            Decimal scalingDiscount = 0;

            Decimal sellingPrice = aPriceEntry.SellingPrice__c;

            aSelectedwrapper.applicableTier = 1;
            aSelectedwrapper.sellingPrice = sellingPrice;
            if(aPriceEntry.DiscountTier2__c != null) {
                aSelectedwrapper.tier2Discount = aPriceEntry.DiscountTier2__c / 100;
            }
            if(aPriceEntry.DiscountTier3__c != null) {
                aSelectedwrapper.tier3Discount = aPriceEntry.DiscountTier3__c / 100;
            }

            if(overallDiscount != null  && productType == Constants.PRODUCT_TYPE_ACCESSORIES && overallDiscount != 0.00) {
                mainDiscount = overallDiscount;
            } else if(disposablesDiscount != null  && productType == Constants.PRODUCT_TYPE_DISPOSABLES && disposablesDiscount != 0.00) {
                //apply Disposables discount
                mainDiscount = disposablesDiscount;
            } else if(servicesDiscount != null  && productType == Constants.PRODUCT_TYPE_SERVICES && servicesDiscount != 0.00) {
                //apply Services discount
                mainDiscount = servicesDiscount;
            }

            System.debug(LoggingLevel.DEBUG, '[applyTier] aQuantity : ' + aQuantity);

            System.debug(LoggingLevel.DEBUG, '[applyTier] aPriceEntry.ScalingTo__c : ' + aPriceEntry.ScalingTo__c);

            if (aQuantity > aPriceEntry.ScalingTo__c) {
                //check ScallingTo2
                if (aPriceEntry.ScalingTo2__c != null && aPriceEntry.DiscountTier2__c != null) {
                    scalingDiscount = aPriceEntry.DiscountTier2__c;
                    aSelectedwrapper.applicableTier = 2;

                    if (aQuantity > aPriceEntry.ScalingTo2__c) {
                        //check ScallingTo3
                        if (aPriceEntry.ScalingTo3__c != null && aQuantity > aPriceEntry.ScalingTo2__c && aPriceEntry.DiscountTier3__c != null) {
                            scalingDiscount = aPriceEntry.DiscountTier3__c;
                            aSelectedwrapper.applicableTier = 3;
                        }
                    }
                }
            }

            /**
             * Apply the best discount for the client!
             */
            if(mainDiscount > scalingDiscount) {
                discountToApply = mainDiscount;
            } else {
                discountToApply = scalingDiscount;
            }

            aSelectedwrapper.tierDiscount = scalingDiscount / 100;
            aSelectedwrapper.mainDiscount = mainDiscount / 100;
            aSelectedwrapper.appliedDiscount = discountToApply / 100;

            aSelectedwrapper.specialPrice = (aPriceEntry.AccountGroup__c != null);

            sellingPrice = (aPriceEntry.SellingPrice__c * (1 - 0.01 * discountToApply)).setScale(2);

            if(sellingPrice == null) { sellingPrice = 0; }

            System.debug(LoggingLevel.DEBUG, '[applyTier]sellingPrice = ' + sellingPrice);
            applyTiersForWrapper(aSelectedwrapper, sellingPrice);
            aSelectedwrapper.listPriceTierPricing = sellingPrice;

            aSelectedwrapper.listPriceTierPricing = sellingPrice;

            //aSelectedwrapper.item.ListPriceOneItem__c = aPriceEntry.SellingPrice__c;
    /*
            if (aPriceEntry.AccountGroup__c != null && productToListPrice.containsKey(aSelectedwrapper.product.id) &&
                aPriceEntry.AccountGroup__c == mainAccountId) {
                aSelectedwrapper.item.ListPriceOneItem__c = productToListPrice.get(aSelectedwrapper.product.id);
            }
            */
        } else {
            aSelectedwrapper.standardPrice = aPriceEntry.SellingPrice__c;
        }
    }

    /**
     *  Creates the Product Scaling String
     *
     *  Example : ≤ 99,999 : 9,436.50
     */
    public static String getProductScaling(Decimal overallDiscount,
                                           String productType,
                                           Decimal disposablesDiscount,
                                           Decimal servicesDiscount,
                                           PriceEntry__c pps) {

        String productScaling = '';

        if(pps == null) {
            System.debug(Logginglevel.ERROR, '[getProductScaling] No Price Entry');
        } else {
            productScaling += defineStringScallingForDescription(pps.ScalingTo__c, pps.SellingPrice__c.setScale(2));
            if (pps.ScalingTo2__c != null && pps.DiscountTier2__c != null)
            {
                productScaling += defineStringScallingForDescription(pps.ScalingTo2__c, (pps.SellingPrice__c * (1 - 0.01 * pps.DiscountTier2__c)).setScale(2));
                if (pps.ScalingTo3__c != null && pps.DiscountTier3__c != null)
                {
                    productScaling += defineStringScallingForDescription(pps.ScalingTo3__c, (pps.SellingPrice__c * (1 - 0.01 * pps.DiscountTier3__c)).setScale(2));
                }
            }
        }

        return productScaling;
    }

    public static Map<String, Decimal> getProductToListPrice(Map<Id, List<PriceEntry__c>> priceEntryMap) {

        Map<String, Decimal> productToListPrice = new Map<String, Decimal>();

        for(Id key : priceEntryMap.keySet()) {
            List<PriceEntry__c> aPriceEntryList = priceEntryMap.get(key);

            for(PriceEntry__c aPriceEntry : aPriceEntryList) {

                // standard price
                if (aPriceEntry.AccountGroup__c == null) {
                    // get list price and put it into the product and list price map
                    if (!productToListPrice.containsKey(aPriceEntry.Product__c)) {
                        productToListPrice.put(aPriceEntry.Product__c, aPriceEntry.SellingPrice__c);
                    }
                }
            }
        }

        return productToListPrice;
    }

    /*
    public static Map<String, Decimal> getProductToListPrice(Map<String, List<PriceEntry__c>> priceEntryMap, String mainCurrency, String countryCode) {

        Map<String, Decimal> productToListPrice = new Map<String, Decimal>();

        String auxKey = mainCurrency + '|' + countryCode;

        for(String key : priceEntryMap.keySet()) {

            if(key.contains(auxKey)) {

                List<PriceEntry__c> aPriceEntryList = priceEntryMap.get(key);

                for(PriceEntry__c aPriceEntry : aPriceEntryList) {

                    // standard price
                    if (aPriceEntry.AccountGroup__c == null) {
                        // get list price and put it into the product and list price map
                        if (!productToListPrice.containsKey(aPriceEntry.Product__c)) {
                            productToListPrice.put(aPriceEntry.Product__c, aPriceEntry.SellingPrice__c);
                        }
                    }
                }
            }
        }

        return productToListPrice;
    }
    */

    private static void applyTiersForWrapper(LineItemWrapper wrapper, Decimal sellingPrice) {
        // in case of deleting a product from the list a unit price and list price can differ. In this case don't assign the selling price to the unit price
        if (wrapper.UnitPrice == wrapper.listPriceTierPricing) {
            wrapper.UnitPrice = sellingPrice;
        }
    }

    private static void applyTiersForWrapper(LineItemWrapper wrapper, PriceEntry__c aPriceEntry) {
        // in case of deleting a product from the list a unit price and list price can differ. In this case don't assign the selling price to the unit price
        if (wrapper.UnitPrice == wrapper.listPriceTierPricing) {
            wrapper.UnitPrice = aPriceEntry.SellingPrice__c;
        }
    }

    private static String defineStringScallingForDescription(Decimal scalingTo, Decimal sellingPrice) {
        return '≤ ' + String.valueOf(scalingTo.setScale(0)) + ': ' + Utils.decimalToCurrencyString(sellingPrice) + ' \n';
    }

    /**
    * @description Get the price entry for the context
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param priceEntryMap
    * @param productId
    * @param accountGroup
    * @param currencyIsoCode
    * @param billingCountryCode
    * @param discountPercentage
    * @param discountDisposables
    * @param discountServices
    * @return PriceEntry__c
    **/
    public static PriceEntry__c getCurrentPriceEntry(Map<String, PriceEntry__c> priceEntryMap,
                                                     Id productId,
                                                     String accountGroup,
                                                     String currencyIsoCode,
                                                     String billingCountryCode,
                                                     Decimal discountPercentage,
                                                     Decimal discountDisposables,
                                                     Decimal discountServices) {

        String key = '';
        String keyNoAccount = '|' + productId + '|' + currencyIsoCode + '|' + billingCountryCode;
        Boolean flagAccount = false;

        if(Utils.isDecimalNullOrZero(discountPercentage) && Utils.isDecimalNullOrZero(discountDisposables) && Utils.isDecimalNullOrZero(discountServices)) {
            if(accountGroup != null) {
                flagAccount = true;
                key = accountGroup.left(15) + '|' + productId + '|' + currencyIsoCode + '|' + billingCountryCode;
            } else {
                key = keyNoAccount;
            }
        } else {
            key = keyNoAccount;
        }

        System.debug(LoggingLevel.DEBUG, '[getCurrentPriceEntry] key : ' + key);
        System.debug(LoggingLevel.DEBUG, '[getCurrentPriceEntry] priceEntryMap : ' + priceEntryMap);
        System.debug(LoggingLevel.DEBUG, '[getCurrentPriceEntry] priceEntryMap.get(key) : ' + priceEntryMap.get(key));

        PriceEntry__c priceEntry = priceEntryMap.get(key);

        if(flagAccount && priceEntry == null) {
            priceEntry = priceEntryMap.get(keyNoAccount);
        }

        return priceEntry;
    }

    /**
    * @description Get the price entry for the context (List with one record)
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param priceEntryMap
    * @param productId
    * @param accountGroup
    * @param currencyIsoCode
    * @param billingCountryCode
    * @param discountPercentage
    * @param discountDisposables
    * @param discountServices
    * @return List<PriceEntry__c>
    *
    * @TODO: Change the Wrapper to have only one price entry.
    *
    **/
    public static List<PriceEntry__c> getCurrentPriceEntryList(Map<String, PriceEntry__c> priceEntryMap,
                                                     Id productId,
                                                     String accountGroup,
                                                     String currencyIsoCode,
                                                     String billingCountryCode,
                                                     Decimal discountPercentage,
                                                     Decimal discountDisposables,
                                                     Decimal discountServices) {

        PriceEntry__c priceEntry = getCurrentPriceEntry(priceEntryMap, productId, accountGroup, currencyIsoCode,
                                                        billingCountryCode, discountPercentage, discountDisposables, discountServices);

        if(priceEntry != null) {
            return new List<PriceEntry__c> { priceEntry };
        } else {
            return null;
        }
    }
}
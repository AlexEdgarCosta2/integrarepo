/*
*   @description Test class for Lead Trigger functionality.
*
*   @author ach
*   @copyright PARX
*/
@isTest
public with sharing class Test_LeadTriggerHandler
{
    @testSetup static void setupData()
    {
        AssignmentRule__c assignmentRule = TestUtil.createAssignment('US', 'CA', UserInfo.getUserId(), true);
    }

    /**
     *  @description test change owner action: check Currency.
     */
    static testMethod void testChangeOwner()
    {
        TriggerTemplateV2.setupUnitTest();
        Lead testLead = TestUtil.getLead();
        testLead.CurrencyIsoCode = 'CHF';
        insert testLead;

        Id pId = System.UserInfo.getProfileId();
        User user = new User(
            Alias = 'ekbf',
            Email = 'ekbf@digicomp.ch',
            EmailEncodingKey = 'UTF-8',
            LastName = 'EKBF',
            LanguageLocaleKey = 'de',
            LocaleSidKey = 'de_CH',
            ProfileId = pId,
            TimeZoneSidKey = 'Europe/Berlin',
            UserName='ekbf@digicomp.ch',
            DEFAULTCURRENCYISOCODE  = 'EUR'
        );
        insert user;

        Test.startTest();
        TriggerTemplateV2.startUnitTest();
        testLead.OwnerId = user.Id;
        update testLead;
        Test.stopTest();

        testLead = [SELECT Id, CurrencyIsoCode FROM Lead WHERE Id = :testLead.Id];
        System.assertEquals('EUR', testLead.CurrencyIsoCode, 'Wrong CurrencyIsoCode.');
    }
    
    static testMethod void TestDuplicateLead(){
        //create account
        Account A = TestUtil.getAccount();
        insert A;
        
        //create contact
        Contact C1 = TestUtil.getContact(A.id);
        insert C1;
        
        //create contact
        Contact C2 = TestUtil.getContact(A.id);
        C2.Email = 'test2@gmail.com';
        insert C2;
        
        //create lead
        Lead L = TestUtil.getLead();
        insert L;
        
        L.Email = 'test2@test.com';
        update L;
    }

    /**
     *  @description test lead convert.
     */
    static testMethod void testLeadConversion()
    {
        TriggerTemplateV2.setupUnitTest();
        Lead testLead = TestUtil.getLead();
        insert testLead;

        Test.startTest();
        TriggerTemplateV2.startUnitTest();
        // Create dummy conversion
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(testlead.id);
        lc.setConvertedStatus('Closed - Converted');
//      Database.LeadConvertResult lcr = Database.convertLead(lc);
        Test.stopTest();

        // Make sure conversion was successful
//      System.assert(lcr.isSuccess());

        // Test Contact "Status" field update
//      Opportunity anOpportunity = [SELECT Id, Contact__c FROM Opportunity LIMIT 1];
//      System.assert(anOpportunity.Contact__c != null,'Contact__c should not be null');
    }
}
/*
*   @description Opportunity Produkt picker functionality.
*
*   @author ach
*   @copyright PARX
*/
public with sharing class ProductPickerOpportunityController extends ProductPickerController
{
    public Opportunity opportunity{get;set;}
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Opportunity opp {get;set;}
    // initialize the controller
    public ProductPickerOpportunityController(ApexPages.StandardController controller)
    {
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        opp = (Opportunity)controller.getRecord();
    }
    // Added for Lightning functionality
    public ProductPickerOpportunityController(Id oppId)
    {
        this.opportunity = [
            SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, Contact__c,
            DiscountPercentage__c, DiscountDisposables__c, DiscountServices__c, ProductLanguage__c
            FROM Opportunity
            WHERE Id =: oppId
        ];
    }
    public override void initMainObject()
    {
        // Added for Lightning functionality
        if (this.opportunity == null && ApexPages.currentPage() != null) {
            this.opportunity = [
                SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, Contact__c, DiscountPercentage__c,
	                DiscountDisposables__c,DiscountServices__c, ProductLanguage__c
                FROM Opportunity
                WHERE Id = :(Id) ApexPages.currentPage().getParameters().get('id')
            ];
        }
    }
    public override Id getMainId()
    {
        return this.opportunity.Id;
    }

    public override String getMainCurrency()
    {
        return this.opportunity.CurrencyIsoCode;
    }

    public override String getLanguageForProducts()
    {
	    return this.opportunity.ProductLanguage__c != null ? this.opportunity.ProductLanguage__c : '';
    }

    public override String getMainBillingCountryCode()
    {
        return this.opportunity.Account.BillingCountryCode;
    }

    public override Id getMainAccountGroup()
    {
        return this.opportunity.Account.AccountGroup__c;
    }

    protected override void applyAdditionalConditions(AndCondition andCondition)
    {
        andCondition.add(new FieldCondition('DoNotShowOnOpportunity__c', Operator.NOT_EQUALS, true));
    }

    public override Decimal getOverallDiscount()
    {
        return this.opportunity.DiscountPercentage__c;
    }
    public override Decimal getDisposablesDiscount()
    {
        return this.opportunity.DiscountDisposables__c;
    }
    public override Decimal getServicesDiscount()
    {
        return this.opportunity.DiscountServices__c;
    }

    /**
    *   @description add products to the Database.
    */
    public override PageReference doAddProducts()
    {
        System.debug('doAddProducts');
        Set<Id> productIds = new Set<Id>();
        for (LineItemWrapper wrapper: selectedLineItemWrappers)
        {
            productIds.add(wrapper.product.id);
        }

        Map<Id, PricebookEntry> productForPricebookEntry = new Map<Id, PricebookEntry>();
        for (PricebookEntry p : [SELECT Id, Product2Id, UnitPrice FROM PricebookEntry WHERE
                Product2Id IN : productIds AND CurrencyIsoCode = :getMainCurrency()])
        {
            productForPricebookEntry.put(p.Product2Id, p);
        }

        List<OpportunitylineItem> anItems = new List<OpportunitylineItem>();
        for (LineItemWrapper wrapper: selectedLineItemWrappers)
        {
            OpportunitylineItem anItem = wrapper.item;
            System.debug('getMainId');
            System.debug(getMainId());
            anItem.OpportunityId = getMainId();
            anItem.PricebookEntryId = productForPricebookEntry.get(wrapper.product.id).id;
            anItem.UnitPrice = wrapper.unitPrice;
            anItem.Quantity = wrapper.qty;
            anItem.ListPriceTierPricing__c = wrapper.listPriceTierPricing;
            anItems.add(anItem);
        }
        insert anItems;
        System.debug('anItems');
        System.debug(anItems);
        return redirectToPreviousPage();
    }
    public override PageReference cloneOpportunityWithLineItem()
    {
        return redirectToPreviousPage();
    }
    public override PageReference reCalculateProductPrice()
    {
        return redirectToPreviousPage();
    }
}
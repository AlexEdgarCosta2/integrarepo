@IsTest
public with sharing class Test_RecalculateProductPriceController {

    public static Opportunity testOpportunity;
    public static Quote testQuote;

    @TestSetup
    private static void initData()
    {
        AccountGroup__c anAccountGroup = new AccountGroup__c(Name = 'asd');
        insert anAccountGroup;

        Account anAccount = new Account(Name = 'asd account', AccountGroup__c = anAccountGroup.Id, BillingCountryCode = 'US', BillingState = 'California');
        insert anAccount;

        List<Product2> aProducts = new List<Product2>{
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductNameDE__c = 'test 1 de', ProductNameFR__c = 'test 1 FR', ProductFamily__c = 'f1', PricingGroup__c = '1',
                ProductLanguage__c = 'English'),
            new Product2(Name = 'inactive product', IsActive = false, ProductCode = 'test_asd2', ProductNameDE__c = 'test de ', ProductNameFR__c = 'test FR', ProductFamily__c = 'f1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductNameDE__c = 'test 2 de', ProductNameFR__c = 'test 2 FR', ProductFamily__c = 'f1', PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductNameDE__c = 'test 3 de', ProductNameFR__c = 'test 3 FR', ProductFamily__c = 'f2', PricingGroup__c = '2')};
        insert aProducts;

        PriceBook2 pb22=new PriceBook2();
        pb22.Name = 'Standardpreisbuch';
        pb22.IsActive = true;
        insert pb22;

        Id standardPriceBookId = Test.getStandardPricebookId();

        List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
            new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
        insert markets;

        List<PriceEntry__c> aPriceEntries = new List<PriceEntry__c>{//CurrencyIsoCode='USD',
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 10, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today(), Market__c = markets[0].Id),
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 9, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today().addDays(-2), Market__c = markets[0].Id),
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 100, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today(), Market__c = markets[1].Id),
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 99999, SellingPrice__c = 100, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today().addDays(2), Market__c = markets[0].Id),
            new PriceEntry__c(Product__c = aProducts.get(2).Id, ScalingTo__c = 6, SellingPrice__c = 200,
                ScalingTo2__c = 999999, DiscountTier2__c = 25,
                ValidFrom__c = date.today(), Market__c = markets[0].Id),
            new PriceEntry__c(Product__c = aProducts.get(3).Id, ScalingTo__c = 3, SellingPrice__c = 600,
                ScalingTo2__c = 999999, DiscountTier2__c = 10, //540, not 500
                ValidFrom__c = date.today(), Market__c = markets[0].Id),
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 1, SellingPrice__c = 20, ValidFrom__c = date.today(), Market__c = markets[0].Id)};
        insert aPriceEntries;

        PricebookEntry pbe2 = new Pricebookentry(IsActive = true, Product2id = aProducts.get(0).Id, Pricebook2Id = standardPriceBookId, Unitprice = 4.99, CurrencyIsoCode = 'USD');
        insert pbe2;

        //PricebookEntry pbe2 =new PricebookEntry(UnitPrice=30,Product2Id=aProducts.get(0).Id,Pricebook2Id=pb22.Id,isActive=true,CurrencyIsoCode = 'USD',UseStandardPrice = True);//
        //insert pbe2;

        testOpportunity = new Opportunity(DiscountPercentage__c = 10, DiscountDisposables__c = 10, DiscountServices__c = 10,
            Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert',
            CloseDate = date.today(), CurrencyIsoCode = 'USD');
        insert testOpportunity;
        List<OpportunityLineItem> anItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(Product2Id = aProducts.get(0).Id, OpportunityId = testOpportunity.Id,Quantity = 1000000, PricebookEntryId=pbe2.Id,
                UnitPrice = 30.00, ListPriceTierPricing__c = 30.00, ListPriceOneItem__c=40.00),
            new OpportunityLineItem(Product2Id = aProducts.get(0).Id, OpportunityId = testOpportunity.Id,Quantity = 1000000, PricebookEntryId=pbe2.Id,
                UnitPrice = 30.00, ListPriceTierPricing__c = 30.00, ListPriceOneItem__c=40.00),
            new OpportunityLineItem(Product2Id = aProducts.get(0).Id, OpportunityId = testOpportunity.Id,Quantity = 1000000, PricebookEntryId=pbe2.Id,
                UnitPrice = 30.00, ListPriceTierPricing__c = 30.00, ListPriceOneItem__c=40.00)
        };
        insert anItems;

        testQuote = new Quote(
            Name = 'Test Quote',
            OpportunityId = testOpportunity.Id,
            DiscountProducts__c = 20,
            DiscountDisposables__c = 20,
            DiscountServices__c = 20
        );
        insert testQuote;
    }

    /*
    *   @description test main logic.
    */
    @IsTest
    static void testMainLogic_Opportunity()
    {
        testOpportunity = [
            SELECT Id, DiscountDisposables__c
            FROM Opportunity
            LIMIT 1
        ];
        System.debug('#### testOpportunity '+ testOpportunity);
        PageReference aPage = Page.reCalculateProductPriceQuote;
        aPage.getParameters().put('id', testOpportunity.Id);
        Test.setCurrentPage(aPage);

        Test.startTest();

        ApexPages.StandardController sc = new ApexPages.standardController(testOpportunity);
        RecalculateProductPriceController  aController = new RecalculateProductPriceController (sc);

        List<OpportunitylineItem> anItems = [
            SELECT Id,Product2Id, OpportunityId, Quantity, UnitPrice, PricebookEntryId,
                ListPriceTierPricing__c, ListPriceOneItem__c
            FROM OpportunityLineItem
            WHERE OpportunityId = :testOpportunity.Id
        ];

        System.assertEquals(3, anItems.size(), 'wrong number of line items');
        aController.reCalculateProductPrice();

        testOpportunity = [
            SELECT Id, DiscountPercentage__c, DiscountDisposables__c, DiscountServices__c
            FROM Opportunity
            WHERE Id = :testOpportunity.Id
            LIMIT 1
        ];

        System.assertNotEquals(0, testOpportunity.DiscountPercentage__c , 'Percentage Discount not updated');
        System.assertNotEquals(0, testOpportunity.DiscountDisposables__c , 'Disposables Discount not updated');
        System.assertNotEquals(0, testOpportunity.DiscountServices__c , 'Services Discount not updated');

        Test.stopTest();
    }

    /*
    *   @description test main logic.
    */
    @IsTest
    static void testMainLogic_Quote()
    {
        testQuote = [
            SELECT Id
            FROM Quote
            LIMIT 1
        ];

        PageReference aPage = Page.reCalculateProductPriceQuote;
        aPage.getParameters().put('id', testQuote.Id);
        Test.setCurrentPage(aPage);

        Test.startTest();

        ApexPages.StandardController sc = new ApexPages.standardController(testQuote);
        RecalculateProductPriceController  aController = new RecalculateProductPriceController (sc);

        List<QuoteLineItem> lineItems = [
            SELECT Id, UnitPrice, PricebookEntryId,
                ListPriceTierPricing__c, ListPriceOneItem__c
            FROM QuoteLineItem
            WHERE QuoteId = :testQuote.Id
        ];

        //System.assertEquals(3, lineItems.size(), 'wrong number of line items');
        aController.reCalculateProductPrice();

        testQuote = [
            SELECT Id, DiscountDisposables__c, DiscountProducts__c, DiscountServices__c
            FROM Quote
        ];

        System.assertEquals(0, testQuote.DiscountDisposables__c , 'Disposables Discount has not been updated');
        System.assertEquals(0, testQuote.DiscountProducts__c , 'Products Discount has not been updated');
        System.assertEquals(0, testQuote.DiscountServices__c , 'Services Discount has not been updated');

        Test.stopTest();
    }
}
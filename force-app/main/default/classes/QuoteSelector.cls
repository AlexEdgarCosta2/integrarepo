/**
 * @description       : Selector class for Quotes
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class QuoteSelector {
    /**
    * @description Get Quote by Id
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param oppId
    * @return Quote
    **/
    public static Quote getQuoteById(String oppId) {
        List<Quote> quoteList = [SELECT Id, Name, DiscountProducts__c, DiscountServices__c, DiscountDisposables__c,
                                      CurrencyIsoCode, Quote.Account.BillingCountryCode, Quote.AccountId,
                                      Account_Group__c
                                 FROM Quote WHERE Id =: oppId];
        if(quoteList.isEmpty()) {
            return null;
        } else {
            return quoteList[0];
        }
    }

    /**
    * @description Get Quotes by Ids
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param oppIds
    * @return List<Quote>
    **/
    public static List<Quote> getQuotesByIds(Set<Id> oppIds) {
        return [SELECT Id, Name, DiscountProducts__c, DiscountServices__c, DiscountDisposables__c,
                       CurrencyIsoCode, Quote.Account.BillingCountryCode, Quote.AccountId, Opportunity.Owner.Name,
                       Quote.Account.Name, Quote.Owner.Name, Approval_Status__c, Account_Group__c, Approvers__c
                  FROM Quote WHERE Id IN : oppIds];
    }
}
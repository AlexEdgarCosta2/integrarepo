/**
 * @description       : Selector for ProcessInstance
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class ProcessInstanceSelector {
    /**
    * @description Get by Id
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param recordId
    * @return ProcessInstance[]
    **/
    public static ProcessInstance[] getPendingById(Id recordId) {
        return [Select ID, Status, TargetObject.Name,
            (SELECT Id, ActorId, ProcessInstanceId FROM Workitems),
            (SELECT Id, StepStatus, Comments FROM Steps) From ProcessInstance
            Where TargetObjectID =: recordId AND Status = 'Pending'];
    }
}

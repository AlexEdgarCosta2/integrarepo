/*
*   @description Test class for OpportunityQuoteHelper functionality.
*
*   @author swapnil gadkari
*   @copyright PARX
*/
@isTest
public with sharing class Test_OpportunityQuoteHelper {
    
    static testMethod void testOverallDiscountsOpportunity()
    {
        TriggerTemplateV2.setupUnitTest();
        Account anAccount = TestUtil.getAccount();
        insert anAccount;

        Product2 prod1 = new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductNameDE__c = 'test 1 de', 
                ProductNameFR__c = 'test 1 FR', ProductFamily__c = 'f1', PricingGroup__c = '1', ProductLanguage__c = 'English', ProductType__c = 'Products and Accessories');
        insert prod1;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry entry = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod1.Id,
                UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');
        insert entry;
        
        Opportunity testOpportunity1 = new Opportunity(Name = 'asd1', AccountId = anAccount.Id, StageName = 'Interessiert', 
                CloseDate = date.today(), CurrencyIsoCode = 'USD');
        insert testOpportunity1;
        Opportunity testOpportunity2 = new Opportunity(Name = 'asd2', AccountId = anAccount.Id, StageName = 'Interessiert', 
                CloseDate = date.today(), CurrencyIsoCode = 'USD');
        
        Test.startTest();

        insert testOpportunity2;
        List<Opportunity> opps = [SELECT Id, Name, CurrencyIsoCode, StageName  FROM Opportunity];

        List<OpportunitylineItem> lineItems = new List<OpportunitylineItem>{new OpportunitylineItem(OpportunityId = testOpportunity1.Id,
                ListPriceOneItem__c = 100, PricebookEntryId = entry.Id, UnitPrice =  90, ListPriceTierPricing__c = 90, Quantity = 10),
                new OpportunitylineItem(OpportunityId = testOpportunity2.Id,
                ListPriceOneItem__c = 100, PricebookEntryId = entry.Id, UnitPrice =  90, ListPriceTierPricing__c = 90, Quantity = 10)
        };
        insert lineItems;
        
        TriggerTemplateV2.startUnitTest();
        testOpportunity1.DiscountPercentage__c = 20;
        update new List<Opportunity>{testOpportunity1, testOpportunity2};
        OpportunitylineItem line1 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM OpportunitylineItem WHERE OpportunityId = :testOpportunity1.Id];
        System.assertEquals(80, line1.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertEquals(80, line1.UnitPrice, 'Wrong UnitPrice value');

        OpportunitylineItem line2 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM OpportunitylineItem WHERE OpportunityId = :testOpportunity2.Id];
        System.assertEquals(90, line2.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertEquals(90, line2.UnitPrice, 'Wrong UnitPrice value');

        testOpportunity1.DiscountPercentage__c = 1;
        testOpportunity2.DiscountPercentage__c = 30;
        update new List<Opportunity>{testOpportunity1, testOpportunity2};
        line1 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM OpportunitylineItem WHERE OpportunityId = :testOpportunity1.Id];
        System.assertNotEquals(80, line1.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertNotEquals(80, line1.UnitPrice, 'Wrong UnitPrice value');

        line2 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM OpportunitylineItem WHERE OpportunityId = :testOpportunity2.Id];
        System.assertEquals(70, line2.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertEquals(70, line2.UnitPrice, 'Wrong UnitPrice value');

        testOpportunity2.DiscountPercentage__c = 40;
        update new List<Opportunity>{testOpportunity1, testOpportunity2};


        Test.stopTest();
        line1 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM OpportunitylineItem WHERE OpportunityId = :testOpportunity1.Id];
        System.assertNotEquals(80, line1.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertNotEquals(80, line1.UnitPrice, 'Wrong UnitPrice value');

        line2 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM OpportunitylineItem WHERE OpportunityId = :testOpportunity2.Id];
        System.assertEquals(60, line2.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertEquals(60, line2.UnitPrice, 'Wrong UnitPrice value');
    }

    static testMethod void testOverallDiscountsQuote()
    {
        TriggerTemplateV2.setupUnitTest();
        Account anAccount = TestUtil.getAccount();
        insert anAccount;

        Product2 prod1 = new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductNameDE__c = 'test 1 de', 
                ProductNameFR__c = 'test 1 FR', ProductFamily__c = 'f1', PricingGroup__c = '1', ProductLanguage__c = 'English', ProductType__c = 'Products and Accessories');
        insert prod1;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry entry = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod1.Id,
                UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');
        insert entry;
        
        Opportunity testOpportunity1 = new Opportunity(Name = 'asd1', AccountId = anAccount.Id, StageName = 'Interessiert', 
                CloseDate = date.today(), CurrencyIsoCode = 'USD');
        insert testOpportunity1;
        Opportunity testOpportunity2 = new Opportunity(Name = 'asd2', AccountId = anAccount.Id, StageName = 'Interessiert', 
                CloseDate = date.today(), CurrencyIsoCode = 'USD');
        insert testOpportunity2;

        Quote quote1 = new Quote(Name = 'quote1', OpportunityId = testOpportunity1.Id, DiscountProducts__c = 20, Pricebook2Id = pricebookId);
        insert quote1;
        Quote quote2 = new Quote(Name = 'quote1', OpportunityId = testOpportunity2.Id, DiscountProducts__c = 20, Pricebook2Id = pricebookId);
        insert quote2;

        Test.startTest();

        List<QuoteLineItem> lineItems = new List<QuoteLineItem>{new QuoteLineItem(QuoteId = quote1.Id,
                ListPriceOneItem__c = 100, PricebookEntryId = entry.Id, UnitPrice =  90, ListPriceTierPricing__c = 90, Quantity = 10),
                new QuoteLineItem(QuoteId = quote2.Id,
                ListPriceOneItem__c = 100, PricebookEntryId = entry.Id, UnitPrice =  90, ListPriceTierPricing__c = 90, Quantity = 10)
        };
        insert lineItems;
        
        TriggerTemplateV2.startUnitTest();
        quote1.DiscountProducts__c = 20;
        update new List<Opportunity>{testOpportunity1, testOpportunity2};
        QuoteLineItem line1 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM QuoteLineItem WHERE QuoteId = :quote1.Id];
        System.assertNotEquals(80, line1.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertNotEquals(80, line1.UnitPrice, 'Wrong UnitPrice value');

        QuoteLineItem line2 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM QuoteLineItem WHERE QuoteId = :quote2.Id];
        System.assertEquals(90, line2.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertEquals(90, line2.UnitPrice, 'Wrong UnitPrice value');

        TriggerTemplateV2.startUnitTest();
        quote1.DiscountProducts__c = 1;
        quote2.DiscountProducts__c = 30;
        update new List<Quote>{quote1, quote2};
        line1 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM QuoteLineItem WHERE QuoteId = :quote1.Id];
        System.assertNotEquals(80, line1.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertNotEquals(80, line1.UnitPrice, 'Wrong UnitPrice value');

        line2 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM QuoteLineItem WHERE QuoteId = :quote2.Id];
        System.assertEquals(70, line2.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertEquals(70, line2.UnitPrice, 'Wrong UnitPrice value');

        TriggerTemplateV2.startUnitTest();
        quote2.DiscountProducts__c = 40;
        update new List<Quote>{quote1, quote2};

        Test.stopTest();
        line1 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM QuoteLineItem WHERE QuoteId = :quote1.Id];
        System.assertNotEquals(80, line1.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertNotEquals(80, line1.UnitPrice, 'Wrong UnitPrice value');

        line2 = [SELECT Id, UnitPrice, ListPriceTierPricing__c FROM QuoteLineItem WHERE QuoteId = :quote2.Id];
        System.assertEquals(60, line2.ListPriceTierPricing__c, 'Wrong ListPriceTierPricing__c value');
        System.assertEquals(60, line2.UnitPrice, 'Wrong UnitPrice value');
    }
}
/**
 * @description Batch that updates currency code of leads after completion of assignment rules.
 *				This batch is needed as a workaroud, because assigment rules executed after all triggers.
 *
 * @author eru,ach,akr
 * @copyright PARX
 */
public class ChangeLeadCurrencyBatch implements Database.Batchable<sObject>
{

	/**
	 * @description prepares the query to get updated leads.
	 */
	public Database.QueryLocator start(Database.BatchableContext context)
	{
		DateTime aDate = DateTime.now();
		aDate = aDate.addHours(-24);
		String query = 'SELECT CurrencyIsoCode, OwnerId FROM Lead WHERE CreatedDate >=:aDate';
		return Database.getQueryLocator(query);
	}

	/**
	 * @description executes update of currency code based on owner's currency code.
	 */
   	public void execute(Database.BatchableContext context, List<Lead> scope)
   	{
   		LeadService.updateCurrencyAndExecuteUpdate(scope);
	}

	/**
	 * @description all fulctionality is processed in execute method.
	 */
	public void finish(Database.BatchableContext context)
	{
		//DO NOTHING
	}

}
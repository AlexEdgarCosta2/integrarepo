public with sharing class OpportunityQuoteHelper {
	public static List<String> customLineItemFieldsToSync = new List<String>{
			'UnitPrice',
			'Quantity',
			'PricebookEntryId',
			'ListPriceTierPricing__c',
			'ListPriceOneItem__c',
			'SortOrder__c',
			'Description',
			'DemoEquipment__c'
	};

	public static Boolean isSyncedFromOpportunity = false;
	public static Boolean isSyncedFromQuote = false;

	public static void updateLineItemsWithOverallDiscount(List<sObject> newValues, Map<Id, sObject> oldValues){
		List<String> recordFields = new List<String>{'DiscountServices__c','DiscountDisposables__c'};
		List<String> lineItemFields = new List<String>{'Id','ProductType__c', 'ListPriceOneItem__c, ListPriceTierPricing__c, UnitPrice'};
		alParx.SoqlBuilder pbeQuery;
		Boolean isOpportunity = false;
		Boolean isQuote = false;

		if (String.valueOf(newValues[0].Id).startsWith('0Q0')) {
			isQuote = true;
		} else if (String.valueOf(newValues[0].Id).startsWith('006')) {
			isOpportunity = true;
		}

		if (isQuote) {
			recordFields.add('DiscountProducts__c');

			lineItemFields.add('QuoteId');
			pbeQuery = new alParx.SoqlBuilder()
					.selectx(lineItemFields)
					.fromx('QuoteLineItem');

		} else if (isOpportunity) {
			recordFields.add('DiscountPercentage__c');

			lineItemFields.add('OpportunityId');
			pbeQuery = new alParx.SoqlBuilder()
					.selectx(lineItemFields)
					.fromx('OpportunityLineItem');
		}

		Set<SObject> aModifiedObjects = TriggerUtils.getModifiedObjectsObj(recordFields, newValues,
				oldValues);

		system.debug('aModifiedObjects :: '+aModifiedObjects);

		if (aModifiedObjects.size() > 0)
		{
			Map<Id, SObject> allNewOpps = new Map<Id, SObject>(newValues);
			Set<Id> objIds = new Map<Id, SObject>(new List<SObject>(aModifiedObjects)).keySet();
			List<SObject> lineItemsToUpdate	 = new List<SObject>();

			System.debug('pbequery :: '+pbeQuery);
			if(isQuote){
				pbeQuery = pbeQuery.wherex(new alParx.SetCondition('QuoteId',alParx.Operator.INX, new List<Id>(objIds)));
			}
			else if(isOpportunity) {
				pbeQuery = pbeQuery.wherex(new alParx.SetCondition('OpportunityId',alParx.Operator.INX, new List<Id>(objIds)));
			}
			System.debug('pbequery :: '+pbeQuery);

			List<SObject> lineItems = Database.query(pbeQuery.toSoql());

			for (SObject item : lineItems)
			{
				if (isQuote) {
					Quote qte = (Quote)allNewOpps.get(((QuoteLineItem)item).QuoteId);
					QuoteLineItem qteli = (QuoteLineItem)item;

					Decimal newListPriceTierPricing;
					Decimal newUnitPrice;

					if (qte.DiscountProducts__c != null && qteli.ProductType__c =='Products and Accessories' && qteli.ListPriceOneItem__c != null)
					{
						newListPriceTierPricing = (qteli.ListPriceOneItem__c * (1 - 0.01 * qte.DiscountProducts__c)).setScale(2);
						newUnitPrice = (qteli.ListPriceOneItem__c * (1 - 0.01 * qte.DiscountProducts__c)).setScale(2);
					}
					if (qte.DiscountServices__c != null && qteli.ProductType__c =='Services' && qteli.ListPriceOneItem__c != null)
					{
						newListPriceTierPricing = (qteli.ListPriceOneItem__c * (1 - 0.01 * qte.DiscountServices__c)).setScale(2);
						newUnitPrice = (qteli.ListPriceOneItem__c * (1 - 0.01 * qte.DiscountServices__c)).setScale(2);
					}
					if (qte.DiscountDisposables__c != null && qteli.ProductType__c =='Disposables' && qteli.ListPriceOneItem__c != null)
					{
						newListPriceTierPricing = (qteli.ListPriceOneItem__c * (1 - 0.01 * qte.DiscountDisposables__c)).setScale(2);
						newUnitPrice = (qteli.ListPriceOneItem__c * (1 - 0.01 * qte.DiscountDisposables__c)).setScale(2);
					}

					if (
							newListPriceTierPricing != null &&
									newUnitPrice != null &&
									(
											qteli.ListPriceTierPricing__c != newListPriceTierPricing ||
													qteli.UnitPrice != newUnitPrice
									)
							) {
						qteli.ListPriceTierPricing__c =  newListPriceTierPricing;
						qteli.UnitPrice = newUnitPrice;
						lineItemsToUpdate.add(qteli);
					}

					isSyncedFromQuote = true;
				}
				else if (isOpportunity) {
					Opportunity opp = (Opportunity)allNewOpps.get(((OpportunityLineItem)item).OpportunityId);
					OpportunityLineItem oppli = (OpportunityLineItem)item;

					Decimal newListPriceTierPricing;
					Decimal newUnitPrice;
					if (opp.DiscountPercentage__c != null && oppli.ProductType__c =='Products and Accessories' && oppli.ListPriceOneItem__c != null)
					{
						newListPriceTierPricing =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountPercentage__c)).setScale(2);
						newUnitPrice =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountPercentage__c)).setScale(2);
					}
					if (opp.DiscountServices__c != null && oppli.ProductType__c =='Services' && oppli.ListPriceOneItem__c != null)
					{
						newListPriceTierPricing =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountServices__c)).setScale(2);
						newUnitPrice = (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountServices__c)).setScale(2);
					}
					if (opp.DiscountDisposables__c != null && oppli.ProductType__c =='Disposables' && oppli.ListPriceOneItem__c != null)
					{
						newListPriceTierPricing =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountDisposables__c)).setScale(2);
						newUnitPrice =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountDisposables__c)).setScale(2);
					}

					/*if (opp.DiscountPercentage__c != null && oppli.ProductType__c =='Products and Accessories' && oppli.ListPriceOneItem__c != null)
                    {
                        oppli.ListPriceTierPricing__c =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountPercentage__c)).setScale(2);
                        oppli.UnitPrice =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountPercentage__c)).setScale(2);
                        lineItemsToUpdate.add(oppli);
                    }
                    if (opp.DiscountServices__c != null && oppli.ProductType__c =='Services' && oppli.ListPriceOneItem__c != null)
                    {
                        oppli.ListPriceTierPricing__c =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountServices__c)).setScale(2);
                        oppli.UnitPrice =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountServices__c)).setScale(2);
                        lineItemsToUpdate.add(oppli);
                    }
                    if (opp.DiscountDisposables__c != null && oppli.ProductType__c =='Disposables' && oppli.ListPriceOneItem__c != null)
                    {
                        oppli.ListPriceTierPricing__c =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountDisposables__c)).setScale(2);
                        oppli.UnitPrice =  (oppli.ListPriceOneItem__c * (1 - 0.01 * opp.DiscountDisposables__c)).setScale(2);
                        lineItemsToUpdate.add(oppli);
                    }*/

					if (
							newListPriceTierPricing != null &&
									newUnitPrice != null &&
									(
											oppli.ListPriceTierPricing__c != newListPriceTierPricing ||
													oppli.UnitPrice != newUnitPrice
									)
							) {
						oppli.ListPriceTierPricing__c =  newListPriceTierPricing;
						oppli.UnitPrice = newUnitPrice;
						lineItemsToUpdate.add(oppli);
					}

					isSyncedFromOpportunity = true;
				}
			}

			if (!lineItemsToUpdate.isEmpty()) {
				update lineItemsToUpdate;
			}
		}
	}
}
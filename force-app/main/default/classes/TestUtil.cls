/**
 * @description       : Test data factory
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
@isTest
public class TestUtil {

    public static AccountGroup__c getAccountGroup(String name) {
        return new AccountGroup__c(Name = name);
    }

    public static AccountGroup__c getAccountGroup(String name, String currencyIsoCode) {
        return new AccountGroup__c(Name = name, CurrencyIsoCode = currencyIsoCode);
    }

    public static Account getAccount() {
        Account a = new Account(name = 'Test Account', BillingCountryCode='US', BillingState='California', BillingStateCode='CA', CurrencyIsoCode = 'USD');
        return a;
    }

    public static Account getAccount(String name,
                                     Id accountGroupId,
                                     String billingCountryCode,
                                     String billingState) {
        return new Account(Name = name, AccountGroup__c = accountGroupId, BillingCountryCode = billingCountryCode, BillingStateCode = billingState);
    }

    public static Account getAccount(String name,
                                     Id accountGroupId,
                                     String billingCountryCode) {
        return new Account(Name = name, AccountGroup__c = accountGroupId, BillingCountryCode = billingCountryCode);
    }

    public static List<Account> selectAccount() {
        return [SELECT Id, Name, AccountGroup__c, BillingCountryCode, BillingState FROM Account LIMIT 100];
    }

    public static lead getLead() {
        Lead L = new Lead(FirstName = 'Test', LastName = 'test', email = 'test@test.com', Company = 'test company', Country = 'United States', CountryCode='US', State='California', StateCode='CA', PostalCode='94001');
        return L;
    }

    public static Contact getContact(id AccID) {
        Contact contactInstance = new Contact(AccountID = AccID, Email = 'test@test.com', LastName = 'test',
                                MailingCountry = 'United States',
                                MailingState = 'California',
                                Preferred_Customer_Language__c = Constants.CONTACT_PREFERRED_CUSTOMER_LANGUAGE_ENGLISH);
        return contactInstance;
    }

    public static Opportunity getOpportunity(id AccID) {
        Opportunity o = new Opportunity(name = 'test opp', CloseDate = date.today(), stagename = 'Prospect', AccountID = AccID, CurrencyIsoCode = 'CHF');
        return o;
    }


    public static Opportunity getOpportunity(String name, id accountId, String stageName, Date closeDate, String currencyIsoCode,
                                             Decimal discountPercentage, Decimal discountDisposables, Decimal discountServices) {
        return new Opportunity(Name = name, AccountId = accountId, StageName = stageName, CloseDate = closeDate, CurrencyIsoCode = currencyIsoCode,
                               DiscountPercentage__c = discountPercentage, DiscountDisposables__c = discountDisposables, DiscountServices__c = discountServices);
    }

    public static List<Opportunity> selectAllOpportunity() {
        return [SELECT Id, Name, StageName, CloseDate, CurrencyIsoCode, DiscountPercentage__c, DiscountDisposables__c,
                       DiscountServices__c, ProductLanguage__c, Account.BillingCountryCode,
                       Account.AccountGroup__c
                  FROM Opportunity LIMIT 20];
    }

    public static OpportunityLineItem getOpportunityLineItem(Id pricebookEntryId, Id opportunityId, id productId, Decimal quantity, Decimal totalPrice) {
        return new OpportunityLineItem(PricebookEntryId = pricebookEntryId,
            OpportunityId = opportunityId,
            Product2Id = productId,
            Quantity = quantity,
            TotalPrice = totalPrice);
    }

    public static List<OpportunityLineItem> selectAllOpportunityLineItems() {
        return [SELECT Id, OpportunityId, Product2Id, Product2.Name, Product2.ProductType__c, Quantity, UnitPrice FROM OpportunityLineItem
                 LIMIT 20];
    }

    public static Quote getQuote(id opportunityId) {
        Quote q = new Quote(name = 'test quote', OpportunityId = opportunityId, CurrencyIsoCode = 'CHF');
        return q;
    }

    public static Quote getQuote(String name, Id opportunityId, String currencyIsoCode,
                                       Decimal discountPercentage, Decimal discountDisposables, Decimal discountServices) {
        return new Quote(Name = name, OpportunityId = opportunityId, CurrencyIsoCode = currencyIsoCode,
                               DiscountProducts__c = discountPercentage, DiscountDisposables__c = discountDisposables,
                               DiscountServices__c = discountServices);
    }

    public static List<Quote> selectAllQuote() {
        return [SELECT Id, Name, CurrencyIsoCode, DiscountProducts__c, DiscountDisposables__c,
                       DiscountServices__c, Approval_Status__c, Approval_Type__c,
                       ProductLanguage__c, Account.BillingCountryCode, Account.AccountGroup__c
                  FROM Quote LIMIT 20];
    }

    public static QuoteLineItem getQuoteLineItem(Id pricebookEntryId, Id quoteId, id productId, Decimal quantity, Decimal unitPrice) {
        return new QuoteLineItem(PricebookEntryId = pricebookEntryId,
            QuoteId = quoteId,
            Product2Id = productId,
            Quantity = quantity,
            UnitPrice = unitPrice);
    }

    public static List<QuoteLineItem> selectAllQuoteLineItems() {
        return [SELECT Id, QuoteId, Product2Id, Product2.Name, Product2.ProductType__c, Quantity, UnitPrice, TotalPrice FROM QuoteLineItem
                 LIMIT 20];
    }

    public static Product2 getProduct() {
        Product2 p = new Product2(Name = 'Test Prod', ProductCode = 'ABCD', IsActive = true);
        return p;
    }

    public static Product2 getProduct(String name,
                                      String productCode,
                                      String productType,
                                      String pricingGroup) {
        return new Product2(Name = name, IsActive = true, ProductCode = productCode, ProductType__c = productType, PricingGroup__c =pricingGroup);
    }

    public static Product2 getProduct(String name,
                                      String productCode,
                                      String productType,
                                      String pricingGroup,
                                      String productFamily,
                                      String productFamilyNumber) {
        return new Product2(Name = name, IsActive = true, ProductCode = productCode, ProductType__c = productType, PricingGroup__c =pricingGroup,
            ProductFamily__c = productFamily, ProductFamilyNumber__c = productFamilyNumber);
    }

    public static List<Product2> selectProduct() {
        return [SELECT Id, Name, ProductType__c, PricingGroup__c, ProductCode, ProductFamily__c, ProductFamilyNumber__c
                  FROM Product2 LIMIT 100];
    }

    public static PricebookEntry getPricebookEntry(id ProdID, id PricebookID) {
        PricebookEntry PBE = new PricebookEntry(Pricebook2id = PricebookID , Product2id = ProdID, UnitPrice = 100, isActive = true);
        return PBE;
    }

    public static PricebookEntry getPricebookEntry(id pricebookID, id prodID, Decimal unitPrice, String currencyIsoCode) {
        return new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prodId, UnitPrice = unitPrice, IsActive = true, CurrencyIsoCode = currencyIsoCode);
    }

    public static PriceEntry__c getPriceEntry(Id productId, Decimal sellingPrice, String currencyIsoCode,
                                              Decimal scalingTo, Decimal scalingTo2, Decimal scalingTo3,
                                              Decimal discountTier2, Decimal discountTier3,
                                              Id marketId) {
        return new PriceEntry__c (
            Product__c =  productId,
            SellingPrice__c = sellingPrice,
            CurrencyIsoCode = currencyIsoCode,
            ScalingTo__c = scalingTo,
            ScalingTo2__c = scalingTo2,
            ScalingTo3__c = scalingTo3,
            DiscountTier2__c = discountTier2,
            DiscountTier3__c = discountTier3,
            Market__c = marketId
        );
    }

    public static PriceEntry__c getPriceEntry(Id productId, Decimal sellingPrice, String currencyIsoCode,
                                              Decimal scalingTo, Decimal scalingTo2, Decimal scalingTo3,
                                              Decimal discountTier2, Decimal discountTier3,
                                              Id marketId, Id accountGroup) {
        return new PriceEntry__c (
            Product__c =  productId,
            SellingPrice__c = sellingPrice,
            CurrencyIsoCode = currencyIsoCode,
            ScalingTo__c = scalingTo,
            ScalingTo2__c = scalingTo2,
            ScalingTo3__c = scalingTo3,
            DiscountTier2__c = discountTier2,
            DiscountTier3__c = discountTier3,
            Market__c = marketId,
            AccountGroup__c = accountGroup
        );
    }

    public static List<PriceEntry__c> selectPriceEntry() {
        return [SELECT id, Product__c, SellingPrice__c, CurrencyIsoCode, ScalingTo__c, ScalingTo2__c, ScalingTo3__c, DiscountTier2__c, DiscountTier3__c, Market__c
                  FROM PriceEntry__c LIMIT 100];
    }

    public static Market__c getMarket(String marketCode, String countryList) {
        return new Market__c(MarketCode__c = marketCode, CountryList__c = countryList);
    }

    public static Market__c getMarket(String marketName, String marketCode, String countryList) {
        return new Market__c(Name = marketName, MarketCode__c = marketCode, CountryList__c = countryList);
    }

    public static List<Market__c> selectMarkets() {
        return [SELECT id, Name, MarketCode__c, CountryList__c FROM Market__c];
    }

    public static OpportunityLineItem getOLIs(id OppID, id PEID) {
        OpportunityLineItem OLI = new OpportunityLineItem(OpportunityID = OppID, Quantity = 5, TotalPrice  = 10, PricebookEntryID = PEID);
        return OLI;
    }

    public static Account getAccountByName(String accountName, Boolean withInsert) {
        Account account = getAccount();
        account.Name = accountName;
        account.BillingCountryCode = 'US';
        account.BillingState = 'California';

        if (withInsert) {
            insert account;
        }

        return account;
    }

    public static Contact getContactByName(String firstName, String lastName, Id accountId, Boolean withInsert) {
        Contact contact = getContact(accountId);
        contact.FirstName = firstName;
        contact.LastName = lastName;
        contact.MailingCountryCode = 'US';
        contact.MailingState = 'California';

        if (withInsert) {
            insert contact;
        }

        return contact;
    }

    public static Lead getLeadByName(String firstName, String lastName, Boolean withInsert) {
        Lead lead = getLead();
        lead.FirstName = firstName;
        lead.LastName = lastName;
        lead.State = 'California';

        if (withInsert) {
            insert lead;
        }

        return lead;
    }

    public static AssignmentRule__c createAssignment(String countryCode, String stateCode, Id salesRepId, Boolean doCommit) {
        AssignmentRule__c assignment = new AssignmentRule__c(CountryCode__c = 'US', StateCode__c = 'CA', StateDefault__c = true,
                                                             SalesRep__c = salesRepId,CountryDefault__c = true);

        if ( doCommit ) {
            insert assignment;
        }

        return assignment;
    }

    public static User createAdmin(Boolean doCommit) {
        Id pId = System.UserInfo.getProfileId();
        User user = new User(
            Alias = 'ekbf',
            Email = 'ekbf@testintegra.test.ch',
            EmailEncodingKey = 'UTF-8',
            LastName = 'EKBF',
            LanguageLocaleKey = 'de',
            LocaleSidKey = 'de_CH',
            ProfileId = pId,
            TimeZoneSidKey = 'Europe/Berlin',
            UserName='ekbf@digicomp.ch',
            DefaultCurrencyIsoCode  = 'CHF'
        );

        if ( doCommit ) {
            insert user;
        }

        return user;
    }

    public static List<PricebookEntry> getStandardPriceBook(Id pricebookId, Id productId) {
        List<PricebookEntry> standardPrices = new List<PricebookEntry> {
            getPricebookEntry(pricebookId, productId, 0, 'CAD'),
            getPricebookEntry(pricebookId, productId, 0, 'CHF'),
            getPricebookEntry(pricebookId, productId, 0, 'CNY'),
            getPricebookEntry(pricebookId, productId, 0, 'EUR'),
            getPricebookEntry(pricebookId, productId, 0, 'GBP'),
            getPricebookEntry(pricebookId, productId, 0, 'JPY'),
            getPricebookEntry(pricebookId, productId, 0, 'USD')
        };
        return standardPrices;
    }

    public static List<PricebookEntry> selectStandardPriceBookByProductAndCurrency(Id prodId, String currencyIsoCode) {
        return [SELECT Id FROM PricebookEntry WHERE Product2id =: prodId AND CurrencyIsoCode =: currencyIsoCode];
    }

    public static Promotion_Product__c getActivePromotionProduct1(Id prodId, Id marketId) {
        return new Promotion_Product__c (
            Product__c = prodId,
            Market__c = marketId,
            Start_Date__c = Date.Today()
        );
    }

    public static Promotion_Product__c getActivePromotionProduct2(Id prodId, Id marketId) {
        return new Promotion_Product__c (
            Product__c = prodId,
            Market__c = marketId,
            Start_Date__c = Date.Today().addDays(-10),
            End_Date__c = Date.Today().addDays(10)
        );
    }

    public static Promotion_Product__c getFuturePromotionProduct(Id prodId, Id marketId) {
        return new Promotion_Product__c (
            Product__c = prodId,
            Market__c = marketId,
            Start_Date__c = Date.Today().addDays(10),
            End_Date__c = Date.Today().addDays(20)
        );
    }

    public static Promotion_Product__c getOldPromotionProduct(Id prodId, Id marketId) {
        return new Promotion_Product__c (
            Product__c = prodId,
            Market__c = marketId,
            Start_Date__c = Date.Today(),
            End_Date__c = Date.Today()
        );
    }

    public static Promotion_Product__c getActivePromotionFamily1(String family, Id marketId) {
        return new Promotion_Product__c (
            Product_Family__c = family,
            Market__c = marketId,
            Start_Date__c = Date.Today()
        );
    }

    public static Promotion_Product__c getActivePromotionFamily2(String family, Id marketId) {
        return new Promotion_Product__c (
            Product_Family__c = family,
            Market__c = marketId,
            Start_Date__c = Date.Today().addDays(-10),
            End_Date__c = Date.Today().addDays(10)
        );
    }

    public static Promotion_Product__c getFuturePromotionFamily(String family, Id marketId) {
        return new Promotion_Product__c (
            Product_Family__c = family,
            Market__c = marketId,
            Start_Date__c = Date.Today().addDays(10),
            End_Date__c = Date.Today().addDays(20)
        );
    }

    public static Promotion_Product__c getOldPromotionFamily(String family, Id marketId) {
        return new Promotion_Product__c (
            Product_Family__c = family,
            Market__c = marketId,
            Start_Date__c = Date.Today(),
            End_Date__c = Date.Today()
        );
    }

    public static Case getDiscountsApproval_Approved_Case(Id recordId) {
        return new Case(Subject = 'RE: [DISCOUNTS APPROVAL PROCESS] Id:' + recordId,
            RecordTypeId = Constants.CASE_DISCOUNTS_APPROVAL_RT_ID,
            Description = 'Approved');
    }

    public static Case getDiscountsApproval_Rejected_Case(Id recordId) {
        return new Case(Subject = 'RE: [DISCOUNTS APPROVAL PROCESS] Id:' + recordId,
            RecordTypeId = Constants.CASE_DISCOUNTS_APPROVAL_RT_ID,
            Description = 'rejected');
    }

    public static void createOpportunityBase() {
        List<AccountGroup__c> accountGroups = new List<AccountGroup__c> {
            getAccountGroup('Genentech', 'USD'),
            getAccountGroup('GSA', 'USD'),
            getAccountGroup('Industrial Customers', 'CHF'),
            getAccountGroup('Pfizer Accounts', 'USD'),
            getAccountGroup('Uni ZH Catalogue', 'CHF'),
            getAccountGroup('University Customers', 'CHF'),
            getAccountGroup('VWR Accounts', 'CHF')
        };
        insert accountGroups;

        List<Account> accounts = new List<Account> {
            getAccount('Genentech', accountGroups[0].Id, 'US', 'OR'),
            getAccount('VA Medical Center WLA', accountGroups[1].Id, 'US', 'CA'),
            getAccount('Lonza AG', accountGroups[2].Id, 'CH', null),
            getAccount('Pfizer', accountGroups[3].Id, 'US', 'NY'),
            getAccount('Universität Zürich', accountGroups[5].Id, 'CH', null),
            getAccount('Novartis Pharma AG', accountGroups[6].Id, 'CH', null),
            getAccount('Kaugummi GmbH', null, 'DE', 'HH')
        };
        insert accounts;

        List<Contact> contacts = new List<Contact> {
            getContact(accounts[0].Id)
        };
        insert contacts;

        List<Market__c> markets = new List<Market__c>{
            getMarket('Canada', 'CA,', 'CA'),
            getMarket('China', 'Cn', 'CN'),
            getMarket('France', 'FR', 'FR; RE'),
            getMarket('Germany', 'DE', 'AT; DE'),
            getMarket('Japan', 'JP', 'JP'),
            getMarket('North America', 'NA', 'US; AR; AW; BB; BO; BR; BZ;CA; CL; CO; CR; CW; DO; EC; GF; GT; GY; HN; JM; MX; NI; PA; PE; PY; SV; SR; TT; VE; UY'),
            getMarket('Switzerland', 'CH', 'CH; LI'),
            getMarket('United Kingdom', 'UK', 'GB; IE')
        };
        insert markets;

        List<Product2> aProducts = new List<Product2> {
            getProduct('VIAFLO 96 Base Unit', '6001', Constants.PRODUCT_TYPE_ACCESSORIES, '', 'VIAFLO 96', '114'),
            getProduct('EVOLVE Pipette 1-Channel, 0.2 - 2 µl', '3011', Constants.PRODUCT_TYPE_ACCESSORIES, '01', 'EVOLVE', '122'),
            getProduct('0.5-999µl, 16 Channel Cassette, Sterile', '5742', Constants.PRODUCT_TYPE_SERVICES, '', 'VIAFILL', '41'),
            getProduct('Pipette Calibration 1-Channel', '990001', Constants.PRODUCT_TYPE_SERVICES, '02', 'Services', '136'),
            getProduct('12.5 µl LONG, 10 ECO Racks of 96 Tips, Non-Sterile', '3406', Constants.PRODUCT_TYPE_DISPOSABLES, '03', 'GRIPTIP 3000', '150'),
            getProduct('150 ml, Trial Pack, Sterile, Auto Friendly, Polypropylene', '6308', Constants.PRODUCT_TYPE_DISPOSABLES, '', 'RESERVOIR', '124')
        };
        insert aProducts;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>();
        for(Product2 product : aProducts) {
            standardPrices.addAll(getStandardPriceBook(pricebookId, product.Id));
        }
        insert standardPrices;

        List<PriceEntry__c> priceEntryList = new List<PriceEntry__c> {
            getPriceEntry(aProducts[0].Id, 16776.00, 'CAD', 1, 4, 99999, 4, 8, markets[0].Id),
            getPriceEntry(aProducts[0].Id, 142274.00, 'CNY', 1, null, null, null, null, markets[1].Id),
            getPriceEntry(aProducts[0].Id, 10700.00, 'EUR', 1, 4, 99999, 4, 8, markets[2].Id),
            getPriceEntry(aProducts[0].Id, 10700.00, 'EUR', 1, 4, 99999, 4, 8, markets[3].Id),
            getPriceEntry(aProducts[0].Id, 1400000.00, 'JPY', 1, null, null, null, null, markets[4].Id),
            getPriceEntry(aProducts[0].Id, 11650.00, 'USD', 1, 4, 99999, 4, 8, markets[5].Id),
            getPriceEntry(aProducts[0].Id, 9902.50, 'USD', 1, null, null, null, null, markets[5].Id, accountGroups[3].Id),
            getPriceEntry(aProducts[0].Id, 8991.29, 'USD', 1, null, null, null, null, markets[5].Id, accountGroups[1].Id),
            getPriceEntry(aProducts[0].Id, 11565.00, 'CHF', 1, null, null, null, null, markets[6].Id, accountGroups[6].Id),
            getPriceEntry(aProducts[0].Id, 12850.00, 'CHF', 1, 4, 99999, 4, 8, markets[6].Id),
            getPriceEntry(aProducts[0].Id, 8700.00, 'GBP', 1, 4, 99999, 4, 8, markets[7].Id),
            getPriceEntry(aProducts[1].Id, 17776.00, 'CAD', 1, 4, 99999, 4, 8, markets[0].Id),
            getPriceEntry(aProducts[1].Id, 152274.00, 'CNY', 1, null, null, null, null, markets[1].Id),
            getPriceEntry(aProducts[1].Id, 11700.00, 'EUR', 1, 4, 99999, 4, 8, markets[2].Id),
            getPriceEntry(aProducts[2].Id, 18776.00, 'CAD', 1, 4, 99999, 4, 8, markets[0].Id),
            getPriceEntry(aProducts[2].Id, 162274.00, 'CNY', 1, null, null, null, null, markets[1].Id),
            getPriceEntry(aProducts[2].Id, 12700.00, 'EUR', 1, 4, 99999, 4, 8, markets[2].Id)
        };
        insert priceEntryList;

        Opportunity opportunity = getOpportunity('Test Opportunity ', accounts[6].Id, 'Quotation', date.today(), 'EUR', null, null, null);
        insert opportunity;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem> {
            getOpportunityLineItem(standardPrices[3].Id, opportunity.Id, aProducts[0].Id, 1, 10700.00),
            getOpportunityLineItem(standardPrices[10].Id, opportunity.Id, aProducts[1].Id, 1, 11700.00),
            getOpportunityLineItem(standardPrices[17].Id, opportunity.Id, aProducts[2].Id, 1, 12565.00)
        };
        insert opportunityLineItems;

        List<Promotion_Product__c> promoList = new List<Promotion_Product__c> {
            TestUtil.getOldPromotionProduct(aProducts[0].Id, markets[3].Id),
            TestUtil.getFuturePromotionProduct(aProducts[0].Id, markets[3].Id),
            TestUtil.getOldPromotionFamily('VIAFLO 96', markets[3].Id),
            TestUtil.getFuturePromotionFamily('VIAFLO 96', markets[3].Id)
        };

        insert promoList;
    }

    public static void createQuoteBase() {
        List<AccountGroup__c> accountGroups = new List<AccountGroup__c> {
            getAccountGroup('Genentech', 'USD'),
            getAccountGroup('GSA', 'USD'),
            getAccountGroup('Industrial Customers', 'CHF'),
            getAccountGroup('Pfizer Accounts', 'USD'),
            getAccountGroup('Uni ZH Catalogue', 'CHF'),
            getAccountGroup('University Customers', 'CHF'),
            getAccountGroup('VWR Accounts', 'CHF')
        };
        insert accountGroups;

        List<Account> accounts = new List<Account> {
            getAccount('Genentech', accountGroups[0].Id, 'US', 'OR'),
            getAccount('VA Medical Center WLA', accountGroups[1].Id, 'US', 'CA'),
            getAccount('Lonza AG', accountGroups[2].Id, 'CH', null),
            getAccount('Pfizer', accountGroups[3].Id, 'US', 'NY'),
            getAccount('Universität Zürich', accountGroups[5].Id, 'CH', null),
            getAccount('Novartis Pharma AG', accountGroups[6].Id, 'CH', null),
            getAccount('Kaugummi GmbH', null, 'DE', 'HH')
        };
        insert accounts;

        List<Market__c> markets = new List<Market__c>{
            getMarket('Canada', 'CA,', 'CA'),
            getMarket('China', 'Cn', 'CN'),
            getMarket('France', 'FR', 'FR; RE'),
            getMarket('Germany', 'DE', 'AT; DE'),
            getMarket('Japan', 'JP', 'JP'),
            getMarket('North America', 'NA', 'US; AR; AW; BB; BO; BR; BZ;CA; CL; CO; CR; CW; DO; EC; GF; GT; GY; HN; JM; MX; NI; PA; PE; PY; SV; SR; TT; VE; UY'),
            getMarket('Switzerland', 'CH', 'CH; LI'),
            getMarket('United Kingdom', 'UK', 'GB; IE')
        };
        insert markets;

        List<Product2> aProducts = new List<Product2> {
            getProduct('VIAFLO 96 Base Unit', '6001', Constants.PRODUCT_TYPE_ACCESSORIES, '', 'VIAFLO 96', '114'),
            getProduct('EVOLVE Pipette 1-Channel, 0.2 - 2 µl', '3011', Constants.PRODUCT_TYPE_ACCESSORIES, '01', 'EVOLVE', '122'),
            getProduct('0.5-999µl, 16 Channel Cassette, Sterile', '5742', Constants.PRODUCT_TYPE_SERVICES, '', 'VIAFILL', '41'),
            getProduct('Pipette Calibration 1-Channel', '990001', Constants.PRODUCT_TYPE_SERVICES, '02', 'Services', '136'),
            getProduct('12.5 µl LONG, 10 ECO Racks of 96 Tips, Non-Sterile', '3406', Constants.PRODUCT_TYPE_DISPOSABLES, '03', 'GRIPTIP 3000', '150'),
            getProduct('150 ml, Trial Pack, Sterile, Auto Friendly, Polypropylene', '6308', Constants.PRODUCT_TYPE_DISPOSABLES, '', 'RESERVOIR', '124')
        };
        insert aProducts;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>();
        for(Product2 product : aProducts) {
            standardPrices.addAll(getStandardPriceBook(pricebookId, product.Id));
        }
        insert standardPrices;

        List<PriceEntry__c> priceEntryList = new List<PriceEntry__c> {
            getPriceEntry(aProducts[0].Id, 16776.00, 'CAD', 1, 4, 99999, 4, 8, markets[0].Id),
            getPriceEntry(aProducts[0].Id, 142274.00, 'CNY', 1, null, null, null, null, markets[1].Id),
            getPriceEntry(aProducts[0].Id, 10700.00, 'EUR', 1, 4, 99999, 4, 8, markets[2].Id),
            getPriceEntry(aProducts[0].Id, 10700.00, 'EUR', 1, 4, 99999, 4, 8, markets[3].Id),
            getPriceEntry(aProducts[0].Id, 1400000.00, 'JPY', 1, null, null, null, null, markets[4].Id),
            getPriceEntry(aProducts[0].Id, 11650.00, 'USD', 1, 4, 99999, 4, 8, markets[5].Id),
            getPriceEntry(aProducts[0].Id, 9902.50, 'USD', 1, null, null, null, null, markets[5].Id, accountGroups[3].Id),
            getPriceEntry(aProducts[0].Id, 8991.29, 'USD', 1, null, null, null, null, markets[5].Id, accountGroups[1].Id),
            getPriceEntry(aProducts[0].Id,  11565.00, 'CHF', 1, null, null, null, null, markets[6].Id, accountGroups[6].Id),
            getPriceEntry(aProducts[0].Id, 12850.00, 'CHF', 1, 4, 99999, 4, 8, markets[6].Id),
            getPriceEntry(aProducts[0].Id, 8700.00, 'GBP', 1, 4, 99999, 4, 8, markets[7].Id)
        };
        insert priceEntryList;

        Opportunity opportunity = getOpportunity('Test Opportunity ', accounts[6].Id, 'Quotation', date.today(), 'EUR', null, null, null);
        insert opportunity;

        Quote quote = getQuote('Test Quote ', opportunity.Id, 'EUR', 0.00, 0.00, 0.00);
        quote.Pricebook2Id = pricebookId;
        insert quote;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem> {
            getOpportunityLineItem(standardPrices[3].Id, opportunity.Id, aProducts[0].Id, 1, 10700.00)
        };
        insert opportunityLineItems;

        List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem> {
            getQuoteLineItem(standardPrices[3].Id, quote.Id, aProducts[0].Id, 1, 10700.00)
        };
        insert quoteLineItems;
        List<Promotion_Product__c> promoList = new List<Promotion_Product__c> {
            TestUtil.getOldPromotionProduct(aProducts[0].Id, markets[3].Id),
            TestUtil.getFuturePromotionProduct(aProducts[0].Id, markets[3].Id),
            TestUtil.getOldPromotionFamily('VIAFLO 96', markets[3].Id),
            TestUtil.getFuturePromotionFamily('VIAFLO 96', markets[3].Id)
        };

        insert promoList;
    }
}
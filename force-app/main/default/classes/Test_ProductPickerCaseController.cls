/*
*	@description Test class for ProductPickerCaseController functionality.
*
*	@author ach
*	@copyright PARX
*/
@isTest
public with sharing class Test_ProductPickerCaseController
{
	/*
	*	@description 
	*/
	static testMethod void testMainFunctionality()
	{
		TriggerTemplateV2.setupUnitTest();
		Case caseObj = new Case(CurrencyIsoCode= 'EUR');
		insert caseObj;
		List<CaseProduct__c> lineItems = new List<CaseProduct__c>{
				new CaseProduct__c(Case__c = caseObj.Id, EffectiveCost__c = 2, AppliedCost__c = 3, Quantity__c = 2),
				new CaseProduct__c(Case__c = caseObj.Id, EffectiveCost__c = 10, AppliedCost__c = 20, Quantity__c = 1)};
		
		Test.startTest();
		TriggerTemplateV2.startUnitTest();
		PageReference aPage = Page.ProductPickerCase;
		aPage.getParameters().put('id', caseObj.Id);
		Test.setCurrentPage(aPage);
		ProductPickerCaseController controller = new ProductPickerCaseController();
		controller.doFiterProducts();
		controller.doAddProducts();
		Test.stopTest();
		
		System.assertEquals(caseObj.Id, controller.getMainId(), 'Wrong object id');
		System.assertEquals('EUR', controller.getMainCurrency(), 'Wrong currency');
	}
}
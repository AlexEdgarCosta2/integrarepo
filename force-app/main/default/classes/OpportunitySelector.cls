/**
 * @description       : Opportunity Selector Class
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/

public with sharing class OpportunitySelector {
    /**
    * @description Get Opportunity by Id
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param oppId
    * @return Opportunity
    **/
    public static Opportunity getOpportunityById(String oppId) {
        List<Opportunity> oppList = [SELECT Id, Name, DiscountPercentage__c, DiscountServices__c, DiscountDisposables__c,
                                            CurrencyIsoCode, opportunity.Account.BillingCountryCode, opportunity.AccountId,
                                            Account_Group__c
                                       FROM Opportunity WHERE Id =: oppId];
        if(oppList.isEmpty()) {
            return null;
        } else {
            return oppList[0];
        }
    }

    /**
    * @description Get Opportunities by Set of Ids
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param oppIds
    * @return List<Opportunity>
    **/
    public static List<Opportunity> getOpportunityByIds(Set<Id> oppIds) {
        return [SELECT Id, Name, DiscountPercentage__c, DiscountServices__c, DiscountDisposables__c,
                       CurrencyIsoCode, opportunity.Account.BillingCountryCode, opportunity.AccountId,
                       opportunity.Account.Name, opportunity.Owner.Name, Account_Group__c
                  FROM Opportunity WHERE Id IN : oppIds];
    }
}
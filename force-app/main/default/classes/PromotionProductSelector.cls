/**
 * @description       : Selector for Promotion Product
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class PromotionProductSelector {
    /**
    * @description Get a Map <String, Promotion_Product__c> for a Set of Products and families
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param productIds
    * @param families
    * @return Map<String, Promotion_Product__c>
    **/
    public static Map<String, Promotion_Product__c> getMapAllFiltered(Set<Id> productIds, Set<String> families) {
        Map<String, Promotion_Product__c> promotionsMap = new Map<String, Promotion_Product__c>();

        for(Promotion_Product__c promo : [SELECT id, Product__c, Product_Family__c, Market__r.CountryList__c, Discount_Threshold__c
                  FROM Promotion_Product__c
                 WHERE (Product__c in : productIds OR Product_Family__c in : families)
                   AND ((Start_Date__c <= TODAY AND End_Date__c > TODAY) OR (Start_Date__c <= TODAY AND End_Date__c = null))]) {

            if(promo.Market__r.CountryList__c != null) {
                List<String> countries = promo.Market__r.CountryList__c.split(';');

                for(String country : countries) {
                    String key = '';
                    if(promo.Product__c != null) {
                        key = promo.Product__c + '|' + country.Trim();
                    } else if(promo.Product_Family__c != null) {
                        key = promo.Product_Family__c + '|' + country.Trim();
                    }
                    if(key != '') {
                        if(promotionsMap.get(key) == null) {
                            promotionsMap.put(key, promo);
                        }
                    }
                }
            }
        }
        return promotionsMap;
    }
}

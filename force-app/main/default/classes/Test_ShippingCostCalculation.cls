/**
 * @description  : 
 * @author       : Tim.lehmann@integra-biosciences.com
**/
@isTest
public with sharing class Test_ShippingCostCalculation {
    

        /**
        * @description 
        * @author Tim.lehmann@integra-biosciences.com | 09-24-2021 
        **/
        @isTest static void testShippingCostReturn(){

        List<Shipping_Cost_Germany__mdt> sCost = new List<Shipping_Cost_Germany__mdt>();
        sCost = ShippingCostCalculation.ShippingCostCalculation(TestUtil.setWebCart());
        
        Double dValue = sCost[0].Economy__c;
        
        System.assertNotEquals(NULL, sCost, 'No NULL values are allowed');
        System.assertEquals(150.00, dValue, 'Value should be');
        
       
        }
    
         
       @isTest static void testWeightValueConditions(){
        
        ShippingCostCalculation.ShippingCostCalculation(TestUtil.setWebCartv2());
        
        if(ShippingCostCalculation.weightSum <= ShippingCostCalculation.weightComparativeValue_1){
                System.assertEquals(ShippingCostCalculation.weightScore,1);
        }
        else if(ShippingCostCalculation.weightSum <= ShippingCostCalculation.weightComparativeValue_2){
                System.assertEquals(ShippingCostCalculation.weightScore,2);
        }
        else if(ShippingCostCalculation.weightSum <= ShippingCostCalculation.weightComparativeValue_3){
                System.assertEquals(ShippingCostCalculation.weightScore,3);
        }
        else if(ShippingCostCalculation.weightSum <= ShippingCostCalculation.weightComparativeValue_4){
                System.assertEquals(ShippingCostCalculation.weightScore,4);
        }
        else if(ShippingCostCalculation.weightSum > ShippingCostCalculation.weightComparativeValue_4){
                System.assertEquals(ShippingCostCalculation.weightScore,5);
        }
        
        

       }

       @isTest static void testVolumeValueConditions(){
        
        ShippingCostCalculation.ShippingCostCalculation(TestUtil.setWebCartv2());

        if(ShippingCostCalculation.volumeSum <= ShippingCostCalculation.volumeComparativeValue_1){
                System.assertEquals(ShippingCostCalculation.volumeScore,1);
        }
        else if(ShippingCostCalculation.volumeSum <= ShippingCostCalculation.volumeComparativeValue_2){
                System.assertEquals(ShippingCostCalculation.volumeScore,2);
        }
        else if(ShippingCostCalculation.volumeSum <= ShippingCostCalculation.volumeComparativeValue_3){
                System.assertEquals(ShippingCostCalculation.volumeScore,4);
        }
        else if(ShippingCostCalculation.volumeSum > ShippingCostCalculation.volumeComparativeValue_3){
                System.assertEquals(ShippingCostCalculation.volumeScore,5);
        }
       
}
}

/*****************************************************
 * Name : DeleteOppProdServerController
 * Developer: Vinay Vernekar
 * Reference: https://www.fiverr.com/kingofwebhost/do-standard-or-custom-development-in-salesforce
 * Website: https://sfdcdevelopers.com
 * Email: support@sfdcdevelopers.com
 * Purpose: Controller for lightning component
 * Date: 24 Sep 2018
 * Last Modified Date: 24 Sep 2018
*******************************************************/
public class DeleteOppProdServerController {
	@AuraEnabled
    public static List<ProductWrapper> getOpportunityProducts(id OpportunityID) {
        List<ProductWrapper> ProdWrap = new List<ProductWrapper>();
		for(OpportunityLineItem OLI : [select id, Product2.Name, Product2Id, ProductCode, Quantity from OpportunityLineItem where OpportunityId =: OpportunityID order by Product2.Name asc])
            ProdWrap.add(new ProductWrapper(false, OLI));
        return ProdWrap;
    }
    @AuraEnabled
    public static void deleteOppProduct(String OLIIds) {
        List<id> OLIIDList = (List<ID>) JSON.deserialize(OLIIds, List<id>.class);
        List<OpportunityLineItem> OLIs = new List<OpportunityLineItem>();
        for(id i : OLIIDList)
            OLIs.add(new OpportunityLineItem(id = i));
        delete OLIs;
    }
    
    public class ProductWrapper{
        @AuraEnabled 
        public boolean isSelected {get;set;}
        @AuraEnabled 
        public OpportunityLineItem OLI{get;set;}
        
        public ProductWrapper(boolean isSelected, OpportunityLineItem OLI){
            this.isSelected = isSelected;
            this.OLI = OLI;
        }
    }
}
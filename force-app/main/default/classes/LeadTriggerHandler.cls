/**
 * Trigger handler by extending the TriggerHandlerAdapter from the TriggerTemplateV2
 * and overriding only the in the trigger registered events
 *
 * @author ach, eru
 * @copyright PARX
 */
public with sharing class LeadTriggerHandler extends TriggerTemplateV2.TriggerHandlerAdapter
{
	public override void onBeforeInsert (List<sObject> newList)
	{
		LeadService.updateCurrency(newList);
        LeadService.CheckDuplicates(newList, NULL);
	}

	public override void onBeforeUpdate (List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap)
	{
		LeadService.updateCurrency(newList);
        LeadService.CheckDuplicates(newList, (Map<id, Lead>)oldMap);
	}

	public override void onAfterUpdate (List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap)
	{
		LeadService.updateRelatedOpportunityOnConvert(newList, oldMap);
	}

	public override void onAfterInsert (List<sObject> newList, Map<Id, sObject> newMap)
	{
		LeadService.scheduleBatch();
	}

}
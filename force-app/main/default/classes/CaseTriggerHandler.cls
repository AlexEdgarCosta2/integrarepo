/**
 * @description       : Case trigger Handler
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class CaseTriggerHandler extends TriggerTemplateV2.TriggerHandlerAdapter {
	/**
	* @description onBeforeInsert
	* @author alexandre.costa@integra-biosciences.com | 04-28-2021
	* @param newList
	**/
	public override void onBeforeInsert (List<sObject> newList) {
		CaseService.approveRejectsDiscounts((List<Case>)newList);
	}
}

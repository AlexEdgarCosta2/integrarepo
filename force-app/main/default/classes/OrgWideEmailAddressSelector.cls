/**
 * @description       : Selector for OrgWideEmailAddress
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class OrgWideEmailAddressSelector {
    /**
    * @description Get By Display name
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param displayName
    * @return OrgWideEmailAddress
    **/
    public static OrgWideEmailAddress getOrgWideEmailAddressByDisplayName(String displayName) {
        List<OrgWideEmailAddress> oa = [SELECT id FROM OrgWideEmailAddress WHERE displayName =: displayName];

        if(oa == null || oa.isEmpty()) {
            return null;
        } else {
            return oa[0];
        }
    }
}

/**
 * @description  :
 * @author       : Tim.lehmann@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                Modification
 * 1.0   07-14-2021   Tim.lehmann@integra-biosciences.com   Initial Version
**/
public with sharing class CartItemSelector {

    /**
    * @description
    * @author Tim.lehmann@integra-biosciences.com | 07-14-2021
    * @return List<CartItem>
    **/
    public static List<CartItem> getCartItemsFromActualCart(String webCartId) {

        return [SELECT Product2Id,
                       SalesPrice,
                       Quantity,
                       Product2.B2B_Product_Volume__c,
                       Product2.B2B_Product_Weight__c
                       FROM CartItem
                       WHERE CartId =: webCartId
                       ORDER BY Product2Id ASC];

    }

    /**
    * @description
    * @author Tim.lehmann@integra-biosciences.com | 07-14-2021
    * @return List<CartItem>
    **/
    public static List<CartItem> checkIfCapitalEqupimentInCart(List<WebCart> wCart) {

        return [SELECT Id FROM CartItem WHERE CartId =: wCart[0].Id AND Product2.Capital_Equipment__c = TRUE ];
    }



}
/**
*	@description Class contains some useful features, that can be used in triggers and controllers.
*
*	@author akr, bbo, mfe, cde, ach
*	@copyright PARX
*/
public with sharing class TriggerUtils
{

	/**
	*	@description Autonumber for Object and Field if needed
	*/
	
	private static Map<String, Autonumber__c> autonumbers
	{
		get
		{
			if(autonumbers == null)
			{
				autonumbers = new Map<String, Autonumber__c>();
				for(Autonumber__c c : [SELECT ID, Name, CurrentNumber__c FROM Autonumber__c])
				{
					autonumbers.put(c.Name, c);
				}
			}
			return autonumbers;
		}
		set;
	}
	
	/**
	*	@description generate new autonumber by its name.
	*/
	public static Double getAutonumber(String name)
	{
		if(!autonumbers.containsKey(name))
		{
			autonumbers.put(name, new Autonumber__c(Name = name, CurrentNumber__c = 0));
		}
		return ++autonumbers.get(name).CurrentNumber__c;
	}
	
	/**
	*	@description Save Autonumber for Object and Field if needed
	*/
	public static void saveAutoNumber()
	{
		upsert autonumbers.values();
	}
	
		/**
		Get object set of modified object by defined list of fields.
		That means we can list of objects, were one or more fields where modified.
	*/
	public static Set<sObject> getModifiedObjectsObj(List<String> theFieldsList, List<sObject> theObjects, Map<Id, sObject> theOldObjects)
	{
		Set<sObject> theResult = new Set<sObject>();
		if(theObjects != null)
		{
			for (sObject anObject: theObjects)
			{
				if (theOldObjects == null)
				{
					theResult.add(anObject);
				} else
				{
					sObject anOldObject = theOldObjects.get(anObject.Id);
					if (anOldObject == null)
					{
						theResult.add(anObject);
					} else
					{
						for (String aField: theFieldsList)
						{
							if (anObject.get(aField) != anOldObject.get(aField))
							{
								theResult.add(anObject);
							}
						}
					}
				}
			}
		}

		return theResult;
	}
	
	public static List<String> getSobjectUpdatableFields(String objectName)
	{
		List<String> fields = new List<String>();
		Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName)
				.getDescribe().fields.getMap();
		for (String fieldName : fieldMap.keySet())
		{
			if(fieldMap.get(fieldName).getDescribe().isUpdateable())
			{
				fields.add(fieldName);
			}
		}
		return fields;
	}
	
	/**
	*	@description Get sObject Name by record Id.
	*/
	public static String getsObjectNameById(String recordId)
	{
		String returnValue = null;
		final String keyPrefixToMatch = recordId.substring(0, 3);
		String keyPrefix = null;
		final Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
		for(String objectNameKey : globalDescribe.keySet())
		{
			keyPrefix = globalDescribe.get(objectNameKey).getDescribe().getKeyPrefix();
			if(String.isNotBlank(keyPrefix) && keyPrefixToMatch.equalsIgnoreCase(keyPrefix))
			{
				returnValue = objectNameKey;
				break;
			}
		}
		if(String.isBlank(returnValue))
		{
			throw new IllegalArgumentException('Invalid id argument : key prefix not found: ' + keyPrefixToMatch);
		}
		return returnValue;
	}
	public class IllegalArgumentException extends Exception {}  
}
/*****************************************************
 * Name : TestDeleteOppProdServerController
 * Developer: Vinay Vernekar
 * Reference: https://www.fiverr.com/kingofwebhost/do-standard-or-custom-development-in-salesforce
 * Website: https://sfdcdevelopers.com
 * Email: support@sfdcdevelopers.com
 * Purpose: Test class for DeleteOppProdServerController
 * Date: 24 Sep 2018
 * Last Modified Date: 24 Sep 2018
*******************************************************/
@isTest
public class TestDeleteOppProdServerController {

    static testmethod void coverclass(){

        //create account
        Account acc = TestUtil.getAccount();
        insert acc;

        acc = [SELECT Id, CurrencyIsoCode FROM Account WHERE Id =: acc.Id LIMIT 1];
        
        //create product
        Product2 prod = TestUtil.getProduct();
        insert prod;

        PriceBook2 pricebook = new PriceBook2(Name = 'CustomPricebook', IsActive = true);
        insert pricebook;

        //create Pricebook entry
        PricebookEntry stdPrice = new PricebookEntry();
        stdPrice.Product2Id = prod.Id;
        stdPrice.Pricebook2Id = Test.getStandardPricebookId();
        stdPrice.IsActive = true;
        stdPrice.UnitPrice = 10;
        stdPrice.CurrencyIsoCode = acc.CurrencyIsoCode;
        insert stdPrice;

        PricebookEntry entry = new PricebookEntry();
        entry.Product2Id = prod.Id;
        entry.Pricebook2Id = pricebook.Id;
        entry.IsActive = true;
        entry.UnitPrice = 10;
        entry.CurrencyIsoCode = acc.CurrencyIsoCode;
        insert entry;

        //create opportunity
        Opportunity opp = TestUtil.getOpportunity(acc.Id);
        opp.CurrencyIsoCode = acc.CurrencyIsoCode;
        opp.Pricebook2Id = pricebook.Id;
        insert opp;

        //create OLI
        OpportunityLineItem oli = TestUtil.getOLIs(opp.id, entry.id);
        insert oli;

        
        //retrieve opportunity products via controller
        DeleteOppProdServerController.getOpportunityProducts(opp.id);
        
        //delete opportunity product via controller
        DeleteOppProdServerController.deleteOppProduct(JSON.Serialize(new list<id>{oli.id}));
    }
}
/**
 * @description       : Selector for email templates
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class EmailTemplateSelector {
    /**
    * @description Get Email Template by developer name
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param name
    * @return EmailTemplate
    **/
    public static EmailTemplate getEmailTemplateByDeveloperName(String name) {
        EmailTemplate et = [Select id, HtmlValue from EmailTemplate where DeveloperName =: name limit 1];
        return et;
    }
}

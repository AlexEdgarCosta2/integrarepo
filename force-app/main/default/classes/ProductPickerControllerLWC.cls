/**
 * @description  :
 * @author       : alexandre.costa@integra-biosciences.com
**/
public with sharing class ProductPickerControllerLWC extends ProductPickerController {

    public static final String NAME_FIELD_TO_FILTER = 'Name';

    public Opportunity opportunity{get;set;}
    public Quote quote{get;set;}
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Opportunity opp {get;set;}
    private Quote qte {get;set;}

    // initialize the controller
     public ProductPickerControllerLWC(ApexPages.StandardController controller)
    {
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        opp = (Opportunity)controller.getRecord();
        qte = (Quote)controller.getRecord();
    }

      // Added for Lightning functionality
    public ProductPickerControllerLWC(Id objId)
    {
        if (String.valueOf(objId).startsWith('006'))
        {
            this.opportunity = [
                SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, Contact__c,
                   DiscountPercentage__c, DiscountDisposables__c, DiscountServices__c, ProductLanguage__c
                FROM Opportunity
                WHERE Id =: objId
            ];
        }
        else if (String.valueOf(objId).startsWith('0Q0'))
        {
            this.quote = [
                SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode,
                    DiscountProducts__c, DiscountDisposables__c, DiscountServices__c, ProductLanguage__c
                FROM Quote
                WHERE Id =: objId
            ];
        }
    }

    public override void initMainObject()
    {
        // Added for Lightning functionality
        if (this.opportunity == null && ApexPages.currentPage() != null) {
            this.opportunity = [
                SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, Contact__c, DiscountPercentage__c,
                        DiscountDisposables__c,DiscountServices__c
                FROM Opportunity
                WHERE Id = :(Id) ApexPages.currentPage().getParameters().get('id')
            ];
        }
        else if (this.quote == null && Apexpages.currentPage() != null) {
            this.quote = [SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, DiscountProducts__c, DiscountDisposables__c, DiscountServices__c
                FROM Quote WHERE Id = :(Id) ApexPages.currentPage().getParameters().get('id')];
        }
    }

    public override Id getMainId()
    {
        if(this.opportunity != null){
            return this.opportunity.Id;
        }else if(this.quote != null){
            return this.quote.Id;
        }
        return null;
    }

    public override String getMainCurrency()
    {
        if(this.opportunity != null){
            return this.opportunity.CurrencyIsoCode;
        }else if(this.quote != null){
            system.debug('getMainCurrency :: '+this.quote);
            return this.quote.CurrencyIsoCode;
        }
        return null;
    }

    public override String getLanguageForProducts()
    {
        System.debug('--- getLanguageForProducts ---');
        System.debug('opportunity = ' + this.opportunity);
        System.debug('this.quote = ' + this.quote);
        if (this.opportunity != null) {
            return this.opportunity.ProductLanguage__c != null ? this.opportunity.ProductLanguage__c : '';
        }
        else if (this.quote != null) {
            return this.quote.ProductLanguage__c != null ? this.quote.ProductLanguage__c : '';
        }
        return null;
    }

    public override String getMainBillingCountryCode()
    {
        if(this.opportunity != null){
            return this.opportunity.Account.BillingCountryCode;
        }
        else if(this.quote != null){
            return this.quote.Account.BillingCountryCode;
        }
        return null;
    }

    public override Id getMainAccountGroup()
    {
        if(this.opportunity != null){
            return this.opportunity.Account.AccountGroup__c;
        }
        else if(this.quote != null){
            return this.quote.Account.AccountGroup__c;
        }
        return null;
    }

    protected override void applyAdditionalConditions(AndCondition andCondition)
    {
        andCondition.add(new FieldCondition('DoNotShowOnOpportunity__c', Operator.NOT_EQUALS, true));
    }

     public override Decimal getOverallDiscount()
    {
        if(this.opportunity != null){
            system.debug('this.opportunity.DiscountPercentage__c :: '+this.opportunity.DiscountPercentage__c);
            return this.opportunity.DiscountPercentage__c;
        }
        else if(this.quote != null){
            return this.quote.DiscountProducts__c;
        }
        return null;
    }
    public override Decimal getDisposablesDiscount()
    {
        if(this.opportunity != null){
            system.debug('this.opportunity.DiscountPercentage__c :: '+this.opportunity.DiscountDisposables__c);
            return this.opportunity.DiscountDisposables__c;
        }
        else if(this.quote != null){
            return this.quote.DiscountDisposables__c;
        }
        return null;
    }
    public override Decimal getServicesDiscount()
    {
        if(this.opportunity != null){
            system.debug('this.opportunity.DiscountPercentage__c :: '+this.opportunity.DiscountServices__c);
            return this.opportunity.DiscountServices__c;
        }
        else if(this.quote != null){
            return this.quote.DiscountServices__c;
        }
        return null;
    }

    @AuraEnabled
    public static List<LineItemWrapper> getOpportunityLineItems(Id opportunityId)
    {
        system.debug('getOpportunityLineItems called');
        List<LineItemWrapper> lineItemWrappers = new List<LineItemWrapper>();

        alParx.SoqlBuilder oliQuery =
            new alParx.SoqlBuilder()
            .selectAll()
            .fromx('OpportunityLineItem');

        oliQuery.wherex(new alParx.FieldCondition('OpportunityId', alParx.Operator.EQUALS, opportunityId));

		System.debug(oliQuery.toSoql(new alParx.SoqlOptions().wildcardStringsInLikeOperators()));
        List<OpportunityLineItem> olis = (List<OpportunityLineItem>) Database.query(oliQuery.toSoql(new alParx.SoqlOptions().wildcardStringsInLikeOperators()));

        if (olis.size() == 0)
            return lineItemWrappers;

        Map<Id, PricebookEntry> pbeMap = new Map<Id, PricebookEntry>();
        for (OpportunityLineItem oli : olis)
        {
            pbeMap.put(oli.PricebookEntryId, null);
        }

        alParx.SoqlBuilder pbeQuery =
            new alParx.SoqlBuilder()
            .selectAll()
            .fromx('PricebookEntry');

        pbeQuery.wherex(new alParx.SetCondition('Id',alParx.Operator.INX,new List<Id>(pbeMap.keySet())));

        for (PricebookEntry pbe :  (List<PricebookEntry>) Database.query(pbeQuery.toSoql(new alParx.SoqlOptions().wildcardStringsInLikeOperators())))
        {
            pbeMap.put(pbe.Id, pbe);
        }

        // for (OpportunityLineItem oli : olis)
        // {
        //     LineItemWrapper li = new LineItemWrapper();
        //     li.priceBookEntry = pbeMap.get(oli.PricebookEntryId);
        //     li.quantity = oli.Quantity;
        //     li.lineItem = oli;
        //     lineItemWrappers.add(li);
        // }

        //enhanceLineItemWrappers(lineItemWrappers);
        return lineItemWrappers;
    }


    @AuraEnabled
    public static List<LineItemWrapper> addLineItems(List<LineItemWrapper> items)
    {
        System.debug(items);
        // List<SObject> lineItemsInsert = new List<SObject>();
        // List<SObject> lineItemsUpdate = new List<SObject>();
        // List<SObject> lineItemsDelete = new List<SObject>();
        // for (LineItemWrapper line : items)
        // {
        //     if (line.lineItem.get('Id') == null)
        //     {
        //         if (line.lineItem.get('Quantity') != 0)
        //         {
        //             lineItemsInsert.add(line.lineItem);
        //         }
        //     }
        //     else {
        //         if (line.lineItem.get('Quantity') == 0)
        //         {
        //             lineItemsDelete.add(line.lineItem);
        //         }
        //         else {
        //             lineItemsUpdate.add(line.lineItem);
        //         }
        //     }
        // }
        // System.debug(lineItemsInsert);
        // System.debug(lineItemsUpdate);
        // try {

        //     insert lineItemsInsert;
        //     update lineItemsUpdate;
        //     delete lineItemsDelete;
        // } catch (Exception ex) {
        //     throw new AuraHandledException(ex.getMessage());
        // }
        return new List<LineItemWrapper>();
    }

    @AuraEnabled
    public static List<LineItemWrapper> getQuoteLineItems(Id quoteId)
    {
        List<LineItemWrapper> lineItemWrappers = new List<LineItemWrapper>();

        List<QuoteLineItem> qlis = [
            SELECT
                Id, LineNumber, CurrencyIsoCode, QuoteId, PricebookEntryId,
                OpportunityLineItemId, Quantity, UnitPrice, Discount, Description, ServiceDate,
                Product2Id, SortOrder, ListPrice, Subtotal, TotalPrice, List_Price__c
            FROM QuoteLineItem
            WHERE
                QuoteId = : quoteId];

        if (qlis.size() == 0)
            return lineItemWrappers;

        Map<Id, PricebookEntry> pbeMap = new Map<Id, PricebookEntry>();
        for (QuoteLineItem qli : qlis)
        {
            pbeMap.put(qli.PricebookEntryId, null);
        }


        alParx.SoqlBuilder pbeQuery =
            new alParx.SoqlBuilder()
            .selectAll()
            .fromx('PricebookEntry');

        pbeQuery.wherex(new alParx.SetCondition('Id',alParx.Operator.INX,new List<Id>(pbeMap.keySet())));
        pbeQuery.orderByx(
            new List<alParx.OrderBy>
            {
                    new alParx.OrderBy('CreatedDate').descending()
            });

        for (PricebookEntry pbe :  (List<PricebookEntry>) Database.query(pbeQuery.toSoql(new alParx.SoqlOptions().wildcardStringsInLikeOperators())))
        {
            pbeMap.put(pbe.Id, pbe);
        }

        // for (QuoteLineItem qli : qlis)
        // {
        //     LineItemWrapper li = new LineItemWrapper();
        //     li.priceBookEntry = pbeMap.get(qli.PricebookEntryId);
        //     li.quantity = qli.Quantity;
        //     li.lineItem = qli;
        //     lineItemWrappers.add(li);
        // }

        //enhanceLineItemWrappers(lineItemWrappers);
        return lineItemWrappers;
    }



    @AuraEnabled
    public static List<LineItemWrapper> getFilteredProducts(Map<String,Condition> filter, String recordId,String productContext, String prodIds)
    {
        System.debug('getFilteredProducts alertIds :: '+prodIds+ ' productContext '+productContext);
        List<String> strArr = new List<String>();
        if(prodIds != null){
            prodIds = prodIds.remove('(').remove(')');
            strArr = prodIds.split(',');
            System.debug('strArr :: '+strArr);
        }

        Opportunity opty = new Opportunity();
        Quote qu = new Quote();

        ProductPickerControllerLWC ppc;

        String CurrIsoCode = '';
        String countryCode = '';
        String mainAccGroup = '';
        String overAllDis = '';
        String disposableDis = '';
        String serviceDis = '';

        if(recordId.startsWith('006')){
            //  ppc = new ProductPickerOpportunityController((Id)recordId);
            ppc = new ProductPickerControllerLWC((Id)recordId);
            //quert opty to get data
            opty = [Select id, CurrencyIsoCode, Contact__c, Account.BillingCountryCode, Account.AccountGroup__c, DiscountPercentage__c, DiscountDisposables__c, DiscountServices__c
                    from Opportunity where id =: recordId];

            // CurrIsoCode = opty.CurrencyIsoCode;
            CurrIsoCode = ppc.getMainCurrency();
            countryCode = '%' + opty.Account.BillingCountryCode + '%';
            mainAccGroup = opty.Account.AccountGroup__c;
            overAllDis += opty.DiscountPercentage__c;
            disposableDis += opty.DiscountDisposables__c;
            serviceDis += opty.DiscountServices__c;

        }else if(recordId.startsWith('0Q0')){
            System.debug('ProductPickerControllerLWC getFilteredProducts called');
            ppc = new ProductPickerControllerLWC((Id)recordId);
            //query quote to get data
            qu = [Select id, Account.CurrencyIsoCode, Account.BillingCountryCode, Account.AccountGroup__c, DiscountProducts__c, DiscountDisposables__c, DiscountServices__c
                    from Quote where id =: recordId];

            ppc.getMainCurrency();

            CurrIsoCode = qu.Account.CurrencyIsoCode;
            countryCode = '%' + qu.Account.BillingCountryCode + '%';
            mainAccGroup = qu.Account.AccountGroup__c;
            overAllDis += qu.DiscountProducts__c;
            disposableDis += qu.DiscountDisposables__c;
            serviceDis += qu.DiscountServices__c;
        }
        System.debug(filter);

        alParx.SoqlBuilder pbeQuery;
        // if(productContext!='RelatedProduct') {
        //  pbeQuery = new alParx.SoqlBuilder()
        //     .selectAll()
        //     .fromx('Product2');
        //  }else {
        //   pbeQuery = new alParx.SoqlBuilder()
        //     .selectx('id')
        //     .fromx('Product2');
        //  }

        if(productContext =='Product') {
         pbeQuery = new alParx.SoqlBuilder()
            .selectAll()
            .fromx('Product2');
         }else if(productContext =='Related Products') {
          pbeQuery = new alParx.SoqlBuilder()
            .selectx('id')
            .fromx('Product2');
         }  else if(productContext =='Alerts') {
             pbeQuery = new alParx.SoqlBuilder()
                .selectAll()
                .fromx('Product2');
         }

        alParx.AndCondition andCond = new alParx.AndCondition();

        andCond.add(new alParx.FieldCondition('IsActive', alParx.Operator.EQUALS, true));
        andCond.add(new alParx.FieldCondition('DoNotShowOnOpportunity__c', alParx.Operator.EQUALS, false));

        if (filter != null){
            for (String apiFieldName: filter.keyset()) {
                System.debug('apiFieldName = ' + apiFieldName);
                Condition cond = filter.get(apiFieldName);

                String nameToFilter = apiFieldName;

                if (nameToFilter == NAME_FIELD_TO_FILTER) {
                    if (ppc.getLanguageForProducts() == 'French')
                    {
                        nameToFilter = 'ProductNameFR__c';
                    } else if (ppc.getLanguageForProducts() == 'German')
                    {
                        nameToFilter = 'ProductNameDE__c';
                    }
                }
                System.debug('nameToFilter = ' + nameToFilter);

                if (cond.value != null && String.isNotBlank(cond.value.toString()))
                {
                    if (cond.operator.equalsIgnoreCase('LIKEX'))
                        andCond.add(new alParx.FieldCondition(nameToFilter, alParx.Operator.LIKEX, cond.value));
                    if (cond.operator.equalsIgnoreCase('EQUALS'))
                    {
                        andCond.add(new alParx.FieldCondition(nameToFilter, alParx.Operator.EQUALS, cond.value));
                    }
                    if (cond.operator.equalsIgnoreCase('SET_INCLUDES'))
                        andCond.add(new alParx.SetCondition(nameToFilter).includes((cond.value == null ? '' : cond.value.toString()).split(';')));
                }
            }
        }


        pbeQuery.wherex(andCond);
	    pbeQuery.orderByx(new alParx.OrderBy('ProductCode'));
        List<Product2> queriedProducts;
        // below commented code is working copy
        // if(productContext!='RelatedProduct') {
        //    queriedProducts = (List<Product2>) Database.query(pbeQuery.toSoql(new alParx.SoqlOptions().wildcardStringsInLikeOperators()));
        // }else {
        //    List<Product2> queriedProducts2 = (List<Product2>) Database.query(pbeQuery.toSoql(new alParx.SoqlOptions().wildcardStringsInLikeOperators()));

        //    system.debug('queriedProducts2' + queriedProducts2);

        //    queriedProducts = [ Select CreatedById,CreatedDate,CurrencyIsoCode,Description,DisplayUrl,DoNotShowOnCase__c,DoNotShowOnOpportunity__c,ExternalDataSourceId,ExternalId,Family,HiddenDescription__c,Id,IsActive,IsArchived,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Name,PricingGroup__c,ProductCode,ProductFamilyNumber__c,ProductFamily__c,ProductLanguage__c,ProductName2DE__c,ProductName2FR__c,ProductName2__c,ProductNameDE__c,ProductNameFR__c,ProductTypeNumber__c,ProductType__c,Product_ID__c,Product_Name_3__c,Product__c,QuantityUnitOfMeasure,RecordTypeId,StockKeepingUnit,SystemModstamp,X18_Digit_Product_ID__c FROM Product2 where id IN (SELECT RelatedProduct__c FROM RelatedProduct__c where Product__c in :queriedProducts2) ];

        //    system.debug('queriedProducts ' + queriedProducts);
        // }

        if(productContext=='Product') {
           queriedProducts = (List<Product2>) Database.query(pbeQuery.toSoql(new alParx.SoqlOptions().wildcardStringsInLikeOperators()));

            // system.debug('queriedProducts Product ' + queriedProducts);

        }else if(productContext == 'Related Products'){
           List<Product2> queriedProducts2 = (List<Product2>) Database.query(pbeQuery.toSoql(new alParx.SoqlOptions().wildcardStringsInLikeOperators()));

        //    system.debug('queriedProducts2' + queriedProducts2);

           queriedProducts = [ Select CreatedById,CreatedDate,CurrencyIsoCode,Description,DisplayUrl,DoNotShowOnCase__c,DoNotShowOnOpportunity__c,ExternalDataSourceId,ExternalId,Family,HiddenDescription__c,Id,IsActive,IsArchived,IsDeleted,LastModifiedById,LastModifiedDate,/*LastReferencedDate,*/LastViewedDate,Name,PricingGroup__c,ProductCode,ProductFamilyNumber__c,ProductFamily__c,ProductLanguage__c,ProductName2DE__c,ProductName2FR__c,ProductName2__c,ProductNameDE__c,ProductNameFR__c,ProductTypeNumber__c,ProductType__c,Product_ID__c,Product_Name_3__c,Product__c,QuantityUnitOfMeasure,RecordTypeId,StockKeepingUnit,SystemModstamp,X18_Digit_Product_ID__c FROM Product2 where id IN (SELECT RelatedProduct__c FROM RelatedProduct__c where Product__c in :queriedProducts2) AND IsActive = true];

        //    system.debug('queriedProducts RelatedProduct ' + queriedProducts);
        } else if(productContext == 'Alerts'){

            // List<Product2> queriedProducts2 = (List<Product2>) Database.query(pbeQuery.toSoql(new alParx.SoqlOptions().wildcardStringsInLikeOperators()));

            // system.debug('queriedProducts2 Alerts' + queriedProducts2);

            queriedProducts = [ Select CreatedById,CreatedDate,CurrencyIsoCode,Description,DisplayUrl,DoNotShowOnCase__c,DoNotShowOnOpportunity__c,ExternalDataSourceId,ExternalId,Family,HiddenDescription__c,Id,IsActive,IsArchived,IsDeleted,LastModifiedById,LastModifiedDate,Name,PricingGroup__c,ProductCode,ProductFamilyNumber__c,ProductFamily__c,ProductLanguage__c,ProductName2DE__c,ProductName2FR__c,ProductName2__c,ProductNameDE__c,ProductNameFR__c,ProductTypeNumber__c,ProductType__c,Product_ID__c,Product_Name_3__c,Product__c,QuantityUnitOfMeasure,RecordTypeId,StockKeepingUnit,SystemModstamp,X18_Digit_Product_ID__c FROM Product2 where id IN :strArr AND IsActive = true];

            // system.debug('queriedProducts ' + queriedProducts);
        }

        Map<String, PriceEntry__c> priceEntryMap = PriceEntrySelector.getpriceEntryMapByProducts2(queriedProducts);

        List<LineItemWrapper> lineItemWrappers = new List<LineItemWrapper>();

/*
		Map<Id, List<PriceEntry__c>> productWithStandardPrices = new Map<Id, List<PriceEntry__c>>();
        Map<Id, List<PriceEntry__c>> productWithSpecialPrices = new Map<Id, List<PriceEntry__c>>();
        Map<String, Double> productToListPrice = new Map<String, Double>();

        //To get the getMainBillingCountryCode getMainBillingCountryCode()  line no. 147
        for (PriceEntry__c aPriceEntry : [SELECT id, Product__c, ScalingTo__c, SellingPrice__c, ScalingTo2__c, DiscountTier2__c, ScalingTo3__c, DiscountTier3__c, AccountGroup__c, ValidFrom__c
                                          FROM PriceEntry__c WHERE Product__c IN : queriedProducts
                                          AND CurrencyIsoCode =: ppc.getMainCurrency() AND Market__r.CountryList__c LIKE :countryCode
                                          AND ValidFrom__c <= :System.Today() ORDER BY ScalingTo__c ASC])
        {
            if (aPriceEntry.AccountGroup__c == null) // standard price
            {
                addNewPrice(productWithStandardPrices, aPriceEntry);

                // get list price and put it into the product and list price map
                if (productToListPrice.containsKey(aPriceEntry.Product__c))
                {
                    productToListPrice.put(aPriceEntry.Product__c, aPriceEntry.SellingPrice__c);
                }
            }
            //Need opty id to get the following data
            else if (aPriceEntry.AccountGroup__c == mainAccGroup &&
                     (isDecimalStringNull(overAllDis) || isDecimalStringNull(disposableDis) || isDecimalStringNull(serviceDis))) // special price
            {
                addNewPrice(productWithSpecialPrices, aPriceEntry);
            }
        }

*/

        List<PricebookEntry> pListToInsert = new List<PricebookEntry>();
        system.debug('Size: '+queriedProducts.size());

        for (Product2 p : queriedProducts)
        {
            LineItemWrapper tempWrapper = null;

            List<PriceEntry__c> currentPriceEntry = PricingService.getCurrentPriceEntryList(priceEntryMap,
                p.Id,
                ppc.getMainAccountGroup(),
                ppc.getMainCurrency(),
                ppc.getMainBillingCountryCode(),
                ppc.getOverallDiscount(),
                ppc.getDisposablesDiscount(),
                ppc.getServicesDiscount());

            List<PriceEntry__c> standardPriceEntry;

            if(ppc.getMainAccountGroup() != null) {
                standardPriceEntry = PricingService.getCurrentPriceEntryList(priceEntryMap,
                    p.Id,
                    null,
                    ppc.getMainCurrency(),
                    ppc.getMainBillingCountryCode(),
                    ppc.getOverallDiscount(),
                    ppc.getDisposablesDiscount(),
                    ppc.getServicesDiscount());
            } else {
                standardPriceEntry = currentPriceEntry;
            }

            System.debug('[getFilteredProducts] currentPriceEntry : ' + currentPriceEntry);

            if(currentPriceEntry != null) {
                tempWrapper = new LineItemWrapper(p,
                    ppc.getProductNameForLanguage(p, ppc.getLanguageForProducts()),
                    currentPriceEntry,
                    standardPriceEntry,
                    ppc.getOverallDiscount(),
                    ppc.getDisposablesDiscount(),
                    ppc.getServicesDiscount(),
                    p.ProductType__c,
                    false,
                    false);
            }


/*
        List<PricebookEntry> pListToInsert = new List<PricebookEntry>();
        system.debug('Size: '+queriedProducts.size());
        for (Product2 p : queriedProducts)
        {

            LineItemWrapper tempWrapper = null;

            if (productWithSpecialPrices.containsKey(p.Id) && productWithSpecialPrices.get(p.Id).size() > 0)
            {
                // use special prices
                tempWrapper = new LineItemWrapper(
                    p,
                    ppc.getProductNameForLanguage(p, ppc.getLanguageForProducts()),
                    productWithSpecialPrices.get(p.Id),
	                productWithStandardPrices.get(p.Id),
                    ppc.getOverallDiscount(),
                    ppc.getDisposablesDiscount(),
                    ppc.getServicesDiscount(),
                    p.ProductType__c, false, false);
            }
            else if (productWithStandardPrices.containsKey(p.Id) && productWithStandardPrices.get(p.Id).size() > 0)
            {
                // use standard prices
                tempWrapper = new LineItemWrapper(
                    p,
                    ppc.getProductNameForLanguage(p,  ppc.getLanguageForProducts()),
                    productWithStandardPrices.get(p.Id),
	                productWithStandardPrices.get(p.Id),
                    ppc.getOverallDiscount(),
                    ppc.getDisposablesDiscount(),
                    ppc.getServicesDiscount(),
                    p.ProductType__c, false, false);
            }
            */


            if (tempWrapper != null) //we have at least one price.
            {
                lineItemWrappers.add(tempWrapper);
                tempWrapper.productToListPrice = ppc.ProductToListPrice;

                // if a product does not have a price book entry create one
                if (p.PricebookEntries.size() == 0)
                {
                    pListToInsert.add(new PricebookEntry(UnitPrice= 0, Product2Id = p.id, IsActive = true,
                            Pricebook2Id = ProductPickerController.priceBookId,
                            UseStandardPrice = false,
                            CurrencyIsoCode = CurrIsoCode));
                }
            } else
            {
                // no prices (all are in wrong currency)
            }
        }
        System.debug('lineItemWrappers :: '+lineItemWrappers);
	    lineItemWrappers.sort();
	    System.debug('lineItemWrappers :: '+lineItemWrappers);

       //ProductPickerControllerLWC.getRelatedProducts(lineItemWrappers,ppc);
       return lineItemWrappers;
    }

	public static Boolean isDecimalStringNull(String decimalString) {
		return String.isBlank(decimalString) || Decimal.valueOf(decimalString) == 0;
	}

    public static List<PriceEntry__c> addNewPrice(Map<Id, List<PriceEntry__c>> theProductWithPrices, PriceEntry__c thePriceEntry)
    {
        if (!theProductWithPrices.containsKey(thePriceEntry.Product__c))
        {
            theProductWithPrices.put(thePriceEntry.Product__c, new List<PriceEntry__c>());
        }
        List<PriceEntry__c> prices = theProductWithPrices.get(thePriceEntry.Product__c);
        if (prices.size() > 0 && prices[prices.size()-1].ScalingTo__c == thePriceEntry.ScalingTo__c)
        {
            // same scale, we should check valid from. The latest should win.
            if (prices[prices.size()-1].ValidFrom__c < thePriceEntry.ValidFrom__c)
            {
                //our price win, replace the existing price with our new one.
                prices.remove(prices.size()-1);
                prices.add(thePriceEntry);
            } else
            {
                // our price lost at the same price, do nothing
            }
        } else
        {
            // add price anyway
            prices.add(thePriceEntry);
        }
        return prices;
    }

    @AuraEnabled
    public static String getSelectedProducts(String recordId, String newSelectedproducts, String alreadyselectedProducts)
    {
        System.debug('getSelectedProducts() :: '+recordId+  ' newSelectedproducts  ::'+newSelectedproducts+ ' alreadyselectedProducts :: '+alreadyselectedProducts);

        Map<String, String> resultMap = new Map<String, String>();
        List<LineItemWrapper> lineItemWrappers = (List<LineItemWrapper>)JSON.deserialize(newSelectedproducts, List<LineItemWrapper>.class);
        List<LineItemWrapper> selectedLineItemWrappers = (List<LineItemWrapper>)JSON.deserialize(alreadyselectedProducts, List<LineItemWrapper>.class);

        // ProductPickerOpportunityController ppo = new ProductPickerOpportunityController(oppId);
        ProductPickerControllerLWC ppo = new ProductPickerControllerLWC(recordId);

        ppo.lineItemWrappers = lineItemWrappers;//newSelectedproducts
        ppo.selectedLineItemWrappers = selectedLineItemWrappers;//alreadyselectedProducts
        ppo.productToListPrice = lineItemWrappers[0].productToListPrice;

        ppo.doAddFilteredProducts();

        return JSON.serializePretty(ppo.selectedLineItemWrappers);
    }

    @AuraEnabled
    public static String addSelectedProductToOpp(String recordId, String selectedProducts)
    {
        System.debug('addSelectedProductToOpp called '+selectedProducts+ ' recordId '+recordId);
        Database.SaveResult[] sr;
        ProductPickerControllerLWC ppc = new ProductPickerControllerLWC((Id)recordId);
        List<SObject> sobjList = new List<SObject>();

        List<LineItemWrapper> selectedLineItemWrappers = (List<LineItemWrapper>)JSON.deserialize(selectedProducts, List<LineItemWrapper>.class);

        Set<Id> productIds = new Set<Id>();
        for (LineItemWrapper wrapper: selectedLineItemWrappers)
        {
            productIds.add(wrapper.product.id);
            system.debug('wrapper.product.id '+wrapper.product.id);
        }

        system.debug('productIds'+productIds);
        system.debug('ppc.getMainCurrency()'+ppc.getMainCurrency());
        system.debug('addSelectedProductToOpp selectedLineItemWrappers '+selectedLineItemWrappers);

        Map<Id, PricebookEntry> productForPricebookEntry = new Map<Id, PricebookEntry>();

        List<PricebookEntry> pbe = [SELECT Id, Product2Id, UnitPrice FROM PricebookEntry WHERE
                Product2Id IN : productIds AND CurrencyIsoCode =: ppc.getMainCurrency() ];

                 system.debug('pbe :: '+pbe);

      for (PricebookEntry p : pbe)
        {
            system.debug('PricebookEntry p '+p);
            system.debug('p.Product2Id '+p.Product2Id);

            productForPricebookEntry.put(p.Product2Id, p);
        }
        system.debug('productForPricebookEntry :: '+productForPricebookEntry);

        for (LineItemWrapper wrapper: selectedLineItemWrappers)
        {
	        System.debug('wrapper = ' + wrapper);
            if(String.valueOf(recordId).startsWith('006'))
            {
                OpportunityLineItem anItem = wrapper.item;
	            System.debug('wrapper.item = ' + wrapper.item);
	            System.debug('anItem = ' + anItem);
	            anItem.OpportunityId = recordId;
                system.debug('wrapper.product.id '+wrapper.product.id);
                anItem.PricebookEntryId = productForPricebookEntry.get(wrapper.product.id).id;
                anItem.UnitPrice = wrapper.unitPrice;
                anItem.Quantity = wrapper.qty;
                anItem.ListPriceTierPricing__c = wrapper.listPriceTierPricing;
                anItem.Standard_Price__c = wrapper.standardPrice;

                if(wrapper.isDemoEquipment == true)
                {
                    anItem.DemoEquipment__c = wrapper.isDemoEquipment;
                }
                else{
                    anItem.DemoEquipment__c = false;
                }
                sobjList.add(anItem);
            }
            else if(String.valueOf(recordId).startsWith('0Q0'))
            {
                QuoteLineItem anItem = wrapper.qteLineItem;
                anItem.QuoteId = recordId;
                anItem.PricebookEntryId = productForPricebookEntry.get(wrapper.product.id).id;
                anItem.UnitPrice = wrapper.unitPrice;
                anItem.Quantity = wrapper.qty;
                anItem.ListPriceTierPricing__c = wrapper.listPriceTierPricing;
                anItem.Standard_Price__c = wrapper.standardPrice;

	            if(wrapper.isDemoEquipment == true)
                {
                    anItem.DemoEquipment__c = wrapper.isDemoEquipment;
                }
                else{
                    anItem.DemoEquipment__c = false;
                }

                sobjList.add(anItem);
            }
        }

        system.debug('sobjList: '+sobjList);

        try {
            if(!sobjList.isEmpty()){
                sr = Database.insert(sobjList);
            }
        } catch (Exception e)
        {
	        System.debug('Exception: ' + e.getMessage());
            return 'Failure';
        }

        if(sr != null && !sr.isEmpty())
        {
            System.debug('sr :: '+sr);
            for (Database.SaveResult result : sr) {
                System.debug('sr result :: '+result);
                if(result.isSuccess())
                {
                    return 'Success';
                }
                else {
                        for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred. '+err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }
        }
        return 'Failure';
    }

    /**
    *   @description add products to the Database.
    */
    public override PageReference doAddProducts()
    {
        System.debug('##### doAddProd');
        Set<Id> productIds = new Set<Id>();
        for (LineItemWrapper wrapper: selectedLineItemWrappers)
        {
            productIds.add(wrapper.product.id);
        }

        Map<Id, PricebookEntry> productForPricebookEntry = new Map<Id, PricebookEntry>();
        for (PricebookEntry p : [SELECT Id, Product2Id, UnitPrice FROM PricebookEntry WHERE
                Product2Id IN : productIds AND CurrencyIsoCode = :getMainCurrency()])
        {
            productForPricebookEntry.put(p.Product2Id, p);
        }

        List<OpportunitylineItem> anItems = new List<OpportunitylineItem>();
        for (LineItemWrapper wrapper: selectedLineItemWrappers)
        {
            OpportunitylineItem anItem = wrapper.item;
            anItem.OpportunityId = getMainId();
            anItem.PricebookEntryId = productForPricebookEntry.get(wrapper.product.id).id;
            anItem.UnitPrice = wrapper.unitPrice;
            anItem.Quantity = wrapper.qty;
            anItem.ListPriceTierPricing__c = wrapper.listPriceTierPricing;
            anItem.Standard_Price__c = wrapper.standardPrice;
            // anItem.DemoEquipment__c = wrapper.DemoEquipment__c;
            anItems.add(anItem);
        }
        insert anItems;
        return redirectToPreviousPage();
    }

    public override PageReference cloneOpportunityWithLineItem()
    {
        return redirectToPreviousPage();
    }
    public override PageReference reCalculateProductPrice()
    {
        return redirectToPreviousPage();
    }


    public class Condition
    {
        @AuraEnabled
        public String operator {get;set;}

        @AuraEnabled
        public Object value {get;set;}
    }

    private static List<String> getStringArray(System.JSONParser p) {
        List<String> res = new List<String>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(p.getText());
        }
        return res;
    }
}
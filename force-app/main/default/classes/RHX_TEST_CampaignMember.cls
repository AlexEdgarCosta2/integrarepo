/**

**/
@isTest(SeeAllData=true)
public without sharing class RHX_TEST_CampaignMember {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id
			FROM CampaignMember LIMIT 10];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new CampaignMember()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}
/**
 * @description       :
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   05-07-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class QuoteLineItemService {

	public static final String TYPE_PRODUCT_CHANGED = 'Product Changed';
	public static final String TYPE_PRODUCT_ADDED = 'Product Added';
	public static final String TYPE_PRODUCT_REMOVED = 'Product Removed';

	public static void syncToOpportunityLineItems(List<QuoteLineItem> quoteLineItems) {
		Set<Id> quoteIds = new Set<Id>();
		Map<Id, QuoteLineItem> quoteLineItemByOppLineItemMap = new Map<Id, QuoteLineItem>();

		for (QuoteLineItem quoteLineItem : quoteLineItems) {
			quoteIds.add(quoteLineItem.QuoteId);
			quoteLineItemByOppLineItemMap.put(quoteLineItem.OpportunityLineItemId, quoteLineItem);
		}

		Map<Id, Quote> quoteMap = new Map<Id, Quote>([
			SELECT Id, IsSyncing
			FROM Quote
			WHERE Id IN :quoteIds
		]);
		if (quoteLineItemByOppLineItemMap.isEmpty()) {
			return;
		}
		List<OpportunityLineItem> opportunityLineItems = [
			SELECT Id, Description, DemoEquipment__c, SortOrder__c, UnitPrice, Quantity, ListPriceTierPricing__c, ListPriceOneItem__c, PricebookEntryId, ListPrice, List_Price__c
			FROM OpportunityLineItem
			WHERE Id IN :quoteLineItemByOppLineItemMap.keySet()
		];
		Set<OpportunityLineItem> opportunityLineItemsToUpdate = new Set<OpportunityLineItem>();

		for (OpportunityLineItem opportunityLineItem : opportunityLineItems) {
			QuoteLineItem quoteLineItem = quoteLineItemByOppLineItemMap.get(opportunityLineItem.Id);
			Quote quote = quoteMap.get(quoteLineItem.QuoteId);
			if (!quote.IsSyncing) {
				continue;
			}

			Boolean needToUpdate = false;
			for (String customFieldToSync : OpportunityQuoteHelper.customLineItemFieldsToSync) {
				if (opportunityLineItem.get(customFieldToSync) != quoteLineItem.get(customFieldToSync)) {
					opportunityLineItem.put(customFieldToSync, quoteLineItem.get(customFieldToSync));

					needToUpdate = true;
				}
			}

			if (needToUpdate) {
				opportunityLineItemsToUpdate.add(opportunityLineItem);
			}
		}

		if (!opportunityLineItemsToUpdate.isEmpty()) {
			update new List<OpportunityLineItem>(opportunityLineItemsToUpdate);
		}
	}

	public static void getDataFromSyncedOpportunityLineItems(List<QuoteLineItem> quoteLineItems) {
		Set<Id> quoteIds = new Set<Id>();
		Set<Id> opportunityLineItemIds = new Set<Id>();

		for (QuoteLineItem quoteLineItem : quoteLineItems) {
			quoteIds.add(quoteLineItem.QuoteId);
			opportunityLineItemIds.add(quoteLineItem.OpportunityLineItemId);
		}

		if (opportunityLineItemIds.isEmpty()) {
			return;
		}

		Map<Id, OpportunityLineItem> opportunityLineItemMap = new Map<Id, OpportunityLineItem>([
			SELECT Id, Description, DemoEquipment__c, SortOrder__c, UnitPrice, Quantity, ListPriceTierPricing__c, ListPriceOneItem__c, PricebookEntryId, List_Price__c, ListPrice
			FROM OpportunityLineItem
			WHERE Id IN :opportunityLineItemIds
		]);

		Map<Id, Quote> quoteMap = new Map<Id, Quote>([
			SELECT Id, IsSyncing
			FROM Quote
			WHERE Id IN :quoteIds
		]);

		for (QuoteLineItem quoteLineItem : quoteLineItems) {
			if (!opportunityLineItemMap.containsKey(quoteLineItem.OpportunityLineItemId) || !quoteMap.get(quoteLineItem.QuoteId).IsSyncing) {
				continue;
			}

			OpportunityLineItem opportunityLineItem = opportunityLineItemMap.get(quoteLineItem.OpportunityLineItemId);
			for (String customFieldToSync : OpportunityQuoteHelper.customLineItemFieldsToSync) {
				if (opportunityLineItem.get(customFieldToSync) != quoteLineItem.get(customFieldToSync)) {
					quoteLineItem.put(customFieldToSync, opportunityLineItem.get(customFieldToSync));
				}
			}
		}
	}

	public static void createQuoteLIHistoty(Map<Id, sObject> newMap, Map<Id, sObject> oldMap)
	{
		OpportunityLineItemService.createObjectLIHistoty(newMap, oldMap);
	}

	public static void setDiscountApprovalProcesses(List<SObject> newList, Map<Id, SObject> oldMap) {
		ApprovalUtil.setDiscountApprovalProcesses(Constants.OBJECT_TYPE_QUOTE_LINE_ITEM, (List<QuoteLineItem>)newList, (Map<Id, QuoteLineItem>)oldMap);
	}

}
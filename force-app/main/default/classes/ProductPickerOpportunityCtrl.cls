/*
*	@description Opportunity Produkt picker functionality for Lightning component.
*
*	@author dyas, ikl
*	@copyright PARX
*/
public with sharing class ProductPickerOpportunityCtrl
{

    @AuraEnabled
    public static String getFilteredProducts(String oppId, String productCode, String productName, String productFamily, String productType)
    {

        List<LineItemWrapper> lineItemWrappers = new List<LineItemWrapper>();

        ProductPickerOpportunityController ppo = new ProductPickerOpportunityController(oppId);

        productType = (productType == 'All') ? '' : productType;

        ppo.filterProductCode = productCode;
        ppo.filterProductName = productName;
        ppo.filterProductFamily = productFamily;
        ppo.filterProductType = productType;

        ppo.doFiterProducts();

        return JSON.serializePretty(ppo.lineItemWrappers);

    }

    @AuraEnabled
    public static Map<String, String> getSelectedProducts(String oppId, String products, String selectedProducts)
    {

        Map<String, String> resultMap = new Map<String, String>();
        List<LineItemWrapper> lineItemWrappers = (List<LineItemWrapper>)JSON.deserialize(products, List<LineItemWrapper>.class);
        List<LineItemWrapper> selectedLineItemWrappers = (List<LineItemWrapper>)JSON.deserialize(selectedProducts, List<LineItemWrapper>.class);

        ProductPickerOpportunityController ppo = new ProductPickerOpportunityController(oppId);

        ppo.lineItemWrappers = lineItemWrappers;
        ppo.selectedLineItemWrappers = selectedLineItemWrappers;
        ppo.productToListPrice = lineItemWrappers[0].productToListPrice;

        ppo.doAddFilteredProducts();

        resultMap.put('products', JSON.serializePretty(ppo.lineItemWrappers));
        resultMap.put('selectedProducts', JSON.serializePretty(ppo.selectedLineItemWrappers));

        return resultMap;

    }

    @AuraEnabled
    public static String removeSelectedProduct(String oppId, String productId, String selectedProducts)
    {

        List<LineItemWrapper> selectedLineItemWrappers = (List<LineItemWrapper>)JSON.deserialize(selectedProducts, List<LineItemWrapper>.class);

        ProductPickerOpportunityController ppo = new ProductPickerOpportunityController(oppId);

        ppo.productIdParam = productId;
        ppo.selectedLineItemWrappers = selectedLineItemWrappers;

        ppo.doDeleteItem();

        return JSON.serializePretty(ppo.selectedLineItemWrappers);

    }

    @AuraEnabled
    public static void addSelectedProductToOpp(String oppId, String selectedProducts)
    {
        System.debug('addSelectedProductToOpp');
        System.debug(oppId);
        System.debug(selectedProducts);
        List<LineItemWrapper> selectedLineItemWrappers = (List<LineItemWrapper>)JSON.deserialize(selectedProducts, List<LineItemWrapper>.class);
        System.debug('selectedLineItemWrappers');
        System.debug(selectedLineItemWrappers);
        ProductPickerOpportunityController ppo = new ProductPickerOpportunityController(oppId);

        ppo.selectedLineItemWrappers = selectedLineItemWrappers;

        ppo.doAddProducts();

    }

    @AuraEnabled
    public static Map<String, List<Map<String, String>>> getPicklistOptions()
    {

        Map<String, List<Map<String, String>>> picklistOptionsMap = new Map<String, List<Map<String, String>>>();

        picklistOptionsMap.put('ProductType__c', getPicklistValues('Product2', 'ProductType__c'));

        return picklistOptionsMap;

    }

    public static List<Map<String, String>> getPicklistValues(String objName, String fieldName)
    {

        List<Map<String, String>> optionsMapList = new List<Map<String, String>>{
            new Map<String, String>{'label' => 'All', 'value' => 'All'}
        };
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(objName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        list<Schema.PicklistEntry> values = fieldMap.get(fieldName).getDescribe().getPickListValues();

        for (Schema.PicklistEntry pe : values)
        {

            Map<String, String> optionsMap = new Map<String, String>();
            optionsMap.put('label', pe.getLabel());
            optionsMap.put('value', pe.getLabel());
            optionsMapList.add(optionsMap);

        }

        return optionsMapList;

    }

}
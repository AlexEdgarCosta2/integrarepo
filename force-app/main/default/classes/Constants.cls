/**
 * @description       : Constants class
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class Constants {
    // Email templates
    public static final String DISCOUNT_APPROVAL_NOTIFICATION_EMAIL_TEMPLATE = 'Discount_Approval_Notification';

    // OrgWideEmailAddress
    public static final String DISCOUNT_APPROVAL_EMAIL_ADDRESS = System.Label.DISCOUNT_APPROVAL_EMAIL_ADDRESS;

    //Labels
    public static final String APPROVAL_MAIL_PRODUCT =  System.Label.APPROVAL_MAIL_PRODUCT;
    public static final String APPROVAL_MAIL_QUANTITY =  System.Label.APPROVAL_MAIL_QUANTITY;
    public static final String APPROVAL_MAIL_SELLING_PRICE =  System.Label.APPROVAL_MAIL_SELLING_PRICE;
    public static final String APPROVAL_MAIL_NEW_PRICE =  System.Label.APPROVAL_MAIL_NEW_PRICE;
    public static final String APPROVAL_MAIL_DISCOUNT =  System.Label.APPROVAL_MAIL_DISCOUNT;
    public static final String APPROVAL_MAIL_COUNTRY_MANAGER_SALUTATION = System.Label.APPROVAL_MAIL_COUNTRY_MANAGER_SALUTATION;
    public static final String APPROVAL_MAIL_GLOBAL_HEAD_SALUTATION = System.Label.APPROVAL_MAIL_GLOBAL_HEAD_SALUTATION;
    public static final String APPROVAL_MAIL_DIRECTOR_SALUTATION = System.Label.APPROVAL_MAIL_DIRECTOR_SALUTATION;

    // Record Types
    public static final String CASE_DISCOUNTS_APPROVAL_RT_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Discounts Approval').getRecordTypeId();

    public static Id standardPriceBookId {
        get {
            if(standardPriceBookId == null) {
                if(Test.isRunningTest()) { standardPriceBookId = Test.getStandardPricebookId(); } else { standardPriceBookId = PriceBookSelector.getStandardPriceBookId(); }
            }
            return StandardPriceBookId;
        }
        private set;
    }

    public static final String OBJECT_TYPE_OPPORTUNITY = 'OPPORTUNITY';
    public static final String OBJECT_TYPE_QUOTE = 'QUOTE';
    public static final String OBJECT_TYPE_OPPORTUNITY_LINE_ITEM = 'OPPORTUNITY_LINE_ITEM';
    public static final String OBJECT_TYPE_QUOTE_LINE_ITEM = 'QUOTE_LINE_ITEM';

    public static final String PRODUCT_TYPE_SERVICES = 'Services';
    public static final String PRODUCT_TYPE_DISPOSABLES = 'Disposables';
    public static final String PRODUCT_TYPE_ACCESSORIES = 'Products and Accessories';

    //public static final Integer APPROVAL_GLOBAL_HEAD_OR_DIRECTOR = 3;
    //public static final Integer APPROVAL_GLOBAL_HEAD = 2;
    public static final Integer APPROVAL_COUNTRY_MANAGER = 1;
    public static final Integer NO_APPROVAL = 0;

    public static final String APPROVAL_PROCESS_APPROVE_ACTION = 'Approve';
    public static final String APPROVAL_PROCESS_REJECT_ACTION = 'Reject';

    public static final String DISCOUNT_APPROVER_GLOBAL_HEAD = 'Global Head';
    public static final String DISCOUNT_APPROVER_DIRECTOR_EU = 'Director EU';
    public static final String DISCOUNT_APPROVER_COUNTRY_MANAGER = 'Country Manager';

    public static final String APPROVAL_STATUS_PENDING = 'Pending';
    public static final String APPROVAL_STATUS_REJECTED = 'Rejected';

    public static final String DISCOUNTS_APPROVAL_MAIL_SUBJECT_TAG = '[DISCOUNTS APPROVAL PROCESS]';

    public static final String APPROVAL_ACTION_APPROVED = 'Approve';
    public static final String APPROVAL_ACTION_REJECTED = 'Reject';
    public static final String APPROVAL_ACTION_REMOVED = 'Removed';

    public static final String APPROVAL_MAIL_OUTCOME_NO = 'NO';
    public static final String APPROVAL_MAIL_OUTCOME_YES = 'YES';
    public static final String APPROVAL_MAIL_OUTCOME_REJECT = 'REJECT';
    public static final String APPROVAL_MAIL_OUTCOME_APPROVE = 'APPROVE';
    public static final String APPROVAL_MAIL_OUTCOME_REJECTED = 'REJECTED';
    public static final String APPROVAL_MAIL_OUTCOME_APPROVED = 'APPROVED';

    public static final String INTEGRA_BLUE = '#2D9D9F';
    public static final String INTEGRA_LIGHT_BLUE = '#E1F0F0';
    public static final String INTEGRA_YELLOW = '#FF9933';
    public static final String INTEGRA_SALMON_PINK = '#FFCC99';
    public static final String INTEGRA_GREY = '#666666';

    public static final String OTHER_COUNTRIES_DISCOUNT_CONFIG = 'ALL';

    // Opportunity.Stage Name
    public static final String OPPORTUNITY_STAGE_NAME_OPPORTUNITY_ANALYSIS = 'Opportunity Analysis';
    public static final String OPPORTUNITY_STAGE_NAME_QUOTATION = 'Quotation';

    public static final String CONTACT_PREFERRED_CUSTOMER_LANGUAGE_ENGLISH = 'English';
    public static final String CONTACT_PREFERRED_CUSTOMER_LANGUAGE_CHINESE = 'Chinese';
    public static final String CONTACT_PREFERRED_CUSTOMER_LANGUAGE_FRENCH = 'French';
    public static final String CONTACT_PREFERRED_CUSTOMER_LANGUAGE_GERMAN = 'German';
    public static final String CONTACT_PREFERRED_CUSTOMER_LANGUAGE_JAPANESE = 'Japanese';
}
/*
*	@description Test class for AktionTriggerHandlerUpdateRelations functionality.
*
*	@author ach
*	@copyright PARX
*/
@isTest
public with sharing class Test_CaseProductTriggerHandler
{
	/*
	*	@description test rollup summary.
	*/
	static testMethod void testRollupSummary()
	{
		TriggerTemplateV2.setupUnitTest();
		Case caseObj = new Case(CurrencyIsoCode = 'CHF');
		insert caseObj;
		List<CaseProduct__c> lineItems = new List<CaseProduct__c>{
				new CaseProduct__c(Case__c = caseObj.Id, EffectiveCost__c = 2, AppliedCost__c = 3, Quantity__c = 2, CurrencyIsoCode = 'CHF'),
				new CaseProduct__c(Case__c = caseObj.Id, EffectiveCost__c = 10, AppliedCost__c = 20, Quantity__c = 1, CurrencyIsoCode = 'CHF')};
		Test.startTest();
		TriggerTemplateV2.startUnitTest();
		insert lineItems;
		caseObj = [SELECT Id, TotalEffectiveCost__c, TotalAppliedCost__c FROM Case WHERE Id = :caseObj.Id];
		System.assertEquals(12, caseObj.TotalEffectiveCost__c, 'Wrong TotalEffectiveCost__c');
		System.assertEquals(23, caseObj.TotalAppliedCost__c, 'Wrong TotalAppliedCost__c');
		
		TriggerTemplateV2.setupUnitTest();
		TriggerTemplateV2.startUnitTest();
		lineItems[0].AppliedCost__c = 0;
		update lineItems;
		caseObj = [SELECT Id, TotalEffectiveCost__c, TotalAppliedCost__c FROM Case WHERE Id = :caseObj.Id];
		System.assertEquals(12, caseObj.TotalEffectiveCost__c, 'Wrong TotalEffectiveCost__c');
		System.assertEquals(20, caseObj.TotalAppliedCost__c, 'Wrong TotalAppliedCost__c');
		
		TriggerTemplateV2.setupUnitTest();
		TriggerTemplateV2.startUnitTest();
		delete lineItems[1];
		
		Test.stopTest();
		caseObj = [SELECT Id, TotalEffectiveCost__c, TotalAppliedCost__c FROM Case WHERE Id = :caseObj.Id];
		System.assertEquals(2, caseObj.TotalEffectiveCost__c, 'Wrong TotalEffectiveCost__c');
		System.assertEquals(0, caseObj.TotalAppliedCost__c, 'Wrong TotalAppliedCost__c');
	}
}
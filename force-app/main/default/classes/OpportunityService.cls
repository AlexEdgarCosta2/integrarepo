/**
 * Opportunity business logic.
 *
 * @author ach
 */
public with sharing class OpportunityService {
	public static Map<String, String> fieldsToSyncMap = new Map<String, String>{
		'DiscountDisposables__c' => 'DiscountDisposables__c',
		'DiscountProducts__c' => 'DiscountPercentage__c',
		'DiscountServices__c' => 'DiscountServices__c'
	};

	public static void syncToQuotes(List<Opportunity> opportunitiesNewList, Map<Id, Opportunity> opportunitiesOldMap) {
		List<Quote> quoteList = [
			SELECT Id, OpportunityId, DiscountDisposables__c, DiscountProducts__c, DiscountServices__c
			FROM Quote
			WHERE OpportunityId IN :opportunitiesNewList
		];

		if (quoteList.isEmpty()) {
			return;
		}

		Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>(opportunitiesNewList);
		List<Quote> quotesToUpdate = new List<Quote>();

		for (Quote quote : quoteList) {
			Boolean needToUpdate = false;
			Opportunity opportunityNew = opportunityMap.get(quote.OpportunityId);
			Opportunity opportunityOld = opportunitiesOldMap.get(quote.OpportunityId);

			for (String fieldQuote : fieldsToSyncMap.keySet()) {
				String fieldOpportunity = fieldsToSyncMap.get(fieldQuote);
				if (
					opportunityNew.get(fieldOpportunity) != opportunityOld.get(fieldOpportunity) &&
						quote.get(fieldQuote) != opportunityNew.get(fieldOpportunity)
					) {
					quote.put(fieldQuote, opportunityNew.get(fieldOpportunity));
					needToUpdate = true;
				}

			}

			if (needToUpdate) {
				quotesToUpdate.add(quote);
			}
		}

		if (!quotesToUpdate.isEmpty()) {
			OpportunityQuoteHelper.isSyncedFromOpportunity = true;
			update quotesToUpdate;
		}
	}

	/**
	* @description On insert or update sets the Stage to Quotation when the conditions are met.
	* @author alexandre.costa@integra-biosciences.com | 05-27-2021
	* @param opportunityRecord
	**/
	public static void setStageQuotation(Opportunity opportunityRecord) {
		if(opportunityRecord.StageName == Constants.OPPORTUNITY_STAGE_NAME_OPPORTUNITY_ANALYSIS &&
			opportunityRecord.Version__c > 1 &&
			opportunityRecord.HasOpportunityLineItem) {

			opportunityRecord.StageName = Constants.OPPORTUNITY_STAGE_NAME_QUOTATION;
		}
	}

	/**
	* @description Init the discounts with 0 if they are null
	* @author alexandre.costa@integra-biosciences.com | 05-27-2021
	* @param opportunityRecord
	**/
	public static void initDiscountFields(Opportunity opportunityRecord) {
		if(opportunityRecord.DiscountPercentage__c == null) {
			opportunityRecord.DiscountPercentage__c = 0;
		}
		if(opportunityRecord.DiscountServices__c == null) {
			opportunityRecord.DiscountServices__c = 0;
		}
		if(opportunityRecord.DiscountDisposables__c == null) {
			opportunityRecord.DiscountDisposables__c = 0;
		}
	}

	/**
	* @description Set the
	* @author alexandre.costa@integra-biosciences.com | 05-27-2021
	* @param opportunityRecord
	**/
	public static void setAutoNumberOppName(Opportunity opportunityRecord) {
		String autoNumber = String.valueOf(TriggerUtils.getAutonumber('Opportunity')).replace('.0', '');

		String autoNumber2 = autoNumber.leftPad(8,'0');

		opportunityRecord.Name = String.valueOf(Date.today().year()).substring(2) + autoNumber2;
	}

	/**
	* @description
	* @author alexandre.costa@integra-biosciences.com | 06-07-2021
	* @param opportunityRecord
	**/
	public static void setLanguage(Opportunity opportunityRecord) {
		if(opportunityRecord.Created_from_Lead_Conversion__c == false) {
			opportunityRecord.ProductLanguage__c = opportunityRecord.Preferred_Customer_Language__c;
		}
	}
}
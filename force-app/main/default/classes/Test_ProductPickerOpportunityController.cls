/*
*   @description Test class for ProductPickerOpportunityController functionality.
*
*   @author ach, yle
*   @copyright PARX
*/
@isTest
public with sharing class Test_ProductPickerOpportunityController
{
    public static Opportunity testOpportunity;

    private static void initData()
    {
        AccountGroup__c anAccountGroup = new AccountGroup__c(Name = 'asd');
        insert anAccountGroup;

        Account anAccount = new Account(Name = 'asd account', AccountGroup__c = anAccountGroup.Id, BillingCountryCode = 'US', BillingState='California', CurrencyIsoCode = 'USD');
        insert anAccount;

        List<Product2> aProducts = new List<Product2>{
                new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductNameDE__c = 'test 1 de', ProductNameFR__c = 'test 1 FR', ProductFamily__c = 'f1', PricingGroup__c = '1',
                        ProductLanguage__c = 'English'),
                new Product2(Name = 'inactive product', IsActive = false, ProductCode = 'test_asd2', ProductNameDE__c = 'test de ', ProductNameFR__c = 'test FR', ProductFamily__c = 'f1'),
                new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductNameDE__c = 'test 2 de', ProductNameFR__c = 'test 2 FR', ProductFamily__c = 'f1', PricingGroup__c = '1'),
                new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductNameDE__c = 'test 3 de', ProductNameFR__c = 'test 3 FR', ProductFamily__c = 'f2', PricingGroup__c = '2')};
        insert aProducts;

        testOpportunity = new Opportunity(Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert', CloseDate = date.today(), ProductLanguage__c = 'English',
                                         CurrencyIsoCode = 'USD');
        insert testOpportunity;

        List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
                new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
        insert markets;

        List<PriceEntry__c> aPriceEntries = new List<PriceEntry__c>{
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 10, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),
            //valid date is older than for previous price:
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 9, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today().addDays(-2), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),
            //wrong market:
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 999999, SellingPrice__c = 100, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today(), Market__c = markets[1].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),
            //wrong valid date:
            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 99999, SellingPrice__c = 100, AccountGroup__c = anAccountGroup.Id, ValidFrom__c = date.today().addDays(2), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),

            new PriceEntry__c(Product__c = aProducts.get(2).Id, ScalingTo__c = 6, SellingPrice__c = 200,
                    ScalingTo2__c = 999999, DiscountTier2__c = 25,
                    ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),
            //new PriceEntry__c(Product__c = aProducts.get(2).Id, ScalingTo__c = 999999, SellingPrice__c = 150, ValidFrom__c = date.today(), Market__c = markets[0].Id),

            new PriceEntry__c(Product__c = aProducts.get(3).Id, ScalingTo__c = 3, SellingPrice__c = 600,
                    ScalingTo2__c = 999999, DiscountTier2__c = 10, //540, not 500
                    ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode),
            //new PriceEntry__c(Product__c = aProducts.get(3).Id, ScalingTo__c = 999999, SellingPrice__c = 500, ValidFrom__c = date.today(), Market__c = markets[0].Id),

            new PriceEntry__c(Product__c = aProducts.get(0).Id, ScalingTo__c = 1, SellingPrice__c = 20, ValidFrom__c = date.today(), Market__c = markets[0].Id, CurrencyIsoCode = testOpportunity.CurrencyIsoCode)};
        insert aPriceEntries;

        Id pricebookId = Test.getStandardPricebookId();

        List<PricebookEntry> pbes = new List<PricebookEntry>{
                new PricebookEntry(Pricebook2id = pricebookId, Product2id = aProducts.get(0).ID, isActive = true, CurrencyIsoCode = 'USD', unitPrice = 100),
                new PricebookEntry(Pricebook2id = pricebookId, Product2id = aProducts.get(1).ID, isActive = true, CurrencyIsoCode = 'USD', unitPrice = 100),
                new PricebookEntry(Pricebook2id = pricebookId, Product2id = aProducts.get(2).ID, isActive = true, CurrencyIsoCode = 'USD', unitPrice = 100),
                new PricebookEntry(Pricebook2id = pricebookId, Product2id = aProducts.get(3).ID, isActive = true, CurrencyIsoCode = 'USD', unitPrice = 100)
            };
            insert pbes;
    }

    /*
    *   @description test main logic.
    */
//     static testMethod void testMainLogic()
//     {
//         initData();

//         PageReference aPage = Page.ProductPickerOpportunity;
//         aPage.getParameters().put('id', testOpportunity.Id);
//         Test.setCurrentPage(aPage);
//         Test.startTest();
//         ApexPages.StandardController sc = new ApexPages.standardController(testOpportunity);
//         ProductPickerOpportunityController aController = new ProductPickerOpportunityController(sc);
//         aController.filterProductFamily = 'f1';
//         aController.filterProductCode = 'test_asd1';
//         aController.filterProductName = 'active';
//         aController.filterLanguage = 'English';
//         aController.doFiterProducts();
//         System.assertEquals(1, aController.lineItemWrappers.size(), 'wrong number of selected products');

//         aController.filterLanguage = '';
//         aController.doFiterProducts();
//         System.assertEquals(1, aController.lineItemWrappers.size(), 'wrong number of selected products');
//         System.assertEquals('≤ 1: 20.00 \n', aController.lineItemWrappers.get(0).productScaling, 'wrong Tier Pricing for first product');

//         //set quantity and add to selected products
//         aController.lineItemWrappers.get(0).quantity = '1';
//         aController.doAddFilteredProducts();

//         //check the price
//         System.assertEquals(20.00, aController.selectedLineItemWrappers.get(0).UnitPrice, 'wrong UnitPrice for first product');
//         System.assertEquals(20.00, aController.selectedLineItemWrappers.get(0).ListPriceTierPricing, 'wrong ListPriceTierPricing__c for first product');

//         // check whether the special price list the appropriate list price
//         System.assertEquals(20.00, aController.selectedLineItemWrappers.get(0).item.ListPriceOneItem__c, 'wrong ListPriceOneItem__c for first product');

//         aController.doClearProducts();
//         System.assertEquals(0, aController.selectedLineItemWrappers.size(), 'wrong number of selected products');

//         //set quantity and add to selected products
//         aController.lineItemWrappers.get(0).quantity = '4';
//         aController.doAddFilteredProducts();

//         //check the price
//         System.assertEquals(20.00, aController.selectedLineItemWrappers.get(0).UnitPrice, 'wrong UnitPrice for first product');

//         aController.productIdParam = aController.selectedLineItemWrappers.get(0).product.Id;
//         aController.doDeleteItem();
//         System.assertEquals(0, aController.selectedLineItemWrappers.size(), 'wrong UnitPrice for first product');
//     }

    /*
    *   @description test main logic.
    */
    static testMethod void testOverallDiscount()
    {
        initData();
        testOpportunity.DiscountPercentage__c = 10;
        update testOpportunity;

        PageReference aPage = Page.ProductPickerOpportunity;
        aPage.getParameters().put('id', testOpportunity.Id);
        Test.setCurrentPage(aPage);
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(testOpportunity);
        ProductPickerOpportunityController aController = new ProductPickerOpportunityController(sc);
        //ProductPickerOpportunityController aController = new ProductPickerOpportunityController();
        aController.filterProductFamily = 'f1';
        aController.filterProductCode = 'test_asd1';
        aController.filterProductName = 'active';
        aController.filterLanguage = 'English';
        aController.doFiterProducts();
        System.assertEquals(1, aController.lineItemWrappers.size(), 'wrong number of selected products');

        aController.filterLanguage = '';
        aController.doFiterProducts();
        System.assertEquals(1, aController.lineItemWrappers.size(), 'wrong number of selected products');

        //set quantity and add to selected products
        aController.lineItemWrappers.get(0).quantity = '1';
        aController.doAddFilteredProducts();

        // check whether the special price list the appropriate list price
        System.assertEquals(20.00, aController.selectedLineItemWrappers.get(0).item.ListPriceOneItem__c, 'wrong ListPriceOneItem__c for first product');

        aController.doClearProducts();
        System.assertEquals(0, aController.selectedLineItemWrappers.size(), 'wrong number of selected products');
        Test.stopTest();
    }
}
/**
 * @description       : Selector for custom metadata
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class CustomMetadataSelector {
    /**
    * @description Get by developer name
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param developerName
    * @param objectName
    * @param fields
    * @return SObject
    **/
    public static SObject getByDeveloperName(String developerName, String objectName, Set<String> fields) {
        String soqlStr = 'SELECT ';

        for(String field : fields) {
            soqlStr += field + ',';
        }

        soqlStr = soqlStr.removeEnd(',');

        soqlStr += ' FROM ' + objectName + ' WHERE developerName=\'' + developerName + '\'';

        List<SObject> auxList = Database.query(soqlStr);

        if(auxList == null || auxList.isEmpty()) {
            return null;
        } else {
            return auxList[0];
        }
    }

    /**
    * @description get all from custom metadata object
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param objectName
    * @param fields
    * @return List<SObject>
    **/
    public static List<SObject> getByAll(String objectName, Set<String> fields) {
        String soqlStr = 'SELECT ';

        for(String field : fields) {
            soqlStr += field + ',';
        }

        soqlStr = soqlStr.removeEnd(',');

        soqlStr += ' FROM ' + objectName;

        return Database.query(soqlStr);
    }

    /**
    * @description
    * @author alexandre.costa@integra-biosciences.com | 05-05-2021
    * @return Discount_Config__mdt
    **/
    public static Discount_Config__mdt getDiscountConfig() {
		Set<String> fields = new Set<String>();
		fields.add('Discount_Limit__c');
		fields.add('Product_Group_Exclude__c');
        fields.add('IsActive__c');

        Discount_Config__mdt config;

        if(Test.isRunningTest()) {
            config = new Discount_Config__mdt(
                Discount_Limit__c = 20,
                Product_Group_Exclude__c = 'EVOLVE',
                isActive__c = True
            );
        } else {
            config = (Discount_Config__mdt)CustomMetadataSelector.getByDeveloperName('Main_Discount_Config', 'Discount_Config__mdt', fields);
        }

        return config;
    }
}

/**
*	@description Controller for filling sObject name.
*
*	@author akr
*	@copyright PARX
*/
public class SObjectFillNameController
{
	/*
	*	@description Going through Salesforce Object and associate Object's name with its ID prefix
	*/
	public static Map<String, String> sObjectKeyPrefix
	{
		get
		{
			if(sObjectKeyPrefix == null)
			{
				sObjectKeyPrefix = new Map<String, String>();
				Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
				for(String sObj : Schema.getGlobalDescribe().keySet()){
					Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
					String tempName = r.getName();
					String tempPrefix = r.getKeyPrefix();
					System.debug('Processing Object['+tempName + '] with Prefix ['+ tempPrefix+']');
					sObjectKeyPrefix.put(tempName, tempPrefix);
				}
			}
			return sObjectKeyPrefix;
		}
		set;
	}
	
	ApexPages.StandardController stdController;
	
	/**
	*	@description Constructor with parameters.
	*
	*	@param controller is a ApexPages.StandardController
	*/
	public SObjectFillNameController(ApexPages.StandardController controller)
	{
		stdController = controller;
		stdController.getRecord();
	}
	
	/**
	*	@description Redirect method with additional parameters
	*
	*	@return PageReference is a URL on which will be redirect in the end.
	*/
	public PageReference redirect()
	{
		PageReference redirectPR = new PageReference('/'+ sObjectKeyPrefix.get(String.valueOf(stdController.getRecord().getSObjectType()))+'/e?');
		redirectPR.getParameters().putAll(ApexPages.currentPage().getParameters());
		
		redirectPR.getParameters().put('nooverride', '1');
		redirectPR.getParameters().put('opp3', Label.OPPORTUNITY_AUTOMATICALLY_GENARATED);
		
	
		redirectPR.getParameters().remove('save_new');
		return redirectPR;
	}
	
}
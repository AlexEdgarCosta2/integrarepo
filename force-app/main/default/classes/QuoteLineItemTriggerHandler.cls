/**
 * @description       : Trigger Handler for Quote Line Items.
 * @author            : PARX
**/
public with sharing class QuoteLineItemTriggerHandler extends TriggerTemplateV2.TriggerHandlerAdapter {

	public override void onBeforeInsert(List<sObject> newList)
	{
	}
	public override void onAfterInsert(List<sObject> newList, Map<Id, sObject> newMap) {
		QuoteLineItemService.syncToOpportunityLineItems(newList);
		//QuoteLineItemService.setDiscountApprovalProcesses(newList, null);
	}

	public override void onAfterUpdate (List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {
		QuoteLineItemService.syncToOpportunityLineItems(newList);
		//QuoteLineItemService.setDiscountApprovalProcesses(newList, oldMap);
	}

	public override void onAfterDelete (List<sObject> oldList, Map<Id, sObject> oldMap) {
	}
}
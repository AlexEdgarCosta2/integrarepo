/*
*   @description product picker for Case: im parent class we use OpportunityLineItems in the VF page, so in save action we
        convert it to CaseProduct__c:
        CaseProduct__c.TierPriceOneitem__c = OpportunitylineItem.ListPriceTierPricing__c
        CaseProduct__c.EffectiveCost__c = OpportunitylineItem.ListPriceTierPricing__c * OpportunitylineItem.Quantity
        CaseProduct__c.AppliedCost__c = OpportunitylineItem.UnitPrice
*
*   @author ach
*   @copyright PARX
*/
public with sharing class ProductPickerCaseController extends ProductPickerController
{
    public Case caseObject {get;set;}

    public override void initMainObject()
    {
        this.caseObject = [SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode FROM Case
                WHERE Id = :(Id)ApexPages.currentPage().getParameters().get('id')];
    }

    public override Id getMainId()
    {
        return this.caseObject.Id;
    }

    public override String getMainCurrency()
    {
        return this.caseObject.CurrencyIsoCode;
    }

    public override String getMainBillingCountryCode()
    {
        return this.caseObject.Account.BillingCountryCode;
    }

    public override Id getMainAccountGroup()
    {
        return this.caseObject.Account.AccountGroup__c;
    }

    public override String getLanguageForProducts()
    {
        return '';
    }

    public override Decimal getOverallDiscount()
    {
        return null;
    }

    public override Decimal getDisposablesDiscount()
    {
        return null;
    }
    public override Decimal getServicesDiscount()
    {
        return null;
    }

    public override PageReference cloneOpportunityWithLineItem()
    {
        return redirectToPreviousPage();
    }
    public override PageReference reCalculateProductPrice()
    {
        return redirectToPreviousPage();
    }

    /**
    *   @description add products to the Database.
    */
    public override PageReference doAddProducts()
    {
        List<CaseProduct__c> items = new List<CaseProduct__c>();
        for (LineItemWrapper wrapper: selectedLineItemWrappers)
        {
            CaseProduct__c item = new CaseProduct__c(Case__c = getMainId(), Product__c = wrapper.product.id,
                    TierPriceOneitem__c =  wrapper.listPriceTierPricing,
                    EffectiveCost__c = (wrapper.listPriceTierPricing * wrapper.qty).setScale(2),
                    AppliedCost__c = wrapper.UnitPrice,
                    Quantity__c = wrapper.qty,
                    CurrencyIsoCode = getMainCurrency());
            items.add(item);
        }
        insert items;
        System.debug('items');
        System.debug(items);
//        return redirectToPreviousPage();
        return null;
    }

    protected override void applyAdditionalConditions(AndCondition andCondition)
    {
        andCondition.add(new FieldCondition('DoNotShowOnCase__c', Operator.NOT_EQUALS, true));
    }

    protected override void addExistingProduct(LineItemWrapper wrapper)
    {
        wrapper.UnitPrice = (wrapper.listPriceTierPricing * wrapper.qty).setScale(2);
    }
/*
    protected void applyTiersForWrapper(LineItemWrapper wrapper, PriceEntry__c aPriceEntry)
    {
        // in case of deleting a product from the list a unit price and list price can differ. In this case don't assign the selling price to the unit price
        if (wrapper.listPriceTierPricing != null && wrapper.UnitPrice != wrapper.listPriceTierPricing * wrapper.qty)
        {
        }
        else
        {
            system.debug(Logginglevel.DEBUG, '#### applyTiersForWrapper aPriceEntry: good');
            wrapper.UnitPrice = (aPriceEntry.SellingPrice__c * wrapper.qty).setScale(2);
        }
    }
    */
}
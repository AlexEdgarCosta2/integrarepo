/*****************************************************
 * Name : SortLineItemsCtrl
 * Developer: Vinay Vernekar
 * Reference: https://www.fiverr.com/kingofwebhost/do-standard-or-custom-development-in-salesforce
 * Website: https://sfdcdevelopers.com
 * Email: support@sfdcdevelopers.com
 * Purpose: Controller for Sorting Line Items
 * Date: 8th June 2019
 * Last Modified Date: 8th June 2019
*******************************************************/
public with sharing class SortLineItemsCtrl {

	@AuraEnabled
	public static Map<String, String> getData(Id parentId)
	{
		Map<String, String> resultMap = new Map<String, String>();
		Map<String, List<WrapLI>> liMap = new Map<String, List<WrapLI>>();
		Map<String, String> productTypeVariableMapping = new Map<String, String>{
			'Products and Accessories' => 'fromSectionAccessories',
			'Disposables' => 'fromSectionDisposables',
			'Services' => 'fromSectionServices'
		};

		Boolean flagOpportunity = (Utils.getObjectType(parentId) == Constants.OBJECT_TYPE_OPPORTUNITY);
		Boolean flagQuote = (Utils.getObjectType(parentId) == Constants.OBJECT_TYPE_QUOTE);

		String currencyIsoCode = '';
		Decimal discountPercentage = 0;
		Decimal discountDisposables = 0;
		Decimal discountServices = 0;
		String billingCountryCode = '';
		String accountGroup = '';

		String parentSObjectTypeName = parentId.getSobjectType().getDescribe().getName();
		String childSObjectTypeName = parentSObjectTypeName + 'LineItem';
		String parentRelationship = parentSObjectTypeName + 'Id';
		String query = '' +
			'SELECT Id, ' + parentRelationship + ' , SortOrder__c, PricebookEntry.Product2.ProductCode, PricebookEntry.Product2Id,' +
			        'PricebookEntry.Product2.ProductType__c,' +
			        'PricebookEntry.Product2.Name, Quantity, UnitPrice, PricebookEntry.Product2.Id, DemoEquipment__c ' +
			'FROM ' + childSObjectTypeName + ' ' +
			'WHERE ' + parentRelationship + ' = :parentId ' +
			'AND ProductType__c != null ' +
			'ORDER BY Product2.ProductType__c ASC, SortOrder__c ASC NULLS LAST, PricebookEntry.ProductCode';
		//find all line items
		System.debug('query = ' + query);

		List<SObject> childItemList = Database.query(query);

		if(!childItemList.isEmpty()) {

			if(flagOpportunity) {
				Opportunity opp = OpportunitySelector.getOpportunityById(parentId);
				currencyIsoCode = opp.CurrencyIsoCode;
				discountPercentage = opp.DiscountPercentage__c;
				discountDisposables = opp.DiscountDisposables__c;
				discountServices = opp.DiscountServices__c;
				billingCountryCode = opp.Account.BillingCountryCode;
				accountGroup = opp.Account_Group__c;
			} else if(flagQuote) {
				Quote quote  = QuoteSelector.getQuoteById(parentId);
				currencyIsoCode = quote.CurrencyIsoCode;
				discountPercentage = quote.DiscountProducts__c;
				discountDisposables = quote.DiscountDisposables__c;
				discountServices = quote.DiscountServices__c;
				billingCountryCode = quote.Account.BillingCountryCode;
				accountGroup = quote.Account_Group__c;
			}

			String countryCode = '%' + billingCountryCode + '%';

			Set<Id> productIds = new Set<Id>();
			for (SObject childItem : childItemList) {
				Id productId;
				if (flagOpportunity) {
					OpportunityLineItem opportunityLineItem = (OpportunityLineItem) childItem;
					productId = opportunityLineItem.PricebookEntry.Product2Id;
				} else if (flagQuote) {
					QuoteLineItem quoteLineItem = (QuoteLineItem) childItem;
					productId = quoteLineItem.PricebookEntry.Product2Id;
				}

				productIds.add(productId);
			}

			Map<String, PriceEntry__c> priceEntryMap = PriceEntrySelector.getpriceEntryMapByProducts2(productIds);

			for (SObject childItem : childItemList) {
				if (flagOpportunity) {
					OpportunityLineItem opportunityLineItem = (OpportunityLineItem) childItem;

					PriceEntry__c currentPriceEntry = PricingService.getCurrentPriceEntry(priceEntryMap,
						opportunityLineItem.PricebookEntry.Product2Id,
						accountGroup,
						currencyIsoCode,
						billingCountryCode,
						discountPercentage,
						discountDisposables,
						discountServices);

					System.debug('[getData 1] currentPriceEntry : ' + currentPriceEntry);

					String productScaling = PricingService.getProductScaling(discountPercentage,
														opportunityLineItem.PricebookEntry.Product2.ProductType__c,
														discountDisposables,
														discountServices,
														currentPriceEntry);
														//priceEntryMap.get(opportunityLineItem.PricebookEntry.Product2Id)[0]);

					if (liMap.containsKey(productTypeVariableMapping.get(opportunityLineItem.PricebookEntry.Product2.ProductType__c))) {
						liMap.get(productTypeVariableMapping.get(opportunityLineItem.PricebookEntry.Product2.ProductType__c)).add(new WrapLI(opportunityLineItem, productScaling));
					} else {
						liMap.put(productTypeVariableMapping.get(opportunityLineItem.PricebookEntry.Product2.ProductType__c), new list<WrapLI>{new WrapLI(opportunityLineItem, productScaling)});
					}
				} else if (flagQuote) {
					QuoteLineItem quoteLineItem = (QuoteLineItem) childItem;

					PriceEntry__c currentPriceEntry = PricingService.getCurrentPriceEntry(priceEntryMap,
						quoteLineItem.PricebookEntry.Product2Id,
						accountGroup,
						currencyIsoCode,
						billingCountryCode,
						discountPercentage,
						discountDisposables,
						discountServices);

					System.debug('[getData 2] currentPriceEntry : ' + currentPriceEntry);

					String productScaling = PricingService.getProductScaling(discountPercentage,
														quoteLineItem.PricebookEntry.Product2.ProductType__c,
														discountDisposables,
														discountServices,
														currentPriceEntry);
														//priceEntryMap.get(quoteLineItem.PricebookEntry.Product2Id)[0]);

					if (liMap.containsKey(productTypeVariableMapping.get(quoteLineItem.PricebookEntry.Product2.ProductType__c))) {
						liMap.get(productTypeVariableMapping.get(quoteLineItem.PricebookEntry.Product2.ProductType__c)).add(new WrapLI(quoteLineItem, productScaling));
					} else {
						liMap.put(productTypeVariableMapping.get(quoteLineItem.PricebookEntry.Product2.ProductType__c), new list<WrapLI>{new WrapLI(quoteLineItem, productScaling)});
					}
				}
			}

			for(String productType : liMap.keySet()) {
				resultMap.put(productType, JSON.serialize(liMap.get(productType)));
			}
		}
			return resultMap;
	}

	@AuraEnabled
	public static void saveData(String lineItemString) {
        Set<Id> lineItemIds = new Set<Id>();
		Set<Id> productIds = new Set<Id>();
		List<SObject> lineItems = new List<SObject>();
        Map<Id, SObject> lineItemsMap = new Map<Id, SObject>();

		for (WrapLI wLI : (List<WrapLI>)JSON.deserialize(lineItemString, list<WrapLI>.class)) {
          	lineItemIds.add(wLI.lineItem.Id);
			productIds.add((Id)wLI.lineItem.get('Product2Id'));
			lineItems.add(wLI.lineItem);
            lineItemsMap.put(wLI.lineItem.Id, wLI.lineItem);
		}

		if(!lineItemIds.isEmpty()) {

			Boolean flagOpportunity = (Utils.getObjectType( (new list<Id>(lineItemIds) )[0] ) == Constants.OBJECT_TYPE_OPPORTUNITY_LINE_ITEM);
			Boolean flagQuote = (Utils.getObjectType( (new list<Id>(lineItemIds) )[0] ) == Constants.OBJECT_TYPE_QUOTE_LINE_ITEM);

			String currencyIsoCode = '';
			Decimal discountPercentage = 0;
			Decimal discountDisposables = 0;
			Decimal discountServices = 0;
			String billingCountryCode = '';
			Id accountId;
			String accountGroup;

			List<SObject> liList;
			if(flagOpportunity) {
				liList = OpportunityLineItemSelector.getOpportunityLineItemsByIds(lineItemIds);
			} else if(flagQuote) {
				liList = QuoteLineItemSelector.getQuoteLineItemsByIds(lineItemIds);
			}

			/**
			 * Uppon changed quatity apply the discounts
			 *
			 *
			 */
			if((flagOpportunity || flagQuote) && !liList.isEmpty()) {

				Map<Id, Product2> productsMap = ProductSelector.getProductsMap(productIds);

				if(flagOpportunity) {
					Opportunity opp = OpportunitySelector.getOpportunityById((Id)liList[0].get('OpportunityId'));
					currencyIsoCode = opp.CurrencyIsoCode;
					discountPercentage = opp.DiscountPercentage__c;
					discountDisposables = opp.DiscountDisposables__c;
					discountServices = opp.DiscountServices__c;
					accountId = opp.AccountId;
					billingCountryCode = opp.Account.BillingCountryCode;
					accountGroup = opp.Account_Group__c;

				} else if(flagQuote) {
					Quote quote  = QuoteSelector.getQuoteById((Id)liList[0].get('QuoteId'));
					currencyIsoCode = quote.CurrencyIsoCode;
					discountPercentage = quote.DiscountProducts__c;
					discountDisposables = quote.DiscountDisposables__c;
					discountServices = quote.DiscountServices__c;
					accountId = quote.AccountId;
					billingCountryCode = quote.Account.BillingCountryCode;
					accountGroup = quote.Account_Group__c;
				}

				String countryCode = '%' + billingCountryCode + '%';

				Map<String, PriceEntry__c> priceEntryMap = PriceEntrySelector.getpriceEntryMapByProducts2(productIds);

				Map <String, Set<id>> priceGroupsMap = new Map <String, Set<id>>();
				for (SObject lineItem : liList) {
					Product2 prod = (Product2)productsMap.get((Id)lineItem.get('Product2Id'));

					if(prod.PricingGroup__c != null) {
						if(priceGroupsMap.get(prod.PricingGroup__c) == null) {
							Set<Id> aux = new Set<Id>();
							aux.add(prod.Id);
							priceGroupsMap.put(prod.PricingGroup__c, aux);
						} else {
							priceGroupsMap.get(prod.PricingGroup__c).add(prod.Id);
						}
					}
				}

				// Saves the products with pricing group where the quantity was changed.
				Map<String, Set<Id>> productsWithPricingGroup = new Map<String, Set<Id>>();

				// Saves all the other line itens with pricing group where the quantity was not changed.
				Map<String, List<SObject>> excludedProductsWithPricingGroup = new Map<String, List<SObject>>();
				List<LineItemWrapper> lwList = new List<LineItemWrapper>();
				for (SObject lineItem : liList) {

					Product2 prod = (Product2)productsMap.get((Id)lineItem.get('Product2Id'));

					Decimal newQuantity = (Decimal)lineItemsMap.get(lineItem.Id).get('Quantity');
					Decimal newUnitPrice = (Decimal)lineItemsMap.get(lineItem.Id).get('UnitPrice');

					Decimal oldQuantity = (Decimal)lineItem.get('Quantity');
					Decimal oldUnitPrice = (Decimal)lineItem.get('UnitPrice');

					if((newQuantity != oldQuantity) && (newUnitPrice == oldUnitPrice)) {

						if(prod.PricingGroup__c != null) {
							if(productsWithPricingGroup.get(prod.PricingGroup__c) == null) {
								Set<Id> aux = new Set<Id>();
								aux.add(prod.Id);
								productsWithPricingGroup.put(prod.PricingGroup__c, aux);
							} else {
								productsWithPricingGroup.get(prod.PricingGroup__c).add(prod.Id);
							}
						}

						List<PriceEntry__c> currentPriceEntry = PricingService.getCurrentPriceEntryList(priceEntryMap,
							(Id)lineItem.get('Product2Id'),
							accountGroup,
							currencyIsoCode,
							billingCountryCode,
							discountPercentage,
							discountDisposables,
							discountServices);

						List<PriceEntry__c> standardPriceEntry;

						if (accountGroup != null) {
							standardPriceEntry = PricingService.getCurrentPriceEntryList(priceEntryMap,
								(Id)lineItem.get('Product2Id'),
								null,
								currencyIsoCode,
								billingCountryCode,
								discountPercentage,
								discountDisposables,
								discountServices);
						} else {
							standardPriceEntry = currentPriceEntry;
						}

						System.debug('[saveData] currentPriceEntry : ' + currentPriceEntry);

						LineItemWrapper lw =
							new LineItemWrapper(prod,
								prod.Name,
								currentPriceEntry,
								standardPriceEntry,
								discountPercentage,
								discountDisposables,
								discountServices,
								prod.ProductType__c);

						lw.qty = newQuantity;

						if(flagOpportunity) {
							lw.item = (OpportunityLineItem)lineItem;
						} else if(flagQuote) {
							lw.qteLineItem = (QuoteLineItem)lineItem;
						}

						lwList.add(lw);

					} else {
						if(prod.PricingGroup__c != null) {
							if(excludedProductsWithPricingGroup.get(prod.PricingGroup__c) == null) {
								List<Sobject> aux = new List<Sobject>();
								aux.add(lineItem);
								excludedProductsWithPricingGroup.put(prod.PricingGroup__c, aux);
							} else {
								excludedProductsWithPricingGroup.get(prod.PricingGroup__c).add(lineItem);
							}
						}
					}
				}

				// Create a list with all the line itens that are in the same price group of line itens where the quantity was changed.
				List<SObject> extraLineItems = new List<SObject>();
				if(productsWithPricingGroup != null && !productsWithPricingGroup.isEmpty()) {
					for(String key : productsWithPricingGroup.keySet()) {
						if(excludedProductsWithPricingGroup.get(key) != null) {
							extraLineItems.addAll(excludedProductsWithPricingGroup.get(key));
						}
					}
				}

				for(SObject lineItem : extraLineItems) {

					Product2 prod = (Product2)productsMap.get((Id)lineItem.get('Product2Id'));

					List<PriceEntry__c> currentPriceEntry = PricingService.getCurrentPriceEntryList(priceEntryMap,
						(Id)lineItem.get('Product2Id'),
						accountGroup,
						currencyIsoCode,
						billingCountryCode,
						discountPercentage,
						discountDisposables,
						discountServices);

					List<PriceEntry__c> standardPriceEntry;

					if(accountGroup != null) {
						standardPriceEntry = PricingService.getCurrentPriceEntryList(priceEntryMap,
							(Id)lineItem.get('Product2Id'),
							null,
							currencyIsoCode,
							billingCountryCode,
							discountPercentage,
							discountDisposables,
							discountServices);
					} else {
						standardPriceEntry = currentPriceEntry;
					}

					System.debug('[saveData] currentPriceEntry : ' + currentPriceEntry);

					LineItemWrapper lw =
						new LineItemWrapper(prod,
							prod.Name,
							currentPriceEntry,
							standardPriceEntry,
							discountPercentage,
							discountDisposables,
							discountServices,
							prod.ProductType__c);

					if(flagOpportunity) {
						lw.item = (OpportunityLineItem)lineItem;
					} else if(flagQuote) {
						lw.qteLineItem = (QuoteLineItem)lineItem;
					}

					lwList.add(lw);

				}

				PricingService.applyTiers(lwList,
										discountPercentage,
										discountDisposables,
										discountServices);
										//accountId,
										//PricingService.getProductToListPrice(priceEntryMap));

				for(LineItemWrapper lw : lwList) {
					if(flagOpportunity) {
						lineItemsMap.get(lw.item.Id).put('UnitPrice', lw.listPriceTierPricing);
						lineItemsMap.get(lw.item.Id).put('Standard_Price__c', lw.standardPrice);
					} else if(flagQuote) {
						lineItemsMap.get(lw.qteLineItem.Id).put('UnitPrice', lw.listPriceTierPricing);
						lineItemsMap.get(lw.qteLineItem.Id).put('Standard_Price__c', lw.standardPrice);
					}
				}
			}
		}

		if (!lineItems.isEmpty())
		    update lineItems;
	}

	@AuraEnabled
	public static void deleteSelectedRecords(List<Id> lineItemIds){
		Database.delete(lineItemIds);
	}

	public class WrapLI{
		public Boolean check {get;set;}
		public SObject lineItem {get;set;}
		public String productScaling {get;set;}

		public WrapLI(SObject lineItem, String productScaling){
			this.check = false;
			this.lineItem = lineItem;
			this.productScaling = productScaling;
		}
	}
}
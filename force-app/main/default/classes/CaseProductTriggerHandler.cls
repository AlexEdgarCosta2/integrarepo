/**
*	@description CaseProduct__c Trigger Handler functionality.
*
*	@author ach
*	@copyright PARX
*/
public with sharing class CaseProductTriggerHandler extends TriggerTemplateV2.TriggerHandlerAdapter
{
	public override void onAfterInsert (List<sObject> newList, Map<Id, sObject> newMap)
	{
		CaseService.rollupSumaryToCases(newList);
	}
	
	public override void onAfterUpdate (List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap)
	{
		CaseService.rollupSumaryToCases(newList);
	}
	
	public override void onAfterDelete (List<sObject> oldList, Map<Id, sObject> oldMap)
	{
		CaseService.rollupSumaryToCases(oldList);
	}
}
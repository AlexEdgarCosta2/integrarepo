/*****************************************************
 * Name : Test_SortLineItemsCtrl
 * Developer: Vinay Vernekar
 * Reference: https://www.fiverr.com/kingofwebhost/do-standard-or-custom-development-in-salesforce
 * Website: https://sfdcdevelopers.com
 * Email: support@sfdcdevelopers.com
 * Purpose: Test class for SortLineItemsCtrl
 * Date: 8th June 2019
 * Last Modified Date: 8th June 2019
*******************************************************/
@IsTest
private class Test_SortLineItemsCtrl
{

    public static Opportunity testOpportunity;

    public static Quote testQuote;

    public static List<OpportunityLineItem> items;

    public static List<QuoteLineItem> quoteLineItems;

    public static String PRODUCT_TYPE_SERVICES = 'Services';

    public static String PRODUCT_TYPE_DISPOSABLES = 'Disposables';

    public static String PRODUCT_TYPE_ACCESSORIES = 'Products and Accessories';

    private static void initData()
    {
        AccountGroup__c anAccountGroup = new AccountGroup__c(Name = 'asd');
        insert anAccountGroup;

        Account anAccount = new Account(Name = 'asd account', AccountGroup__c = anAccountGroup.Id, BillingCountryCode = 'US', BillingState='California');
        insert anAccount;

        List<Product2> aProducts = new List<Product2>
        {
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = Test_SortLineItemsCtrl.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = Test_SortLineItemsCtrl.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = Test_SortLineItemsCtrl.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')};
        insert aProducts;

        List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
            new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
        insert markets;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = aProducts.get(0).Id, UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = aProducts.get(1).Id, UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = aProducts.get(2).Id, UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert standardPrices;

        List<PriceEntry__c> standardPrices2 = new List<PriceEntry__c>
        {
            new PriceEntry__c(Product__c =  aProducts.get(0).Id, SellingPrice__c = 10000, CurrencyIsoCode = 'USD', ScalingTo__c = 1, ScalingTo2__c = 10, ScalingTo3__c = 9999, DiscountTier2__c = 0.05, DiscountTier3__c = 0.1, Market__c = markets[0].Id),
            new PriceEntry__c(Product__c =  aProducts.get(1).Id, SellingPrice__c = 10000, CurrencyIsoCode = 'USD', ScalingTo__c = 1, ScalingTo2__c = 10, ScalingTo3__c = 9999, DiscountTier2__c = 0.05, DiscountTier3__c = 0.1, Market__c = markets[0].Id),
            new PriceEntry__c(Product__c =  aProducts.get(2).Id, SellingPrice__c = 10000, CurrencyIsoCode = 'USD', ScalingTo__c = 1, ScalingTo2__c = 10, ScalingTo3__c = 9999, DiscountTier2__c = 0.05, DiscountTier3__c = 0.1, Market__c = markets[0].Id)
        };
        insert standardPrices2;

        List<Opportunity> oppList = new List<Opportunity> {
            new Opportunity(Name = 'opportunity1', AccountId = anAccount.Id, StageName = 'Interessiert', CloseDate = date.today(), CurrencyIsoCode = 'USD', DiscountPercentage__c = 10),
            new Opportunity(Name = 'opportunity2', AccountId = anAccount.Id, StageName = 'Interessiert', CloseDate = date.today(), CurrencyIsoCode = 'USD', DiscountDisposables__c = 10),
            new Opportunity(Name = 'opportunity3', AccountId = anAccount.Id, StageName = 'Interessiert', CloseDate = date.today(), CurrencyIsoCode = 'USD', DiscountServices__c = 10)
        };
        insert oppList;

        testOpportunity = oppList[0];

        items = new List<OpportunityLineItem>
        {
            new OpportunityLineItem(PricebookEntryId = standardPrices[0].Id, OpportunityId = testOpportunity.Id, Quantity = 10, TotalPrice = 100000, Product2Id = aProducts.get(0).Id),
            new OpportunityLineItem(PricebookEntryId = standardPrices[1].Id, OpportunityId = testOpportunity.Id, Quantity = 10, TotalPrice = 100000, Product2Id = aProducts.get(1).Id),
            new OpportunityLineItem(PricebookEntryId = standardPrices[2].Id, OpportunityId = testOpportunity.Id, Quantity = 10, TotalPrice = 100000, Product2Id = aProducts.get(2).Id)
        };
        insert items;

        List<Quote> quoteList = new List<Quote> {
            new Quote(Name = 'quote1', Pricebook2Id = Test.getStandardPricebookId(), CurrencyIsoCode = 'USD', DiscountProducts__c = 10, OpportunityId = oppList[0].Id),
            new Quote(Name = 'quote2', Pricebook2Id = Test.getStandardPricebookId(), CurrencyIsoCode = 'USD', DiscountDisposables__c = 10, OpportunityId = oppList[1].Id),
            new Quote(Name = 'quote3', Pricebook2Id = Test.getStandardPricebookId(), CurrencyIsoCode = 'USD', DiscountServices__c = 10, OpportunityId = oppList[2].Id)
        };

        insert quoteList;

        testQuote = quoteList[0];

        quoteLineItems = new List<QuoteLineItem> {
            new QuoteLineItem(PricebookEntryId = standardPrices[0].Id, QuoteId = quoteList[0].Id, Quantity = 10, UnitPrice = 10000, Product2Id = aProducts.get(0).Id),
            new QuoteLineItem(PricebookEntryId = standardPrices[1].Id, QuoteId = quoteList[0].Id, Quantity = 10, UnitPrice = 10000, Product2Id = aProducts.get(1).Id),
            new QuoteLineItem(PricebookEntryId = standardPrices[2].Id, QuoteId = quoteList[0].Id, Quantity = 10, UnitPrice = 10000, Product2Id = aProducts.get(2).Id)
        };
        insert quoteLineItems;
    }

    @IsTest
    static void testActions()
    {
        initData();

        Map<String, String> data = SortLineItemsCtrl.getData(testOpportunity.Id);
        List<SortLineItemsCtrl.WrapLI> fromSectionAccessories = (List<SortLineItemsCtrl.WrapLI>)JSON.deserialize(data.get('fromSectionAccessories'), List<SortLineItemsCtrl.WrapLI>.class);
        List<SortLineItemsCtrl.WrapLI> fromSectionDisposables = (List<SortLineItemsCtrl.WrapLI>)JSON.deserialize(data.get('fromSectionDisposables'), List<SortLineItemsCtrl.WrapLI>.class);
        List<SortLineItemsCtrl.WrapLI> fromSectionServices = (List<SortLineItemsCtrl.WrapLI>)JSON.deserialize(data.get('fromSectionServices'), List<SortLineItemsCtrl.WrapLI>.class);

        //save line items
        SortLineItemsCtrl.saveData(JSON.serializePretty(fromSectionServices));

        List<id> OLIIds = new List<Id>();
        for(OpportunityLineitem OLI : [select id from OpportunityLineItem limit 2])
            OLIIds.add(OLI.id);

        //save line items
        SortLineItemsCtrl.saveData(JSON.serializePretty(fromSectionAccessories));

        OLIIds = new List<Id>();
        for(OpportunityLineitem OLI : [select id from OpportunityLineItem limit 2])
            OLIIds.add(OLI.id);

        //save line items
        SortLineItemsCtrl.saveData(JSON.serializePretty(fromSectionDisposables));

        OLIIds = new List<Id>();
        for(OpportunityLineitem OLI : [select id from OpportunityLineItem limit 2])
            OLIIds.add(OLI.id);

        //delete line items
        SortLineItemsCtrl.deleteSelectedRecords(OLIIds);
    }

    @IsTest
    static void testActionsQuote()
    {
        initData();

        Map<String, String> data = SortLineItemsCtrl.getData(testQuote.Id);
        List<SortLineItemsCtrl.WrapLI> fromSectionAccessories = (List<SortLineItemsCtrl.WrapLI>)JSON.deserialize(data.get('fromSectionAccessories'), List<SortLineItemsCtrl.WrapLI>.class);
        List<SortLineItemsCtrl.WrapLI> fromSectionDisposables = (List<SortLineItemsCtrl.WrapLI>)JSON.deserialize(data.get('fromSectionDisposables'), List<SortLineItemsCtrl.WrapLI>.class);
        List<SortLineItemsCtrl.WrapLI> fromSectionServices = (List<SortLineItemsCtrl.WrapLI>)JSON.deserialize(data.get('fromSectionServices'), List<SortLineItemsCtrl.WrapLI>.class);

        //save line items
        SortLineItemsCtrl.saveData(JSON.serializePretty(fromSectionServices));

        List<id> OLIIds = new List<Id>();
        for(QuoteLineItem OLI : [select id from QuoteLineItem limit 2])
            OLIIds.add(OLI.id);

        //delete line items
        SortLineItemsCtrl.deleteSelectedRecords(OLIIds);
    }

    static testMethod void test_newItem() {

        Product2 prod = TestUtil.getProduct();

        List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
        new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
        insert markets;

        List<PriceEntry__c> standardPrices2 = new List<PriceEntry__c>
        {
            new PriceEntry__c(Product__c =  prod.Id, SellingPrice__c = 10000, CurrencyIsoCode = 'USD', ScalingTo__c = 1, ScalingTo2__c = 10, ScalingTo3__c = 9999, DiscountTier2__c = 0.05, DiscountTier3__c = 0.1, Market__c = markets[0].Id)
        };
        insert standardPrices2;

        prod.ProductType__c = 'Products and Accessories';

        Decimal aux = 0.0;

        //public LineItemWrapper(Product2 thePoduct, String productName, List<PriceEntry__c> thePriceEntries, List<PriceEntry__c> standardPrices,
        //Decimal overallDiscount,Decimal disposablesDiscount,Decimal servicesDiscount, String productType, Boolean isRelatedProduct, Boolean demoEquipment)

        LineItemWrapper lw = new LineItemWrapper(prod, prod.Name, standardPrices2, standardPrices2,
        aux, aux, aux, prod.ProductType__c, false, false);

    }
}
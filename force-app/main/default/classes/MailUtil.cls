/**
 * @description       : Util class for mails
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class MailUtil {

    /**
    * @description Send the approval discounts mail.
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param oppQuote
    * @param approversList
    * @param orgWideEmailAddressId
    * @param approvalType
    * @param productsStr
    * @return String
    **/
    public static String sendApprovalDiscountMail(SObject oppQuote, List<Discount_Approver__mdt> approversList,
        List<Discount_Approver__mdt> ccApproversList,
        Id orgWideEmailAddressId, Integer approvalType, String productsStr) {

        String emails = '';

        if(!approversList.isEmpty()) {

            String docType = '';
            String accountName = '';
            String ownerName = '';
            if(Utils.getObjectType(oppQuote.Id) == Constants.OBJECT_TYPE_OPPORTUNITY) {
                Opportunity opp = (Opportunity)oppQuote;
                accountName = opp.Account.Name;
                ownerName = opp.Owner.Name;
                docType = 'Opportunity';
            } else if (Utils.getObjectType(oppQuote.Id) == Constants.OBJECT_TYPE_QUOTE) {
                Quote quote = (Quote)oppQuote;
                accountName = quote.Account.Name;
                ownerName = quote.Opportunity.Owner.Name;
                docType = 'Quote';
            }

            String salutation = '';
            if(approvalType == 1) {
                salutation = Constants.APPROVAL_MAIL_COUNTRY_MANAGER_SALUTATION + ',';
            } else if(approvalType == 2) {
                salutation = Constants.APPROVAL_MAIL_GLOBAL_HEAD_SALUTATION + ',';
            } else if(approvalType == 3) {
                salutation = Constants.APPROVAL_MAIL_DIRECTOR_SALUTATION + ',';
            }

            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

            EmailTemplate et = EmailTemplateSelector.getEmailTemplateByDeveloperName(Constants.DISCOUNT_APPROVAL_NOTIFICATION_EMAIL_TEMPLATE);

            List<String> toList = new List<String>();
            for(Discount_Approver__mdt approver : approversList) {
                toList.add(approver.Approver_Email_Address__c);
            }
            List<String> ccList = new List<String>();
            for(Discount_Approver__mdt approver : ccApproversList) {
                ccList.add(approver.Approver_Email_Address__c);
            }

            System.debug(LoggingLevel.DEBUG, '[sendApprovalDiscountMail] sending approval mails to : ' + toList);
            System.debug(LoggingLevel.DEBUG, '[sendApprovalDiscountMail] in CC : ' + ccList);

            emails = String.join(toList,', ');

            String body = et.HtmlValue;

            body = body.replaceAll('docType', docType);
            body = body.replaceAll('salutation', salutation);
            body = body.replaceAll('docName', (String)oppQuote.get('Name'));
            body = body.replaceAll('AccountName', accountName);
            body = body.replaceAll('owver', ownerName);
            body = body.replaceAll('headerTag', productsStr);

            message.toAddresses = toList;
            message.ccAddresses = ccList;
            message.setSubject(Constants.DISCOUNTS_APPROVAL_MAIL_SUBJECT_TAG + ' Id:' + oppQuote.Id);
            message.setHtmlBody(body);
            message.setWhatId((Id)oppQuote.Id);
            message.setOrgWideEmailAddressId(OrgWideEmailAddressId);

            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

            if (results[0].success) {
                System.debug(LoggingLevel.INFO, '[sendApprovalDiscountMail] The email was sent successfully.');
            } else {
                System.debug(LoggingLevel.ERROR, '[sendApprovalDiscountMail] The email failed to send: ' + results[0].errors[0].message);
            }
        }
        return emails;
    }
}

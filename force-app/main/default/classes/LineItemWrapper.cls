/**
 * @description       : Wrapper Class used to represent a Opportunity line item or a Quote Line Item
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class LineItemWrapper  implements Comparable {
    @AuraEnabled
    public OpportunitylineItem item {get;set;}
    @AuraEnabled
    public QuotelineItem qteLineItem {get;set;}
    @AuraEnabled
    public List<PriceEntry__c> pps{get;set;}
    @AuraEnabled
    public List<PriceEntry__c> standardPrices{get;set;}
    public PriceEntry__c currentPriceEntry{get;set;}
    @AuraEnabled
    public String productScaling {get;set;}
    @AuraEnabled
    public Product2 product {get;set;}
    @AuraEnabled
    public String productName {get;set;}
    @AuraEnabled
    public String productType {get;set;}
    @AuraEnabled
    public Decimal listPriceTierPricing {get;set;}
    @AuraEnabled
    public Decimal unitPrice {get;set;}

    public Decimal qty {get;set;}
    @AuraEnabled
    public Decimal overallDiscount{get;set;}
    @AuraEnabled
    public Decimal disposablesDiscount{get;set;}
    @AuraEnabled
    public Decimal servicesDiscount{get;set;}
    //text input in bottom section
    @AuraEnabled
    public String quantity{get;set;}
    @AuraEnabled
    public String PricebookEntryId {get;set;}
    @AuraEnabled
    public Map<String, Double> productToListPrice {get; set;}
    @AuraEnabled
    public Boolean isRelatedProduct {get; set;}
    @AuraEnabled
    public Boolean isDemoEquipment {get; set;}

    public Integer applicableTier {get; set;}
    public Decimal tierDiscount {get; set;}
    public Decimal mainDiscount {get; set;}
    public Decimal tier2Discount {get; set;}
    public Decimal tier3Discount {get; set;}
    public Decimal appliedDiscount {get; set;}
    public Decimal sellingPrice {get; set;}
    public Decimal standardPrice {get; set;}

    public Boolean specialPrice {get; set;}
    public String bilingCountryCode {get; set;}

    public Double productCodeNumber;


    /**
    * @description Constructor
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param thePoduct
    * @param productName
    * @param thePriceEntries
    * @param standardPrices
    * @param overallDiscount
    * @param disposablesDiscount
    * @param servicesDiscount
    * @param productType
    **/
    public LineItemWrapper(Product2 thePoduct, String productName, List<PriceEntry__c> thePriceEntries, List<PriceEntry__c> standardPrices,
                           Decimal overallDiscount,Decimal disposablesDiscount,Decimal servicesDiscount, String productType)
    {
        item = new OpportunitylineItem();
        qteLineItem = new QuotelineItem();
        this.product = thePoduct;
        this.productName = productName;
        this.qty = 0;
        this.quantity = '0';
        this.productScaling = '';
        this.overallDiscount = overallDiscount;
        this.disposablesDiscount = disposablesDiscount;
        this.servicesDiscount = servicesDiscount;
        this.pps = thePriceEntries;
        this.standardPrices = standardPrices;
        this.productType = productType;

        createDiscountStrings();

    }


    /**
    * @description Constructor with two extra flags
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param thePoduct
    * @param productName
    * @param thePriceEntries
    * @param standardPrices
    * @param overallDiscount
    * @param disposablesDiscount
    * @param servicesDiscount
    * @param productType
    * @param isRelatedProduct
    * @param demoEquipment
    **/
    public LineItemWrapper(Product2 thePoduct, String productName, List<PriceEntry__c> thePriceEntries, List<PriceEntry__c> standardPrices,
                           Decimal overallDiscount,Decimal disposablesDiscount,Decimal servicesDiscount, String productType, Boolean isRelatedProduct, Boolean demoEquipment)
    {
        item = new OpportunitylineItem();
        qteLineItem = new QuotelineItem();
        this.product = thePoduct;
        this.productName = productName;
        this.qty = 0;
        this.quantity = '0';
        this.productScaling = '';
        this.overallDiscount = overallDiscount;
        this.disposablesDiscount = disposablesDiscount;
        this.servicesDiscount = servicesDiscount;
        this.pps = thePriceEntries;
        this.standardPrices = standardPrices;
        this.productType = productType;
        this.isRelatedProduct = isRelatedProduct;
        this.isDemoEquipment = demoEquipment;

        createDiscountStrings();
    }

    /**
    * @description Set the tier discounts string
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    **/
    private void createDiscountStrings() {

        if (this.pps != null && !this.pps.isEmpty()) {

            this.productScaling = PricingService.getProductScaling(this.overallDiscount,
                                                                   this.productType,
                                                                   this.disposablesDiscount,
                                                                   this.servicesDiscount,
                                                                   this.pps[this.pps.size() - 1]);

            System.debug(LoggingLevel.DEBUG, '[LineItemWrapper 0] productScaling :' + this.productScaling);
            if (this.item != null && this.item.ListPriceOneItem__c == null) {
                this.item.ListPriceOneItem__c = this.standardPrices[this.standardPrices.size() - 1].SellingPrice__c;
            }
            if (this.qteLineItem != null && this.qteLineItem.ListPriceOneItem__c == null) {
                this.qteLineItem.ListPriceOneItem__c = this.standardPrices[this.standardPrices.size() - 1].SellingPrice__c;
            }

            this.setProductCodeNumber();
        }
    }

    /**
    * @description Copy
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @return LineItemWrapper
    **/
    public LineItemWrapper getCopy() {
        LineItemWrapper aClonnedWrapper = new LineItemWrapper(this.product,
            this.productName,
            this.pps,
            this.standardPrices,
            this.overallDiscount,
            this.disposablesDiscount,
            this.servicesDiscount,
            this.productType,
            this.isRelatedProduct,
            this.isDemoEquipment);
        aClonnedWrapper.qty = this.qty;
        aClonnedWrapper.productScaling = this.productScaling;
        return aClonnedWrapper;
    }

    /**
    * @description Compare
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param lineItemWrapperToCompareObject
    * @return Integer
    **/
    public Integer compareTo(Object lineItemWrapperToCompareObject) {
        LineItemWrapper lineItemWrapperToCompare = (LineItemWrapper) lineItemWrapperToCompareObject;
        return this.productCodeNumber > lineItemWrapperToCompare.productCodeNumber ?
            1 :
            lineItemWrapperToCompare.productCodeNumber == this.productCodeNumber ?
                0 :
                -1;
    }

    /**
    * @description Set the product Code Number
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @TODO : Put this in a product service class
    **/
    private void setProductCodeNumber() {
        this.productCodeNumber = 0;

        if (this.product == null || String.isBlank(this.product.ProductCode)) {
            return;
        }

        System.debug(LoggingLevel.DEBUG, '[setProductCodeNumber] this.product.ProductCode : ' + this.product.ProductCode);

        String numberPart = '';
        List<String> productCodeParts = this.product.ProductCode.split('-');
        if (productCodeParts.size() <= 2) {
            numberPart = productCodeParts[0];
        } else {
            numberPart = this.product.ProductCode;
        }
        try {
            this.productCodeNumber = Double.valueOf(numberPart.replaceAll('[^0-9]', ''));
        } catch(Exception e) {
            System.debug(LoggingLevel.ERROR, '[setProductCodeNumber] Invalid product code : ' + this.product.ProductCode);
        }
    }
}

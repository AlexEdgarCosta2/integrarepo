/**
 * @description       : Selector for Product2
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class ProductSelector {
    /**
    * @description Get Map <Id, Product2> for a Set of product Ids
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param productIds
    * @return Map<Id, Product2>
    **/
    public static Map<Id, Product2> getProductsMap(Set<Id> productIds) {
        return new Map<ID, Product2>([SELECT Id, ProductCode, Name, PricingGroup__c,
                                             ProductType__c, ProductFamily__c FROM Product2 WHERE Id IN: productIds]);
    }
}
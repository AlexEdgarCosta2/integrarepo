/**
 * @description       :
 * @author            : ????
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 2.0   04-29-2021   alexandre.costa@integra-biosciences.com   Added Standard Price
**/
public with sharing class NewQuoteController
{

	@AuraEnabled
	public static List<Id> createQuote(Id opportunityId, Integer quoteNumber)
	{
		List<Id> newQuoteIds = new List<Id>();
		Opportunity opportunity = [
			SELECT Id, Contact__c, Pricebook2Id, AccountId, DiscountServices__c, DiscountDisposables__c, DiscountPercentage__c, ProductLanguage__c, TextInQuote__c, QuoteExpirationDate__c,
						(SELECT Quantity, UnitPrice, TotalPrice, PricebookEntryId, Product2Id, DemoEquipment__c, List_Price__c,  SortOrder__c,
								PricebookEntry.Name, PricebookEntry.Product2.Family, ListPriceTierPricing__c, ListPriceOneItem__c, Standard_Price__c
							FROM OpportunityLineItems)
			FROM Opportunity
			WHERE Id = :opportunityId
		];
		List<Quote> newQuotes = new List<Quote>();

		for (Integer i=1; i <= quoteNumber; i++)
		{
			Quote newQuote = new Quote();
			newQuote.Name = '--- Auto Generated Name ---';
			newQuote.OpportunityId = opportunityId;
			newQuote.ContactId = opportunity.Contact__c;
			newQuote.DiscountDisposables__c = opportunity.DiscountDisposables__c;
			newQuote.DiscountProducts__c = opportunity.DiscountPercentage__c;
			newQuote.DiscountServices__c = opportunity.DiscountServices__c;
			newQuote.TextInQuote__c = opportunity.TextInQuote__c;
			newQuote.ExpirationDate = opportunity.QuoteExpirationDate__c;
			newQuote.Pricebook2Id = opportunity.Pricebook2Id;
			newQuotes.add(newQuote);
		}
		insert newQuotes;

		List<QuoteLineItem> quoteLIs = new List<QuoteLineItem>();
		for (Quote itemQuote : newQuotes)
		{
			for (OpportunityLineItem itemOpp :opportunity.OpportunityLineItems)
			{
				QuoteLineItem newQuoteLI = new QuoteLineItem();
				newQuoteLI.Quantity = itemOpp.Quantity;
				newQuoteLI.QuoteId = itemQuote.Id;
				newQuoteLI.OpportunityLineItemId = itemOpp.Id;
				newQuoteLI.UnitPrice = itemOpp.UnitPrice;
				newQuoteLI.Product2Id = itemOpp.Product2Id;
				newQuoteLI.ListPriceTierPricing__c = itemOpp.ListPriceTierPricing__c;
				newQuoteLI.ListPriceOneItem__c = itemOpp.ListPriceOneItem__c;
				newQuoteLI.SortOrder__c = itemOpp.SortOrder__c;
				newQuoteLI.PricebookEntryId = itemOpp.PricebookEntryId;
				newQuoteLI.DemoEquipment__c = itemOpp.DemoEquipment__c;
				newQuoteLI.Standard_Price__c = itemOpp.Standard_Price__c;
				quoteLIs.add(newQuoteLI);
			}
			newQuoteIds.add(itemQuote.Id);
		}
		insert quoteLIs;

		return newQuoteIds;
	}
}
/**
 * =============================================================
 * AUTHOR         : Alex Costa
 * CREATE DATE    : 08.04.2021
 * PURPOSE        : Class to query Quote Line Item Records
 * SPECIAL NOTES  :
 * =============================================================
 * Change History :
 * Date        Author               Description
 * ==========  ==================   ============================
 * 08.04.2021  Alex Costa           Created
 * ============================================================= 
 */
public with sharing class QuoteLineItemSelector {
    public static List<QuoteLineItem> getQuoteLineItemsByIds(Set<Id> lineItemIds) {
        return [SELECT Id, QuoteId, Product2Id, Product2.Name, Product2.ProductType__c, Quantity, UnitPrice FROM QuoteLineItem 
                 WHERE Id in : lineItemIds];
    }
}
/**
 * Trigger handler by extending the TriggerHandlerAdapter from the TriggerTemplateV2
 * and overriding only the in the trigger registered events
 *
 * @author PARX, ach
 */
public with sharing class OpportunitylineItemTriggerHandler extends TriggerTemplateV2.TriggerHandlerAdapter {

	public override void onBeforeInsert (List<sObject> newList) {

	}

	public override void onBeforeUpdate (List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {

	}

	public override void onAfterInsert(List<sObject> newList, Map<Id, sObject> newMap) {
//		OpportunityLineItemService.getDataFromQuoteLineItems(newList);
		//OpportunityLineItemService.setDiscountApprovalProcesses(newList, null);
	}

	public override void onAfterUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {
		OpportunityLineItemService.syncToQuoteLineItems(newList);
		//OpportunityLineItemService.setDiscountApprovalProcesses(newList, oldMap);
	}

	public override void onAfterDelete(List<sObject> oldList, Map<Id, sObject> oldMap) {
	}
}
/**
 * @description  :
 * @author       : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   06-10-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class PriceBookSelector {
    /**
    * @description Get the Standard Price book ID
    * @author alexandre.costa@integra-biosciences.com | 06-10-2021
    * @return Id
    **/
    public static Id getStandardPriceBookId() {
        return [SELECT Id FROM PriceBook2 WHERE IsStandard = true AND IsActive = true]?.Id;
    }
}

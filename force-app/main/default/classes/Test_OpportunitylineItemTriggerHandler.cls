/*
*	@description Test class for AktionTriggerHandlerUpdateRelations functionality.
*
*	@author ach
*	@copyright PARX
*/
@isTest
public with sharing class Test_OpportunitylineItemTriggerHandler
{
	public static Account account{get;set;}
	
	public static Opportunity testOpportunity{get;set;}
	
	public static PricebookEntry standardPrice{get;set;}
	
	//@testSetup
	/**
	* Create test data.
	*/
	public static void createTestData()
	{
		account = new Account(Name = 'test account');
		insert account;
		testOpportunity = new Opportunity(Name = 'opp1', AccountId = account.Id, StageName = 'Interessiert', 
				CloseDate = date.today());
		insert testOpportunity;
		
		Product2 prod = new Product2(Name = 'Laptop X200', Family = 'Hardware');
		insert prod;
		
		Id pricebookId = Test.getStandardPricebookId();
		standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id,
				UnitPrice = 10000, IsActive = true);
		insert standardPrice;
	}
}
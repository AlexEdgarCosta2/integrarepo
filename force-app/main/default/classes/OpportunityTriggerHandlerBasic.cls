/*
*   @description OpportunityTriggerHandlerBasic functionality.
*
*   @author ach
*   @copyright PARX
*
*   mgo 1.7.2020 refactoring for new Discount fields API Names
*/
public with sharing class OpportunityTriggerHandlerBasic implements TriggerTemplate.Handler
{
    private List<Opportunity> newValues;
    private Map<Id, Opportunity> oldValues;

    /**
    *   @description Set Trigger.new and Trigger.old to newValues and oldValues accordingly
    *
    *   @param theNewValues is a Trigger.new records
    *   @param theOldValues is a Trigger.old.Map records
    */
    public void setValues(List<sObject> theNewValues, Map<Id, sObject> theOldValues)
    {
        newValues = theNewValues;
        oldValues = (Map<Id, Opportunity>)theOldValues;
    }

    /**
    *   @description Handle method for actions
    *
    *   @param theAction is a Trigger actions
    */
    public void handle(TriggerTemplate.TriggerAction theAction)
    {
        if (theAction == TriggerTemplate.TriggerAction.beforeInsert)
        {
            for (Opportunity opportunityRecord : newValues) {
                OpportunityService.setAutoNumberOppName(opportunityRecord);

                OpportunityService.initDiscountFields(opportunityRecord);
                OpportunityService.setStageQuotation(opportunityRecord);

                OpportunityService.setLanguage(opportunityRecord);
            }

            TriggerUtils.saveAutoNumber();

        }
        if (theAction == TriggerTemplate.TriggerAction.afterinsert)
        {
            //oppCloneWithLineItem();
            //cloneOpportunityWithOpportunityLineItems();
        }
        if (theAction == TriggerTemplate.TriggerAction.beforeupdate)
        {
            checkUniqueNames();
            for (Opportunity opportunityRecord : newValues) {
                OpportunityService.setStageQuotation(opportunityRecord);
            }
        }

        if (theAction == TriggerTemplate.TriggerAction.afterupdate)
        {
            updateRelatedLinesWithOverallDiscount();
        }
    }


    private void updateRelatedLinesWithOverallDiscount()
    {
        Set<SObject> aModifiedObjects = TriggerUtils.getModifiedObjectsObj(new List<String>{'DiscountPercentage__c','DiscountServices__c','DiscountDisposables__c'}, newValues,
                 oldValues);

        if (aModifiedObjects.size() > 0)
        {
            Map<Id, Opportunity> allNewOpps = new Map<Id, Opportunity>(newValues);
            Set<Id> oppIds = new Map<Id, SObject>(new List<SObject>(aModifiedObjects)).keySet();
            List<OpportunityLineItem> lineItemsToUpdate  = new List<OpportunityLineItem>();
            List<OpportunityLineItem> lineItems = [SELECT Id,ProductType__c, ListPriceOneItem__c, OpportunityId  FROM OpportunityLineItem WHERE OpportunityId IN :oppIds];
            for (OpportunityLineItem item : lineItems)
            {
                Opportunity newOpportunity = allNewOpps.get(item.OpportunityId);
                Opportunity oldOpportunity = this.oldValues.get(item.OpportunityId);

                Opportunity parentOpportunity = allNewOpps.get(item.OpportunityId);
                if (this.shouldRecalculateDiscount(newOpportunity, oldOpportunity, Opportunity.DiscountPercentage__c) && item.ProductType__c == 'Products and Accessories' && parentOpportunity.DiscountPercentage__c != null && item.ListPriceOneItem__c != null)
//                if (parentOpportunity.DiscountPercentage__c != null && item.ProductType__c == 'Products and Accessories')
                {
                    item.ListPriceTierPricing__c =  (item.ListPriceOneItem__c * (1 - 0.01 * parentOpportunity.DiscountPercentage__c)).setScale(2);
                    item.UnitPrice =  (item.ListPriceOneItem__c * (1 - 0.01 * parentOpportunity.DiscountPercentage__c)).setScale(2);
                    lineItemsToUpdate.add(item);
                }

                if (this.shouldRecalculateDiscount(newOpportunity, oldOpportunity, Opportunity.DiscountServices__c) && item.ProductType__c =='Services' && item.ListPriceOneItem__c != null)
//                if (parentOpportunity.DiscountServices__c != null && item.ProductType__c =='Services')
                {
                    item.ListPriceTierPricing__c =  (item.ListPriceOneItem__c * (1 - 0.01 * parentOpportunity.DiscountServices__c)).setScale(2);
                    item.UnitPrice =  (item.ListPriceOneItem__c * (1 - 0.01 * parentOpportunity.DiscountServices__c)).setScale(2);
                    lineItemsToUpdate.add(item);
                }

                if (this.shouldRecalculateDiscount(newOpportunity, oldOpportunity, Opportunity.DiscountDisposables__c) && item.ProductType__c =='Disposables' && parentOpportunity.DiscountPercentage__c != null && item.ListPriceOneItem__c != null)
//                if (parentOpportunity.DiscountDisposables__c != null && item.ProductType__c =='Disposables')
                {
                    item.ListPriceTierPricing__c =  (item.ListPriceOneItem__c * (1 - 0.01 * parentOpportunity.DiscountDisposables__c)).setScale(2);
                    item.UnitPrice =  (item.ListPriceOneItem__c * (1 - 0.01 * parentOpportunity.DiscountDisposables__c)).setScale(2);
                    lineItemsToUpdate.add(item);
                }
            }

            update lineItemsToUpdate;
        }
    }

    private Boolean shouldRecalculateDiscount(Opportunity newOpportunity, Opportunity oldOpportunity, SObjectField discountField)
    {
        Decimal newValue = (Decimal) newOpportunity.get(discountField);
        Decimal priorValue = (Decimal) oldOpportunity.get(discountField);

        return newValue != null && newValue != priorValue;
    }

    /**
    *   @description Check if the updated name is Unique
    */
    private void checkUniqueNames()
    {
        //@TODO truggerUtils get modified objects
        Set<sObject> aModifiedObjects = TriggerUtils.getModifiedObjectsObj(new List<String>{'Name'}, newValues,
                 oldValues);
        Set<String> names = new Set<String>();
        List<Id> anIds = new List<Id>();
        List<Opportunity> aModifiedOpps = new List<Opportunity>();
        for (sObject anObject : aModifiedObjects)
        {
            Opportunity anItem = (Opportunity)anObject;
            names.add(anItem.Name);
            anIds.add(anItem.Id);
            aModifiedOpps.add(anItem);
        }

        List<Opportunity> anExistOpportunities = [SELECT Id, Name FROM Opportunity WHERE Name IN
                :names AND Id NOT IN :anIds];
        if (anExistOpportunities.size() > 0)
        {
            for(Opportunity anItem : aModifiedOpps)
            {
                for (Opportunity anExistItem : anExistOpportunities)
                {
                    if (anExistItem.Name == anItem.Name)
                    {
                        anItem.Name.addError(Label.EROR_NAME_IS_NOT_UNIQUE);
                    }
                }
            }
        }
        //check the same names in the new ones
        for(Opportunity anItem : aModifiedOpps)
        {
            for(Opportunity anItem2 : aModifiedOpps)
            {
                if (anItem != anItem2 && anItem.Name == anItem2.Name)
                {
                    anItem2.Name.addError(Label.EROR_NAME_IS_NOT_UNIQUE);
                }
            }
        }

    }

    /**
    *   @description update the ooportunity name.
    */
    /*
    private void updateTheName()
    {
        for(Opportunity anItem : newValues)
        {
            String autoNummer = String.valueOf(TriggerUtils.getAutonumber('Opportunity')).replace('.0', '');
            for(integer i = autoNummer.length(); i <8; i++)
            {
                autoNummer= '0'+autonummer;
            }
            anItem.Name = String.valueOf(Date.today().year()).substring(2) + autoNummer;
        }
        TriggerUtils.saveAutoNumber();
    }

    /**
	 * Sets Discount* fields to 0 if they are not set
	 */
    /*
    private void populateDiscountFields(List<Opportunity> newOpportunityRecords)
    {
        for (Opportunity newOpportunityRecord : newOpportunityRecords)
        {
            for (SObjectField discountField : new List<SObjectField>{Opportunity.DiscountPercentage__c, Opportunity.DiscountServices__c, Opportunity.DiscountDisposables__c})
            {
                if (newOpportunityRecord.get(discountField) == null)
                {
                    newOpportunityRecord.put(discountField, 0);
                }
            }
        }
    }
    */
}
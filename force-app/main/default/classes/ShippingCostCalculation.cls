/**
 * @description  : 
 * @author       : Tim.lehmann@integra-biosciences.com
 * Modifications Log 
 * Ver   Date         Author                                Modification
 * 1.0   07-22-2021   Tim.lehmann@integra-biosciences.com   Initial Version
 * 1.1   09-27-2021   Tim.lehmann@integra-biosciences.com   Made variables public ones
**/
public with sharing class ShippingCostCalculation {
     
     public static Double  volumeSum = 0.0;
     public static Double  weightSum  = 0.0; 

     public static Integer weightScore = 0;
     public static Integer volumeScore = 0;
	 
     public static Double weightComparativeValue_1 = Double.valueOf(Label.Weight_Sum_Comparitive_Value_1);
     public static Double weightComparativeValue_2 = Double.valueOf(Label.Weight_Sum_Comparitive_Value_2);
     public static Double weightComparativeValue_3 = Double.valueOf(Label.Weight_Sum_Comparitive_Value_3);
     public static Double weightComparativeValue_4 = Double.valueOf(Label.Weight_Sum_Comparitive_Value_4);
     
     public static Double volumeComparativeValue_1 = Double.valueOf(Label.Volume_Sum_Comparative_Value_1);
     public static Double volumeComparativeValue_2 = Double.valueOf(Label.Volume_Sum_Comparative_Value_2);
     public static Double volumeComparativeValue_3 = Double.valueOf(Label.Volume_Sum_Comparative_Value_3);
   

    @InvocableMethod(label='CalculateShippingCost')
    
    
    public static List<Shipping_Cost_Germany__mdt> ShippingCostCalculation(List<Webcart> webCartList) {

        String webCartId = webCartList[0].Id;
        
        List <CartItem> cartItemList = CartItemSelector.getCartItemsFromActualCart(webCartId);

        for (cartItem cartItem : cartItemList) {
            
            volumeSum += cartItem.Product2.B2B_Product_Volume__c * cartItem.Quantity;
            weightSum += cartItem.Product2.B2B_Product_Weight__c * cartItem.Quantity;
        
        }
        
        if(weightSum <= weightComparativeValue_1) {
            weightScore = 1;
         } else if(weightSum <= weightComparativeValue_2) {
              weightScore = 2;
         } else if(weightSum <= weightComparativeValue_3) {
              weightScore = 3;
         } else if(weightSum <= weightComparativeValue_4) {
              weightScore = 4;
         } else if(weightSum > weightComparativeValue_4) {
              weightScore = 5;
         }

         // VolumeScore 3 does not exsist because there is no large parcel for Volume
         if(volumeSum <= volumeComparativeValue_1) {
             volumeScore = 1;
          } else if(volumeSum <= volumeComparativeValue_2) {
               volumeScore = 2;
          } else if(volumeSum <= volumeComparativeValue_3) {
               volumeScore = 4;
          } else if(volumeSum > volumeComparativeValue_3) {
               volumeScore = 5;
          }
        
         Integer maxValue = Math.max(weightScore,volumeScore);
         
         List<Shipping_Cost_Germany__mdt> sCostGermany = [SELECT Economy__c,Express__c,Go__c FROM Shipping_Cost_Germany__mdt WHERE Number__c =: maxValue ];
         
         return sCostGermany;
     }
}
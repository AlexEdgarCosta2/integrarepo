/**
 * @description       : Class that handles Discounts Approvals
 * @author            : alexandre.costa@integra-biosciences.com
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-28-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
public with sharing class ApprovalUtil {
	/**
	* @description Sets the approval processes on Quotes
	* @author alexandre.costa@integra-biosciences.com | 04-28-2021
	* @param objectName
	* @param newList
	* @param oldMap
	**/
	public static void setDiscountApprovalProcesses(String objectName, List<QuoteLineItem> newList, Map<Id, QuoteLineItem> oldMap) {

        System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] ApprovalUtil BEGIN');

        Discount_Config__mdt config = CustomMetadataSelector.getDiscountConfig();

        Map<String, Discount_Market_Config__mdt> marketConfigMap = getMarketConfigMap();

        /*
        If there is no config or the existing config says that the approvals are not active
        */
        if(config == null || config != null && !config.IsActive__c) {
            return;
        }

        Set<Id> lineItemIds = new Set<Id>();
		Set<Id> productIds = new Set<Id>();
		List<QuoteLineItem> lineItems = new List<QuoteLineItem>();
        Map<Id, QuoteLineItem> lineItemsMap = new Map<Id, QuoteLineItem>();
		Set<Id> quoteIds = new Set<Id>();
        Set<Id> rejectedQuoteIds = new Set<Id>();
		Map<Id, List<QuoteLineItem>> quoteItems = new Map<Id, List<QuoteLineItem>>();

		for (QuoteLineItem li : newList) {
            Boolean flagInclude = true;
            if(oldMap != null) {
                flagInclude = ((li.get('UnitPrice') != oldMap.get(li.Id).get('UnitPrice')) ||
                               (oldMap.get(li.Id).get('Approval_Status__c') == Constants.APPROVAL_STATUS_REJECTED)
                              );
            }

            if(flagInclude) {
                //Id quoteId = (Id)li.get('QuoteId');

                productIds.add((Id)li.get('Product2Id'));

                quoteIds.add(li.QuoteId);

                lineItemIds.add((Id)li.get('Id'));

                lineItems.add(li);
                lineItemsMap.put(li.Id, li);

                if(quoteItems.get(li.QuoteId) == null) {
                    List<QuoteLineItem> aux = new List<QuoteLineItem>();
                    aux.add(li);
                    quoteItems.put(li.QuoteId, aux);
                } else {
                    quoteItems.get(li.QuoteId).add(li);
                }
                if(oldMap != null && (oldMap.get(li.Id).Approval_Status__c == Constants.APPROVAL_STATUS_REJECTED)) {
                    rejectedQuoteIds.add(li.QuoteId);
                }
            }
	  	}

        System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] lineItemsMap : ' + lineItemsMap);
        System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] quoteIds : ' + quoteIds);
        System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] quoteItems : ' + quoteItems.keySet());

        OrgWideEmailAddress orgWideEmailAddress = OrgWideEmailAddressSelector.getOrgWideEmailAddressByDisplayName(Constants.DISCOUNT_APPROVAL_EMAIL_ADDRESS);

		Map<String, List<Discount_Approver__mdt>> approversMap = getApproversMap();

		String currencyIsoCode = '';
		Decimal discountPercentage = 0;
		Decimal discountDisposables = 0;
		Decimal discountServices = 0;
		String billingCountryCode = '';
        String accountGroup = '';
		Id accountId;

		Map<Id, Product2> productsMap = ProductSelector.getProductsMap(productIds);

        Set<String> families = new Set<String>();
        for(Id key : productsMap.keySet()) {
            Product2 prod = productsMap.get(key);
            families.add(prod.ProductFamily__c);
        }

        System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] productIds : ' + productIds);
        System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] families : ' + families);

        Map<String, Promotion_Product__c> promotionMap = PromotionProductSelector.getMapAllFiltered(productIds, families);

        System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] promotionMap : ' + promotionMap);

		List<Quote> quoteList = QuoteSelector.getQuotesByIds(quoteIds);

		Map<String, PriceEntry__c> priceEntryMap = PriceEntrySelector.getpriceEntryMapByProducts2(productIds);

		System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] priceEntryMap : ' + priceEntryMap);
        System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] quoteList : ' + quoteList);

		Boolean flagApproveGlobalHead = false;
		Boolean flagApproveCountryManager = false;

        List<SObject> quoteToUpdate = new List<SObject>();
        List<SObject> quoteToUpdateRejected = new List<SObject>();
        List<SObject> allQuotes = new List<SObject>();

		for(Quote quote : quoteList) {

			flagApproveGlobalHead = false;
			flagApproveCountryManager = false;

            currencyIsoCode = quote.CurrencyIsoCode;
            discountPercentage = quote.DiscountProducts__c;
            discountDisposables = quote.DiscountDisposables__c;
            discountServices = quote.DiscountServices__c;
            accountId = quote.AccountId;
            billingCountryCode = quote.Account.BillingCountryCode;
            accountGroup = quote.Account_Group__c;

			List<LineItemWrapper> lwList = new List<LineItemWrapper>();
			for (QuoteLineItem lineItem : quoteItems.get(quote.Id)) {

				Product2 prod = (Product2)productsMap.get((Id)lineItem.get('Product2Id'));

                List<PriceEntry__c> priceEntryList = PricingService.getCurrentPriceEntryList(priceEntryMap,
                    prod.Id,
                    accountGroup,
                    currencyIsoCode,
                    billingCountryCode,
                    discountPercentage,
                    discountDisposables,
                    discountServices);

                System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] 1 product : ' + prod.Id + ' PriceEntry : ' + priceEntryList);

                if(priceEntryList != null && !priceEntryList.isEmpty()) {
                    LineItemWrapper lw =
                        new LineItemWrapper(prod,
                            prod.Name,
                            priceEntryList,
                            priceEntryList,
                            0,
                            0,
                            0,
                            prod.ProductType__c);

                    lw.bilingCountryCode = billingCountryCode;

                    lw.qteLineItem = (QuoteLineItem)lineItem;
                    lw.qty = ((QuoteLineItem)lineItem).quantity;

                    lwList.add(lw);
                } else {
                    System.debug(LoggingLevel.ERROR, '[setDiscountApprovalProcesses] No price entry found for the Product : ' + prod.Name + ' (' + prod.Id + ')');
                }
			}

			PricingService.applyTiers(lwList,
									 discountPercentage,
									 discountDisposables,
									 discountServices);
									 //accountId,
									 //PricingService.getProductToListPrice(priceEntryMap, currencyIsoCode, billingCountryCode));

			integer approvalType = 0;

            List<LineItemWrapper> apprvLines = new List<LineItemWrapper>();
			for(LineItemWrapper lw : lwList) {
				Integer aux = getApprovalTypeQuote(lineItemsMap, lw, config, productsMap, promotionMap, marketConfigMap);

                System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] product : ' + lw.qteLineItem.Product2Id + ' Approval : ' + aux);

                if(aux != Constants.NO_APPROVAL) {
                    apprvLines.add(lw);
                }
				if(approvalType < aux) {
					approvalType = aux;
				}
			}

            System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] rejectedQuoteIds : ' + rejectedQuoteIds);
            System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] quote.Id : ' + quote.Id);
            System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] approvalType : ' + approvalType);

			if(approvalType != Constants.NO_APPROVAL) {
				ApproversWrapper approversWrapper = getProductDiscountApprovers(approvalType, billingCountryCode, approversMap);

                String productsStr = getLineItemMailString(lineItemsMap, apprvLines, productsMap, currencyIsoCode);

				String approvers = MailUtil.sendApprovalDiscountMail(quote, approversWrapper.approversList, approversWrapper.ccApproversList, orgWideEmailAddress.Id, approvalType, productsStr);

                quote.Approval_Status__c = Constants.APPROVAL_STATUS_PENDING;
                quote.Approval_Type__c = approvalType;
                quote.Approvers__c = approvers;
                quoteToUpdate.add(quote);

			} else {
                if(rejectedQuoteIds.contains(quote.Id)) {
                    quote.Approval_Status__c = null;
                    quote.Approval_Type__c = null;
                    quote.Approvers__c = null;
                    quoteToUpdateRejected.add(quote);
                }
            }
		}

        System.debug(LoggingLevel.DEBUG, '[setDiscountApprovalProcesses] quoteToUpdate : ' + quoteToUpdate);

        allQuotes.addAll(quoteToUpdate);
        allQuotes.addAll(quoteToUpdateRejected);

        if(!allQuotes.isEmpty()) {
            update allQuotes;

            for(SObject quote : quoteToUpdate) {
                setApprovalOnRecord(quote.Id, 'Discount approval on Quote');
            }
        }
	}

    /**
    * @description Gets the approvers emais
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param approvalType
    * @param billingCountryCode
    * @param approversMap
    * @return ApproversWrapper
    **/
    public static ApproversWrapper getProductDiscountApprovers(Integer approvalType, String billingCountryCode,
                                                       Map<String, List<Discount_Approver__mdt>> approversMap) {
        /*
        APPROVAL_GLOBAL_HEAD_OR_DIRECTOR = 3;
        APPROVAL_GLOBAL_HEAD = 2;
        APPROVAL_COUNTRY_MANAGER = 1;
        NO_APPROVAL = 0;
        1|CH
        2|CH
        3|CH
        */

        ApproversWrapper approversWrapper = new ApproversWrapper();

        if (approvalType == Constants.APPROVAL_COUNTRY_MANAGER) {
            //
            // Approval will allways be done by the country manager with global head and director eu in cc
            //
            addApprovers(Constants.DISCOUNT_APPROVER_COUNTRY_MANAGER + '|' + billingCountryCode, approversWrapper.approversList, approversMap);

            addApprovers(Constants.DISCOUNT_APPROVER_GLOBAL_HEAD, approversWrapper.ccApproversList, approversMap);
            addApprovers(Constants.DISCOUNT_APPROVER_DIRECTOR_EU, approversWrapper.ccApproversList, approversMap);

            if(approversWrapper.approversList.isEmpty()) {
                addApprovers(Constants.DISCOUNT_APPROVER_COUNTRY_MANAGER, approversWrapper.approversList, approversMap);
            }
        }

        System.debug(LoggingLevel.DEBUG, '[getProductDiscountApprovers] approversWrapper : ' + approversWrapper);

        return approversWrapper;
    }

    /**
    * @description Adds approvers to the approvers list.
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param key
    * @param approversList
    * @param approversMap
    **/
    private static void addApprovers(String key, List<Discount_Approver__mdt> approversList, Map<String, List<Discount_Approver__mdt>> approversMap) {
        List<Discount_Approver__mdt> apprv = approversMap.get(key);
        if(apprv != null) {
            approversList.addAll(apprv);
        }
    }

    /**
    * @description Get the approval type for an Quote based on the discounts given
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param lineItemsMap
    * @param lw
    * @param config
    * @param productsMap
    * @param promotionMap
    * @return Integer
    **/
    public static Integer getApprovalTypeQuote(Map<Id, SObject> lineItemsMap, LineItemWrapper lw, Discount_Config__mdt config,
                                               Map<Id, Product2> productsMap, Map<String, Promotion_Product__c> promotionMap,
                                               Map<String, Discount_Market_Config__mdt> marketConfigMap) {

        if(config != null) {
            QuoteLineItem li = (QuoteLineItem)lineItemsMap.get(lw.qteLineItem.Id);

            System.debug('[getApprovalTypeQuote] lineItemsMap : ' + lineItemsMap);
            System.debug('[getApprovalTypeQuote] lw : ' + lw);
            System.debug('[getApprovalTypeQuote] config : ' + config);
            System.debug('[getApprovalTypeQuote] productsMap : ' + productsMap);
            System.debug('[getApprovalTypeQuote] lw.sellingPrice : ' + lw.sellingPrice);
            System.debug('[getApprovalTypeQuote] li.UnitPrice : ' + li.UnitPrice);

            Decimal appliedDiscount;
            if(lw.sellingPrice == 0) {
                appliedDiscount = 0;
            } else {
                appliedDiscount = (1 - (li.UnitPrice / lw.sellingPrice));
            }

            System.debug('[getApprovalTypeQuote] appliedDiscount : ' + appliedDiscount);

            return calcApprovalType(appliedDiscount, (config.Discount_Limit__c / 100), lw,
                                                      productsMap.get(lw.product.Id).ProductFamily__c, config, promotionMap,
                                                      marketConfigMap);

        } else {
            System.debug(LoggingLevel.ERROR, '[getApprovalTypeQuote] No "MAIN DISCOUNT CONFIG" in the system!!!!!! Please ask your Salesforce Admin to configure one');
            return 0;
        }
    }

    /**
    * @description Creates the HTML table, with the line items under approval, to be used in the approval email.
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param lineItemsMap
    * @param lw
    * @param config
    * @param productsMap
    * @param promotionMap
    * @return Integer
    **/
    public static String getLineItemMailString(Map<Id, SObject> lineItemsMap, List<LineItemWrapper> lwList,
        Map<Id, Product2> productsMap, String currencyIsoCode) {

        String header = '<table style="border: 1px solid black; border-collapse: collapse;">' +
                 '<tr style="background-color: ' + Constants.INTEGRA_BLUE + '; color: white;">' +
                 '<th style="text-align: left; border: 1px solid black; padding: 8px;">' + Constants.APPROVAL_MAIL_PRODUCT +'</th>' +
                 '<th style="text-align: right; border: 1px solid black; padding: 8px;">' + Constants.APPROVAL_MAIL_QUANTITY + '</th>' +
                 '<th style="text-align: right; border: 1px solid black; padding: 8px;">' + Constants.APPROVAL_MAIL_SELLING_PRICE + ' (' + currencyIsoCode + ')</th>' +
                 '<th style="text-align: right; border: 1px solid black; padding: 8px;">' + Constants.APPROVAL_MAIL_NEW_PRICE + ' (' + currencyIsoCode + ')</th>' +
                 '<th style="text-align: right; border: 1px solid black; padding: 8px;">' + Constants.APPROVAL_MAIL_DISCOUNT + '</th>' +
                 '</tr>';

        for(LineItemWrapper lw : lwList) {
            Decimal quantity;
            Decimal newPrice;
            Id productId;

            QuoteLineItem li = (QuoteLineItem)lineItemsMap.get(lw.qteLineItem.Id);
            newPrice = li.UnitPrice;
            quantity = li.quantity;
            quantity = li.quantity;
            productId = li.Product2Id;

            if(productId != null) {
                Decimal appliedDiscount = (1 - (newPrice / lw.sellingPrice)) * 100;
                Product2 product = productsMap.get(productId);

                header += '<tr>' +
                        '<td style="text-align: left; border: 1px solid black; padding: 8px;">' + product.Name + '</td>' +
                        '<td style="text-align: right; border: 1px solid black; padding: 8px;">' + String.valueOf(quantity) + '</td>' +
                        '<td style="text-align: right; border: 1px solid black; padding: 8px;">' + Utils.decimalToCurrencyString(lw.sellingPrice) + '</td>' +
                        '<td style="text-align: right; border: 1px solid black; padding: 8px;">' + Utils.decimalToCurrencyString(newPrice) + '</td>' +
                        '<td style="text-align: right; border: 1px solid black; padding: 8px;">' + Utils.decimalToCurrencyString(appliedDiscount) + '%</td>' +
                        '</tr>';
            }
        }
        header += '</table>';

        return header;
    }

    /**
    * @description Calculates the approval type.
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param appliedDiscount
    * @param LimitDiscount
    * @param lw
    * @param productFamily
    * @param config
    * @param promotionMap
    * @return Integer
    **/
    private static Integer calcApprovalType(Decimal appliedDiscount, Decimal LimitDiscount, LineItemWrapper lw,
                                            String productFamily, Discount_Config__mdt config, Map<String, Promotion_Product__c> promotionMap,
                                            Map<String, Discount_Market_Config__mdt> marketConfigMap) {
        Integer approvalType = Constants.NO_APPROVAL;

        System.debug(LoggingLevel.DEBUG, '[calcApprovalType] appliedDiscount : ' + appliedDiscount);

        appliedDiscount = appliedDiscount.setScale(4);
        Decimal lineValue = lw.qteLineItem.Quantity * lw.listPriceTierPricing;
        lineValue = lineValue.setScale(2);

        System.debug(LoggingLevel.DEBUG, '[calcApprovalType] appliedDiscount : ' + appliedDiscount);
        System.debug(LoggingLevel.DEBUG, '[calcApprovalType] lw.applicableTier : ' + lw.applicableTier);
        System.debug(LoggingLevel.DEBUG, '[calcApprovalType] lw.tier3Discount : ' + lw.tier3Discount);
        System.debug(LoggingLevel.DEBUG, '[calcApprovalType] lw.tier2Discount : ' + lw.tier2Discount);
        System.debug(LoggingLevel.DEBUG, '[calcApprovalType] lineValue : ' + lineValue);


        Decimal discountLimit = config.Discount_Limit__c;

        /* Check Market Config */
        Discount_Market_Config__mdt marketConfig = marketConfigMap.get(lw.bilingCountryCode);

        if(marketConfig == null) {
            marketConfig = marketConfigMap.get(Constants.OTHER_COUNTRIES_DISCOUNT_CONFIG);
        }

        if(marketConfig != null) {
            if(marketConfig.Discount_Limit__c != null) {
                discountLimit = marketConfig.Discount_Limit__c;
            }
        }

        System.debug(LoggingLevel.DEBUG, '[calcApprovalType] marketConfig : ' + marketConfig);

        /*
        APPROVAL_GLOBAL_HEAD_OR_DIRECTOR = 3;
        APPROVAL_GLOBAL_HEAD = 2;
        APPROVAL_COUNTRY_MANAGER = 1;
        NO_APPROVAL = 0;
        */

        //
        // Check if product belongs to a promotion
        // 1sh check by family
        // 2nd check by product
        String key = productFamily + '|' + lw.bilingCountryCode;
        String key2 = lw.product.Id + '|' + lw.bilingCountryCode;

        system.debug(LoggingLevel.DEBUG, '1) PromotionMap : ' + promotionMap.get(key));
        system.debug(LoggingLevel.DEBUG, '2) PromotionMap : ' + promotionMap.get(key2));

        if(promotionMap.get(key) != null && (promotionMap.get(key).Discount_Threshold__c == null ||
                                            (promotionMap.get(key).Discount_Threshold__c != null && promotionMap.get(key).Discount_Threshold__c / 100 > appliedDiscount))) {
            //
            // PROMOTION - NO APPROVAL
            //
            System.debug(LoggingLevel.DEBUG, 'PROMOTION BY FAMILY -> NO APPROVAL.');
            approvalType = Constants.NO_APPROVAL;
        } else if(promotionMap.get(key2) != null && (promotionMap.get(key2).Discount_Threshold__c == null ||
                                                    (promotionMap.get(key2).Discount_Threshold__c != null && promotionMap.get(key2).Discount_Threshold__c / 100 > appliedDiscount))) {

            //
            // PROMOTION - NO APPROVAL
            //
            System.debug(LoggingLevel.DEBUG, 'PROMOTION BY PRODUCT -> NO APPROVAL.');
            approvalType = Constants.NO_APPROVAL;
        } else if(productFamily != null && config.Product_Group_Exclude__c.contains(productFamily)) {
            //
            // EVOLVE -> NO APPROVAL.
            //
            System.debug(LoggingLevel.DEBUG, 'EVOLVE -> NO APPROVAL.');
            approvalType = Constants.NO_APPROVAL;
        } else if(appliedDiscount > (discountLimit / 100) && promotionMap.get(key) == null && promotionMap.get(key2) == null) {
            // The product is not in a promotion
            //
            // APPLIED DISCOUNT BIGGER THAN 30%
            //
            System.debug(LoggingLevel.DEBUG, 'APPLIED DISCOUNT > ' + discountLimit + '%');
            approvalType = Constants.APPROVAL_COUNTRY_MANAGER;
        } else if(promotionMap.get(key) != null && (promotionMap.get(key).Discount_Threshold__c != null && promotionMap.get(key).Discount_Threshold__c / 100 < appliedDiscount)) {
            //
            // Applied discount bigger than threshold
            //
            System.debug(LoggingLevel.DEBUG, 'PROMOTION BY FAMILY -> Threshold discount applied.');
            approvalType = Constants.APPROVAL_COUNTRY_MANAGER;
        } else if(promotionMap.get(key2) != null && (promotionMap.get(key2).Discount_Threshold__c != null && promotionMap.get(key2).Discount_Threshold__c / 100 < appliedDiscount)) {

            //
            // Applied discount bigger than threshold
            //
            System.debug(LoggingLevel.DEBUG, 'PROMOTION BY PRODUCT -> Threshold discount applied.');
            approvalType = Constants.APPROVAL_COUNTRY_MANAGER;
        }


        //
        // When the line value is smaller than a configured threshold then there is no approval!
        //
        if(approvalType != Constants.NO_APPROVAL &&
           marketConfig?.Line_Item_Threshold__c != null && marketConfig?.Line_Item_Threshold__c >= lineValue) {

            System.debug(LoggingLevel.DEBUG, '[calcApprovalType] NO APPROVAL BY LINE VALUE');
            System.debug(LoggingLevel.DEBUG, '[calcApprovalType] marketConfig.Line_Item_Threshold__c : ' + marketConfig.Line_Item_Threshold__c);

            approvalType = Constants.NO_APPROVAL;
        }

        return approvalType;
    }

    /**
    * @description Starts am approval process in a record
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param recordId
    * @param comments
    * @return Boolean
    **/
    public static Boolean setApprovalOnRecord(Id recordId, String comments) {
        Approval.ProcessSubmitRequest newApprovalRequest = new Approval.ProcessSubmitRequest();

        newApprovalRequest.setComments(comments);
        newApprovalRequest.setObjectId(recordId);

        Approval.ProcessResult result = Approval.process(newApprovalRequest);

        if(!result.isSuccess()) {
            System.debug(LoggingLevel.ERROR, 'Error seting a approval process on the record : ' + recordId + ' -> ' + result.getErrors());
        }
        return result.isSuccess();
    }

    /**
    * @description Approve/Reject approval process
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param recordId
    * @param action
    **/
    public static void approveRejectProcess(id recordId, String action) {
        Approval.ProcessWorkitemRequest[] prWkItems = New Approval.ProcessWorkItemRequest[]{};

        ProcessInstance[] pi = ProcessInstanceSelector.getPendingById(recordId);

        for(ProcessInstance instance : pi) {
            for(ProcessInstanceWorkItem workItem : instance.WorkItems){
                Approval.ProcessWorkitemRequest prWkItem = new Approval.ProcessWorkitemRequest();
                prWkItem.setWorkItemID(workItem.id);
                prWkItem.setAction(action);
                prWkItems.add(prWkItem);

            }
        }

        if(!prWkItems.isEmpty()) {
            System.debug(LoggingLevel.DEBUG, '[approveRejectProcess] ' + action + '!!!!');
            Approval.ProcessResult[] appResult = Approval.process(prWkItems);
        }
    }

    /**
    * @description Gets the approvers config in the Custom Metadata Discount_Approver__mdt
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @return Map<String, List<Discount_Approver__mdt>>
    **/
    public static Map<String, List<Discount_Approver__mdt>> getApproversMap() {
        Map<String, List<Discount_Approver__mdt>> approversMap = new Map<String, List<Discount_Approver__mdt>>();

		Set<String> fields = new Set<String>();
		fields.add('Approver_Email_Address__c');
		fields.add('Approver_Type__c');
        fields.add('Countries__c');
		List<Discount_Approver__mdt> configList = (List<Discount_Approver__mdt>)CustomMetadataSelector.getByAll('Discount_Approver__mdt', fields);

        for(Discount_Approver__mdt c : configList) {
            if(c.Countries__c != null) {
                List<String> countryList = c.Countries__c.split(';');

                for(String country : countryList) {
                    String key = c.Approver_Type__c + '|' + country.trim();
                    if(approversMap.get(key) == null) {
                        List<Discount_Approver__mdt> aux = new List<Discount_Approver__mdt>();
                        aux.add(c);
                        approversMap.put(key, aux);
                    } else {
                        approversMap.get(key).add(c);
                    }
                }
            } else {
                String key = c.Approver_Type__c;
                if(approversMap.get(key) == null) {
                    List<Discount_Approver__mdt> aux = new List<Discount_Approver__mdt>();
                    aux.add(c);
                    approversMap.put(key, aux);
                } else {
                    approversMap.get(key).add(c);
                }
            }
        }

        return approversMap;
    }

    public static Map<String, Discount_Market_Config__mdt> getMarketConfigMap() {
        Map<String, Discount_Market_Config__mdt> marketConfigMap = new Map<String, Discount_Market_Config__mdt>();

		Set<String> fields = new Set<String>();
		fields.add('Countries__c');
		fields.add('Discount_Limit__c');
        fields.add('Line_Item_Threshold__c');
		List<Discount_Market_Config__mdt> configList = (List<Discount_Market_Config__mdt>)CustomMetadataSelector.getByAll('Discount_Market_Config__mdt', fields);

        for(Discount_Market_Config__mdt c : configList) {
            List<String> countryList = c.Countries__c.split(';');

            for(String country : countryList) {
                marketConfigMap.put(country.trim(), c);
            }
        }

        return marketConfigMap;
    }

    /**
    * @description Translates
    * @author alexandre.costa@integra-biosciences.com | 04-28-2021
    * @param outcome
    * @return String
    **/
    public static String getDiscountsApprovalStatus(String outcome) {
        String approvalStatus = '';

        if(outcome.left(2).Trim().toUpperCase() == Constants.APPROVAL_MAIL_OUTCOME_NO) {
            approvalStatus = Constants.APPROVAL_ACTION_REJECTED;
        } else if(outcome.left(3).Trim().toUpperCase() == Constants.APPROVAL_MAIL_OUTCOME_YES) {
            approvalStatus = Constants.APPROVAL_ACTION_APPROVED;
        } else if(outcome.left(6).Trim().toUpperCase() == Constants.APPROVAL_MAIL_OUTCOME_REJECT) {
            approvalStatus = Constants.APPROVAL_ACTION_REJECTED;
        } else if(outcome.left(7).Trim().toUpperCase() == Constants.APPROVAL_MAIL_OUTCOME_APPROVE) {
            approvalStatus = Constants.APPROVAL_ACTION_APPROVED;
        } else if(outcome.left(8).Trim().toUpperCase() == Constants.APPROVAL_MAIL_OUTCOME_REJECTED) {
            approvalStatus = Constants.APPROVAL_ACTION_REJECTED;
        } else if(outcome.left(8).Trim().toUpperCase() == Constants.APPROVAL_MAIL_OUTCOME_APPROVED) {
            approvalStatus = Constants.APPROVAL_ACTION_APPROVED;
        }

        return approvalStatus;
    }

    public class ApproversWrapper {
        List<Discount_Approver__mdt> approversList {get;set;}
        List<Discount_Approver__mdt> ccApproversList {get;set;}

        public ApproversWrapper() {
            approversList = new List<Discount_Approver__mdt>();
            ccApproversList = new List<Discount_Approver__mdt>();
        }
    }
}
/**
 * @description       :
 * @author            : alexandre.costa@integra-biosciences.com
<<<<<<< HEAD
 * @group             :
 * @last modified on  : 05-05-2021
 * @last modified by  : alexandre.costa@integra-biosciences.com
=======
>>>>>>> feature/2601-Salesforce-Approval-Process-Implementation
 * Modifications Log
 * Ver   Date         Author                                    Modification
 * 1.0   04-29-2021   alexandre.costa@integra-biosciences.com   Initial Version
**/
@isTest
public with sharing class Test_QuoteService {

    @TestSetup
    static void makeData() {
        TestUtil.createOpportunityBase();
    }

    @isTest
    public static void test_setQuoteName() {
        TriggerTemplateV2.setupUnitTest();
        Account Account = TestUtil.getAccount();
        insert Account;

        Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3));
        insert opportunity;

        System.assertEquals(
            0, [
                SELECT Id
                FROM Quote
            ].size(),
            'Wrong amount of Quotes');

        // Create 1st Quote
        NewQuoteController.createQuote(opportunity.Id, 1);

        List<Quote> quotes = [
            SELECT Id, QuoteNumber, Name
            FROM Quote
            ORDER BY CreatedDate ASC
        ];
        System.assertEquals(
            1,
            quotes.size(),
            'Quote record has not been created'
        );

        opportunity = [
            SELECT Id, Name, LastAddedQuoteNumber__c
            FROM Opportunity
            WHERE Id = :opportunity.Id
        ];

        System.assertEquals(
            opportunity.Name + ' - ' + '01',
            quotes[0].Name,
            'Wrong Name has been set to new Quote record'
        );

        System.assertEquals(
            1,
            opportunity.LastAddedQuoteNumber__c,
            'Opportunity field \'LastAddedQuoteNumber\' has not been updated'
        );

        // Create 2nd Quote
        NewQuoteController.createQuote(opportunity.Id, 1);

        quotes = [
            SELECT Id, QuoteNumber, Name
            FROM Quote
            ORDER BY CreatedDate ASC
        ];

        System.assertEquals(
            2,
            quotes.size(),
            'Quote record has not been created'
        );

        opportunity = [
            SELECT Id, Name, LastAddedQuoteNumber__c
            FROM Opportunity
            WHERE Id = :opportunity.Id
        ];

        System.assertEquals(
            opportunity.Name + ' - ' + '02',
            quotes[1].Name,
            'Wrong Name has been set to new Quote record'
        );

        System.assertEquals(
            2,
            opportunity.LastAddedQuoteNumber__c,
            'Opportunity field \'LastAddedQuoteNumber\' has not been updated'
        );

        quotes[0].Name = '12345';
        update quotes[0];

        Quote quoteAfterNameUpdate = [
            SELECT Id, Name
            FROM Quote
            WHERE Id = :quotes[0].Id
        ];
        System.assertNotEquals('12345', quoteAfterNameUpdate.Name, 'Wrong Quote.Name after update');
        System.assertEquals(opportunity.Name + ' - ' + '01', quoteAfterNameUpdate.Name, 'Wrong Quote.Name after update');
    }

    @isTest
    public static void test_syncToOpportunities() {
        TriggerTemplateV2.setupUnitTest();
        /*
        Account account = TestUtil.getAccount();
        insert account;

        List<Product2> aProducts = new List<Product2>
        {
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')};
        insert aProducts;

        List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
            new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
        insert markets;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert standardPrices;

        PriceBook2 customPriceBook = new PriceBook2();
        customPriceBook.Name='Custom Pricebook';
        customPriceBook.IsActive=true;
        insert customPriceBook;

        List<PricebookEntry> customPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert customPrices;

        Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3), CurrencyIsoCode = 'USD', Pricebook2Id = customPriceBook.Id);
        insert opportunity;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(PricebookEntryId = customPrices[0].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[1].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[2].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1)
        };
        insert opportunityLineItems;
        */

        Opportunity opportunity = TestUtil.selectAllOpportunity()[0];

        System.assertEquals(
            0, [
                SELECT Id
                FROM Quote
            ].size(),
            'Wrong amount of Quotes');

        // Create 1st Quote
        NewQuoteController.createQuote(opportunity.Id, 1);

        List<Quote> quotes = [
            SELECT Id, QuoteNumber, Name
            FROM Quote
            ORDER BY CreatedDate ASC
        ];
        System.assertEquals(
            1,
            quotes.size(),
            'Quote record has not been created'
        );

        quotes[0].DiscountDisposables__c = 10;
        quotes[0].DiscountProducts__c = 11;
        quotes[0].DiscountServices__c = 12;
        update quotes[0];

        opportunity.SyncedQuoteId = quotes[0].Id;
        update opportunity;

        opportunity = [
            SELECT Id, DiscountDisposables__c, DiscountPercentage__c, DiscountServices__c
            FROM Opportunity
            WHERE Id = :opportunity.Id
        ];

        System.assertEquals(10, opportunity.DiscountDisposables__c, 'DiscountDisposables__c has not been synced');
        System.assertEquals(11, opportunity.DiscountPercentage__c, 'DiscountPercentage__c has not been synced');
        System.assertEquals(12, opportunity.DiscountServices__c, 'DiscountServices__c has not been synced');

        Test.startTest();
        OpportunityQuoteHelper.isSyncedFromOpportunity = false;
        OpportunityQuoteHelper.isSyncedFromQuote = false;

        quotes[0].DiscountDisposables__c = 20;
        quotes[0].DiscountProducts__c = 21;
        quotes[0].DiscountServices__c = 22;
        update quotes[0];

        Test.stopTest();

        opportunity = [
            SELECT Id, DiscountDisposables__c, DiscountPercentage__c, DiscountServices__c
            FROM Opportunity
            WHERE Id = :opportunity.Id
        ];

        System.assertEquals(20, opportunity.DiscountDisposables__c, 'DiscountDisposables__c has not been synced');
        System.assertEquals(21, opportunity.DiscountPercentage__c, 'DiscountPercentage__c has not been synced');
        System.assertEquals(22, opportunity.DiscountServices__c, 'DiscountServices__c has not been synced');
    }

    @isTest
    public static void test_syncLineItemsToOpportunities() {
        TriggerTemplateV2.setupUnitTest();

/*
        Account account = TestUtil.getAccount();
        insert account;

        List<Product2> aProducts = new List<Product2>
        {
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')};
        insert aProducts;

        List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
            new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
        insert markets;

        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert standardPrices;

        PriceBook2 customPriceBook = new PriceBook2();
        customPriceBook.Name='Custom Pricebook';
        customPriceBook.IsActive=true;
        insert customPriceBook;

        List<PricebookEntry> customPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert customPrices;

        Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3), CurrencyIsoCode = 'USD', Pricebook2Id = customPriceBook.Id);
        insert opportunity;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(PricebookEntryId = customPrices[0].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[1].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[2].Id, OpportunityId = opportunity.Id, Quantity = 1, TotalPrice = 1)
        };
        insert opportunityLineItems;
        */

        Opportunity opportunity = TestUtil.selectAllOpportunity()[0];

        System.assertEquals(
            0, [
                SELECT Id
                FROM Quote
            ].size(),
            'Wrong amount of Quotes');

        // Create 1st Quote
        NewQuoteController.createQuote(opportunity.Id, 1);

        List<Quote> quotes = [
            SELECT Id, QuoteNumber, Name
            FROM Quote
            ORDER BY CreatedDate ASC
        ];
        System.assertEquals(
            1,
            quotes.size(),
            'Quote record has not been created'
        );

        List<QuoteLineItem> quoteLineItems = [
            SELECT Id, UnitPrice, Quantity, SortOrder__c
            FROM QuoteLineItem
            ORDER BY CreatedDate ASC
        ];
        quoteLineItems[0].UnitPrice = 10700;
        quoteLineItems[0].Quantity = 1;
        quoteLineItems[0].SortOrder__c = 1;
        quoteLineItems[1].UnitPrice = 80;
        quoteLineItems[1].Quantity = 2;
        quoteLineItems[1].SortOrder__c = 2;
        quoteLineItems[2].UnitPrice = 90;
        quoteLineItems[2].Quantity = 3;
        quoteLineItems[2].SortOrder__c = 3;

        update quoteLineItems;

        Test.startTest();

        opportunity.SyncedQuoteId = quotes[0].Id;
        update opportunity;

        Test.stopTest();

        opportunity = [
            SELECT Id, DiscountDisposables__c, DiscountPercentage__c, DiscountServices__c
            FROM Opportunity
            WHERE Id = :opportunity.Id
        ];

        List<OpportunityLineItem> opportunityLineItems = [
            SELECT Id, UnitPrice, Quantity, SortOrder__c
            FROM OpportunityLineItem
        ];
        System.debug('opportunityLineItems = ' + opportunityLineItems);
        System.debug([
            SELECT Id, IsSyncing
            FROM Quote
        ]);
        System.assertEquals(1, opportunityLineItems[0].Quantity, 'Opportunity Line Item has not been synced');
        System.assertEquals(10700, opportunityLineItems[0].UnitPrice, 'Opportunity Line Item has not been synced');
        System.assertEquals(2, opportunityLineItems[1].Quantity, 'Opportunity Line Item has not been synced');
        System.assertEquals(80, opportunityLineItems[1].UnitPrice, 'Opportunity Line Item has not been synced');
        System.assertEquals(null, opportunityLineItems[1].SortOrder__c, 'Opportunity Line Item has not been synced');
        System.assertEquals(3, opportunityLineItems[2].Quantity, 'Opportunity Line Item has not been synced');
        System.assertEquals(90, opportunityLineItems[2].UnitPrice, 'Opportunity Line Item has not been synced');
        System.assertEquals(null, opportunityLineItems[2].SortOrder__c, 'Opportunity Line Item has not been synced');
    }

    @isTest
    public static void testDeleteQuote()
    {
        TriggerTemplateV2.setupUnitTest();

        List<Opportunity> oppList = TestUtil.selectAllOpportunity();

/*
        Account account = TestUtil.getAccount();
        insert account;

        List<Product2> aProducts = new List<Product2>
        {
            new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductType__c = SortLineItemsController.PRODUCT_TYPE_ACCESSORIES, PricingGroup__c = '1'),
            new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductType__c = SortLineItemsController.PRODUCT_TYPE_DISPOSABLES, PricingGroup__c = '1'),
            new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductType__c = SortLineItemsController.PRODUCT_TYPE_SERVICES, PricingGroup__c = '2')};
        insert aProducts;

        List<Market__c> markets = new List<Market__c>{new Market__c(MarketCode__c= 'America', CountryList__c = 'US; CA'),
            new Market__c(MarketCode__c= 'Germany', CountryList__c = 'AT; DE')};
        insert markets;
        */
/*
        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = pricebookId, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert standardPrices;
*/
/*
        PriceBook2 customPriceBook = new PriceBook2();
        customPriceBook.Name='Custom Pricebook';
        customPriceBook.IsActive=true;
        insert customPriceBook;

        List<PricebookEntry> customPrices = new List<PricebookEntry>
        {
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD'),
            new PricebookEntry(Pricebook2Id = customPriceBook.Id, Product2Id =  aProducts.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert customPrices;

        Opportunity opportunity = new Opportunity(Name = '12345', AccountId = account.Id, StageName = 'Interessiert', CloseDate = date.today().addDays(3), CurrencyIsoCode = 'USD', Pricebook2Id = customPriceBook.Id);
        insert opportunity;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>{
            new OpportunityLineItem(PricebookEntryId = customPrices[0].Id, OpportunityId = opportunity.Id, Quantity = 1, ListPriceOneItem__c = 3, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[1].Id, OpportunityId = opportunity.Id, Quantity = 1, ListPriceOneItem__c = 3, TotalPrice = 1),
            new OpportunityLineItem(PricebookEntryId = customPrices[2].Id, OpportunityId = opportunity.Id, Quantity = 1, ListPriceOneItem__c = 3, TotalPrice = 1)
        };
        insert opportunityLineItems;
*/
        // Create 1st Quote
        NewQuoteController.createQuote(oppList[0].Id, 1);

        List<Quote> quotes = [
            SELECT Id, QuoteNumber, Name
            FROM Quote
            ORDER BY CreatedDate ASC
        ];

        oppList[0].SyncedQuoteId = quotes[0].Id;
        update oppList[0];

        String errorMessage;
        try
        {
            delete quotes[0];
        } catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }
        System.assert(errorMessage.contains(Label.ErrorMessageForRemovingQuote));
    }
}
/**
*   @description Class contains some useful features, that can be used in triggers and controllers.
*
*   @author Kamal Patil(Persistent System ltd.), ach
*   @copyright PARX
*
*   2018-11-26 Ebo: Changes reagrding PriceEntries with AccountGroups
*/
public with sharing class OppCloneWithOppLineItem extends ProductPickerController
{
    public Opportunity opportunity{get;set;}
    private ApexPages.StandardController controller {get; set;}
    // add the instance for the variables being passed by id on the url
    private Opportunity opp {get;set;}
    // initialize the controller
    public OppCloneWithOppLineItem(ApexPages.StandardController controller)
    {
        //initialize the standard controller
        controller = controller;
        // load the current record
        this.opp = (Opportunity)controller.getRecord();
        System.debug(this.opp);
    }
    public override void initMainObject()
    {
        this.opportunity = [SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, Contact__c, DiscountPercentage__c,
                DiscountDisposables__c, DiscountServices__c FROM Opportunity
        WHERE Id = :(Id)ApexPages.currentPage().getParameters().get('id')];
    }

    public override Id getMainId()
    {
        return this.opportunity.Id;
    }

    public override String getMainCurrency()
    {
        return this.opportunity.CurrencyIsoCode;
    }

    public override String getLanguageForProducts()
    {
        return this.opportunity.ProductLanguage__c != null ? this.opportunity.ProductLanguage__c : '';
    }

    public override String getMainBillingCountryCode()
    {
        return this.opportunity.Account.BillingCountryCode;
    }

    public override Id getMainAccountGroup()
    {
        return this.opportunity.Account.AccountGroup__c;
    }

    protected override void applyAdditionalConditions(AndCondition andCondition)
    {
        andCondition.add(new FieldCondition('DoNotShowOnOpportunity__c', Operator.NOT_EQUALS, true));
    }

    public override Decimal getOverallDiscount()
    {
        return this.opportunity.DiscountPercentage__c;
    }
    public override Decimal getDisposablesDiscount()
    {
        return this.opportunity.DiscountDisposables__c;
    }
    public override Decimal getServicesDiscount()
    {
        return this.opportunity.DiscountServices__c;
    }

    /**
    *   @description : Clone Opportunity and its LineItems on Opprtunity
    *   oppCloneWithLineItem() method called from the VF's action attribute(Clone Button Clicked) to
    *   clone the Opportunity and it's OpportunityLineItem
    */
    public override PageReference cloneOpportunityWithLineItem()
    {
        Opportunity newOpp;
        opp = [
                SELECT o.AccountId, o.Account.Name, o.Contact__c, o.DiscountDisposables__c, o.DiscountPercentage__c,
                        o.DiscountServices__c,o.Amount, o.CloseDate, o.Id,o.Name, o.StageName, o.CurrencyIsoCode, o.QuoteExpirationDate__c
                FROM Opportunity o
                WHERE o.Id = :opp.Id
        ];
        System.debug('opportunity>>>'+opp);
        newOpp = opp.clone(false);
        newOpp.StageName = 'Opportunity Analysis';
        insert newOpp;
        System.debug('newOpp: '+newOpp);
        if(newOpp.Id != null)
        {
            calculateNewProductPrice(opp.Id, newOpp.Id);
        }

        return new PageReference('/'+newOpp.id);
    }
    public void calculateNewProductPrice(Id oppId,Id newOppId)
    {
        List<OpportunitylineItem> anItems = new List<OpportunitylineItem>();
        Map<Id,List<OpportunitylineItem>> productIdMap = new Map<Id,List<OpportunitylineItem>>();
        List<OpportunityLineItem> lineItemList = new List<OpportunitylineItem>();
        if(oppId != null)
        {
            List<OpportunityLineItem> itemList =
                    new List<OpportunityLineItem>([SELECT Id,Product2Id,product2.PricingGroup__c,OpportunityId,Quantity,UnitPrice,PricebookEntryId,
                            ListPriceTierPricing__c,ListPriceOneItem__c From OpportunityLineItem
                    WHERE OpportunityId = :opp.id AND Product2.IsActive = true]);

            if(itemList.Size() > 0 || itemList != null)
            {
                for(OpportunitylineItem lineItem: itemList)
                {
                    OpportunityLineItem anItem = lineItem.clone(false, true);
                    anItem.OpportunityId = newOppId;
                    anItem.PricebookEntryId = lineItem.PricebookEntryId;
                    anItem.Quantity = lineItem.Quantity;
                    anItem.Product2Id = lineItem.Product2Id;
                    anItem.ListPriceOneItem__c =  lineItem.ListPriceOneItem__c;

                    anItems.add(anItem);
                    productIdMap.put(lineItem.product2Id,anItems);
                }
                lineItemList = applyNewPrice(productIdMap);
                if(lineItemList != null || lineItemList.size() > 0)
                {
                    insert lineItemList;
                }
            }
        }
    }

    public override PageReference reCalculateProductPrice()
    {
        System.debug('--- reCalculateProductPrice ---');
        reCalculateProductPrice(opp.Id);
        if (UserInfo.getUiThemeDisplayed() == 'Theme3' || UserInfo.getUiThemeDisplayed() == 'Theme2')
        {
            return new PageReference('/'+opp.Id);
        }
        return null;
    }
    public void reCalculateProductPrice(Id oppId)
    {
        List<OpportunitylineItem> anItems = new List<OpportunitylineItem>();
        Map<Id,List<OpportunitylineItem>> productIdMap = new Map<Id,List<OpportunitylineItem>>();
        List<OpportunitylineItem> lineItemList  = new List<OpportunitylineItem>();
        if (oppId != null)
        {
            List<Opportunity> updateDiscountList = new List<Opportunity>();
            List<Opportunity> opportunityList = [SELECT Id,Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, Contact__c, DiscountPercentage__c,DiscountDisposables__c,DiscountServices__c
            FROM Opportunity WHERE Id = :oppId];
            for(Opportunity oppRecord : opportunityList)
            {
                System.debug('oppRecord = ' + oppRecord);
                oppRecord.DiscountPercentage__c = 0;
                oppRecord.DiscountDisposables__c = 0;
                oppRecord.DiscountServices__c = 0;
                updateDiscountList.add(oppRecord) ;
            }
            update updateDiscountList;

            this.opportunity = [SELECT Id, Account.AccountGroup__c, Account.BillingCountryCode, CurrencyIsoCode, Contact__c, DiscountPercentage__c, DiscountDisposables__c,DiscountServices__c
            FROM Opportunity
            WHERE Id = :oppId];
            System.debug('this.opportunity = ' + this.opportunity);
            /**         List<OpportunityLineItem> itemList =
            new List<OpportunityLineItem>([SELECT Id,UnitPrice,ListPriceTierPricing__c,ListPriceOneItem__c,
                                           Quantity, Product2Id,product2.PricingGroup__c
                                           FROM OpportunityLineItem
                                           WHERE OpportunityId = :opp.id]);
                                           */
            List<OpportunityLineItem> itemList =
                    new List<OpportunityLineItem>([SELECT Id,Product2Id,product2.PricingGroup__c,OpportunityId,Quantity,UnitPrice,PricebookEntryId,
                            ListPriceTierPricing__c,ListPriceOneItem__c From OpportunityLineItem
                    WHERE OpportunityId = :opp.id
                    FOR UPDATE
                    ]);



            for(OpportunitylineItem lineItem: itemList)
            {
                System.debug('lineItem = ' + lineItem);
                lineItem.Quantity = lineItem.Quantity;

                anItems.add(lineItem);
                productIdMap.put(lineItem.product2Id,anItems); // Possibly a mistake as it associates all the Opportunity Line Items with all the Products
            }
            lineItemList = applyNewPrice(productIdMap);
            System.debug('lineItemList>>'+lineItemList);
            if(lineItemList != null || lineItemList.size() > 0)
            {
                update lineItemList;
            }
        }
    }


    public List<OpportunityLineItem> applyNewPrice(Map<Id,List<OpportunityLineItem>> productIdMap)
    {
        System.debug('--- applyNewPrice ---');
        System.debug('productIdMap = ' + productIdMap);
        List<PriceEntry__c> newStdPrices = new List<PriceEntry__c>();
        List<PriceEntry__c> newSpecPrices = new List<PriceEntry__c>();
        Map<Id, List<PriceEntry__c>> productWithStandardPrices = new Map<Id, List<PriceEntry__c>>();
        Map<Id, List<PriceEntry__c>> productWithSpecialPrices = new Map<Id, List<PriceEntry__c>>();
        String countryCode = '%' + getMainBillingCountryCode() + '%';
        Decimal aQuantity ;
        Decimal sellingPrice ;
        Decimal sellingSpecPrice ;
        Map<Id,Decimal> productPriceMap = new Map<Id,Decimal>();
        for (PriceEntry__c aPriceEntry : [SELECT id, Product__c, ScalingTo__c, SellingPrice__c, ScalingTo2__c, DiscountTier2__c, ScalingTo3__c, DiscountTier3__c, AccountGroup__c, ValidFrom__c
        FROM PriceEntry__c WHERE Product__c IN : productIdMap.keySet() AND CurrencyIsoCode = :getMainCurrency() AND
        Market__r.CountryList__c LIKE :countryCode AND ValidFrom__c <= :System.Today() ORDER BY ScalingTo__c ASC])
        {
            System.debug('aPricEntry>>'+aPriceEntry);
            if (aPriceEntry.AccountGroup__c == null) // standard price
            {
                newStdPrices.addAll(addNewPrice(productWithStandardPrices, aPriceEntry)) ;
            }
            else if (aPriceEntry.AccountGroup__c == getMainAccountGroup() &&
                    (getOverallDiscount() == null || getDisposablesDiscount() == null|| getServicesDiscount()== null)) // special price
            {
                newSpecPrices.addAll(addNewPrice(productWithSpecialPrices, aPriceEntry));
            }
        }

        Set<OpportunityLineItem> lineItemList = new Set<OpportunityLineItem>();
        List<OpportunityLineItem> lineItemList1 = new List<OpportunityLineItem>();
        Map<Id,PriceEntry__c> priceEntryStdMap = new Map<Id,PriceEntry__c>();
        Map<Id,PriceEntry__c> priceEntrySpecMap = new Map<Id,PriceEntry__c>();

        for(List<OpportunityLineItem> lineItems : productIdMap.values())
        {
            lineItemList.addAll(lineItems);
        }

        Map<String, Decimal> aPricingGroupQuantityMap= new Map<String, Decimal>(); // Pricing group code => Quantity relation
        for (OpportunityLineItem wrapper : lineItemList)
        {
            System.debug('wrapper.product2Id.PricingGroup__c>>'+wrapper.product2.PricingGroup__c);
            if (wrapper.product2.PricingGroup__c != null)
            {
                System.debug('Inside.product2.PricingGroup__c>>'+wrapper.product2.PricingGroup__c);
                // create a map that commulates quantity to a specific pricing group.
                aPricingGroupQuantityMap.put(wrapper.product2.PricingGroup__c,
                        aPricingGroupQuantityMap.containsKey(wrapper.product2.PricingGroup__c) ?
                                aPricingGroupQuantityMap.get(wrapper.product2.PricingGroup__c) + wrapper.Quantity : wrapper.Quantity);
            }
        }
        System.debug('aPricingGroupQuantityMap>>'+aPricingGroupQuantityMap);
        OpportunityLineItem lineitem1;
        for(PriceEntry__c priceEntryRecord: newStdPrices)
        {
            priceEntryStdMap.put(priceEntryRecord.Product__c,priceEntryRecord);
        }
        for(PriceEntry__c priceEntryRecord: newSpecPrices)
        {
            priceEntrySpecMap.put(priceEntryRecord.Product__c,priceEntryRecord);
        }
        System.debug('priceEntryStdMap = ' + priceEntryStdMap);
        System.debug('priceEntrySpecMap = ' + priceEntrySpecMap);
        for(OpportunityLineItem lineItem : lineItemList)
        {

            if(priceEntryStdMap.get(lineItem.Product2Id) == null) {
                continue;
            }

            System.debug('getOverallDiscount()>>'+getOverallDiscount());
            if (lineItem.Product2.PricingGroup__c != null && aPricingGroupQuantityMap.containsKey((lineItem.product2.PricingGroup__c)))//getOverallDiscount() == null &&
            {
                aQuantity = aPricingGroupQuantityMap.get(lineItem.Product2.PricingGroup__c);
                System.debug('Inside getOverallDiscount aQuantity::'+aQuantity);
            }
            else
            {
                aQuantity = lineitem.Quantity;
                //aQuantity = aPricingGroupQuantityMap.get(lineItem.Product2.PricingGroup__c);
                System.debug('aQuantity::'+aQuantity);
            }

            sellingPrice = priceEntryStdMap.get(lineItem.Product2Id).SellingPrice__c;
            lineItem.ListPriceTierPricing__c = sellingPrice;
            lineItem.UnitPrice = sellingPrice;
            lineItem.ListPriceOneItem__c = priceEntryStdMap.get(lineItem.Product2Id).SellingPrice__c;
            system.debug('sellingPrice:1::'+sellingPrice);
            if((priceEntrySpecMap != null || priceEntrySpecMap.size() > 0) && priceEntrySpecMap.get(lineItem.Product2Id) != null && priceEntrySpecMap.get(lineItem.Product2Id).SellingPrice__c != null) {
                System.debug('1');
                sellingSpecPrice = priceEntrySpecMap.get(lineItem.Product2Id).SellingPrice__c;
                lineItem.ListPriceTierPricing__c = sellingSpecPrice;
                lineItem.UnitPrice = sellingSpecPrice;
                system.debug('sellingSpecPrice:1::'+sellingPrice);
            } else {
                System.debug('2');
                if(aQuantity > priceEntryStdMap.get(lineItem.Product2Id).ScalingTo__c)
                {
                    System.debug('3');
                    lineItem.ListPriceTierPricing__c = sellingPrice;
                    lineItem.UnitPrice = sellingPrice;
                    lineItem.ListPriceOneItem__c = priceEntryStdMap.get(lineItem.Product2Id).SellingPrice__c;
                    system.debug('sellingPrice:2::'+sellingPrice);
                    if (priceEntryStdMap.get(lineItem.Product2Id).ScalingTo2__c != null && priceEntryStdMap.get(lineItem.Product2Id).DiscountTier2__c != null)
                    {
                        System.debug('4');
                        sellingPrice = (priceEntryStdMap.get(lineItem.Product2Id).SellingPrice__c * (1 - 0.01 * priceEntryStdMap.get(lineItem.Product2Id).DiscountTier2__c)).setScale(2);
                        lineItem.ListPriceTierPricing__c = sellingPrice;
                        lineItem.UnitPrice = sellingPrice;
                        lineItem.ListPriceOneItem__c = priceEntryStdMap.get(lineItem.Product2Id).SellingPrice__c;
                        if (aQuantity > priceEntryStdMap.get(lineItem.Product2Id).ScalingTo2__c)
                        {
                            System.debug('5');
                            if (priceEntryStdMap.get(lineItem.Product2Id).ScalingTo3__c != null && aQuantity > priceEntryStdMap.get(lineItem.Product2Id).ScalingTo2__c && priceEntryStdMap.get(lineItem.Product2Id).DiscountTier3__c != null)
                            {
                                System.debug('6');
                                sellingPrice = (priceEntryStdMap.get(lineItem.Product2Id).SellingPrice__c * (1 - 0.01 * priceEntryStdMap.get(lineItem.Product2Id).DiscountTier3__c)).setScale(2);
                                lineItem.ListPriceTierPricing__c = sellingPrice;
                                lineItem.UnitPrice = sellingPrice;
                                lineItem.ListPriceOneItem__c = priceEntryStdMap.get(lineItem.Product2Id).SellingPrice__c;
                            }
                        }
                    }
                }
            }
            lineItemList1.add(lineItem);
        }
        return lineItemList1;
    }
    /**
    *   @description add products to the Database.
    */
    public override PageReference doAddProducts()
    {
        return redirectToPreviousPage();
    }
}


/*  Replaced Code
    public List<OpportunityLineItem> applyNewPrice(Map<Id,List<OpportunityLineItem>> productIdMap)
    {
        List<PriceEntry__c> newPrices = new List<PriceEntry__c>();
        Map<Id, List<PriceEntry__c>> productWithStandardPrices = new Map<Id, List<PriceEntry__c>>();
        Map<Id, List<PriceEntry__c>> productWithSpecialPrices = new Map<Id, List<PriceEntry__c>>();
        String countryCode = '%' + getMainBillingCountryCode() + '%';
        Decimal aQuantity ;
        Decimal sellingPrice ;
        Map<Id,Decimal> productPriceMap = new Map<Id,Decimal>();
        for (PriceEntry__c aPriceEntry : [SELECT id, Product__c, ScalingTo__c, SellingPrice__c, ScalingTo2__c, DiscountTier2__c, ScalingTo3__c, DiscountTier3__c, AccountGroup__c, ValidFrom__c
                FROM PriceEntry__c WHERE Product__c IN : productIdMap.keySet() AND CurrencyIsoCode = :getMainCurrency() AND
                Market__r.CountryList__c LIKE :countryCode AND ValidFrom__c <= :System.Today() ORDER BY ScalingTo__c ASC])
        {
            if (aPriceEntry.AccountGroup__c == null) // standard price
            {
                newPrices.addAll(addNewPrice(productWithStandardPrices, aPriceEntry)) ;
            } 
            else if (aPriceEntry.AccountGroup__c == getMainAccountGroup() && 
                     (getOverallDiscount() == null || getDisposablesDiscount() == null|| getServicesDiscount()== null)) // special price
            {
                newPrices.addAll(addNewPrice(productWithSpecialPrices, aPriceEntry));
            }
        }
        
        Set<OpportunityLineItem> lineItemList = new Set<OpportunityLineItem>();
        List<OpportunityLineItem> lineItemList1 = new List<OpportunityLineItem>();
        Map<Id,PriceEntry__c> priceEntryMap = new Map<Id,PriceEntry__c>();
        
        for(List<OpportunityLineItem> lineItems : productIdMap.values())
        {
            lineItemList.addAll(lineItems);
        }
        
        Map<String, Decimal> aPricingGroupQuantityMap= new Map<String, Decimal>(); // Pricing group code => Quantity relation
        for (OpportunityLineItem wrapper : lineItemList)
        {
            System.debug('wrapper.product2Id.PricingGroup__c>>'+wrapper.product2.PricingGroup__c);
            if (wrapper.product2.PricingGroup__c != null)
            {
                System.debug('Inside.product2.PricingGroup__c>>'+wrapper.product2.PricingGroup__c);
                // create a map that commulates quantity to a specific pricing group. 
                aPricingGroupQuantityMap.put(wrapper.product2.PricingGroup__c, 
                                             aPricingGroupQuantityMap.containsKey(wrapper.product2.PricingGroup__c) ?
                        aPricingGroupQuantityMap.get(wrapper.product2.PricingGroup__c) + wrapper.Quantity : wrapper.Quantity);
            }
        }
        System.debug('aPricingGroupQuantityMap>>'+aPricingGroupQuantityMap);
        OpportunityLineItem lineitem1;
        for(PriceEntry__c priceEntryRecord: newPrices)
        {
            priceEntryMap.put(priceEntryRecord.Product__c,priceEntryRecord);
        }
        for(OpportunityLineItem lineItem : lineItemList)
        {
            System.debug('getOverallDiscount()>>'+getOverallDiscount());
            if (lineItem.Product2.PricingGroup__c != null && aPricingGroupQuantityMap.containsKey((lineItem.product2.PricingGroup__c)))//getOverallDiscount() == null && 
            {
                aQuantity = aPricingGroupQuantityMap.get(lineItem.Product2.PricingGroup__c);
                System.debug('Inside getOverallDiscount aQuantity::'+aQuantity);
            }
            else
            {
                aQuantity = lineitem.Quantity;
                //aQuantity = aPricingGroupQuantityMap.get(lineItem.Product2.PricingGroup__c);
                System.debug('aQuantity::'+aQuantity);
            }
            
            sellingPrice = priceEntryMap.get(lineItem.Product2Id).SellingPrice__c;
            lineItem.ListPriceTierPricing__c = sellingPrice;
            lineItem.UnitPrice = sellingPrice;
            lineItem.ListPriceOneItem__c = priceEntryMap.get(lineItem.Product2Id).SellingPrice__c;
            system.debug('sellingPrice:1::'+sellingPrice);
            if(aQuantity > priceEntryMap.get(lineItem.Product2Id).ScalingTo__c)
            {
                lineItem.ListPriceTierPricing__c = sellingPrice;
                lineItem.UnitPrice = sellingPrice;
                lineItem.ListPriceOneItem__c = priceEntryMap.get(lineItem.Product2Id).SellingPrice__c;
                system.debug('sellingPrice:2::'+sellingPrice);
                if (priceEntryMap.get(lineItem.Product2Id).ScalingTo2__c != null && priceEntryMap.get(lineItem.Product2Id).DiscountTier2__c != null)
                {   
                    sellingPrice = (priceEntryMap.get(lineItem.Product2Id).SellingPrice__c * (1 - 0.01 * priceEntryMap.get(lineItem.Product2Id).DiscountTier2__c)).setScale(2);
                    lineItem.ListPriceTierPricing__c = sellingPrice;
                    lineItem.UnitPrice = sellingPrice;
                    lineItem.ListPriceOneItem__c = priceEntryMap.get(lineItem.Product2Id).SellingPrice__c;
                    if (aQuantity > priceEntryMap.get(lineItem.Product2Id).ScalingTo2__c)
                    {
                        if (priceEntryMap.get(lineItem.Product2Id).ScalingTo3__c != null && aQuantity > priceEntryMap.get(lineItem.Product2Id).ScalingTo2__c && priceEntryMap.get(lineItem.Product2Id).DiscountTier3__c != null)
                        {
                            sellingPrice = (priceEntryMap.get(lineItem.Product2Id).SellingPrice__c * (1 - 0.01 * priceEntryMap.get(lineItem.Product2Id).DiscountTier3__c)).setScale(2);
                            lineItem.ListPriceTierPricing__c = sellingPrice;
                            lineItem.UnitPrice = sellingPrice;
                            lineItem.ListPriceOneItem__c = priceEntryMap.get(lineItem.Product2Id).SellingPrice__c;
                        }
                    }
                }
            } 
            lineItemList1.add(lineItem);
        }
        return lineItemList1;
    }
    /**
    *   @description add products to the Database.

    public override PageReference doAddProducts()
    {
        return redirectToPreviousPage();
    }*/
/**
 * @description  : Quote Service Util
 * @author       : PARX
**/
public with sharing class QuoteService {

	public static final String QUOTE_NAME_TEMPLATE = '{0} - {1}';
	public static Map<String, String> fieldsToSyncMap = new Map<String, String>{
		'DiscountDisposables__c' => 'DiscountDisposables__c',
		'DiscountPercentage__c' => 'DiscountProducts__c',
		'DiscountServices__c' => 'DiscountServices__c'
	};

	private static List<Quote> syncedQuotes;
/*
	public static User currentUser = [
		SELECT Id, Default_Product_Language__c
		FROM User
		WHERE Id = :UserInfo.getUserId()
	];
	*/

	/**
	* @description
	* @author alexandre.costa@integra-biosciences.com | 06-10-2021
	* @param quotes
	**/
	public static void setQuoteName(List<Quote> quotes) {
		Set<Id> opportunityIds = new Set<Id>();

		for (Quote quote : quotes) {
			opportunityIds.add(quote.OpportunityId);
		}

		Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([
			SELECT Id, Name, LastAddedQuoteNumber__c
			FROM Opportunity
			WHERE Id IN :opportunityIds
		]);

		for (Quote quote : quotes) {
			Opportunity opportunity = opportunityMap.get(quote.OpportunityId);

			if (quote.IndexWithinOpportunityQuotes__c == 0) {
                if (opportunity.LastAddedQuoteNumber__c == null)
                {
                    opportunity.LastAddedQuoteNumber__c = 0;
                }
				opportunity.LastAddedQuoteNumber__c += 1;
				quote.IndexWithinOpportunityQuotes__c = opportunity.LastAddedQuoteNumber__c;
			}

			String numberString = quote.IndexWithinOpportunityQuotes__c < 10 ?
				'0' + quote.IndexWithinOpportunityQuotes__c :
				String.valueOf(quote.IndexWithinOpportunityQuotes__c);
			quote.Name = String.format(QUOTE_NAME_TEMPLATE, new List<String>{opportunity.Name, numberString});
		}

		TriggerTemplateV2.allTriggersDisabled = true;

		update opportunityMap.values();

		TriggerTemplateV2.allTriggersDisabled = false;
	}

	public static void syncToOpportunities(List<Quote> quotes) {
		OpportunityQuoteHelper.isSyncedFromQuote = true;
		Map<Id, Quote> quoteByOppMap = new Map<Id, Quote>();

		for (Quote quote : quotes) {
			if (!quote.IsSyncing || quote.OpportunityId == null) {
				continue;
			}
			quoteByOppMap.put(quote.OpportunityId, quote);
		}

		if (quoteByOppMap.isEmpty()) {
			return;
		}

		List<Opportunity> opportunities = [
			SELECT Id, DiscountDisposables__c, DiscountPercentage__c, DiscountServices__c
			FROM Opportunity
			WHERE Id IN :quoteByOppMap.keySet()
		];

		List<Opportunity> opportunitiesToUpdate = new List<Opportunity>();

		for (Opportunity opportunity : opportunities) {
			Boolean needToUpdate = false;
			Quote quote = quoteByOppMap.get(opportunity.Id);

			for (String fieldOpportunity : fieldsToSyncMap.keySet()) {
				String fieldQuote = fieldsToSyncMap.get(fieldOpportunity);
				if (opportunity.get(fieldOpportunity) != quote.get(fieldQuote)) {
					opportunity.put(fieldOpportunity, quote.get(fieldQuote));
					needToUpdate = true;
				}
			}

			if (needToUpdate) {
				opportunitiesToUpdate.add(opportunity);
			}
		}

		if (!opportunitiesToUpdate.isEmpty()) {
			update opportunitiesToUpdate;
		}
	}

	/**
	* @description
	* @author alexandre.costa@integra-biosciences.com | 06-10-2021
	* @param quotesNewList
	**/
	public static void syncLineItemsToOpportunities(List<Quote> quotesNewList) {
		List<Quote> quoteIds = getSyncedQuotes(quotesNewList);

		if (quoteIds.isEmpty()) {
			return;
		}

		List<QuoteLineItem> quoteLineItems = [
			SELECT Id, QuoteId, OpportunityLineItemId, UnitPrice, Quantity, ListPriceTierPricing__c, ListPriceOneItem__c, PricebookEntryId, ListPrice, List_Price__c, SortOrder__c, Description, DemoEquipment__c
			FROM QuoteLineItem
			WHERE QuoteId IN :quoteIds
		];

		if (quoteLineItems.isEmpty()) {
			return;
		}

		QuoteLineItemService.syncToOpportunityLineItems(quoteLineItems);
	}

	/**
	* @description
	* @author alexandre.costa@integra-biosciences.com | 06-10-2021
	* @param quotesNewList
	* @return List<Quote>
	**/
	public static List<Quote> getSyncedQuotes(List<Quote> quotesNewList) {
		if (syncedQuotes != null) {
			return syncedQuotes;
		}
		syncedQuotes = new List<Quote>();

		for (Quote quote : quotesNewList) {
			if (quote.IsSyncing) {
				syncedQuotes.add(quote);
			}
		}

		return syncedQuotes;
	}

	/**
	* @description
	* @author alexandre.costa@integra-biosciences.com | 06-10-2021
	* @param quotes
	**/
	public static void checkQuoteOnSync(List<Quote> quotes)
	{
		for (Quote item :quotes)
		{
			if (item.IsSyncing)
			{
				item.addError(Label.ErrorMessageForRemovingQuote);
				return;
			}
		}
	}

	/**
	* @description
	* @author alexandre.costa@integra-biosciences.com | 06-10-2021
	* @param quotes
	**/
	public static void setPriceBookId(List<Quote> quotes) {
		for (Quote quote : quotes) {
			if(quote.Pricebook2Id == null) {
				quote.Pricebook2Id = Constants.standardPriceBookId;
			}
		}
	}
}
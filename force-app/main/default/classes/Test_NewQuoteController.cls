@isTest
private class Test_NewQuoteController 
{
	@TestSetup
	static void makeData()
	{
		TriggerTemplateV2.setupUnitTest();
		Account anAccount = TestUtil.getAccount();
		anAccount.CurrencyIsoCode = 'USD';
		insert anAccount;
		anAccount = [SELECT Id, Name, AccountGroup__c, BillingCountry, BillingState, CurrencyIsoCode FROM Account WHERE Id =: anAccount.Id LIMIT 1];
		
		List<Product2> aProducts = new List<Product2>{
				new Product2(Name = 'active 1', IsActive = true, ProductCode = 'test_asd1', ProductNameDE__c = 'test 1 de', ProductNameFR__c = 'test 1 FR', ProductFamily__c = 'f1', PricingGroup__c = '1', 
						ProductLanguage__c = 'English'), 
				new Product2(Name = 'inactive product', IsActive = false, ProductCode = 'test_asd2', ProductNameDE__c = 'test de ', ProductNameFR__c = 'test FR', ProductFamily__c = 'f1'),
				new Product2(Name = 'active 2', IsActive = true, ProductCode = 'test_asd2', ProductNameDE__c = 'test 2 de', ProductNameFR__c = 'test 2 FR', ProductFamily__c = 'f1', PricingGroup__c = '1'),
				new Product2(Name = 'active 3', IsActive = true, ProductCode = 'test_asd3', ProductNameDE__c = 'test 3 de', ProductNameFR__c = 'test 3 FR', ProductFamily__c = 'f2', PricingGroup__c = '2')};
		insert aProducts;
		
		PricebookEntry pbe2 = new PricebookEntry(isActive=true,product2id = aProducts.get(0).Id, pricebook2id = Test.getStandardPricebookId(), unitprice = 4.99, CurrencyIsoCode = anAccount.CurrencyIsoCode);
		insert pbe2;
		
		Opportunity testOpportunity = new Opportunity(DiscountPercentage__c=10,DiscountDisposables__c=10,DiscountServices__c=10,
										  Name = 'asd', AccountId = anAccount.Id, StageName = 'Interessiert', 
										  CloseDate = date.today(),CurrencyIsoCode = anAccount.CurrencyIsoCode);
		insert testOpportunity;
		List<OpportunityLineItem> anItems = new List<OpportunityLineItem>{
			new OpportunityLineItem(Product2Id = aProducts.get(0).Id, OpportunityId = testOpportunity.Id,Quantity = 1000000 ,PricebookEntryId=pbe2.Id,
										UnitPrice = 30.00, ListPriceTierPricing__c = 30.00, ListPriceOneItem__c=40.00),
			new OpportunityLineItem(Product2Id = aProducts.get(0).Id, OpportunityId = testOpportunity.Id,Quantity = 1000000 ,PricebookEntryId=pbe2.Id,
										UnitPrice = 30.00, ListPriceTierPricing__c = 30.00, ListPriceOneItem__c=40.00),
			new OpportunityLineItem(Product2Id = aProducts.get(0).Id, OpportunityId = testOpportunity.Id,Quantity = 1000000 ,PricebookEntryId=pbe2.Id,
										UnitPrice = 30.00, ListPriceTierPricing__c = 30.00, ListPriceOneItem__c=40.00)
		};
		insert anItems;
	}

	@isTest
	static void testCreateQuote() 
	{
		List<Quote> quotes = [SELECT Id FROM Quote];
		System.assertEquals(quotes.isEmpty(), true);

		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		NewQuoteController.createQuote(opp.Id, 3);
		
		List<Quote> quotesAfter = [SELECT Id FROM Quote];
		System.assertEquals(quotesAfter.isEmpty(), false);
	}
}
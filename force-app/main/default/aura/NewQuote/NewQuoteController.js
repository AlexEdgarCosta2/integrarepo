({
	closeQA : function(component, event, helper) {
		let quoteId = event.getParam('retVal');

		$A.get("e.force:closeQuickAction").fire();

		// redirect to quote
		if(quoteId != undefined && quoteId != '') {
			var navEvt = $A.get("e.force:navigateToSObject");
			navEvt.setParams({
				"recordId": quoteId
			});
			navEvt.fire();
		} else {
			$A.get('e.force:refreshView').fire();
		}
	}
});
({
    reInit : function(component, event, helper) {
        console.log('reinit called 321'); 

        component.find("productPickerSelectedProductList").refresh();
        component.find("productPickerProductFilter").refresh();


        //added on 19 feb 2020
        component.find("productPickerRelatedProductList").refresh();
        ////////////

        component.find("productPickerProductList").refresh();
        component.find("productPickerProductAlerts").refresh();


        

        component.set('v.prodIds','');
        component.set('v.prodIdsAvailable',false);
        var alertLabel = $A.get("$Label.c.ProductPicker_ProductAlert_Alert_Table_Header_Label");
        component.set("v.alertLabel", alertLabel);
    },

    
    handleselectedprodforalerts :  function(component, event, helper) {
        console.log('handleselectedProdforAlerts called');
        var prms = event.getParam('selectedProdIds');
        console.log('prms :: '+prms); 

        var createJsonString = '('+prms+')';
        console.log('createJsonString :: '+createJsonString);
        
        var action = component.get('c.getRelatedProductIds'); 
        action.setParams({
            "productIds" : createJsonString
        });  
        
        action.setCallback(this, function(response){
            var state = response.getState(); 
            if(state == 'SUCCESS') {
                var responseObj = JSON.parse(response.getReturnValue());
                console.log(responseObj);

                var prodId = responseObj.prodIdList;
                console.log('prodId '+prodId);
              
                if(prodId !=  null && prodId.length > 0)
                {
                    // component.set('v.prodIdsAvailable', true);    
                    // component.set('v.prodIds', responseObj.prodIdList);

                    //new implementation
                    var serviceProduct = component.get('c.getServiceProducts'); 
                    var resp = responseObj.prodIdList;
                    var relatedProductIds = '('+resp+')';
                    var recId = component.get("v.pageReference.state.c__record");
                    console.log('recId :: '+recId); 
                    serviceProduct.setParams({
                        "productIds" : relatedProductIds,
                        "recordId" : recId
                    });  
                    serviceProduct.setCallback(this, function(response1){
                        var state = response1.getState(); 
                        if(state == 'SUCCESS') {
                            var responseObj = response1.getReturnValue();
                            console.log('serviceProduct responseObj '+JSON.stringify(responseObj));

                            if(responseObj.length > 0){
                                console.log('serviceProduct length greater than zero');
                                component.set('v.prodIdsAvailable', true);    
                                component.set('v.prodIds', prodId);
                            }
                        }
                    });
                    $A.enqueueAction(serviceProduct);
                }
                else {
                    component.set('v.prodIdsAvailable', false);    
                }
                // var comp = component.find("productPickerProductAlerts");    
                // console.log('comp :: '+comp);
            }
        });
        $A.enqueueAction(action);
    },

    onRender : function(component, event, helper) {
        var prodIds = component.get('v.prodIds');
        console.log('onRender called '+prodIds); 

        if(component.get('v.prodIdsAvailable'))
        {
            component.find("productPickerProductAlerts").getAlertsData();
        }
        var alertLabel = $A.get("$Label.c.ProductPicker_ProductAlert_Alert_Table_Header_Label");
        component.set("v.alertLabel", alertLabel);
    },

    handleServices : function(component, event, helper) {
        console.log('handleServices called '); 
       
        var prms = event.getParam('size');
        console.log('prms :: '+JSON.stringify(prms));

        if(prms > 0){
            component.set('v.prodIdsAvailable', true); 
        }else{
            component.set('v.prodIdsAvailable', false); 
        }
    },
    
    handleRelatedProducts : function(component, event) {
        console.log('handleRelatedProducts called '); 
        var resp = event.getParam('relatedProdSize');
        console.log('prms :: '+JSON.stringify(resp));

        if(resp > 0){
            component.set('v.relatedProdIdsAvailable', true); 
        }else{
            component.set('v.relatedProdIdsAvailable', false); 
        }
    }
})
({
    scriptsLoaded : function(component, event, helper) {
        console.log('Script loaded..'); 
    },
    
    doInit : function(component) {
        var action = component.get("c.getOpportunityProducts");
        action.setParams({ OpportunityID : component.get("v.recordId") });
        action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log(response.getReturnValue());
                    component.set("v.ProductList", response.getReturnValue());
                    setTimeout(function(){ 
                        component.set("v.isLoaded", true);
                        $('.oppTable').DataTable({
                            "iDisplayLength": 100
                        });
                        // add lightning class to search filter field with some bottom margin..  
                        $('div.dataTables_filter input').addClass('slds-input');
                        $('div.dataTables_filter input').css("marginBottom", "10px");
                        $('div.dataTables_filter input').css("marginLeft", "0px");
                    }, 500);          
                }
                else{
    				console.log('Error occured');
                }
        });
        $A.enqueueAction(action);
    },
    onSelectAllChange: function(component, event, helper) {
       let AllProducts = component.get('v.ProductList');
       AllProducts.forEach(function(prod) {
           prod.isSelected = !prod.isSelected;
       });
       component.set('v.ProductList', AllProducts);
    },
    massDeleteOLI : function(component) {
        var SelectedOLIIDs = [];
        component.get('v.ProductList').forEach(function(prod) {
          	if(prod.isSelected)
                SelectedOLIIDs.push(prod.OLI.Id);
        });
        if(SelectedOLIIDs.length > 0){
            var action = component.get("c.deleteOppProduct");
            action.setParams({ OLIIds : JSON.stringify(SelectedOLIIDs) });
            action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Success',
                            message: 'Selected Products deleted successfully!',
                            duration:' 4000',
                            type: 'success',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                    else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            message: 'Error occured while deleting the selected Products!',
                            duration:' 4000',
                            type: 'error',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
                    }
                	$A.get("e.force:closeQuickAction").fire();
                	
            });
            $A.enqueueAction(action);
        }
        else{
            alert('Please select atleast one product line!');
        }
    }
})
({
            doInit : function(component, event, helper) {
        var date = new Date();
        date.setDate(date.getDate() + 30);
        var createOpportunityEvent = $A.get("e.force:createRecord");
        console.log(component.get("v.simpleRecord").AccountId);
        createOpportunityEvent.setParams({
            "entityApiName": "Opportunity",
            "defaultFieldValues": {
                'Name' : '-automatically generated-',
                'StageName' : 'Opportunity Analysis',
                'CloseDate' : date.toISOString(),
                'QuoteExpirationDate__c' : date.toISOString(),
                'AccountId' : component.get("v.simpleRecord").AccountId,
                'Contact__c' : component.get("v.recordId"),
                'Probability' : 50,
                'CurrencyIsoCode' : component.get("v.simpleRecord").Account.CurrencyIsoCode
            }
        });
        createOpportunityEvent.fire();
        $A.get("e.force:closeQuickAction").fire()
            }
})
({
	applyJquerySort : function(component, helper) {
		if (component.get("v.lineItems").length > 0 && undefined !== jQuery && !component.get("v.isInitialised")){
			jQuery("td").each(function(){
				jQuery(this).css('width', $(this).width() +'px');
			});
            jQuery(".tblLocations-" + component.get("v.ClassName")).sortable({
				items: 'tr:not(tr:first-child)',
				axis: 'y',
				handle: ".handle",
				start: function(event, ui) {
                    ui.item.startPos = ui.item.index();
				},
				stop: function(event, ui) {
					let startIndex = ui.item.startPos - 1;
					let endIndex = ui.item.index() - 1;
                    const tempStartIndex = startIndex;

                    let lineItems = component.get("v.lineItems");
					lineItems.splice(endIndex, 0, lineItems.splice(startIndex, 1)[0]);
					lineItems.forEach(function(lineItem, index){
						lineItem.lineItem.SortOrder__c = index;
                    });
                    component.set("v.lineItems", lineItems);
				}
			});
			component.set("v.isInitialised", true);
		}
	},
	onChange: function(component, evt) {
		var checkCmp = component.find("selectUnselectAll");
		let allLineItems = component.get("v.lineItems");
		allLineItems.map(function(lineItem){
			lineItem.check = checkCmp.get("v.value");
		});
		component.set("v.lineItems", allLineItems);
	}
})
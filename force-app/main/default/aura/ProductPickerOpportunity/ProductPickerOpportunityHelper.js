({
    getProducts : function(component) {
        var action = component.get("c.getFilteredProducts");
        action.setParams({
            oppId: component.get("v.recordId"),
            productCode: component.get("v.productCode"),
            productName: component.get("v.productName"),
            productFamily: component.get("v.productFamily"),
            productType: component.get("v.selectedProductType")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var products = JSON.parse(response.getReturnValue());
                products.forEach( function(p) {
                    p.productScaling = p.productScaling.split('<br/>').join('\\n');
                });
                component.set("v.products", products);
            } else if (state === "ERROR") {
                console.error(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    getPicklistsInfo : function(component) {
        var action = component.get("c.getPicklistOptions");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.productTypes", response.getReturnValue().ProductType__c);
            } else if (state === "ERROR") {
                console.error(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    addFilteredProducts : function(component) {
        var action = component.get("c.getSelectedProducts");
        action.setParams({
            oppId: component.get("v.recordId"),
            products: JSON.stringify(component.get("v.products")),
            selectedProducts: JSON.stringify(component.get("v.selectedProducts"))
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var products = JSON.parse(result.products);
                var selectedProducts = JSON.parse(result.selectedProducts);
                products.forEach( function(p) {
                    p.productScaling = p.productScaling.split('<br/>').join('\\n');
                });
                selectedProducts.forEach( function(p) {
                    p.productScaling = p.productScaling.split('<br/>').join('\\n');
                });
                component.set("v.products", products);
                component.set("v.selectedProducts", selectedProducts);
                component.set("v.isShowMessage", false);
            } else if (state === "ERROR") {
                console.error(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    removeItem : function(component, productId) {
        var action = component.get("c.removeSelectedProduct");
        action.setParams({
            oppId: component.get("v.recordId"),
            productId: productId,
            selectedProducts: JSON.stringify(component.get("v.selectedProducts"))
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var selectedProducts = JSON.parse(response.getReturnValue());
                selectedProducts.forEach( function(p) {
                    p.productScaling = p.productScaling.split('<br/>').join('\\n');
                });
                component.set("v.selectedProducts", selectedProducts);
            } else if (state === "ERROR") {
                console.error(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    overrideSalesPrice : function(component, productId) {
        var selectedProducts = component.get("v.selectedProducts");
        for (var i = 0; i < selectedProducts.length; i += 1) {
            if (selectedProducts[i].product.Id == productId) {
                selectedProducts[i].unitPrice = selectedProducts[i].item.ListPriceOneItem__c;
                break;
            }
        }
        component.set("v.selectedProducts", selectedProducts);
    },
    addProducts : function(component) {
        debugger;
        var unitPriceCmp = component.find("unitPrice");
        var isValid = true;
        if (Array.isArray(unitPriceCmp)) {
            unitPriceCmp.forEach(function(elem) {
                var value = elem.get("v.value");
                if (value === null) {
                    elem.set("v.errors", [{message: "Value is required."}]);
                    isValid = false;
                } else {
                    elem.set("v.errors", null);
                }
            });
        } else {
            var value = unitPriceCmp.get("v.value");
            if (value === null) {
                unitPriceCmp.set("v.errors", [{message: "Value is required."}]);
                isValid = false;
            } else {
                unitPriceCmp.set("v.errors", null);
            }
        }
        if (!isValid) {
            return;
        }
        var action = component.get("c.addSelectedProductToOpp");
        action.setParams({
            oppId: component.get("v.recordId"),
            selectedProducts: JSON.stringify(component.get("v.selectedProducts"))
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.selectedProducts", []);
                component.set("v.message", "Products were added successfully.");
                component.set("v.isShowMessage", true);

                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.recordId"),
                    "slideDevName": "detail"
                });
                navEvt.fire();
                // location.href = '/' + component.get("v.recordId");
            } else if (state === "ERROR") {
                console.error(response.getError());
            }
        });
        $A.enqueueAction(action);
    }
})
({
    doInit: function (component, event, helper) {
        helper.getPicklistsInfo(component);
    },
    addProducts: function (component, event, helper) {
        event.getSource().set("v.disabled", true);
        helper.addProducts(component);
    },
    doAddProducts: function (component, event, helper) {
        if (event.getParam('keyCode') === 13) {
            helper.addProducts(component);
        }
    },
    clearProducts: function (component, event, helper) {
        component.set("v.selectedProducts", []);
    },
    filterProducts: function (component, event, helper) {
        helper.getProducts(component);
    },
    doFilterProducts: function (component, event, helper) {
        if (event.getParam('keyCode') === 13) {
            helper.getProducts(component);
        }
    },
    changeProductType: function (component, event, helper) {
        helper.getProducts(component);
    },
    clearFilter: function (component, event, helper) {
        component.set("v.productCode", "");
        component.set("v.productName", "");
        component.set("v.productFamily", "");
        component.set("v.productType", "");
    },
    addFilteredProducts: function (component, event, helper) {
        helper.addFilteredProducts(component);
    },
    setValue: function (component, event, helper) {
        var products = component.get("v.products");
        products[event.target.id].quantity = event.target.value;
        component.set("v.products", products);
        if (event.keyCode === 13) {
            helper.addFilteredProducts(component);
        }
    },
    changeValue: function (component, event, helper) {
        var products = component.get("v.products");
        products[event.target.id].quantity = event.target.value;
        component.set("v.products", products);
    },
    overrideSalesPrice: function (component, event, helper) {
        helper.overrideSalesPrice(component, event.target.id);
    },
    removeItem: function (component, event, helper) {
        helper.removeItem(component, event.getSource().get("v.value"));
    }
})
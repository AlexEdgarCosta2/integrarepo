({
    doInit: function(component, event, helper) {
        helper.getInitialData(component, event);
    },
    saveRecords: function(component, event, helper) {
        component.set("v.isLoading", true);
        helper.saveRecords(component, event, false);
        
    },
    cancelSorting: function(component, event, helper) {
        helper.goBack(component, event);
    },
    deleteRecords : function(component, event, helper) {
        component.set("v.isLoading", true);
        helper.saveRecords(component, event, true);
    }
})
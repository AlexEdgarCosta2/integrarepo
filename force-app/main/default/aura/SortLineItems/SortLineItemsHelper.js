({
	getInitialData: function(component, event) {
		component.set("v.isTableLoading", true);
		var action = component.get("c.getData");
		action.setParams({
			parentId: component.get("v.recordId")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var result = response.getReturnValue();
				component.find("one").get("v.label")[0].set("v.value","Products and Accessories (0)");
				component.find("two").get("v.label")[0].set("v.value","Disposables (0)");
				component.find("three").get("v.label")[0].set("v.value","Services (0)");
				if (result.fromSectionAccessories) {
					const fromSectionAccessories = JSON.parse(result.fromSectionAccessories);
					fromSectionAccessories.map(function(rec, index) {
						rec.lineItem.SortOrder__c = index;
					});
					component.find("one").get("v.label")[0].set("v.value","Products and Accessories (" + fromSectionAccessories.length + ")");
					component.set("v.accessoriesRecords", fromSectionAccessories);
				}
				if(result.fromSectionDisposables){
					const fromSectionDisposables = JSON.parse(result.fromSectionDisposables);
					fromSectionDisposables.map(function(rec, index){
						rec.lineItem.SortOrder__c = index;
					});
					component.set("v.disposablesRecords", fromSectionDisposables);
					component.find("two").get("v.label")[0].set("v.value","Disposables (" + fromSectionDisposables.length + ")");
				}
				if(result.fromSectionServices){
					const fromSectionServices = JSON.parse(result.fromSectionServices);
					fromSectionServices.map(function(rec, index) {
						rec.lineItem.SortOrder__c = index;
					});
					component.set("v.servicesRecords", fromSectionServices);
					component.find("three").get("v.label")[0].set("v.value","Services (" + fromSectionServices.length + ")");

				}
				component.set("v.isTableLoading", false);
			} else if (state === "ERROR") {
				console.error(response.getError());
			}
		});
		$A.enqueueAction(action);
	},
	goBack: function(component, event) {
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
			"recordId": component.get("v.recordId")
		});
		navEvt.fire();
	},
	saveRecords: function(component, event, isDelete) {
		var action = component.get("c.saveData");
		let lineItems = component.get("v.accessoriesRecords").concat(component.get("v.disposablesRecords"), component.get("v.servicesRecords"));
		action.setParams({
			lineItemString : JSON.stringify(lineItems)
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				if(isDelete)
					this.deleteRecords(component, event);
				else{
					$A.get("e.force:closeQuickAction").fire();
					$A.get('e.force:refreshView').fire();
				}
			} else if (state === "ERROR") {
				console.error(response.getError());
			}
			if (!isDelete)
				component.set("v.isLoading", false);
		});
		$A.enqueueAction(action);
	},
	deleteRecords : function(component, event) {
		var action = component.get("c.deleteSelectedRecords");
		let allLineItems = component.get("v.accessoriesRecords").concat(component.get("v.disposablesRecords"), component.get("v.servicesRecords"));
		let filteredLineItemIds = [];
		allLineItems.map(function(lineItem){
			if (lineItem.check)
				filteredLineItemIds.push(lineItem.lineItem.Id);
		});
		if (!filteredLineItemIds.length) {
			component.set("v.isLoading", false);
			return;
		}
		if (filteredLineItemIds.length > 0){
			action.setParams({
				lineItemIds : filteredLineItemIds
			});
			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === "SUCCESS") {
					$A.get("e.force:closeQuickAction").fire();
					$A.get('e.force:refreshView').fire();
				} else if (state === "ERROR") {
					console.error(response.getError());
				}
				component.set("v.isLoading", false);
			});
		}
		$A.enqueueAction(action);
	}
});